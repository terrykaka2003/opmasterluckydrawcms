const keystone          = require('keystone');
const mongoose          = require('mongoose');
const async             = require('async');
const _                 = require('lodash');
const moment            = require('moment');


const emailLib          = require(global.__base + '/lib/email');
var PrizeController     = require(global.__base + '/models/game/prize');
var PrizePoolController = require(global.__base + '/models/game/prizePool');
var RedeemCodePoolController = require(global.__base + '/models/game/redeemCodePool');
RedeemCodePoolController = new RedeemCodePoolController();
PrizePoolController = new PrizePoolController();
PrizeController = new PrizeController();

// Configuration
const Config            = require(global.__base + '/config/Config');
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
const senderEmail = conf.smtpUser;
/*
**
** Dispatcher to distribute prize as individual prize into the Prize Poo;
** Terry Chan@01/01/2019
** Updated At 01/01/2019
**
*/
function PrizeDispatcher() {};


PrizeDispatcher.prototype.constructor = PrizeDispatcher;
PrizeDispatcher.prototype.logger = function(error) {
    var message = error;
    if (error instanceof Error) {
        message = error.message;
    }
    console.log("[**** Prize Dispatcher Logger ****]["+ moment().format(cronJobTimeFormat) + "]", message);
};

// CouponDispatcher.prototype.preCodeDistribution = function(coupons) {
//     const tasks = [];
//     const toBeDistributed = [];
//     _.forEach(coupons, function(coupon) {
//         tasks.push(function(cb) {
//             // use the quantity limit to get the redeem code from the RedeemCode Pool
//             RedeemCodePoolController.getAvailableCodeList(coupon, coupon.quantity, function(err, result) {
//                 // console.log(result);
//                 if (result && result.length) {
//                     coupon.redeemCodes = result;
//                     toBeDistributed.push(coupon);
//                 }
//                 // console.log(coupon);
//                 cb(err);
//             });
//         });
//     });
//     if (tasks.length) {
//         this.logger('Pre-Distribute Total number of Coupon to be distributed: ' + toBeDistributed.length);
//         const self = this;
//         async.parallel(tasks, function(err, results) {
//             if (err) {
//                 return self.logger(err);
//             }
//             self.distribution(toBeDistributed);
//         });   
//     } else {
//         this.distribution(toBeDistributed);
//     }
// }

PrizeDispatcher.prototype.preDistribution = function(prizes) {
    const tasks = [];
    const toBeDistributed = [];
    _.forEach(prizes, function(prize) {
        console.log('>>>>>prize>>>>>> ', prize);
        tasks.push(function(cb) {
            if (!prize.enabled) {
                PrizePoolController.removePrize(prize, function(err, result) {
                    cb(err, result);
                });
            } else {
                if (prize.prizeCoupon) {
                    RedeemCodePoolController.getAvailablePrizeCouponCodeList(
                        prize._id, 
                        prize.quantity, 
                        function(err, codes) {
                            console.log('>>>>>codes>>>>>> ', codes);
                        if (!err) {
                            prize.redeemCodes = codes || [];
                            toBeDistributed.push(prize);
                        }
                        cb(err);
                    });
                } else {
                    toBeDistributed.push(prize);
                    cb(null);
                }
                // PrizePoolController.isExistingPrize(prize, function(err, result) {
                //     // console.log(result);
                //     if ((!err && result && !result.length) || (!err && !result)) {
                //         if (prize.prizeCoupon) {
                //             RedeemCodePoolController.getAvailablePrizeCouponCodeList(
                //                 prize._id, 
                //                 prize.quantity, 
                //                 function(err, codes) {
                //                 if (!err) {
                //                     prize.redeemCodes = codes || [];
                //                     toBeDistributed.push(prize);
                //                 }
                //             });
                //         } else {
                //             toBeDistributed.push(prize);
                //         }
                //     }
                //     cb(err, result);
                // });
            }
        });
    });
    if (tasks.length) {
        this.logger('Pre-Distribute Total number of Prize to be distributed: ' + toBeDistributed.length);
        const self = this;
        async.parallel(tasks, function(err, results) {
            if (err) {
                return self.logger(err);
            }
            self.distribution(toBeDistributed);
        });   
    } else {
        this.distribution(toBeDistributed);
    }
}

PrizeDispatcher.prototype.sendNotiEmailFormat = function(totalPrize) {
    const self = this;
    const tpl = 
    "<p><i><b>Last Distribution</b> at " + moment().format(cronJobTimeFormat) + "</i></p>";
    const subject = "[Prize Distribution] distribute the prize ("+ totalPrize +")";
    const mailConfig = {
        from: senderEmail,
        to: 'terrychan@4d.com.hk, kelvinlovelove@gmail.com',
        subject: subject,
        html: tpl
    };
    emailLib.sendMail(mailConfig, function(err, info) {
        self.logger('The email has been sent.');
        // console.log(info);
    });
};

PrizeDispatcher.prototype.removeRedeemPool = function(codes) {
    const self = this;
    // console.log(codes);
    RedeemCodePoolController.removeCodes(codes, function(err, result) {
        // console.log(err);
        if (result) {
            self.logger('The distributed redeemCode has been removed from the RedeemCode Pool');
            console.log(result);
        } else {
            self.logger('No redeemCode is removed from the RedeemCode Pool');
        }
    });
};

PrizeDispatcher.prototype.distribution = function(prizes) {
    const tasks = [];
    var toBeRemoveCode = [];
    _.forEach(prizes, function(prize) {
        if (prize.prizeCoupon) {
            _.map(prize.redeemCodes, function(code) {
                tasks.push(function(cb) {
                    prize.redeemCode = code.redeemCode;
                    toBeRemoveCode.push(code._id);
                    // prize.type = 'bp'; // big prize
                    PrizePoolController.addPrize(prize, cb);
                });
            });
        } else {
            _.times(+prize.quantity, function() {
                tasks.push(function(cb) {
                    // prize.type = 'bp'; // big prize
                    PrizePoolController.addPrize(prize, cb);
                });
            });
        }
    });

    if (tasks.length) {
        this.logger('Distribute the total number of Prize Box: ' + prizes.length);
        this.logger('Distributing the total number of Prize　to the Prize Pool: ' + tasks.length);
        const self = this;
        async.parallel(tasks, function(err, results) {
            if (err) {
                console.log(err);
                return self.logger(err);
            }
            self.removeRedeemPool(toBeRemoveCode);
            self.logger('All of today prize has been assigned into the Prize Pool.');
            // self.sendNotiEmailFormat(tasks.length);
        });   
    } else {
        this.logger('No any today prize needs to be distributed.');
    }
};

/*
** @void: prepare for running the service after remove all of the pass prize from the pool
** @param1: N/A
** @return: N/A
*/
PrizeDispatcher.prototype.run = function() {
    const self = this;
    // PrizePoolController.removeAllExpiredPrize(function(err, result) {
    //     if (err) {
    //         return self.logger(err);
    //     }
    PrizeController.getAvailablePrizeList(function(err, prizes) {
        if (err) {
            return self.logger(err);
        }
        console.log(prizes);
        // return;
        self.preDistribution(prizes);
    });
    // });
};
/*
** @void: start point without using Cronjob against the Cron crash 
** @param1: N/A
** @return: N/A
*/
PrizeDispatcher.prototype.start = function() {
    this.run();
};

exports = module.exports = PrizeDispatcher;
