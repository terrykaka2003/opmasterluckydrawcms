const keystone          = require('keystone');
const mongoose          = require('mongoose');
const async             = require('async');
const _                 = require('lodash');
const moment            = require('moment');


const emailLib          = require(global.__base + '/lib/email');
var CouponController     = require(global.__base + '/models/game/coupon');
var CouponPoolController = require(global.__base + '/models/game/couponPool');
var RedeemCodePoolController = require(global.__base + '/models/game/redeemCodePool');
RedeemCodePoolController = new RedeemCodePoolController();
CouponPoolController = new CouponPoolController();
CouponController = new CouponController();

// Configuration
const Config            = require(global.__base + '/config/Config');
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
const senderEmail = conf.smtpUser;
/*
**
** Dispatcher to distribute coupon as individual coupon into the Coupon Pool with redeemCode;
** Terry Chan@01/01/2019
** Updated At 01/01/2019
**
*/
function CouponDispatcher() {};


CouponDispatcher.prototype.constructor = CouponDispatcher;
CouponDispatcher.prototype.logger = function(error) {
    var message = error;
    if (error instanceof Error) {
        message = error.message;
    }
    console.log("[**** Coupon Dispatcher Logger ****]["+ moment().format(cronJobTimeFormat) + "]", message);
};

CouponDispatcher.prototype.preDistribution = function(coupons) {
    const tasks = [];
    const toBeDistributed = [];
    _.forEach(coupons, function(coupon) {
        tasks.push(function(cb) {
            // use the quantity limit to get the redeem code from the RedeemCode Pool
            RedeemCodePoolController.getAvailableCodeList(coupon, coupon.quantity, function(err, result) {
                // console.log(result);
                if (result && result.length) {
                    coupon.redeemCodes = result;
                    toBeDistributed.push(coupon);
                }
                // console.log(coupon);
                cb(err);
            });
        });
    });
    if (tasks.length) {
        this.logger('Pre-Distribute Total number of Coupon to be distributed: ' + toBeDistributed.length);
        const self = this;
        async.parallel(tasks, function(err, results) {
            if (err) {
                return self.logger(err);
            }
            self.distribution(toBeDistributed);
        });   
    } else {
        this.distribution(toBeDistributed);
    }
}

CouponDispatcher.prototype.sendNotiEmailFormat = function(totalPrize) {
    const self = this;
    const tpl = 
    "<p><i><b>Last Distribution</b> at " + moment().format(cronJobTimeFormat) + "</i></p>";
    const subject = "[Coupon Distribution] distribute the coupon ("+ totalPrize +")";
    const mailConfig = {
        from: senderEmail,
        to: 'terrychan@4d.com.hk, kelvinlovelove@gmail.com',
        subject: subject,
        html: tpl
    };
    emailLib.sendMail(mailConfig, function(err, info) {
        self.logger('The email has been sent.');
        // console.log(info);
    });
};
CouponDispatcher.prototype.removeRedeemPool = function(codes) {
    const self = this;
    // console.log(codes);
    RedeemCodePoolController.removeCodes(codes, function(err, result) {
        // console.log(err);
        if (result) {
            self.logger('The distributed redeemCode has been removed from the RedeemCode Pool');
            console.log(result);
        } else {
            self.logger('No redeemCode is removed from the RedeemCode Pool');
        }
    });
};

CouponDispatcher.prototype.distribution = function(coupons) {
    const tasks = [];
    var toBeRemoved = [];
    _.forEach(coupons, function(coupon) {
        if (coupon.redeemCodes && coupon.redeemCodes.length) {
            _.forEach(coupon.redeemCodes, function(code) {
                toBeRemoved.push(mongoose.Types.ObjectId(code._id));
                tasks.push(function(cb) {
                    // prize.type = 'bp'; // big prize
                    coupon.redeemCode = code.redeemCode;
                    coupon.type = coupon._id;
                    coupon.barcodeImage = code.image;
                    coupon.forDate = moment().startOf('day').toDate();
                    // console.log(code, coupon)
                    CouponPoolController.addCoupon(coupon, function(err) {
                        cb(err);
                    });
                });
                // console.log(toBeRemoved);
            });
        }
    });
    
    if (tasks.length) {
        this.logger('Distribute the total number of Coupon: ' + coupons.length);
        this.logger('Distributing the total number of Coupon to the Coupon Pool: ' + tasks.length);
        const self = this;
        var subTasks = [];
        const total = tasks.length;
        const halfLength = Math.floor(total / 2);
        const firstTasks = tasks.slice(0, halfLength);
        const restTasks = tasks.slice(halfLength, total);

        if (firstTasks.length) {
            subTasks.push(function(cb) {
                console.log('Distributing the first number of Coupon to the Coupon Pool: ', firstTasks.length);
                async.parallel(firstTasks, function(err) {
                    cb(err);
                });
            });
        }

        if (restTasks.length) {
            subTasks.push(function(cb) {
                console.log('Distributing the rest number of Coupon to the Coupon Pool: ', restTasks.length);
                async.parallel(restTasks, function(err) {
                    cb(err);
                });
            });
        }

        // async.waterfall(subTasks)
        if (subTasks.length) {
            async.waterfall(subTasks, function(err, results) {
                if (err) {
                    console.log(err);
                    return self.logger(err);
                }
                self.logger('All of today coupon has been assigned into the Coupon Pool.');
                self.removeRedeemPool(toBeRemoved);
                // self.sendNotiEmailFormat(tasks.length);
            });   
        } else {
            self.logger('No any for assigning into the Coupon Pool.');
        }
    } else {
        this.logger('No any today coupon needs to be distributed.');
    }

};

/*
** @void: prepare for running the service after remove all of the pass prize from the pool
** @param1: N/A
** @return: N/A
*/
CouponDispatcher.prototype.run = function() {
    const self = this;
    // PrizePoolController.removeAllExpiredPrize(function(err, result) {
    //     if (err) {
    //         return self.logger(err);
    //     }
    CouponController.getAvailableCouponList(function(err, coupons) {
        // console.log(coupons);
        if (err) {
            return self.logger(err);
        }
        self.preDistribution(coupons);
    });
    // });
};
/*
** @void: start point without using Cronjob against the Cron crash 
** @param1: N/A
** @return: N/A
*/
CouponDispatcher.prototype.start = function() {
    this.run();
};

exports = module.exports = CouponDispatcher;
