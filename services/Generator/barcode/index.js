const keystone          = require('keystone');
const mongoose          = require('mongoose');
const async             = require('async');
const _                 = require('lodash');
const moment            = require('moment');


const emailLib          = require(global.__base + '/lib/email');
var RedeemCodePoolController = require(global.__base + '/models/game/redeemCodePool');
RedeemCodePoolController = new RedeemCodePoolController();

// Configuration
const Config            = require(global.__base + '/config/Config');
const Utility           = require(global.__base + "/helper/Utility");
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
const senderEmail = conf.smtpUser;
/*
**
** Dispatcher to generate the barcode from the redeem code
** Terry Chan@01/01/2019
** Updated At 01/01/2019
**
*/
function BarcodeGenerator() {};


BarcodeGenerator.prototype.constructor = BarcodeGenerator;
BarcodeGenerator.prototype.logger = function(error) {
    var message = error;
    if (error instanceof Error) {
        message = error.message;
    }
    console.log("[**** Barcode Generator Logger ****]["+ moment().format(cronJobTimeFormat) + "]", message);
};

BarcodeGenerator.prototype.generate = function(codes) {
    const tasks = [];
    const toBeGenerated = [];
    _.forEach(codes, function(code) {
        if (!code.image) {
            tasks.push(function(cb) {
                Utility.generateBarCodeImage(String(data._id), redeemCode, function(err, path) {
                  if (path) {
                    code.image = path;
                  } else {
                    console.log('> generate barcode error: ', err);
                  }
                  code.save(cb);
                });
            });
        }
    });
    if (tasks.length) {
        this.logger('Generating the total number of Barcode: ' + tasks.length);
        const self = this;
        async.parallel(tasks, function(err, results) {
            if (err) {
                console.log(err);
                return self.logger(err);
            }
            self.logger('All of barcode has been generated.');
            // self.removeRedeemPool(toBeRemoved);
            // self.sendNotiEmailFormat(tasks.length);
        });   
    } else {
        this.logger('No any today coupon needs to be distributed.');
    }
    if (tasks.length) {
        this.logger('Pre-Generator Total number of Barcode to be generated: ' + toBeGenerated.length);
        const self = this;
        async.parallel(tasks, function(err, results) {
            if (err) {
                return self.logger(err);
            }
        });   
    }
}

BarcodeGenerator.prototype.sendNotiEmailFormat = function(totalPrize) {
    const self = this;
    const tpl = 
    "<p><i><b>Last Distribution</b> at " + moment().format(cronJobTimeFormat) + "</i></p>";
    const subject = "[Barcode Generator] generate the barcode ("+ totalPrize +")";
    const mailConfig = {
        from: senderEmail,
        to: 'terrychan@4d.com.hk, kelvinlovelove@gmail.com',
        subject: subject,
        html: tpl
    };
    emailLib.sendMail(mailConfig, function(err, info) {
        self.logger('The email has been sent.');
        // console.log(info);
    });
};


/*
** @void: prepare for running the service after remove all of the pass prize from the pool
** @param1: N/A
** @return: N/A
*/
BarcodeGenerator.prototype.run = function() {
    const self = this;
    // PrizePoolController.removeAllExpiredPrize(function(err, result) {
    //     if (err) {
    //         return self.logger(err);
    //     }
    RedeemCodePoolController.getAllCouponCodes(function(err, codes) {
        // console.log(coupons);
        if (err) {
            return self.logger(err);
        }
        self.generate(codes);
    });
    // });
};
/*
** @void: start point without using Cronjob against the Cron crash 
** @param1: N/A
** @return: N/A
*/
BarcodeGenerator.prototype.start = function() {
    this.run();
};

exports = module.exports = BarcodeGenerator;
