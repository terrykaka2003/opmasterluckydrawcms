const keystone          = require('keystone');
const mongoose          = require('mongoose');
const async             = require('async');
const _                 = require('lodash');
const moment            = require('moment');

const emailLib          = require(global.__base + '/lib/email');
var EligibilityLogController     = require(global.__base + '/models/game/log/eligibilityLog');
EligibilityLogController = new EligibilityLogController();
var LotteryLogController     = require(global.__base + '/models/game/log/lotteryDrawnLog');
LotteryLogController = new LotteryLogController();

// Configuration
const Config            = require(global.__base + '/config/Config');
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
const senderEmail = conf.smtpUser;
/*
**
** Recycler to remove all of non-invoice log
** Terry Chan@01/01/2019
** Updated At 01/01/2019
**
*/
function EligibilityLogRecycler() {};

EligibilityLogRecycler.prototype.constructor = EligibilityLogRecycler;
EligibilityLogRecycler.prototype.logger = function(error) {
    var message = error;
    if (error instanceof Error) {
        message = error.message;
    }
    console.log("[**** Eligibility Log Recycler Logger ****]["+ moment().format(cronJobTimeFormat) + "]", message);
};

EligibilityLogRecycler.prototype.sendNotiEmailFormat = function(totalPrize) {
    const self = this;
    const tpl = 
    "<p><i><b>Last Recycle</b> at " + moment().format(cronJobTimeFormat) + "</i></p>";
    const subject = "[Eligibility Log Recycler] recycle the log ("+ totalPrize +")";
    const mailConfig = {
        from: senderEmail,
        to: 'terrychan@4d.com.hk, kelvinlovelove@gmail.com',
        subject: subject,
        html: tpl
    };
    emailLib.sendMail(mailConfig, function(err, info) {
        self.logger('The email has been sent.');
        // console.log(info);
    });
};

EligibilityLogRecycler.prototype.removeDrawResultLog = function(logs) {
    const self = this;
    LotteryLogController.removeLogs(logs, function(err, result) {
        if (err) {
            return self.logger(err);
        }
        self.logger('Remove the total number of Lottery Drawn Log: ' + logs.length);
    });
};

/*
** @void: prepare for running the service after remove all of the non-invoice log
** @param1: N/A
** @return: N/A
*/
EligibilityLogRecycler.prototype.run = function() {
    const self = this;
    var tasks = [];
    var drawLog = [];
    EligibilityLogController.getAllOldNonInvoiceLogs(function(err, logs) {
        if (err) {
            return self.logger(err);
        }
        if (logs.length) {
            drawLog = _.map(logs, function(log) {
                return mongoose.Types.ObjectId(log.resultLog);
            });
            tasks = _.map(logs, function(log) {
                return function(cb) {
                    log.remove(cb);
                }
            });
        }
        // console.log(removed);
        // const total = removed && removed.result && removed.result.n ? removed.result.n : 0;
        // self.sendNotiEmailFormat(removed);
        if (tasks.length) {
            self.logger('Remove the total number of Eligibility Log: ' + logs.length);
            self.logger('Remove the total number of Eligibility Log Tasks: ' + tasks.length);
            async.parallel(tasks, function(err, results) {
                if (err) {
                    console.log(err);
                    return self.logger(err);
                }
                self.logger('Removed all of non-invoices from Eligibility Log');
                if (drawLog.length) {
                    self.removeDrawResultLog(drawLog);
                }
                // self.sendNotiEmailFormat(tasks.length);
            });   
        } else {
            self.logger('No any Eligibility Log needs to removed');
        }
    });
};
/*
** @void: start point without using Cronjob against the Cron crash 
** @param1: N/A
** @return: N/A
*/
EligibilityLogRecycler.prototype.start = function() {
    this.run();
};

exports = module.exports = EligibilityLogRecycler;
