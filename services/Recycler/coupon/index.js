const keystone          = require('keystone');
const mongoose          = require('mongoose');
const async             = require('async');
const _                 = require('lodash');
const moment            = require('moment');


const emailLib          = require(global.__base + '/lib/email');
var CouponController     = require(global.__base + '/models/game/coupon');
var CouponPoolController = require(global.__base + '/models/game/couponPool');
CouponPoolController = new CouponPoolController();
CouponController = new CouponController();

// Configuration
const Config            = require(global.__base + '/config/Config');
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
const senderEmail = conf.smtpUser;
/*
**
** Recycler to get back all of coupons from the pool for the pass
** Terry Chan@01/01/2019
** Updated At 01/01/2019
**
*/
function CouponRecycler() {};


CouponRecycler.prototype.constructor = CouponRecycler;
CouponRecycler.prototype.logger = function(error) {
    var message = error;
    if (error instanceof Error) {
        message = error.message;
    }
    console.log("[**** Coupon Recycler Logger ****]["+ moment().format(cronJobTimeFormat) + "]", message);
};

CouponRecycler.prototype.sendNotiEmailFormat = function(totalPrize) {
    const self = this;
    const tpl = 
    "<p><i><b>Last Recycle</b> at " + moment().format(cronJobTimeFormat) + "</i></p>";
    const subject = "[Coupon Recycle] recycle the copon ("+ totalPrize +")";
    const mailConfig = {
        from: senderEmail,
        to: 'terrychan@4d.com.hk, kelvinlovelove@gmail.com',
        subject: subject,
        html: tpl
    };
    emailLib.sendMail(mailConfig, function(err, info) {
        self.logger('The email has been sent.');
        // console.log(info);
    });
};

/*
** @void: prepare for running the service after remove all of the pass prize from the pool
** @param1: N/A
** @return: N/A
*/
CouponRecycler.prototype.run = function() {
    const self = this;
    CouponPoolController.removeAllExpiredCoupon(function(err, removed) {
        if (err) {
            return self.logger(err);
        }
        // console.log(removed);
        // const total = removed && removed.result && removed.result.n ? removed.result.n : 0;
        // self.sendNotiEmailFormat(removed);
    });
};
/*
** @void: start point without using Cronjob against the Cron crash 
** @param1: N/A
** @return: N/A
*/
CouponRecycler.prototype.start = function() {
    this.run();
};

exports = module.exports = CouponRecycler;
