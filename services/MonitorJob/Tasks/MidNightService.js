const keystone = require('keystone');
const moment = require('moment');
const objectAssign = require('object-assign');

const Task = require(global.__base + '/services/MonitorJob/Tasks');
const Config = require(global.__base + '/config/Config');
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
/*
** MidNightService Scheduler Monitor Checking - the scheduler is run at 00:00
** Terry Chan@17/09/2018
** Updated By 18/09/2018
*/
function MidNightService(model, type, duration) {
    this.message = "Alert: " + type + " job fail for over " +
            (duration > 1 ? duration + " mins" : duration) + " min" + 
        ". Please arrange to check.";
    this.queryResult = null;
    this.type = type;
    this.duration = duration;
    this.task = new Task(model, type, duration);
}
// RegularService.prototype = Object.create(new Task());
MidNightService.prototype.constructor = MidNightService;
MidNightService.prototype.query = function() {
    if (this.queryResult && !this.queryResult.health) {
        const result = this.queryResult;
        result.message = this.message;
        return result;
    }
    return null;
    // return this.queryResult && !this.queryResult.health ? 
    //     objectAssign({}, this.queryResult, { message: this.message }) : null;
};
/*
** @Object obtain datetime of midnight  
** @param1: N/A
** @return: midenight start time (Date)
*/
MidNightService.prototype.getPassedMidNight = function() {
    return moment({ H: 0, m: 0, s: 0 });
};
/*
** @Object obtain buffer datetime of midnight  
** @param1: N/A
** @return: buffer end time (Date)
*/
MidNightService.prototype.getPassedMidNightBuffer = function() {
     // add 2 mins just in case 
    return moment({ H: 0, m: (this.duration + 2), s: 59 });
};
// MidNightService.prototype.getLastHistory = function(condition) {
//     const self = this;
//     return this.task.queryLastHistory(condition)
//         .then(function(result) {
//             self.queryResult = result;
//         })
//         .catch(function (err) {
//             throw err;
//         });
// };
/*
** Asynchronous Method
** @Void execute for checking healthy of MidNightService Scheduler
** @param1: N/A
** @return: N/A
*/
MidNightService.prototype.execute = function(callback) {
    const self = this;
    // return new Promise(function(resovle, reject) {
    const condition = {
        type: self.type,
    };
    self.task.queryLastHistory(condition, function(err, result) {
        var isValid = false;
        self.queryResult = result;
        if (self.queryResult) {
            const lastSuccessAt = self.queryResult.lastSuccessAt;
            const jobId = self.queryResult._id;
            if (lastSuccessAt) {
                // once the midnight task is executed before buffer datetime, it is valid
                // const endNidNight = self.getPassedMidNightBuffer();
                const startMidNight = self.getPassedMidNight();
                self.task.logger("Mid Night valid start of "+ startMidNight.format(cronJobTimeFormat));
                const minute = self.task.getDifferentMinute(moment(lastSuccessAt), startMidNight);
                // const fromStartMinute = self.getDifferentMinute(createdAt, startMidNight);
                self.task.logger("Minute " + minute + " " + self.duration);
                if (minute && minute >= 0 && minute <= self.duration) {
                    self.task.logger("The job " + jobId + " is a valid job");
                    isValid = true;
                } else {
                    self.task.logger("[Error] The job " + jobId + "is not a valid job since " + (moment(lastSuccessAt).format(cronJobTimeFormat)));  
                }
            }
        }
        self.task.updateHealthStatus(self.queryResult, isValid, function() {
            callback(err, result);
        });
        // callback(err, result);
    });
        // update health status 
        // .then(function(isValid) {
        //     self.task.updateHealthStatus(self.queryResult, isValid);
        // })
        // .then(function() {
        //     resovle();
        // })
        // .catch(function (err) {
        //     reject(err);
        // });
    // });
};

module.exports = MidNightService;
