const keystone = require('keystone');
const mongoose = require('mongoose');
const moment = require('moment');

const Config = require(global.__base + '/config/Config');
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
/*
**
** Container Parent for all of scheduler types
** Holding Common Members
** Terry Chan@17/09/2018
** Updated At 17/09/2018
**
*/
function Task(model, type, duration) {
    this.model = model;
    this.monitorType = type;
    // regular duration for each scheduler job (Number)
};

Task.prototype.constructor = Task;
Task.prototype.logger = function(error) {
    var message = error;
    if (error instanceof Error) {
        message = error.message;
    }
    console.log("[**** PureJS Health Monitor " + this.monitorType + " Logger ****][" + moment().format(cronJobTimeFormat) + "]", message);
};
/*
** @Number compare with two moment in minute
** @param1: source date (Date)
** @param2: target date (Date)
** @return: number in minute
*/
Task.prototype.getDifferentMinute = function(sourceDate, targetDate) {
    var minute; // in minute
    // console.log(date, momentDate.toISOString(), nowDate.toISOString());
    if (sourceDate.isValid() && targetDate.isValid()) {
        const duration = moment.duration(sourceDate.diff(targetDate));
        // console.log('duration: ', duration, duration.asMinutes());
        minute = duration.asMinutes();
    }
    return minute;
};
/*
** @Boolean check for it is valid duration for the scheduler job (only for regular)
** The specify datetime for the scheduling should not be guarded from this method
** @param1: regular duration for each scheduler job (Number)
** @param2: current duration of the last scheduler log (Number)
** @return: is valid regular duration
*/
Task.prototype.inRangeJob = function(targetDuration, currentDuration) {
    return currentDuration > targetDuration;
};
/*
** Asynchronous Method
** @Void mark the health status for the last history
** @param1: health status (Boolean)
** @return: N/A
*/
Task.prototype.updateHealthStatus = function(queryResult, status, callback) {
    // const self = this;
    // return new Promise(function(resolve, reject) {
        queryResult.health = status;
        if (!status) {
            queryResult.lastFailAt = moment().toDate();
        }
            // await this.queryResult.save();
        queryResult.save(callback)
        // .then(function() {
        //     resolve(queryResult);
        // })
        // .catch(function(err) {
        //     reject(err);
        // });
    // });
};
/*
** Asynchronous Method
** @Object execute the database to get the latest scheduler job log
** @param1: query condition (Object)
** @return: query result
*/
Task.prototype.queryLastHistory = function(condition, callback) {
    this.model.findOne(condition, callback);
    // .then(function(result) {
    //     return result;
    // }).catch(function(err) {
    //     throw err;
    // });
}

module.exports = Task;
