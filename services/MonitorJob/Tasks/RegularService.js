const keystone = require('keystone');
const moment = require('moment');
const objectAssign = require('object-assign');

const Task = require(global.__base + '/services/MonitorJob/Tasks');
const Config = require(global.__base + '/config/Config');
const conf = new Config();
const cronJobTimeFormat = conf.cronJobTimeFormat;
/*
** RegularService Scheduler Monitor Checking - the scheduler is run for the duration regularly
** Terry Chan@17/09/2018
** Updated By 18/09/2018
*/
function RegularService(model, type, duration) {
    this.message  = type + " cron job is failed for over " +
        (duration > 1 ? duration + " minutes" : duration + " minute") +".";
    this.queryResult = null;
    this.type = type;
    this.duration = duration;
    this.task = new Task(model, type, duration);
}
// RegularService.prototype = Object.create(new Task());
RegularService.prototype.constructor = RegularService;
RegularService.prototype.query = function() {
    // console.log('>>>>>>>>', this.type, '>>>>>>>>>>>', this.queryResult);
    if (this.queryResult && !this.queryResult.health) {
        const result = this.queryResult;
        result.message = this.message;
        return result;
    }
    return null;
    // return this.queryResult && !this.queryResult.health ? 
    //     objectAssign({}, this.queryResult, { message: this.message }) : null;
};
/*
** Asynchronous Method
** @Void execute for checking healthy of CouponService Scheduler
** @param1: N/A
** @return: N/A
*/
RegularService.prototype.execute = function(callback) {
    const self = this;
    const condition = {
        type: self.type,
    };
    self.task.queryLastHistory(condition, function(err, result) {
        var isValid = false;
        self.queryResult = result;
        if (self.queryResult) {
            const lastSuccessAt = self.queryResult.lastSuccessAt;
            const jobId = self.queryResult._id;
            if (lastSuccessAt) {
                const minute = self.task.getDifferentMinute(moment(), moment(lastSuccessAt));
                // console.log(minute, self.regularDuration);
                self.task.logger("Minute " + minute + " " + self.duration);
                if (minute && minute <= self.duration) {
                    self.task.logger("The job " + jobId + " is a valid job");
                    isValid = true;
                } else {
                    self.task.logger("[Error] The job " + jobId + " is not a valid job since " + (moment(lastSuccessAt).format(cronJobTimeFormat)));  
                }
            }
        }
        self.task.updateHealthStatus(self.queryResult, isValid, function() {
            if (self.queryResult && !self.queryResult.health) {
                const result = self.queryResult;
                result.message = self.message;
                return callback(err, result);
            }
            return callback(err, null);
        });
    });
};

module.exports = RegularService;
