const keystone = require('keystone');
const mongoose = require('mongoose');
const async = require('async');
const moment = require('moment');
const toMap = require('lodash/map');
const toFilter = require('lodash/filter');
const toEach = require('lodash/forEach');
const isArray = require('lodash/isArray');
const toReduce = require('lodash/reduce');
const toReplace = require('lodash/replace');
const toFlattenDeep = require('lodash/flattenDeep');
const toConcat = require('lodash/concat');
const hasProp = require('lodash/has');

const emailLib = require(global.__base + '/lib/email');
const types = require(global.__base + '/enum/ScheduleJobTypes');
// Configuration
const Config = require(global.__base + '/config/Config');
const conf = new Config();
const monitorExecutionDuration = conf.monitorExecutionDuration;
const cronJobTimeFormat = conf.cronJobTimeFormat;
const mobileName = conf.name;
const senderEmail = conf.smtpUser;
/*
**
** Health Monitor Service for all of scheduler jobs
** Terry Chan@17/09/2018
** Updated At 17/09/2018
**
*/
function HealthMonitor(model, type, duration) {
    this.monitorModel = keystone.list('ScheduleLog').model;
    this.informerModel = keystone.list('Informer').model;
    this.taskDir = global.__base + '/services/MonitorJob/Tasks/';
    this.toBeReports = [];
    this.regularDuration = 30; // per mins
};

HealthMonitor.prototype.constructor = HealthMonitor;
HealthMonitor.prototype.logger = function(error) {
    var message = error;
    if (error instanceof Error) {
        message = error.message;
    }
    console.log("[**** Health Monitor Service Logger ****]["+ moment().format(cronJobTimeFormat) + "]", message);
};

HealthMonitor.prototype.getNotiEmailFormat = function() {
    var tpl = "<h3>[System Alert] "+ mobileName + "</h3>" +
    "<p><h4>This is automatically generated system alert message.</h4></p>" +
    "<p>" +
    "   <ol>{{_message_}}</ol>" +
    "</p>" +
    "<br />" +
    "Please arrange to check<br />" + 
    "<p><i><b>Last Montiored</b> at " + moment().format(cronJobTimeFormat) + "</i></p>";
    const messageTpl = toReduce(this.toBeReports, function(s, r) {
        s = s + "<li>" + r.message + "</li>";
        return s;
    }, '');
    return toReplace(tpl, '{{_message_}}', messageTpl);
};
/*
** handle error log
** @param1: Post Proccess query for schedule log 
** @return: N/A
*/
// HealthMonitor.prototype.postProcessErrorLog = function(query) {
//     // console.log('postProcessErrorLog1: ', typeof query, query);
//     if (query && !query.inform) {
//         this.toBeReports.push(query);
//     }
//     // console.log('postProcessErrorLog2: ', this.toBeReports);
// };
HealthMonitor.prototype.getInformerEmailList = function(callback) {
    this.informerModel.find({ inform: true }, function(err, emailList) {
        if (err) {
            return callback(err);
        }
        if (emailList && emailList.length) {
            return callback(null, 
                toMap(emailList, function(informer) {
                    return informer.email;
                })
            );
        }
        return callback(null, null);
    });
    // ).then(function(emailList) {
    //     return toMap(emailList, function(informer) {
    //         return informer.email;
    //     });
    // }, function(err) {
    //     throw new Error(err);
    // });
};
HealthMonitor.prototype.postUpdateInformed = function(target, callback) {
    this.monitorModel.update({
        _id: mongoose.Types.ObjectId(target._id),
    }, {
        inform: true,
    }, {}, function(err, informed) {
        if (err) {
            return callback(err);
        }
        return callback(null, informed);
    });
};
HealthMonitor.prototype.preUpdateInformed = function(callback) {
    var tasks = [];
    const self = this;
    this.toBeReports.forEach(function(log) {
        tasks.push(function(cb) {
            self.postUpdateInformed(log, cb);
        });
    });
    async.series(tasks, callback);
};
HealthMonitor.prototype.executeSendErrorLog = function(callback) {
    const self = this;
    // console.log(this.toBeReports);
    //console.log('>>>>>>>>>>>> ', this.toBeReports);
    if (this.toBeReports.length) {
        const tasks = [];
        tasks.push(
            function(cb) {
                self.getInformerEmailList(function(err, emailList) {
                    if (emailList.length) {
                        const message = self.getNotiEmailFormat();
                        self.logger("To be sent out error report: " + message + " to " + emailList);
                        const subject = "[System Failure] " + mobileName + " cronjobs not work";
                        const mailConfig = {
                            from: senderEmail,
                            to: emailList.join(', '),
                            subject: subject,
                            html: message
                        };
                        emailLib.sendMail(mailConfig, function(err, info) {
                            cb(err, info);
                        });
                        // update informed flag for the failed log prevent send repeatly
                    } else {
                        cb(null, null);
                    }
                });
            }
        );
        tasks.push(
            function(result, cb) {
                // console.log('dasfasf');
                // cb(null, null);
                self.preUpdateInformed(cb);
            }
        );
        async.waterfall(tasks, callback);
    }
};
/*
** @void: initialize and execute the monitior service
** @param1: target enum value (Object)
** @param2: regular duration for checking (Number)
** @return: N/A
*/
HealthMonitor.prototype.execute = function(target, callback) {
    // dynamic import the target controller within initializing into the instance
    const Controller = require(this.taskDir + "" + target.value);
    // execute the Target
    const monitor = new Controller(this.monitorModel, target.key, this.regularDuration);
    const self = this;
    monitor.execute(function(err, query) {
        // console.log('>>>>>>>>>>>>> query: ', query);
        if (query && !query.inform) {
            self.toBeReports.push(query);
        }
        callback(err, query);
    });
};
HealthMonitor.prototype.preExecute = function(options, callback) {
    var tasks = [];
    const self = this;
    options.forEach(function(option) {
        tasks.push(function(cb) {
            self.execute(option, cb);
        });
    });
    async.series(tasks, callback);
};
/*
** @void: prepare for running the service with Enum types
** @param1: N/A
** @return: N/A
*/
HealthMonitor.prototype.run = function() {
    this.toBeReports = [];
    const self = this;
        // start for each of scheduler type initialization
    const options = types.enums;
    if (!options.length) {
        this.logger('No any scheduller option is set in /enum/ScheduleJobTypes.js config file.');
        return;        
    }
    self.preExecute(options, function(err, result) {
        if (err) {
            self.logger(err);
            console.log(err);
        }
        // self.logger('>>>>>>>>>>>>>>>>>>>>>>');
        self.executeSendErrorLog(function() {
            self.logger('All of Scheduler Healthy Checkings have been checked without invalid.');
        });
    })  
};
/*
** @void: start point of Monitor Service without using Cronjob against the Cron crash 
** @param1: N/A
** @return: N/A
*/
HealthMonitor.prototype.start = function() {
    // process execution regularly, default 10 mins
    this.run();
    setInterval(this.run, monitorExecutionDuration || 600000);
};

exports = module.exports = HealthMonitor;