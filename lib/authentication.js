const keystone 	= require('keystone');
const jwt 		= require('jsonwebtoken');
const Config = require(global.__base + '/config/Config');
const conf = new Config();

function authentication() {};

authentication.prototype.generateToken = function(payload) {
	return jwt.sign(payload, conf.cookieSecret, {
		expiresIn: '1h'
	});
}

authentication.prototype.validateToken = function(token) {
	var verify_ans;
	try {
		verify_ans = jwt.verify(token, conf.cookieSecret);
	} catch(err) {
		console.log('auth.js -> validateToken -> err:', err);
		verify_ans = false
	};	
	return verify_ans;
}

module.exports = new authentication();
