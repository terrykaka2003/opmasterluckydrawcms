const nodemailer = require('nodemailer');
const _          = require('lodash');
var moment       = require('moment');
const request    = require('request');
const Config = require(global.__base + '/config/Config');
const Utility = require(global.__base + "/helper/Utility");
var CountryListController       = require(global.__base + '/models/game/countryList');
var PrizeController       = require(global.__base + '/models/game/prize');
CountryListController = new CountryListController();
PrizeController = new PrizeController();
const conf = new Config();
const senderEmail = conf.smtpSender;



const rno = conf.promotionNo;
const tandc = {
    en: '<b>Terms & Conditions:</b><br />',
    tc: '<b>使用條款及細則:</b><br />',
    sc: '<b>使用条款及细则:</b><br />',
};
// const tandDate = {
//     en: '年月日',
//     tc: '年月日',
//     sc: '年月日',
// };
const GameTitleTemplate = {
    en: {
        prize: 'Congratulations! You have won: [Prize]',
        ecoupon: 'Congratulations! You have won: [Prize]',
    },
    sc: {
        prize: '恭喜您获得: [Prize]',
        ecoupon: '恭喜您获得: [Prize]',
    },
    tc: {
        prize: '恭喜您獲得: [Prize]',
        ecoupon: '恭喜您獲得: [Prize]',
    }
}
const GameTemplate = {
    en: {
        // in park grand prize email format
        prize: 'Dear <b>[Player]</b>' +
            '<br /><br />' +
            '<br />Congratulations! You have won: <b>[Prize]</b>' +
            '<br />Please present this email notification and personal identification (for verification) at our Prize Redemption Center (near Aqua City Bakery in  Waterfront) on or before Park Close today to claim the prize. Claims after this date will not be accepted. ' +
            '<br /><br />' +
            '<br />Redemption Number: <i>[ReferenceNo]</i>' +
            '<br />Name: [Player]' +
            '<br />HKID/Passport (first 4 digits): [Card]' +
            '<br />Email: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />Contact Number: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />Trade Promotion Competition Licence No. : ' + rno,
        
        // hotel
        hotelPrize: 'Dear <b>[Player]</b>' +
            '<br /><br />' +
            '<br />Congratulations! You have won: <b>[Prize]</b>' +
            '<br />Please present this email notification and personal identification (for verification) to our Prize Redemption Center (near Aqua City Bakery in  Waterfront) on or before Ocean Park Close today to claim the prize. Claims after this date will not be accepted. After Park Closed and before 11pm, you may also present this email notification and personal identification (for verification) to Hong kong Ocean Park Marriott Hotel front desk to claim the prize.' +
            '<br /><br />' +
            '<br />Redemption Number: <i>[ReferenceNo]</i>' +
            '<br />Name: [Player]' +
            '<br />HKID/Passport (first 4 digits): [Card]' +
            '<br />Email: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />Contact Number: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />Trade Promotion Competition Licence No. : ' + rno,

        ecoupon: 'Dear <b>[Player]</b>' +
            '<br /><br />' +
            '<br />Congratulations! You have won: In-park "<b>[Prize]</b>"' +
            '<br />Redemption Number: <i>[ReferenceNo]</i>' +
            '<br />Coupon Valid: <i>[ExpiryDate]</i>' +
            '<br />Name: [Player]' +
            '<br />HKID/Passport (first 4 digits): [Card]' +
            '<br />Email: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />Contact Number: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />Trade Promotion Competition Licence No. : ' + rno,

        // hotelSmallPrize: 'Dear <b>[Player]</b>' +
        //     '<br /><br />' +
        //     '<br />Congratulations! You have won: In-park "<b>[Prize]</b>"' +
        //     '<br />Redemption Number: <i>[ReferenceNo]</i>' +
        //     '<br />Coupon Valid until <i>[ExpiryDate]</i>' +
        //     '<br />Name: [Player]' +
        //     '<br />HKID/Passport (first 4 digits): [Card]' +
        //     '<br />Email: <a href="mailTo:[Email]">[Email]</a>' +
        //     '<br />Contact Number: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
        //     '<br />' +
        //     '[TC]' +
        //     '<br />Trade Promotion Competition Licence No. : ' + rno,

        // ecoupon: 'Congratulations! You have won :  In-park "<b>[CouponName]</b>"' +
        //         '<br />Please stay connected with Ocean Park Wi-Fi for receiving the e-coupon.'+
        //         '<br /><br />' +
        //         'Trade Promotion Competition Licence No. : ' + rno
    },
    tc: {
        prize: '<b>[Player]</b> 先生/女士' +
            '<br /><br />' +
            '<br />恭喜您獲得: <b>[Prize]</b>' +
            '<br />請於<b>今天公園關門前</b>親臨本公園獎品換領處(位於海濱樂園內的水都餅店旁)出示此確認電郵並提供身份證明文件(以作核實用途)領奬。' +
            '<br />逾期恕不接受。' +
            '<br /><br />' +
            '<br />參考編號: <i>[ReferenceNo]</i>' +
            '<br />得獎者姓名: [Player]' +
            '<br />得獎者身份證/護照首4位數字: [Card]' +
            '<br />得獎者電郵: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />得獎者電話號碼: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />推廣生意的競賽牌照號碼: ' + rno,

        hotelPrize: '<b>[Player]</b> 先生/女士' +
            '<br /><br />' +
            '<br />恭喜您獲得: <b>[Prize]</b>' +
            '<br />請於<b>今天公園關門前</b>親臨本公園獎品換領處(位於海濱樂園內的水都餅店旁)出示此確認電郵並提供身份證明文件(以作核實用途)領奬。<br />海洋公園關門後至晚上11時前，您亦可於香港海洋公園萬豪酒店前檯出示此確認電郵並提供身份證明文件(以作核實用途)領奬。' +
            '<br />逾期恕不接受。' +
            '<br /><br />' +
            '<br />參考編號: <i>[ReferenceNo]</i>' +
            '<br />得獎者姓名: [Player]' +
            '<br />得獎者身份證/護照首4位數字: [Card]' +
            '<br />得獎者電郵: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />得獎者電話號碼: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />推廣生意的競賽牌照號碼: ' + rno,

        ecoupon: '<b>[Player]</b> 先生/女士' +
            '<br /><br />' +
            '<br />恭喜您獲得: 园内「<b>[Prize]</b>」电子优惠券1张。' +
            '<br />參考編號: <i>[ReferenceNo]</i>' +
            '<br />有效日期: <i>[ExpiryDate]</i>' +
            '<br />得獎者姓名: [Player]' +
            '<br />得獎者身份證/護照首4位數字: [Card]' +
            '<br />得獎者電郵: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />得獎者電話號碼: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />推廣生意的競賽牌照號碼: ' + rno,
        
        // hotelSmallPrize: '<b>[Player]</b> 先生/女士' +
        //     '<br /><br />' +
        //     '<br />恭喜您獲得: 园内「<b>[Prize]</b>」电子优惠券1张。' +
        //     '<br />參考編號: <i>[ReferenceNo]</i>' +
        //     '<br />有效日期至<i>[ExpiryDate]</i>' +
        //     '<br />得獎者姓名: [Player]' +
        //     '<br />得獎者身份證/護照首4位數字: [Card]' +
        //     '<br />得獎者電郵: <a href="mailTo:[Email]">[Email]</a>' +
        //     '<br />得獎者電話號碼: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
        //     '<br />' +
        //     '[TC]' +
        //     '<br />推廣生意的競賽牌照號碼: ' + rno,

        // ecoupon: '恭喜你獲得: 園內「<b>[CouponName]</b>」電子優惠券1張。' +
        //         '<br />請繼續連接園內Wi-Fi以獲取優惠券。' +
        //         '<br /><br />' +
        //         '推廣生意的競賽牌照號碼: ' + rno
    },
    sc: {
        prize: '<b>[Player]</b> 先生/女士' +
            '<br /><br />' +
            '<br />恭喜您获得: <b>[Prize]</b>' +
            '<br />请于<b>今天公园关门前</b>亲临本公园奖品换领处(位于海滨乐园内的水都饼店旁)出示此确认电邮并提供身份证明文件(以作核实用途)领奖。' +
            '<br />逾期恕不接受。' +
            '<br /><br />' +
            '<br />参考编号: <i>[ReferenceNo]</i>' +
            '<br />得奖者姓名: [Player]' +
            '<br />得奖者身份证/护照首4位数字: [Card]' +
            '<br />得奖者电邮: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />得奖者电话号码: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />推广生意的竞赛牌照号码: ' + rno,
            
        hotelPrize: '<b>[Player]</b> 先生/女士' +
            '<br /><br />' +
            '<br />恭喜您获得: <b>[Prize]</b>' +
            '<br />请于今天公园关门前亲临本公园奖品换领处(位于海滨乐园内的水都饼店旁)出示此确认电邮并提供身份证明文件(以作核实用途)领奖。<br />海洋公园关门后至晚上11时前，您亦可于香港海洋公园万豪酒店前台出示此确认电邮并提供身份证明文件(以作核实用途)领奖。' +
            '<br />逾期恕不接受。' +
            '<br /><br />' +
            '<br />参考编号: <i>[ReferenceNo]</i>' +
            '<br />得奖者姓名: [Player]' +
            '<br />得奖者身份证/护照首4位数字: [Card]' +
            '<br />得奖者电邮: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />得奖者电话号码: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />推广生意的竞赛牌照号码: ' + rno,

        ecoupon: '<b>[Player]</b> 先生/女士' +
            '<br /><br />' +
            '<br />恭喜您获得: 園內「<b>[Prize]</b>」電子優惠券1張。' +
            '<br />参考编号: <i>[ReferenceNo]</i>' +
            '<br />有效日期: <i>[ExpiryDate]</i>' +
            '<br />得奖者姓名: [Player]' +
            '<br />得奖者身份证/护照首4位数字: [Card]' +
            '<br />得奖者电邮: <a href="mailTo:[Email]">[Email]</a>' +
            '<br />得奖者电话号码: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
            '<br />' +
            '[TC]' +
            '<br />推广生意的竞赛牌照号码: ' + rno,
        
        // hotelEcoupon: '<b>[Player]</b> 先生/女士' +
        //     '<br /><br />' +
        //     '<br />恭喜您获得: 園內「<b>[Prize]</b>」電子優惠券1張。' +
        //     '<br />参考编号: <i>[ReferenceNo]</i>' +
        //     '<br />有效日期至<i>[ExpiryDate]</i>' +
        //     '<br />得奖者姓名: [Player]' +
        //     '<br />得奖者身份证/护照首4位数字: [Card]' +
        //     '<br />得奖者电邮: <a href="mailTo:[Email]">[Email]</a>' +
        //     '<br />得奖者电话号码: <a href="[CountryCode][PhoneNo]">[CountryCode] [PhoneNo]</a>' +
        //     '<br />' +
        //     '[TC]' +
        //     '<br />推广生意的竞赛牌照号码: ' + rno,

        // ecoupon: '恭喜你获得: 园内「<b>[CouponName]</b>」电子优惠券1张。' +
        //         '<br />请继续连接园内Wi-Fi以获取优惠券' +
        //         '<br /><br />' +
        //         '推广生意的竞赛牌照号码: ' + rno
    },
};

function sendMail(option, callback) {
    // const transporter = nodemailer.createTransport({
    //     host: conf.smtpHost,
    //     port: 25,
    //     tls: {
    //         rejectUnauthorized: false
    //     }
    // });
    // const config = {
    //     host: conf.smtpHost,
    //     port: conf.smtpPort,
    //     secure: true,
    //     auth: {
    //         user: conf.smtpUser,
    //         pass: conf.smtpPass
    //     },
    // };
    const config = {
        host: conf.internalSmtpHost,
        port: 25,
        tls: {
            rejectUnauthorized: false
        }
    }
    // console.log('Email Config: ', config);
    // const transporter = nodemailer.createTransport(config);
    const transporter = nodemailer.createTransport(config);
    transporter.sendMail(option, function(error, info) {
        if (error) {
            console.log('Send Email Error: ', error);
            throw error;
        }
        console.log('Message sent: %s', info.messageId);

        callback(error, info);
        // Preview only available when sending through an Ethereal account
        // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });
}

function getDrawResultEmailFormat(history, callback) {
    CountryListController.getCountryById(history.country, function(err, country) {
        // console.log(history);
        const resultLog = history.resultLog.resultLog;
        const type = resultLog.prize ? 'prize' : 'ecoupon';
        const language = history.language;

        var tpl = GameTemplate[language].prize || '';
        var email = history.email;
        var subject = '';


        tpl = resultLog.outParkFormat;
        // var tcTpl = "<br />" + resultLog.tcOutParkFormat;
        // var scTpl = "<br />" + resultLog.scOutParkFormat;
        // TODO
        // console.log(history);
        if (history.invoiceNo) {
            // tpl = resultLog.hotelFormat;
            // tcTpl = "<br />" + resultLog.tcHotelFormat;
            // scTpl = "<br />" + resultLog.scHotelFormat;
            tpl = resultLog.hotelFormat;
            if (language === 'tc') {
                tpl = resultLog.tcHotelFormat;
            } else if (language === 'sc') {
                tpl = resultLog.scHotelFormat;
            }
        // app view
        } else if (history.deviceId) {
            // tpl = resultLog.inParkFormat;
            // tcTpl = "<br />" + resultLog.tcInParkFormat;
            // scTpl = "<br />" + resultLog.scInParkFormat;
            tpl = resultLog.inParkFormat;
            if (language === 'tc') {
                tpl = resultLog.tcInParkFormat;
            } else if (language === 'sc') {
                tpl = resultLog.scInParkFormat;
            }
        // operator
        } else {
            if (language === 'tc') {
                tpl = resultLog.tcOutParkFormat;
            } else if (language === 'sc') {
                tpl = resultLog.scOutParkFormat;
            }
        }
        if (tpl) {
            // TODO
            var prizeName = resultLog.name;
            var usageTerm = resultLog.usageTerm;
            var tc = resultLog.tc;
            if (language === 'tc') {
                prizeName = resultLog.tcName;
                usageTerm = resultLog.tcUsageTerm;
                tc = resultLog.tcTC;
            } else if (language === 'sc') {
                prizeName = resultLog.scName;
                usageTerm = resultLog.scUsageTerm;
                tc = resultLog.scTC;
            }

            tpl = tpl.replace(/\[Player\]/g, history.player);
            tpl = tpl.replace(/\[ReferenceNo\]/g, history.referenceNo);
            tpl = tpl.replace(/\[Card\]/g, history.card);
            tpl = tpl.replace(/\[PhoneNo\]/g, history.phone);
            tpl = tpl.replace(/\[PromotionNo\]/g, rno);
            tpl = tpl.replace(/\[Email\]/g, history.email);
            tpl = tpl.replace(/\[CountryCode\]/g, country ? country.code : '');

            // TODO
            tpl = tpl.replace(/\[Prize\]/g, prizeName || '');

            // TODO
           // if (resultLog.prize && resultLog.prize.prizeCoupon && !!tc) {
            if (!!resultLog.tc) {
                tpl = tpl.replace(/\[TC\]/g, tandc[language] + '</br />' + tc.replace(/\r\n/g, "<br />"));
            } else {
                tpl = tpl.replace(/\[TC\]/g, '');
            }


            // TODO replace expiryDate if needed
            var defaultExpiryDate = '19 February, 2019';
            var formatLocale = 'en';
            if (language === 'tc') {
                formatLocale = 'zh-tw';
                defaultExpiryDate = '2019年2月19日';
            } else if (language === 'sc') {
                formatLocale = 'zh-cn';
                defaultExpiryDate = '2019年2月19日';
            }
            moment.locale(formatLocale);
            if (resultLog.expiryDate) {
                const formatedDate = moment(resultLog.expiryDate);
                if (formatedDate.isValid()) {
                    tpl = tpl.replace(/\[ExpiryDate\]/g, formatedDate.format('LL'));
                } else {
                    tpl = tpl.replace(/\[ExpiryDate\]/g, defaultExpiryDate);
                } 
            } else {
                tpl = tpl.replace(/\[ExpiryDate\]/g, defaultExpiryDate);
            }

            tpl = tpl.replace(/\[Today\]/g, moment().format('LL'));
            tpl = tpl.replace(/\[TODAY\]/g, moment().format('LL'));

            tpl = tpl.replace(/\[US\]/g, usageTerm ? usageTerm.replace(/\r\n/g, "<br />") : '');
            if (resultLog.redeemCode && !resultLog.isBigPrize) {
                const barcodePath = conf.absolutePath + 'barcode?code=' +resultLog.redeemCode;
                tpl = tpl.replace(/\[BARCODE\]/g, 
                    '<img style="padding: 0 20px" src="'+ barcodePath + '" alt="'+ resultLog.redeemCode +'"');
            } else {
                tpl = tpl.replace(/\[BARCODE\]/g, '');
            }
            // keystone.get('global upload dir')
            // TODO
            subject = GameTitleTemplate[language].prize.replace(/\[Prize\]/g, prizeName || (resultLog.prize && resultLog.prize.gift || ''));
                    

           if (history.emailList && _.isArray(history.emailList)) {
                email = email+','+history.emailList.join(',');
           }
           const mailConfig = {
                to: email,
                subject: subject,
                // html: encodeURIComponent(tpl + '<br /><br />' + tcTpl + '<br /><br />' + scTpl),
                // TODO
                html: encodeURIComponent(tpl),
                token: conf.appViewSecretKey,
            };
            callback(mailConfig, history);
        } else {
            callback(null, history);
        }
       // sendDrawResultEmail(mailConfig, history, callback);
        // console.log(mailConfig);
        // sendMail(mailConfig, function(err, info) {
        //     console.log('> Prize Email is sent to: ', email);
        // });
    });
}
function sendSingleDrawResultEmailAdpater(mailConfig, callback) {
    // getDrawResultEmailFormat(hisotry, function(mailConfig, history) {
        request({
            method: 'POST',
            uri: conf.emailSenderAPI,
            // body: mailConfig,
            json: mailConfig,
        }, function (error, response, body) {
            // console.log('> url: ', conf.emailSenderAPI);
            // console.log('> sending with email data: ', mailConfig);
            // console.log(error, response, body);
            callback(error);
        });
   //  });
}
function sendSingleDrawResultEmail(history, callback) {
    CountryListController.getCountryById(history.country, function(err, country) {
        // console.log(history);
        const resultLog = history.resultLog.resultLog;
        const type = resultLog.prize ? 'prize' : 'ecoupon';
        const language = history.language;

        var tpl = GameTemplate[language].prize || '';
        var email = history.email;
        var subject = '';



        tpl = resultLog.outParkFormat;
        // var tcTpl = "<br />" + resultLog.tcOutParkFormat;
        // var scTpl = "<br />" + resultLog.scOutParkFormat;
        // TODO
        if (history.invoiceNo) {
            // tpl = resultLog.hotelFormat;
            // tcTpl = "<br />" + resultLog.tcHotelFormat;
            // scTpl = "<br />" + resultLog.scHotelFormat;
            tpl = resultLog.hotelFormat;
            if (language === 'tc') {
                tpl = resultLog.tcHotelFormat;
            } else if (language === 'sc') {
                tpl = resultLog.scHotelFormat;
            }
        // app view
        } else if (history.deviceId) {
            // tpl = resultLog.inParkFormat;
            // tcTpl = "<br />" + resultLog.tcInParkFormat;
            // scTpl = "<br />" + resultLog.scInParkFormat;
            tpl = resultLog.inParkFormat;
            if (language === 'tc') {
                tpl = resultLog.tcInParkFormat;
            } else if (language === 'sc') {
                tpl = resultLog.scInParkFormat;
            }
        // operator
        } else {
            if (language === 'tc') {
                tpl = resultLog.tcOutParkFormat;
            } else if (language === 'sc') {
                tpl = resultLog.scOutParkFormat;
            }
        }
        

        // TODO
        var prizeName = resultLog.name;
        var usageTerm = resultLog.usageTerm;
        var tc = resultLog.tc;
        if (language === 'tc') {
            prizeName = resultLog.tcName;
            usageTerm = resultLog.tcUsageTerm;
            tc = resultLog.tcTC;
        } else if (language === 'sc') {
            prizeName = resultLog.scName;
            usageTerm = resultLog.scUsageTerm;
            tc = resultLog.scTC;
        }

        tpl = tpl.replace(/\[Player\]/g, history.player);
        tpl = tpl.replace(/\[ReferenceNo\]/g, history.referenceNo);
        tpl = tpl.replace(/\[Card\]/g, history.card);
        tpl = tpl.replace(/\[PhoneNo\]/g, history.phone);
        tpl = tpl.replace(/\[PromotionNo\]/g, rno);
        tpl = tpl.replace(/\[Email\]/g, history.email);
        tpl = tpl.replace(/\[CountryCode\]/g, country ? country.code : '');

        // TODO
        tpl = tpl.replace(/\[Prize\]/g, prizeName || '');

        // TODO
       // if (resultLog.prize && resultLog.prize.prizeCoupon && !!tc) {
        if (!!resultLog.tc) {
            tpl = tpl.replace(/\[TC\]/g, tandc[language] + '</br />' + tc.replace(/\r\n/g, "<br />"));
        } else {
            tpl = tpl.replace(/\[TC\]/g, '');
        }


        // TODO replace expiryDate if needed
        var defaultExpiryDate = '19 February, 2019';
        var formatLocale = 'en';
        if (language === 'tc') {
            formatLocale = 'zh-tw';
            defaultExpiryDate = '2019年2月19日';
        } else if (language === 'sc') {
            formatLocale = 'zh-cn';
            defaultExpiryDate = '2019年2月19日';
        }
        moment.locale(formatLocale);
        if (resultLog.expiryDate) {
            const formatedDate = moment(resultLog.expiryDate);
            if (formatedDate.isValid()) {
                tpl = tpl.replace(/\[ExpiryDate\]/g, formatedDate.format('LL'));
            } else {
                tpl = tpl.replace(/\[ExpiryDate\]/g, defaultExpiryDate);
            } 
        } else {
            tpl = tpl.replace(/\[ExpiryDate\]/g, defaultExpiryDate);
        }

        tpl = tpl.replace(/\[Today\]/g, moment().format('LL'));
        tpl = tpl.replace(/\[TODAY\]/g, moment().format('LL'));

        tpl = tpl.replace(/\[US\]/g, usageTerm ? usageTerm.replace(/\r\n/g, "<br />") : '');
        if (resultLog.redeemCode && !resultLog.isBigPrize) {
            const barcodePath = conf.absolutePath + 'barcode?code=' +resultLog.redeemCode;
            tpl = tpl.replace(/\[BARCODE\]/g, 
                '<img style="padding: 0 20px" src="'+ barcodePath + '" alt="'+ resultLog.redeemCode +'"');
        } else {
            tpl = tpl.replace(/\[BARCODE\]/g, '');
        }
        // keystone.get('global upload dir')
        // TODO
        subject = GameTitleTemplate[language].prize.replace(/\[Prize\]/g, prizeName || (resultLog.prize && resultLog.prize.gift || ''));
                

       if (history.emailList && _.isArray(history.emailList)) {
            email = email+','+history.emailList.join(',');
       }
        const mailConfig = {
            to: email,
            subject: subject,
            // html: encodeURIComponent(tpl + '<br /><br />' + tcTpl + '<br /><br />' + scTpl),
            // TODO
            html: encodeURIComponent(tpl),
            token: conf.appViewSecretKey,
        };
        request({
            method: 'POST',
            uri: conf.emailSenderAPI,
            // body: mailConfig,
            json: mailConfig,
        }, function (error, response, body) {
            console.log('> url: ', conf.emailSenderAPI);
            console.log('> sending with email data: ', mailConfig);
            console.log(error, response, body);
            if (callback) callback(error);
        });
        // console.log(mailConfig);
        // sendMail(mailConfig, function(err, info) {
        //     console.log('> Prize Email is sent to: ', email);
        // });
    });
}

function sendDrawResultEmail(history, callback) {
    CountryListController.getCountryById(history.country, function(err, country) {
        // console.log(history);
        const resultLog = history.resultLog.resultLog;
        const type = resultLog.prize ? 'prize' : 'ecoupon';
        const language = history.language;

        // var tpl = GameTemplate[language].prize || '';
        var email = history.email;
        var subject = '';



        tpl = resultLog.outParkFormat;
        var tcTpl = "<br />" + resultLog.tcOutParkFormat;
        var scTpl = "<br />" + resultLog.scOutParkFormat;
        // TODO
        if (history.invoiceNo) {
            tpl = resultLog.hotelFormat;
            tcTpl = "<br />" + resultLog.tcHotelFormat;
            scTpl = "<br />" + resultLog.scHotelFormat;
            // tpl = resultLog.hotelFormat;
            // if (language === 'tc') {
            //     tpl = resultLog.tcHotelFormat;
            // } else if (language === 'sc') {
            //     tpl = resultLog.scHotelFormat;
            // }
        // app view
        } else if (history.deviceId) {
            tpl = resultLog.inParkFormat;
            tcTpl = "<br />" + resultLog.tcInParkFormat;
            scTpl = "<br />" + resultLog.scInParkFormat;
            // tpl = resultLog.inParkFormat;
            // if (language === 'tc') {
            //     tpl = resultLog.tcInParkFormat;
            // } else if (language === 'sc') {
            //     tpl = resultLog.scInParkFormat;
            // }
        // operator
        } else {
            // if (language === 'tc') {
            //     tpl = resultLog.tcOutParkFormat;
            // } else if (language === 'sc') {
            //     tpl = resultLog.scOutParkFormat;
            // }
        }
        

        // TODO
        // var prizeName = resultLog.name;
        // var tc = resultLog.tc;
        // if (language === 'tc') {
        //     prizeName = resultLog.tcName;
        //     tc = resultLog.tcTC;
        // } else if (language === 'sc') {
        //     prizeName = resultLog.scName;
        //     tc = resultLog.scTC;
        // }

        tpl = tpl.replace(/\[Prize\]/g, resultLog.name || '');
        tcTpl = tcTpl.replace(/\[Prize\]/g, resultLog.tcName || '');
        scTpl = scTpl.replace(/\[Prize\]/g, resultLog.scName || '');

        tpl = tpl.replace(/\[Player\]/g, history.player);
        tpl = tpl.replace(/\[ReferenceNo\]/g, history.referenceNo);
        tpl = tpl.replace(/\[Card\]/g, history.card);
        tpl = tpl.replace(/\[PhoneNo\]/g, history.phone);
        tpl = tpl.replace(/\[PromotionNo\]/g, rno);
        tpl = tpl.replace(/\[Email\]/g, history.email);
        tpl = tpl.replace(/\[CountryCode\]/g, country ? country.code : '');

        tcTpl = tcTpl.replace(/\[Player\]/g, history.player);
        tcTpl = tcTpl.replace(/\[ReferenceNo\]/g, history.referenceNo);
        tcTpl = tcTpl.replace(/\[Card\]/g, history.card);
        tcTpl = tcTpl.replace(/\[PhoneNo\]/g, history.phone);
        tcTpl = tcTpl.replace(/\[PromotionNo\]/g, rno);
        tcTpl = tcTpl.replace(/\[Email\]/g, history.email);
        tcTpl = tcTpl.replace(/\[CountryCode\]/g, country ? country.code : '');

        scTpl = scTpl.replace(/\[Player\]/g, history.player);
        scTpl = scTpl.replace(/\[ReferenceNo\]/g, history.referenceNo);
        scTpl = scTpl.replace(/\[Card\]/g, history.card);
        scTpl = scTpl.replace(/\[PhoneNo\]/g, history.phone);
        scTpl = scTpl.replace(/\[PromotionNo\]/g, rno);
        scTpl = scTpl.replace(/\[Email\]/g, history.email);
        scTpl = scTpl.replace(/\[CountryCode\]/g, country ? country.code : '');

        // TODO
        // tpl = tpl.replace(/\[Prize\]/g, prizeName || '');

        // TODO
        // if (resultLog.prize && resultLog.prize.prizeCoupon && !!tc) {
        //     tpl = tpl.replace(/\[TC\]/g, tandc[language] + '<br /</br />' + tc);
        // } else {
        //     tpl = tpl.replace(/\[TC\]/g, '');
        // }

        // if (resultLog.prize && resultLog.prize.prizeCoupon) {
        if (resultLog.tc) {
            tpl = tpl.replace(/\[TC\]/g, tandc.en + '<br />' + resultLog.tc.replace(/\r\n/g, "<br />"));
        } else {
            tpl = tpl.replace(/\[TC\]/g, '');
        }
        if (resultLog.tcTC) {
            tcTpl = tcTpl.replace(/\[TC\]/g, tandc.tc + '<br />' + resultLog.tcTC.replace(/\r\n/g, "<br />"));
        } else {
            tcTpl = tcTpl.replace(/\[TC\]/g, '');
        }
        if (resultLog.scTC) {
            scTpl = scTpl.replace(/\[TC\]/g, tandc.sc + '<br />' + resultLog.scTC.replace(/\r\n/g, "<br />"));
        } else {
            scTpl = scTpl.replace(/\[TC\]/g, '');
        }
        // } else {

        // }

        // moment.locale('en');
        if (resultLog.expiryDate) {
            var formatedDate = moment(resultLog.expiryDate);
            if (moment(resultLog.expiryDate)) {
                moment.locale('en');
                tpl = tpl.replace(/\[ExpiryDate\]/g, formatedDate.format('LL'));
                moment.locale('zh-tw');
                tcTpl = tcTpl.replace(/\[ExpiryDate\]/g, formatedDate.format('LL'));
                moment.locale('zh-cn');
                scTpl = scTpl.replace(/\[ExpiryDate\]/g, formatedDate.format('LL'));
            } else {
                tpl = tpl.replace(/\[ExpiryDate\]/g, '19 February, 2019');
                tcTpl = tcTpl.replace(/\[ExpiryDate\]/g, '2019年2月19日');
                scTpl = scTpl.replace(/\[ExpiryDate\]/g, '2019年2月19日');
            } 
        } else {
            tpl = tpl.replace(/\[ExpiryDate\]/g, '19 February, 2019');
            tcTpl = tcTpl.replace(/\[ExpiryDate\]/g, '2019年2月19日');
            scTpl = scTpl.replace(/\[ExpiryDate\]/g, '2019年2月19日');
        }
        moment.locale('en');
        tpl = tpl.replace(/\[Today\]/g, moment().format('LL'));
        tpl = tpl.replace(/\[TODAY\]/g, moment().format('LL'));
        moment.locale('zh-tw');
        tcTpl = tcTpl.replace(/\[Today\]/g, moment().format('LL'));
        tcTpl = tcTpl.replace(/\[TODAY\]/g, moment().format('LL'));
        moment.locale('zh-cn');
        scTpl = scTpl.replace(/\[Today\]/g, moment().format('LL'));
        scTpl = scTpl.replace(/\[TODAY\]/g, moment().format('LL'));


        if (resultLog.usageTerm) {
            tpl = tpl.replace(/\[US\]/g, resultLog.usageTerm.replace(/\r\n/g, "<br />"));
        } else {
            tpl = tpl.replace(/\[US\]/g, '');
        }
        if (resultLog.tcUsageTerm) {
            tcTpl = tcTpl.replace(/\[US\]/g, resultLog.tcUsageTerm.replace(/\r\n/g, "<br />"));
        } else {
            tcTpl = tcTpl.replace(/\[US\]/g, '');
        }
        if (resultLog.scUsageTerm) {
            scTpl = scTpl.replace(/\[US\]/g, resultLog.scUsageTerm.replace(/\r\n/g, "<br />"));
        } else {
            scTpl = scTpl.replace(/\[US\]/g, '');
        }

        if (resultLog.redeemCode && !resultLog.isBigPrize) {
            const barcodePath = conf.absolutePath + 'barcode?code=' +resultLog.redeemCode;
            tpl = tpl.replace(/\[BARCODE\]/g, 
                '<img style="padding: 0 20px" src="'+ barcodePath + '" alt="'+ resultLog.redeemCode +'"');
            tcTpl = tcTpl.replace(/\[BARCODE\]/g, 
                '<img style="padding: 0 20px" src="'+ barcodePath + '" alt="'+ resultLog.redeemCode +'"');
            scTpl = scTpl.replace(/\[BARCODE\]/g, 
                '<img style="padding: 0 20px" src="'+ barcodePath + '" alt="'+ resultLog.redeemCode +'"');
        } else {
            tpl = tpl.replace(/\[BARCODE\]/g, '');
            tcTpl = tcTpl.replace(/\[BARCODE\]/g, '');
            scTpl = scTpl.replace(/\[BARCODE\]/g, '');
        }

        // TODO replace expiryDate if needed
        // var defaultExpiryDate = '19 February, 2019';
        // var formatLocale = 'en';
        // if (language === 'tc') {
        //     formatLocale = 'zh-tw';
        //     defaultExpiryDate = '2019年2月19日';
        // } else if (language === 'sc') {
        //     formatLocale = 'zh-cn';
        //     defaultExpiryDate = '2019年2月19日';
        // }
        // moment.locale(formatLocale);
        // if (resultLog.expiryDate) {
        //     const formatedDate = moment(resultLog.expiryDate);
        //     if (formatedDate.isValid()) {
        //         tpl = tpl.replace(/\[ExpiryDate\]/g, formatedDate.format('LL'));
        //     } else {
        //         tpl = tpl.replace(/\[ExpiryDate\]/g, defaultExpiryDate);
        //     } 
        // } else {
        //     tpl = tpl.replace(/\[ExpiryDate\]/g, defaultExpiryDate);
        // }

        // tpl = tpl.replace(/\[Today\]/g, moment().format('LL'));

        // TODO
        // subject = GameTitleTemplate[language].prize.replace(/\[Prize\]/g, prizeName || (resultLog.prize && resultLog.prize.gift || ''));
                
        subject = GameTitleTemplate.en.prize.replace(/\[Prize\]/g, resultLog.name || '') + ', ' + 
            GameTitleTemplate.tc.prize.replace(/\[Prize\]/g, resultLog.tcName || '') + ', ' + 
            GameTitleTemplate.sc.prize.replace(/\[Prize\]/g, resultLog.scName || '');



       if (history.emailList && _.isArray(history.emailList)) {
            email = email+','+history.emailList.join(',');
       }
        const mailConfig = {
            to: email,
            subject: subject,
            html: encodeURIComponent(tpl + '<br /><br />' + tcTpl + '<br /><br />' + scTpl),
            // TODO
            // html: encodeURIComponent(tpl),
            token: conf.appViewSecretKey,
        };
        request({
            method: 'POST',
            uri: conf.emailSenderAPI,
            // body: mailConfig,
            json: mailConfig,
        }, function (error, response, body) {
            console.log('> url: ', conf.emailSenderAPI);
            console.log('> sending with email data: ', mailConfig);
            console.log(error, response, body);
            if (callback) callback(error);
        });
        // console.log(mailConfig);
        // sendMail(mailConfig, function(err, info) {
        //     console.log('> Prize Email is sent to: ', email);
        // });
    });
}

function sendBigPrizeEmail(winner, callback) {
    _.forEach(winner.winnerEmail, function(email, i) {
        const country = winner.winnerCountryList[i];
        const language = winner.winnerLanguageList[i];
        var prize = winner.bigPrize[i];

        if (language === 'tc') {
            prize = prize.tcGift;
        } else if (language === 'sc') {
            prize = prize.scGift;
        } else {
            prize = prize.gift;
        }
        // console.log(country, winner.winnerPhoneNoList[i]);
        var tpl = GameTemplate[language].prize;
        tpl = tpl.replace(/\[Name\]/g, winner.winnerNameList[i]);
        tpl = tpl.replace(/\[Prize\]/g, prize);
        tpl = tpl.replace(/\[ReferenceNo\]/g, winner.winnerReferenceNoList[i]);
        tpl = tpl.replace(/\[Player\]/g, winner.winnerNameList[i]);
        tpl = tpl.replace(/\[Card\]/g, winner.winnerCardList[i]);
        tpl = tpl.replace(/\[PhoneNo\]/g, winner.winnerPhoneNoList[i]);
        tpl = tpl.replace(/\[Email\]/g, email);
        tpl = tpl.replace(/\[CountryCode\]/g, country ? country.code : '');

        const subject = GameTitleTemplate[language].prize.replace(/\[Prize\]/g, prize);
        const mailConfig = {
            from: senderEmail,
            to: email,
            subject: subject,
            html: tpl
        };
        // console.log(mailConfig);
        sendMail(mailConfig, function(err, info) {
            console.log('> Prize Email is sent to: ', email);
        });

    });
}

const mailer = { 
    sendMail: sendMail,
    sendBigPrizeEmail: sendBigPrizeEmail,
    sendDrawResultEmail: sendDrawResultEmail,
    sendSingleDrawResultEmail: sendSingleDrawResultEmail,
    sendSingleDrawResultEmailAdpater: sendSingleDrawResultEmailAdpater,
    getDrawResultEmailFormat: getDrawResultEmailFormat,
}


exports = module.exports = mailer;
