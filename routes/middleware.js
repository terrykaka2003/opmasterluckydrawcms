var _ = require('underscore');
const _l = require('lodash');
var keystone = require('keystone');
const MobileDetect = require('mobile-detect');
const request = require('request');
const async = require('async');
var AuditTrail = require(global.__base + "/models/AuditTrail");
var url = require('url');
var Config = require(global.__base + '/config/Config');
var conf = new Config();
var Role = require(global.__base + "/models/user/Role");
const moment 					= require('moment');
const fplocalize 				= require(global.__base + "/localization/fplocalize");
const Utility 					= require(global.__base + "/helper/Utility");

var APIRequestHandler 			= require(global.__base + '/routes/APIRequestHandler');
var LotterySettingController  	= require(global.__base + '/models/game/lotterySetting');
var CountryListController  		= require(global.__base + '/models/game/countryList');
var GameCharacterController  	= require(global.__base + '/models/game/character');
var StaffController  			= require(global.__base + '/models/game/staff');
APIRequestHandler = new APIRequestHandler();
CountryListController = new CountryListController();
LotterySettingController = new LotterySettingController();
GameCharacterController = new GameCharacterController();
StaffController = new StaffController();
const authHeader                = require('auth-header');
// const _ 						= require('lodash');


/**
	Initialises the standard view locals
*/

exports.initLocals = function(req, res, next) {
	
	var locals = res.locals;
	
	locals.navLinks = [
		{ label: 'Home',		key: 'home',		href: '/' },
		{ 
			label: 'Lucky Draw Game (Hotel)',
			key: 'htLuckyDraw',
			href: '/web/hotel/luckyDraw',
		},
		{ 
			label: 'Lucky Draw Game (Operator)',
			key: 'opLuckyDraw',
			href: '/web/operator/luckyDraw',
		}
	];
	
	locals.user = req.user;
	
	next();
	
};


/**
	Fetches and clears the flashMessages before a view is rendered
*/
exports.flashMessages = function(req, res, next) {
	
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error')
	};
	
	res.locals.messages = _.any(flashMessages, function(msgs) { return msgs.length; }) ? flashMessages : false;
	
	next();
	
};

function includeDeviceId(req) {
	if (req.body.deviceId) {
		req.deviceId = req.body.deviceId;
	} else if (req.query.deviceId) {
		req.deviceId = req.query.deviceId;
	}
}

function convertUnicode(content) {
	var result = '';
	for (var i = 0; i < content.length; i++) {
		if (content.charCodeAt(i) > 65248 && content.charCodeAt(i) < 65375) {
			result += String.fromCharCode(content.charCodeAt(i) - 65248);
		} else {
			result += String.fromCharCode(content.charCodeAt(i));
		}
	}
	return result;
}

function stripAllSpacing(content) {
	return content.replace(/ /g, '').replace(/　/g, '');
}

/*
** Determine the request with invoiceNo
** Remove the deviceId no matter it is provided
** @Set: Request(invoiceNo)
** @Return: Callback(Error)
** Terry Chan
** 06/01/2019
*/
function includeInvoiceNo(req, cb) {
	var error_message = '';
	if (!req.body.invoiceNo) {
		error_message = fplocalize(req.language).invalidPassNo;
		return cb(new Error(error_message));
	}
	req.invoiceNo = req.body.invoiceNo;
	// no need device id
	if (req.body.deviceId) {
		req.body.deviceId = null;
		delete req.body.deviceId;
	}
	return cb(null);
}
function checkTicketValid(req, ticketNo, lang, cb) {
	var ticketNoFormat = convertUnicode(ticketNo);
	ticketNoFormat = stripAllSpacing(ticketNoFormat);
	request({
		method: 'POST',
	    uri: conf.ticketValidationURL,
	    json: {
	    	'device_id': req.deviceId,
	    	'bssid': conf.wifiMac,
	    	'langId': lang,
	    	'os': 'ios',	// doesnt care
	    	'tickets': [
	    		{ ticket_id: ticketNoFormat }
	    	]
	    }
	}, function (error, response, body) {
		console.log('> checkTicketValid: ', body);
		if (error) {
			const cError = new Error(fplocalize(req.language).opVerificationTicketError);
			cError.errorType = 406;
			console.log(cError);
			return cb(cError);
		} else if (response && response.body && response.body.error) {
			if (response.body.error.code === '1012') {
				req.passNo = _l.toUpper(ticketNo);
				req.bypassPaired = true;
				return cb(null);
			} else {
				var error = new Error(response.body.error.message);
				error.errorType = 406;
				return cb(error);
			}
			// return APIRequestHandler.sendDefaultJsonErrResponse(res, 
			// 	new Error(response.body.error.message));
		} else {
			req.passNo = _l.toUpper(ticketNo);
			return cb(null);
		}
	});
}
/*
** Determine the request with ticketNo or Anual Pass No
** @Set: Request(passNo)
** @Return: Callback(Error)
** Terry Chan
** 06/01/2019
*/
function includePassNo(req, cb) {
	var error_message = '';
	if (!req.body.ticketNo && !req.query.ticketNo) {
		error_message = fplocalize(req.language).invalidPassNo;
		return cb(new Error(error_message));
		// return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
	}

	var lang = 'en';
	if (req.language === 'tc' || req.language === 'sc') {
		lang = 'zh';
	}

	// 1.0 validation ticket no in the op server
	const ticketNo = req.body.ticketNo || req.query.ticketNo;
	console.log('By pass ticket check: ', conf.byPassPassCheck);
	if (!conf.byPassPassCheck) {
		checkTicketValid(req, ticketNo, lang, cb);
	} else {
		req.passNo = _l.toUpper(ticketNo);
		return cb(null);
	}
}

function extendLotterySetting(req, setting) {
	var withProperties = Object.assign({}, setting);
	withProperties.popoverBanner = Object.assign({}, conf.lotteryProperties.popoverBanner, {
		url: conf.absolutePath + 'images/' + 
			conf.lotteryProperties.popoverBanner.url.replace('{{language}}', req.language),
	});
	withProperties.gameButton = Object.assign({}, conf.lotteryProperties.gameButton, {
		url: conf.absolutePath + 'images/' + conf.lotteryProperties.gameButton.url,
	});

	return withProperties;
}
exports.extendLotterySetting = function(req, setting) {
	return extendLotterySetting(req, setting);
}

exports.testingViewByPass = function(req, res, next) {
	req.byPassRegularWebview = true;
	console.log('> 1');
	next();
}

/*
** Retrieve Lottery Setting from Database
** @Set: Request(setting)
** @Return: Callback(Error)
** Terry Chan
** 06/01/2019
*/
function includeLotterySetting(req, cb) {
	LotterySettingController.getGameSetting(function(err, setting) {
		if (err) {
			return cb(err);
			// return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		var error_message = '';
		if ((!setting || (setting && !setting.enabled)) && !req.byPassRegularWebview) {
			error_message = fplocalize(req.language).notAllowedLotteryPeriod;
			return cb(new Error(error_message));
			// return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
		}
		req.setting = extendLotterySetting(req, setting);
		return cb(null);
	});
}
/*
** Check for all of request accessing in in-park period
** @Return: Callback(Error)
** Terry Chan
** 06/01/2019
*/
function includeParkLotteryPeriod(req, cb) {
	const setting = req.setting;
	// console.log(req.setting);
	const parkStartSetting = setting.parkStartGame;
	const parkEndSetting = setting.parkEndGame;
	const startCheck = moment.duration(moment().diff(moment(parkStartSetting, 'HH:mm:ss'))).asMinutes();
	const endCheck = moment.duration(moment().diff(moment(parkEndSetting, 'HH:mm:ss'))).asMinutes();
	
	if (!req.byPassRegularWebview && !conf.byPassPassCheck && (startCheck <= 0 || endCheck >= 0)) {
		const error_message = fplocalize(req.language).notAllowedLotteryPeriod;
		const error = new Error(error_message);
		error.errorType = '404';
		return cb(error);
		// return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
	}
	return cb(null);
}

/*
** Retrieve Operator Account and checking for loginToken
** @Set: Request(staff)
** @Return: Callback(Error)
** Terry Chan
** 06/01/2019
*/
function includeOperatorStaff(req, cb) {
	var error_message = '';
	StaffController.getstaffInfoByToken('op', req.authorization.token, function(err, staff) {
    	if (err) {
			return cb(err);
    		// return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
    	}
    	if (!staff) {
			error_message = fplocalize(req.language).loginOperatorStaffFirst;
			return cb(new Error(error_message));
    		// return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
    	}
    	req.staff = staff;
    	return cb(null);
    });
}

/*
** Retrieve Hotel Account and checking for loginToken
** @Set: Request(staff)
** @Return: Callback(Error)
** Terry Chan
** 06/01/2019
*/
function includeHotelStaff(req, cb) {
	var error_message = '';
	StaffController.getstaffInfoByToken('ht', req.authorization.token, function(err, staff) {
		if (err) {
			return cb(err);
    		// return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
    	}
    	if (!staff) {
			error_message = fplocalize(req.language).loginStaffFirst;
			return cb(new Error(error_message));
    		// return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
    	}
    	req.staff = staff;
    	return cb(null);
    });
}

/*
** Authorize the Header Bearer Token
** @Set: Request(authorization)
** @Return: Callback(Error)
** Terry Chan
** 06/01/2019
*/
function authorization(req, cb) {
	var authorization = {};
    var error_message = '';
    try {
        // use get query to pass the authorization token
        // just for the case of read book from the urlencode
        // decodeURIComponent used to settle the spacing between the token and Bearar prefix
        authorization = authHeader.parse(req.get('authorization'));
        if (authorization.scheme !== 'Bearer') {
			error_message = fplocalize(req.language).userNotExist;
			return cb(new Error(error_message));
        	// return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
        } 
    } catch (err) {
    	console.log("req.get('authorization'): ", req.get('authorization'), req.headers);
    	console.log(err);
		error_message = fplocalize(req.language).loginStaffFirst;
		return cb(new Error(error_message));
        // return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
    }

    req.authorization = authorization;
	// console.log(authorization.token);    
	return cb(null);
}

exports.formatTicketNumber = function(ticketNo) {
	var ticketNoFormat = convertUnicode(ticketNo);
	ticketNoFormat = stripAllSpacing(ticketNoFormat);
	return ticketNoFormat;
}

exports.checkTicketValidFrom = function(req, ticketNo, lang, cb) {
	checkTicketValid(req, ticketNo, lang, cb);
}
exports.requireInvoiceNo = function(req, res, next) {
	includeInvoiceNo(req, function(err) {
		if (err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		next();
	});
}
exports.requirePassNo = function(req, res, next) {
	includeDeviceId(req);
	
	includePassNo(req, function(err) {
		if (err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		next();
	});
	
}
exports.requireLanguage = function(req, res, next) {
	var lang = req.body.lang ? req.body.lang : (req.query.lang ? req.query.lang : 'en');
	req.language = Utility.convertLangCode(lang);
	// console.log('>>>>>>>>>>>>>>>> ');
 	next();
}
exports.requireGameCharacter = function(req, res, next) {
	GameCharacterController.getAll(function(err, characteres) {
		// req.gameCharacter =  [];
		if (!err) {
			req.gameCharacterList = _l.shuffle(characteres).map(function(c) {
				if (c.icon) {
					return conf.absolutePath+conf.characterStaticPublicPath+'/'+c.icon.filename;
				}
				return '';
			});
			req.gameCharacterList = _.compact(req.gameCharacterList);
			const c = _l.groupBy(characteres, 'type');
			req.gameCharacter = _l.keys(c).map(function(key) {
				// shuffle and change image path
				return _l.shuffle(c[key].map(function(sc) {
					if (sc.icon) {
						sc.icon.fullPath = conf.absolutePath+conf.characterStaticPublicPath+'/'+sc.icon.filename;
					}
					return sc;
				}));
			});
		}

		// console.log(req.gameCharacter);
		next();
	});
}
exports.requireLotterySetting = function(req, res, next) {
	includeLotterySetting(req, function(err) {
		if (err) {
			err.errorType = '404';	// park close, setting not found
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		next();
	});
}

exports.requireNotForceLotterySetting = function(req, res, next) {
	includeLotterySetting(req, function(err) {
		LotterySettingController.getGameSetting(function(err, setting) {
			req.setting = extendLotterySetting(req, setting);
			next();
		});
	});
}

exports.requireHotelLotteryPeriod = function(req, res, next) {
	next();
}

exports.requireParkLotteryPeriod = function(req, res, next) {
	// console.log('> 3');
	includeParkLotteryPeriod(req, function(err) {
		if (err) {
			err.errorType = '404';	// park close, setting not found
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		next();
	});
}

exports.requireOperatorStaff = function(req, res, next) {
	includeOperatorStaff(req, function(err) {
		if (err) {
			err.errorType = 403; // Authorization
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		next();
	});
}

exports.requireHotelStaff = function(req, res, next) {
	includeHotelStaff(req, function(err) {
		if (err) {
			err.errorType = 403; // Authorization
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		next();
	});
}

exports.requireAuthorization = function(req, res, next) {
	authorization(req, function(err) {
		if (err) {
			err.errorType = 403; // Authorization
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		next();
	});
}


/**
	Prevents people from accessing protected pages when they're not signed in
 */

exports.requireUser = function(req, res, next) {
	
	if (!req.user) {
		req.flash('error', 'Please sign in to access this page.');
		res.redirect('/keystone/signin');
	} else {
		next();
	}
	
};

/*
** =============================================================================================
** Start for the frontend middleware parts
** =============================================================================================
** Terry Chan
** 06/01/2019
*/

// function getLoginCookie(req, cookieName) {
	
// }
function clearLoginStatus(res, cookieLang, cookieName, token) {
	var cookieOption = keystone.get('cookie frontend language options');
	// remove the login token stored before for hotel staff
	if (token) {
		cookieOption = Object.assign({}, cookieOption, { expires: new Date(0) });
		res.cookie(cookieName, '', cookieOption);
		res.cookie(cookieLang, '', cookieOption);
	}
}
// get initial login in cookies and setting from db
// no matter it is login or not
// reuturn status of account either logined or logouted
function getInitialLogin (req, cookieName, type, callback) {
	const token = req.cookies[cookieName];
	// var type = 'ht';
	// no cookkie token, then throw error to main flow
	if (!token) {
		return callback(true);
	}
	req.authorization = {
		token: token,
	};
	// console.log(type);
	if (type === 'op') {
		includeOperatorStaff(req, function(err) {
			if (err) {
				callback(err, token);
			} else {
				includeLotterySetting(req, function(err, setting) {
					callback(err, token);
				});
			}
		});
	} else {
		includeHotelStaff(req, function(err) {
			if (err) {
				callback(err, token);
			} else {
				includeLotterySetting(req, function(err, setting) {
					callback(err, token);
				});
			}
		});
	}
}
// middleware for any route, which is checking after login
exports.guardUnderLogin = function(req, res, next) {
	getInitialLogin(req, conf.cookiesMap.hotel, 'ht', function(err) {
		// console.log(err);
		if (err) {
			next();
		// if logined, then force to landing page
		} else {
			res.redirect('/web/hotel/luckyDraw');
		}
	});
}
exports.guardOperatorUnderLogin = function(req, res, next) {
	getInitialLogin(req, conf.cookiesMap.operator, 'op', function(err) {
		// console.log('>>>>>> ', err);
		if (err) {
			next();
		// if logined, then force to landing page
		} else {
			res.redirect('/web/operator/luckyDraw');
		}
	});
}
exports.requiredCountryList = function(req, res, next) {
	CountryListController.getCountryList(function(err, country) {
		if (country) {
			req.countryList = country;
		} else {
			req.countryList = [];
		}
		next();
	});
}

// middleware for any route, which is checking before login
exports.staffRequiredAuthorization = function(req, res, next) {
	const cookieLang = conf.cookiesMap.hotelLang;
	const cookieName = conf.cookiesMap.hotel;
	getInitialLogin(req, cookieName, 'ht', function(err, token) {
		// no login or expired authoriation token
		if (err) {
			clearLoginStatus(res, cookieLang, cookieName, token);
			res.redirect('/web/hotel/signin');
		} else {
			includeDeviceId(req);
			next();
		}
	});
}
exports.operatorRequiredAuthorization = function(req, res, next) {
	const cookieLang = conf.cookiesMap.operatorLang;
	const cookieName = conf.cookiesMap.operator;

	getInitialLogin(req, cookieName, 'op', function(err, token) {
		console.log(err);
		// no login or expired authoriation token
		if (err) {
			clearLoginStatus(res, cookieLang, cookieName, token);
			res.redirect('/web/operator/signin');
		} else {
			includeParkLotteryPeriod(req, function(err) {
				if (err) {
					req.parkError = err.message;
				}
				req.deviceId = conf.universalDeviceId;
				// console.log('>>>>>>');
				includePassNo(req, function(err) {
					// if (err) {
					// 	req.parkError = err.message;
					// }
					next();
				});
			});
		}
	});
}
exports.appRequiredAuthorization = function(req, res, next) {
	// console.log('> user agent: ', req.headers['user-agent']);
	/*
	** mobile checking
	*/
	// const md = new MobileDetect(req.headers['user-agent']);
	// if (!md.mobile() && !conf.byPassMobileDetection) {
	// 	// console.log('>1');
	// 	res.redirect('/404');
	// 	return;
	// }

	const secretKey = conf.appViewSecretKey;
	const requestRule = req.query.type && req.query.type === 'rule';
	var tasks = [
		function(cb) {
			includeLotterySetting(req, cb);
		},
		function(cb) {
			includeParkLotteryPeriod(req, cb);
		},
		function(cb) {
			includeDeviceId(req);
			cb(null);
		}
	];
	// if not enter rule page
	if (!requestRule) {
		if (!req.query.token || !req.query.deviceId || !req.query.ticketNo) {
			res.redirect('/404');
			return;
		}
		if (req.query.token !== secretKey) {
			res.redirect('/404');
			return;
		}
		tasks.push(function(cb) {
			includePassNo(req, cb);
		});
	}

	async.waterfall(tasks, function(err) {
		if (err) {
			// marked for showing the close label in frontend
			req.parkError = err.message;
		}
		next();
	});
	// includeParkLotteryPeriod(req, function(err) {
	// 	if (err) {
	// 		res.redirect('/');
	// 	} else {
	// 		includeLotterySetting(req, function(err) {
	// 			includePassNo(req, function(err) {

	// 				includeDeviceId(req);
	// 				next();
	// 			});
	// 		});
	// 	}
	// });
}


/*
** =============================================================================================
** End for the frontend middleware parts
** =============================================================================================
*/

/**
	Prevents user without permission
 */

exports.requirePermission = function(req, res, next) {

	var pathnameArray = url.parse(req.url).pathname.split('/');
	if(pathnameArray[1] == 'keystone' && pathnameArray[2] != 'signin' && pathnameArray[2] != 'signout'){
		// if access keystone
		if (!req.user) {
			// if not log in
			req.flash('error', 'Please sign in to access this page.');
			res.redirect('/keystone/signin');
		} else {
			// if log in
			if(!req.user.isSuperAdmin){
				// if not superadmin
				if(req.user.role){
					// if role is set
					var role = new Role();
					role.findById(req.user.role, function(err, result){
						result  = JSON.parse(JSON.stringify(result));

						var canAccessHome = pathnameArray[2]==false;
						var canAccessAdminUi = result.adminPermissions[pathnameArray[2]] && result.adminPermissions[pathnameArray[2]] != 'denied';
						var canAccessAdminApi = result.adminPermissions[pathnameArray[3]] && result.adminPermissions[pathnameArray[3]] != 'denied';

						if(canAccessAdminUi || canAccessAdminApi || canAccessHome){
							next();
						}else{
							if(pathnameArray[2]!='api' && pathnameArray[2]!='js' && pathnameArray[2]!='img'){
								// call admin page
								req.list = keystone.list(pathnameArray[2]);
								var view = new keystone.View(req, res);
								view.render('admin/unauthorized', { 
									nav: keystone.nav,
									section: keystone.nav.by.list[req.list.key] || {},
									list: req.list
								});
							}else{
								// call api
								next();
							}
						}
					});
				}else{
					// if role is not set
					var canAccessHome = pathnameArray[2]==false;

					if(canAccessHome){
						next();
					}else{
						if(pathnameArray[2]!='api' && pathnameArray[2]!='js' && pathnameArray[2]!='img' ){
							// call admin page
							req.list = keystone.list(pathnameArray[2]);
							var view = new keystone.View(req, res);
							view.render('admin/unauthorized', { 
								nav: keystone.nav,
								section: keystone.nav.by.list[req.list.key] || {},
								list: req.list
							});
						}else{
							// call api
							next();
						}
					}
				}
			}else{
				// if superadmin
				next();
			}
		}
	}else{
		// if not access keystone
		next();
	}

};


/**
	Audit trail
 */

exports.auditTrailModel = function(req, res, next) {
	var pathnameArray = url.parse(req.url).pathname.split('/');
	var query = url.parse(req.url, true).query;
	if (req.user) {
		if(req.method == 'POST' ){
			if(req.body.action == 'create'){
				// create action
				var auditTrail = new AuditTrail();
				auditTrail.addNewAuditTrail(req.user._id,pathnameArray[pathnameArray.length - 1],'create',JSON.stringify(req.body));
			}else if(req.body.action == 'updateItem'){
				// update action
				var auditTrail = new AuditTrail();
				auditTrail.addNewAuditTrail(req.user._id,pathnameArray[pathnameArray.length - 2],'update',JSON.stringify(req.body));
			}else if(pathnameArray[pathnameArray.length - 1] == 'delete'){
				// delete action
				var auditTrail = new AuditTrail();
				auditTrail.addNewAuditTrail(req.user._id,pathnameArray[pathnameArray.length - 2],'delete','');
			}
		}else if(req.method == 'GET'){
			if(query['delete']){
				// delete action
				var auditTrail = new AuditTrail();
				auditTrail.addNewAuditTrail(req.user._id,pathnameArray[pathnameArray.length - 1],'delete','');
			}
		}
	}

	next();

}

exports.auditTrailSignin = function() {
	var auditTrail = new AuditTrail();
	auditTrail.addNewAuditTrail(this._id,'users','signin','');
}


/**
	Locked account : after a number unsuccessful attempts to login
 */

exports.signinChecking = function(req, res, next) {
	conf = new Config();
 	var loginAttemptLimit = conf.loginAttemptLimit;

	if(req.url != '/keystone/signin' && req.url != '/keystone/signout'){	
		if (req.user && (req.user.status == 'locked' || req.user.status == 'inactive' )){

			req.flash('error', 'Your account has been locked.');
			return res.redirect('/keystone/signin');
		}
		next();
	}
	else{
		var isString = exports.isString = function isString (arg) {
			return typeof arg === 'string';
		};
		var escapeRegExp = exports.escapeRegExp = function escapeRegExp (str) {
			if (str && str.toString) str = str.toString();
			if (!isString(str) || !str.length) return '';
			return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
		};

		var lookup = req.body;
		var User = keystone.list(keystone.get('user model'));
		if ('string' === typeof lookup.email && 'string' === typeof lookup.password) {

			// create regex for email lookup with special characters escaped
			var emailRegExp = new RegExp('^' + escapeRegExp(lookup.email) + '$', 'i');
			// match email address and password
			User.model.findOne({ email: emailRegExp }).exec(function(err, user) {
				if (user) {
					user._.password.compare(lookup.password, function(err, isMatch) {
						if (!err && isMatch) {
							//if password is match
							//console.log('match');
							var conditions = { _id: user._id }
							  , update = { $set: { loginFailed: 0 }}
							  , options = { multi: false };
							User.model.update(conditions, update, options, function(err, result){
						        if (err){
						            console.log("Update login checking failed : " + err);   
						        }
						    });	

						} else {
							//if password is not match
							//console.log('not match');
							var conditions = { _id: user._id }
							  , update = { $inc: { loginFailed: 1 }}
							  , options = { multi: false };

							//if user account reached lock limit
							if(user.loginFailed >= loginAttemptLimit){
								update = { $set: {status : 'locked'}};
							}

							User.model.update(conditions, update, options, function(err, result){
						        console.log('update loginFailed');
						        if (err){
						            console.log("Update login checking failed : " + err);   
						        }
						    });			

						}
					});
				}
			});
		}

		next();
	}

};


/**
	Renew session
 */

exports.renewSession = function(req, res, next) {
	var pathnameArray = url.parse(req.url).pathname.split('/');

	if((pathnameArray[1] === 'keystone' && pathnameArray[2] != 'signin' && pathnameArray[2] != 'api') || pathnameArray[1] === 'sso'){
		var now = moment();
		var expiresAt = moment(now).add(conf.sessionExpireLimit, 'minutes');

		if(req.session.expiresAt){
			if(moment(req.session.expiresAt).diff(moment()) > 0){
				req.session.expiresAt = expiresAt;
				next();
			}else{
				req.session.destroy();
				res.redirect(302, '/keystone/signout');
			}
		}else{
			req.session.expiresAt = expiresAt;
			next();
		}
	}else{
		next();
	}
	
};


/**
	Check password expire
 */

exports.checkPasswordExpire = function(req, res, next) {
	var pathnameArray = url.parse(req.url).pathname.split('/');

	if(pathnameArray[1] === 'keystone' && pathnameArray[2] != 'signin' && pathnameArray[2] != 'api'){
		if(req.user && req.user.passwordUpdatedAt){
			var expiresAt = moment(req.user.passwordUpdatedAt).add(conf.passwordExpireLimit, 'days');

			if(moment(expiresAt).diff(moment()) < 0){
				req.flash('error', 'Your password is expired. Please change it now.');
				res.redirect('/admin/user');
			}else{
				next();
			}
		}else{
			next();
		}
	}else{
		next();
	}
	
};


/**
	Preview API 
 */

exports.previewMode = function(req, res, next) {
	req.previewMode = true;
	next();
}