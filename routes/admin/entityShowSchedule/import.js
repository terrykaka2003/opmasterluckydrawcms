/* 
* @Author: Willy
*/

var keystone = require('keystone');
var _ = require('underscore');
var async = require('async');
var fs = require('fs');
var parse = require('csv-parse');
var moment = require('moment');
var EntityShowSchedule = require(global.__base + '/models/show/EntityShowSchedule');

exports = module.exports = function(req, res) {
	req.list = keystone.list('User');
	var user_id = req.user._id;
	var itemQuery = req.list.model.findById(user_id).select();
	itemQuery.exec(function(err, item) {
		if (err) {
			req.flash('error', 'A database error occurred.');
			return res.redirect('/cms/' + req.list.path);
		}

		if (!item) {
			req.flash('error', 'Item ' + req.params.item + ' could not be found.');
			return res.redirect('/cms/' + req.list.path);
		}

		renderGenerateResult(req, res,item);

	});
};

/*
	Main function
*/
var renderGenerateResult = function(req, res,item) {

	async.waterfall([
		//step 1
		function(doneImport){
			//read data
			if(req.files.showIdsfile && req.files.showIdsfile.path){
				var inputFile = req.files.showIdsfile.path;
				var parser = parse({columns: true}, doneImport)
				fs.createReadStream(inputFile).pipe(parser);
			}else{
				doneImport(null, null);
			}


		},
		function(csvData, doneSavedata){
			var savedObj = [];
			if (csvData){
				async.eachSeries(csvData, function (item, doneUpdateData){
					var entityShowSchedule = new EntityShowSchedule();
					var public = item['display_to_public'] === '1' ? true : false

					var condition = {
						entity: item['Entity'],
						date: moment(item['Date'], 'DD/MM/YYYY').toISOString(),
						startTime: item['Start Time'],
						endTime: item['End Time'], 
						display_to_public: public
					}
					var newData = {
						entity: item['Entity'],
						date: moment(item['Date'],'DD/MM/YYYY').toISOString(),
						startTime: item['Start Time'],
						endTime: item['End Time'], 
						display_to_public: public
					}

					entityShowSchedule.create(condition, newData, function(err, result){
						if(result){
							result = JSON.parse(JSON.stringify(result));
							result.date = moment(result.date).format('DD/MM/YYYY');
				 			savedObj.push(result);
				 		}
						doneUpdateData(err, result);
					});
				}, function done(err){
					doneSavedata(err, savedObj);
				});
			}else{
				doneSavedata(null,null);
			}
		}

		],//done
		function(err,savedResult){

			var count = savedResult ? savedResult.length : 0;

			var data = {
				savedResult: savedResult,
				successMessage: count ? count + " records are updated." : ''
			};
			renderView(req,res,item, data);
		}
	) 
};

/*
	Helper
*/

var renderView = function (req,res,item,data){
	var appName = keystone.get('name') || 'Keystone';					
	var view = new keystone.View(req, res);
	view.render('admin/entityShowSchedule/import', {
		formData :req.body,
		savedResult: data.savedResult,
		successMessage: data.successMessage,
		nav: keystone.nav,
		section: keystone.nav.by.list['Luckydraw'] || {},
		title: appName + ': ' + req.list.singular + ': ' + req.list.getDocumentName(item),
		page: 'item',
		list: req.list,
		item: item,
		csrf_header_key: keystone.security.csrf.CSRF_HEADER_KEY,
		csrf_token_key: keystone.security.csrf.TOKEN_KEY,
		csrf_token_value: keystone.security.csrf.getToken(req, res),
		csrf_query: '&' + keystone.security.csrf.TOKEN_KEY + '=' + keystone.security.csrf.getToken(req, res),
		wysiwygOptions: {
			enableImages: keystone.get('wysiwyg images') ? true : false,
			enableCloudinaryUploads: keystone.get('wysiwyg cloudinary images') ? true : false,
			additionalButtons: keystone.get('wysiwyg additional buttons') || '',
			additionalPlugins: keystone.get('wysiwyg additional plugins') || '',
			additionalOptions: keystone.get('wysiwyg additional options') || {},
			overrideToolbar: keystone.get('wysiwyg override toolbar'),
			skin: keystone.get('wysiwyg skin') || 'keystone',
			menubar: keystone.get('wysiwyg menubar'),
			importcss: keystone.get('wysiwyg importcss') || ''
		}
	});
}
