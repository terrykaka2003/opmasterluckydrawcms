/* 
* @Author: Thomas
*/

var keystone = require('keystone');
var _ = require('underscore');
var async = require('async');
var fs = require('fs');
var parse = require('csv-parse');
var LuckyDrawSubmission = require(global.__base + '/models/animalCollection/LuckyDrawSubmission');

exports = module.exports = function(req, res) {
	req.list = keystone.list('User');
	var user_id = req.user._id;
	var itemQuery = req.list.model.findById(user_id).select();
	itemQuery.exec(function(err, item) {
		if (err) {
			req.flash('error', 'A database error occurred.');
			return res.redirect('/cms/' + req.list.path);
		}

		if (!item) {
			req.flash('error', 'Item ' + req.params.item + ' could not be found.');
			return res.redirect('/cms/' + req.list.path);
		}

		if(req.body.resetPrize){
			renderClearResult(req, res,item);
		}
		else{
			renderGenerateResult(req, res,item);
		}	

	});
};

/*
	Main function
*/
var renderGenerateResult = function(req, res,item) {
	async.waterfall([
		function(doneGetSubmission){
			getSubmission(doneGetSubmission);
		},
		function(submissionResult,doneGenerateWinners){
			if(req.body.prizeName && req.body.quota){
				if(req.files.prizeIdsfile && req.files.prizeIdsfile.path){
					var inputFile = req.files.prizeIdsfile.path;
					var parser = parse({delimiter: ','}, function (err, data) {
						if(data && data[0]){
							var prizeIds = data[0];
							generateWinner(req.body.prizeName, req.body.quota, prizeIds, doneGenerateWinners);	
						}
						else{
							doneGenerateWinners(err,data);	
						}
					})
					fs.createReadStream(inputFile).pipe(parser);
				}
				else{
					var prizeIds = req.body.prizeIds.split(',');
					generateWinner(req.body.prizeName, req.body.quota,prizeIds, doneGenerateWinners);	
				}			
			}
			else{
				doneGenerateWinners(null,'');
			}
		},
		function(generateWinnersResult,doneSaveWinner){		
			saveWinner(generateWinnersResult,function(err,result){
				doneSaveWinner(err,generateWinnersResult,result);
			});
			
		}],function(err,generateWinnersResult,savedWinner){
			var data ={
				generateWinnersResult: generateWinnersResult,
				savedWinner: savedWinner,
				clearFunction: true,
				successMessage: savedWinner.length ? savedWinner.length + ' records are updated.' :'' ,
			};
			renderView(req,res,item,data);
	}) 
};

var renderClearResult = function(req, res,item) {
	async.waterfall([
		function(doneupdateSubmission){
			if(req.body.resetPrizeName){
				clearWinner(req.body.resetPrizeName, doneupdateSubmission);
			}
			else{
				doneupdateSubmission(null,null);						
			}
		}
		],function(err,result){
			console.log(result);
			var data ={
				generateWinnersResult: false,
				savedWinner: false,
				clearFunction: true,
				clearResult: result,
				successMessage: result ? result + ' records are updated.' :'',
			};
			renderView(req,res,item,data);
	}) 
};

/*
	Helper
*/
var renderView = function (req,res,item,data){
	var appName = keystone.get('name') || 'Keystone';					
	var view = new keystone.View(req, res);
	view.render('admin/animalCollection/luckydraw', {
		formData :req.body,
		generateWinnersResult: data.generateWinnersResult,
		savedWinner: data.savedWinner,
		clearFunction: data.clearFunction,
		clearResult: data.clearResult,
		successMessage: data.successMessage,
		nav: keystone.nav,
		section: keystone.nav.by.list['Luckydraw'] || {},
		title: appName + ': ' + req.list.singular + ': ' + req.list.getDocumentName(item),
		page: 'item',
		list: req.list,
		item: item,
		csrf_header_key: keystone.security.csrf.CSRF_HEADER_KEY,
		csrf_token_key: keystone.security.csrf.TOKEN_KEY,
		csrf_token_value: keystone.security.csrf.getToken(req, res),
		csrf_query: '&' + keystone.security.csrf.TOKEN_KEY + '=' + keystone.security.csrf.getToken(req, res),
		wysiwygOptions: {
			enableImages: keystone.get('wysiwyg images') ? true : false,
			enableCloudinaryUploads: keystone.get('wysiwyg cloudinary images') ? true : false,
			additionalButtons: keystone.get('wysiwyg additional buttons') || '',
			additionalPlugins: keystone.get('wysiwyg additional plugins') || '',
			additionalOptions: keystone.get('wysiwyg additional options') || {},
			overrideToolbar: keystone.get('wysiwyg override toolbar'),
			skin: keystone.get('wysiwyg skin') || 'keystone',
			menubar: keystone.get('wysiwyg menubar'),
			importcss: keystone.get('wysiwyg importcss') || ''
		}
	});
}
var getSubmission = function(callback){
	var condition = {};
	keystone.list('LuckyDrawSubmission').model.find(condition).select().lean().exec(function(err, result) {
		callback(err,result);
	});
}

var generateWinner = function(prizeName, quota,prizeIds, callback){	
	var condition = {
		$or: [
			  {prizeName: {$exists: false}},
			  {prizeName: ''}
		]
	};
	keystone.list('LuckyDrawSubmission').model.find(condition).select().lean().exec(function(err, participantResult) {
		var winnerObj = [];
		var participantIndex = 0;
		if(participantResult){
			var randomParticipantList = _.shuffle(participantResult);
			for (var i = 0; i < quota; i++) { 
				if(participantIndex >= randomParticipantList.length){
					break;
				}
				var winnerArray = {
					participantId: randomParticipantList[participantIndex]._id ,
					name : randomParticipantList[participantIndex].chineseName,
					email: randomParticipantList[participantIndex].email,
					prizeName: prizeName,
					prizeId: prizeIds[participantIndex] ? prizeIds[participantIndex] : ''

				};
				winnerObj.push(winnerArray);
				participantIndex ++;
			}
		}
		callback(err,winnerObj);
	});
}

var clearWinner = function(prizeName, callback){

	var condition = {
		prizeName: prizeName,
	};
	var data ={
		prizeName: '',
		prizeId: ''
	}
	var luckyDrawSubmission = new LuckyDrawSubmission();
	luckyDrawSubmission.updates(condition, data , callback);
}

var saveWinner = function(winnerResult, callback){
	var savedObj =[]
	async.each(winnerResult, function (item, doneUpdateSubmission) {
		var luckyDrawSubmission = new LuckyDrawSubmission();
		if(item.participantId){
			var condition = {
				_id: item.participantId
		 	};
			var newData = {
				prizeName: item.prizeName,
				prizeId: item.prizeId
			}
		 	luckyDrawSubmission.update(condition, newData,function(err,result){
		 		if(result){
		 			savedObj.push(result);
		 		}
		 		doneUpdateSubmission(err,result);
		 	});		
		}
		else{
			doneUpdateSubmission('','empty Id');
		}	
	}, function done(err){
		callback(err,savedObj);
	})
}
