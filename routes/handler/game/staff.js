const keystone 					= require('keystone');
const _							= require('lodash');
const async 					= require('async');
const Authentication			= require(global.__base + '/lib/authentication');
var APIRequestHandler 			= require(global.__base + '/routes/APIRequestHandler');
const GameHandler  				= require(global.__base + '/routes/handler/game');
var StaffController  			= require(global.__base + '/models/game/staff');
const fplocalize 				= require(global.__base + "/localization/fplocalize");
const Middleware  				= require(global.__base + '/routes/middleware');
const Config 					= require(global.__base + '/config/Config');
const conf = new Config();
APIRequestHandler = new APIRequestHandler();
StaffController = new StaffController();
// Middleware.includeLotterySetting
function StaffHandler() {};

StaffHandler.prototype.logout = function(req, res) {
	try {
		req.staff.loginToken = null;
		req.staff.save();
		if (req.staff.type === 'op') {
			res.cookie(conf.cookiesMap.operator, null);
		} else {
			res.cookie(conf.cookiesMap.hotel, null);
		}
		
	} catch (err) {
		return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
	}

	
	return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, {});
};
StaffHandler.prototype.status = function(req, res) {
	var data = req.staff.toObject();
	GameHandler.getLotterySettingData(function(err, setting) {
		if (err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		data.setting = setting;
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, data);
	});
};

StaffHandler.prototype.login = function(req, res) {
	var error_message = '';
	var isOperator = true;
	const data = {
		staffId: req.body.staffId,
		password: req.body.password,
	};
	if (!req.body.type) {
		data.type = 'ht';
	} else {
		data.type = req.body.type;
	}

	if (data.type === 'ht') {
		isOperator = false;
	}
	// console.log(req);
	StaffController.login(data, function(err, member) {
		if (err) {
			error_message = fplocalize(req.language).loginStaffInvalid;
			return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
			// return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		member.loginToken = Authentication.generateToken({
			staffId: member.staffId,
			staffName: member.staffName,
		})

		member.save();
		var response = _.pick(member.toObject(), ['staffId', 'loginToken', 'staffName', 'type']);
		// Middleware.includeLotterySetting(req, function(err, setting) {
		// 	if (err) {
		// 		return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		// 	}
		// 	response.setting = _.pick(setting, ['banner', 'webviewUrl', 'enabled', 'popoverBanner', 'gameButton']);
		// 	return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, response);
		// });
		GameHandler.getLotterySettingData(function(err, setting) {
			if (err) {
				error_message = fplocalize(req.language).notAllowedLotteryPeriod;
				return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
			}
			
			if (isOperator) {
				res.cookie(conf.cookiesMap.operator, member.loginToken);
			} else {
				console.log(conf.cookiesMap.hotel);
				res.cookie(conf.cookiesMap.hotel, member.loginToken);
			}
			
			// console.log(res.cookie);
			response.setting = Middleware.extendLotterySetting(req, setting);
			return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, response);
		});
		// return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, response);
		// StaffController.updateInfo(function(err) {
		// 	if (err) {
		// 		return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		// 	}
		// 	return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, member);
		// })
	});
};


exports = module.exports = new StaffHandler();
