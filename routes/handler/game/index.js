const keystone 					= require('keystone');
const moment					= require('moment');
const _							= require('lodash');
const async 					= require('async');
const mongoose					= require('mongoose');
const gua             			= require('universal-analytics');
const emailLib 					= require(global.__base + '/lib/email');
const Config    				= require(global.__base + '/config/Config');
const conf      				= new Config();
const Middleware  				= require(global.__base + '/routes/middleware');
const fplocalize 				= require(global.__base + "/localization/fplocalize");
var LotterySettingController  	= require(global.__base + '/models/game/lotterySetting');
var CountryListController  		= require(global.__base + '/models/game/countryList');
var EligibilityLogController	= require(global.__base + '/models/game/log/eligibilityLog');
var LotteryDrawnLogController	= require(global.__base + '/models/game/log/lotteryDrawnLog');
var PrizePoolController			= require(global.__base + '/models/game/prizePool');
var PrizeController				= require(global.__base + '/models/game/prize');
var LotteryHistoryController	= require(global.__base + '/models/game/lotteryHistory');
var RedeemLotteryHistoryController	= require(global.__base + '/models/game/redeemLotteryHistory');
var DrawFormController			= require(global.__base + '/models/game/bigGame/drawForm');
var CouponPoolController		= require(global.__base + '/models/game/couponPool');
var APIRequestHandler 			= require(global.__base + '/routes/APIRequestHandler');
const PrizeDistributor 			= require(global.__base + "/services/Distributor/prize");
const CouponDistributor 		= require(global.__base + "/services/Distributor/coupon");
const PrizeRecycler 			= require(global.__base + "/services/Recycler/prize");
const CouponRecycler 			= require(global.__base + "/services/Recycler/coupon");
const DrawLogModel				= keystone.list('LotteryDrawnLog').model;
LotterySettingController = new LotterySettingController();
RedeemLotteryHistoryController = new RedeemLotteryHistoryController();
PrizePoolController = new PrizePoolController();
PrizeController = new PrizeController();
DrawFormController = new DrawFormController();
CouponPoolController = new CouponPoolController();
CountryListController = new CountryListController();
EligibilityLogController = new EligibilityLogController();
LotteryDrawnLogController = new LotteryDrawnLogController();
APIRequestHandler = new APIRequestHandler();
LotteryHistoryController = new LotteryHistoryController();

var referenceStart = conf.referenceStart;
const referenceNoPrefix = conf.referenceNoPrefix;
const referencePrefix = conf.referencePrefix;
const referenceNoPostfix = conf.referenceNoPostfix;

const visitorForGA = gua(conf.gaTrackId);

function GameHandler() {};

function countryList(cb) {
	CountryListController.getCountryList(cb);
}

function lotterySetting(cb) {
	LotterySettingController.getGameConfig(cb);
}

function lotterySettingData(cb) {
	async.parallel([
		function(cb){
			lotterySetting(cb);
		},
		// function(cb){
		// 	countryList(cb);
		// }
	], function(err, result){
		if(err) {
			callback(err);
		}
		const setting = _.assign({}, result[0]);
		// setting.countryList = _.map(result[1], function(country) {
		// 	return country.code;
		// });
		if (setting.buttonIcon) {
			setting.buttonIcon = conf.absolutePath+conf.gameStaticPublicPath
				+'/'+setting.buttonIcon.filename;
		}
		if (setting.banner) {
			setting.banner = conf.absolutePath+conf.gameStaticPublicPath
				+'/'+setting.banner.filename;
		}
		cb(null, setting);
	});
}

// function getLotteryLog(id) {
// 	return new Promise(function(resolve, reject) {
// 		LotteryDrawnLogController.getLog(id, function(err, log) {
// 			if (err) {
// 				reject(err);
// 			}
// 			resolve(log);
// 		});
// 	});
// }

function Lottery(profile) {
	const speed = _.times(profile.total);
	const prizeMap = _.times(profile.total);
	const list = [
		{
			key: 0,	// big prize
			proportion: profile.first,
		},
		{
			key: 1, // second prize
			proportion: profile.second,
		}
	];
	var pos;
	// console.log(list);
	_.forEach(list, function(l) {
		// the large proportion will have larger area
		_.times(l.proportion, function(p) {
			pos = Math.floor(Math.random() * speed.length);
			// assign to the mapping
			prizeMap[speed[pos]] = l.key;
			// remove from the speed set
			speed.splice(pos, 1);
			// console.log(speed.length);
		});
	});
	// console.log('> Proportion List: ', _.countBy(prizeMap, Number));
	const result = prizeMap[Math.floor(Math.random() * profile.total)];
	// if the big prize pool is empty, and the luckydraw result is big prize,
	// then force to second gift instead
	if (!profile.pool.length && result === 0) {
		return 1;
	}

	return result;
}

GameHandler.prototype.getLotterySettingData = function(callback) {
	return lotterySettingData(callback);
}

GameHandler.prototype.getLotterySetting = function(req, res) {
	lotterySettingData(function(err, setting) {
		if(err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		const withProperties = Middleware.extendLotterySetting(req, setting);
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, withProperties);
	});
};

GameHandler.prototype.getCountryList = function(req, res) {
	countryList(function(err, country) {
		if (err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, country);
	});
};

// =================================================================================================
// Play Lottery Game Scetions
// =================================================================================================

/*
** [Racing Condition Concern]
** Create Eligibility Log for checking on any ticket no or invoice no for the player
** @Param: Request Object
** @Param: Pass Number (Ticket No, Invoice No, Annual Pass No)
** @Return: Callack(EligibilityLog)
** Terry Chan
** 03/01/2019
*/
// function isEligibilityLog(passNo, cb) {
// 	EligibilityLogController.getLogByPassNo(passNo, cb);
// }

function createEligibilityLog(req, passNo, isInvoice, deviceId, cb) {
	const dayForPlay = moment().startOf('day').format('YYYYMMDD');
	EligibilityLogController.createLog(dayForPlay, passNo, isInvoice, deviceId, function(err, log) {
		// console.log(err, log);
		// 1.0 Error Code (11000) - Duplicated Index
		// if trigger any racing condition for the same ticket no
		// someone may be use many devices with the same ticket to charge the luckydraw process
		if (err && +err.code === 11000) { 
			// 1.1 get the previous eligibility log for the ticket
			EligibilityLogController.getLog(passNo, function(err, eligibilityLog) {
				if (eligibilityLog) {
					// 1.1a if the previous eligibility log is completed
					// Throw Exception for this time game
					if (eligibilityLog.status !== 'hold') {
						error_message = fplocalize(req.language).duplicatedLotteryToday;
		            	cb(new Error(error_message));
		            // 1.1b otherwise, return the previous log instead of a new eligibility log
		            // use the previous log to continue the game
					} else {
						eligibilityLog.playDate = dayForPlay;
						cb(null, eligibilityLog);

					}
				} else {
					error_message = fplocalize(req.language).errorDraw;
		            cb(new Error(error_message));
				}
           	});
        // 2.0 Return a new eligibility log for the new game play
        // Normal case
		} else {
			cb(null, log);
		}
	});
}

/*
** [Racing Condition Concern]
** Create Drew Log for any drew result, if the log cannot be created with duplicated index (racing condition)
** then return null to continue the next process (draw out the next gift by using the drew result)
** otherwise return new Drew Log for the chosen gift
** @Param: Data Object
** @Return: Callack(DrewLog)
** Terry Chan
** 03/01/2019
*/
const createDrawnLog = function(data, cb) {
	LotteryDrawnLogController.createLog(data, function(err, log) {
		// console.log(err, log);
		if (err && +err.code === 11000) { 
			// console.log('> draw log result: duplicated index');
			// if racing condition, continues to create the log with the big prize
			// until all of big prize cannot be drawn
			return cb(null);
		}
		// if no conflict, return the new drew log with the prize
		return cb(true, log, data);
	});
}

/*
** Create Inital History Object
** @Param: Request Object
** @Return: History Bean
** Terry Chan
** 03/01/2019
*/
function initialHistory(req) {
	const referenceNoPart = referenceStart + '' + (Math.floor(Math.random() * 1000) + 1) + 1;
	var historyData = {
		referenceNo: moment().format(referenceNoPrefix) + referenceNoPart + moment().format(referenceNoPostfix),
		agreed: moment().toDate(),
		country: req.body.country,
		card: req.body.card,
		shouldMarketing: !!req.body.marketing,
		player: req.body.name,
		phone: req.body.phone,
		email: req.body.email,
		completed: false,
		language: req.language,
	};

	if (historyData.shouldMarketing) {
		historyData.marketing = moment().toDate();
	}

	if (req.deviceId) {
		historyData.deviceId = req.deviceId;
	}

	if (req.staff) {
		historyData.staff = req.staff._id;
		historyData.drawPlatform = '2'; // hotel web
		if (req.staff.type === 'op') {
			historyData.drawPlatform = '1'; // operator web
			delete historyData.deviceId;
		}
	} else {
		historyData.drawPlatform = '0';	// app play
	}
	if (req.invoiceNo) {
		historyData.invoiceNo = req.invoiceNo;
	}
	if (req.passNo) {
		historyData.ticketNo = req.passNo;
	}

	return historyData;
}

/*
** [Racing Condition Concern]
** Update the Eligibility Log after drew out the result of the game
** If the update action is triggered with non-empty drew result log field
** and the resultLog is updated by other third (racing condition)
** If there is no any updated document, then obtain the latest Eligibility Log with non-empty drew result log
** @Param: Eligibility Log Bean
** @Return: Callback(Error, Eligibility Log)
** Terry Chan
** 03/01/2019
*/
function updateEligibilityLog(log, cb) {
	EligibilityLogController.updateLog(log, function(err, updated) {
		// the log is updated by the other entry (racing condition triggered)
		if (updated === 0) {
			EligibilityLogController.getLogById(log, cb);
		// otherwise
		} else {
			cb();
		}
	});
}

/*
** Remove the new drew log if there is any drew log for the game created before
** no any new drew log for the game, use the previous log instead
** @Param: Lottery Drawn Log Bean
** Terry Chan
** 03/01/2019
*/
function removeNewDrawnLog(log) {
	console.log('>>>> [removeNewDrawnLog] remove the current drew result log.', log._id);
	LotteryDrawnLogController.removeLog(log._id);
}

function removeEligibilityLog(log) {
	console.log('>>>> [removeEligibilityLog] remove the current eligibility result log.', log._id);
	EligibilityLogController.removeLog(log._id);
}

/*
** Save the initial history defined before with the drew log result
** @Param: Initial History Bean
** @Return: Callback(Error, Updated History Bean)
** Terry Chan
** 03/01/2019
*/
function saveInitialLotteryHistory(history, cb) {
	LotteryHistoryController.createHistory(history, cb);
}

/*
** Reduce concurrency concern for the same prize from the prize pool
** shuffle the prize pool according to the priority setting (descending ordering)
** @Param: Prizes List
** Terry Chan
** 03/01/2019
*/
function shufflePrizePool(prizes) {
	var shuffledWithPriority = [];
	var grouped = _.groupBy(prizes, 'priority');
	_.forOwn(grouped, function(group, priority) {
		shuffledWithPriority = _.concat([], shuffledWithPriority, _.shuffle(group));
	});
	return shuffledWithPriority.reverse();
}

/*
** Mark holding status from the pool either PrizePool or CouponPool
** @Param: Drew Log Bean
** @Return: Callback(Error, Updated Result)
** Terry Chan
** 03/01/2019
*/
function holdTheTargetGift(log, cb) {
	// console.log('log: ', log);
	if (log.prize) {
		PrizePoolController.updateStatus({
			hold: true,
			id: mongoose.Types.ObjectId(log.prizePool),
		}, cb);
	} else {
		CouponPoolController.updateStatus({
			hold: true,
			id: mongoose.Types.ObjectId(log.coupon),
		}, cb);
	}
}

/*
** Prepare the correct probability for the big prize and ecoupon gift
** using Lottery Setting for each of period set by admin
** adjust the probability with special checkpoint or special resetpoint
** @Param: Reqeust Object
** @Return: Callback(Prizes List)
** Terry Chan
** 03/01/2019
*/
function getProbabilities(req, cb) {
	const setting = req.setting;
	const timeformat = 'HH:mm:ss';
	var checkpoint = null; // marks as not drew out checkpoint
	var resetpoint = null; // marks as drew out checkpoint
	var probabilities = 0;
	var restProbabilites = 100;
	var current = moment();
	// console.log('>>>>>>> ', req.setting, current.isSameOrAfter(moment('16:30:00', timeformat)));
	// after close park, still need to serve hotel guest
	// if (current.isSameOrAfter(moment('19:00:00', timeformat))) {
	// 	probabilities = setting.checkProbability5;
	// 	checkpoint = setting.check5;
	// } else 
	if (current.isAfter(moment('00:00:00', timeformat)) && 
		current.isBefore(moment('11:00:00', timeformat))) {
		probabilities = +setting.startPeriod;
	} else if (current.isSameOrAfter(moment('16:30:00', timeformat))) {
		probabilities = +setting.checkProbability4;
		checkpoint = +setting.check4;
	} else if (current.isSameOrAfter(moment('15:00:00', timeformat))) {
		probabilities = +setting.checkProbability3;
		checkpoint = +setting.check3;
	} else if (current.isSameOrAfter(moment('13:00:00', timeformat))) {
		probabilities = +setting.checkProbability2;
		checkpoint = +setting.check2;
		resetpoint = +setting.checkmax2;
	} else if (current.isSameOrAfter(moment('11:00:00', timeformat))) {
		probabilities = +setting.checkProbability1;
		checkpoint = +setting.check1;
		resetpoint = +setting.checkmax1;
	// open the park
	// } else if (current.isSameOrAfter(moment('09:00:00', timeformat))) {
	// 	probabilities = +setting.startPeriod;
	}
	// else {
	// 	probabilities = +setting.startPeriod;
	// }

	if (!probabilities) {
		probabilities = 100;
	}

	// restProbabilites = Math.floor(100 - probabilities);
	// console.log(probabilities, restProbabilites, checkpoint);
	// get all of prize can be drew
	PrizePoolController.getAvailablePrizeList(function(err, prizes) {
		var toBeDrewCounter = 0;
		if (prizes) {
			toBeDrewCounter = prizes.length;
		}
		console.log('>>>>> can be draw count: ', toBeDrewCounter);
		// get total of prize box today
		PrizeController.getAvailablePrizeList(function(err, prizeBoxes) {
			var qty = 0;
			if (prizeBoxes) {
				qty = _.sumBy(prizeBoxes, 'quantity');
			}
			console.log('>>>>> total big prize (with drew prize) ', qty);
			// no any big prize can be drawn, set to 0% for the probability
			if (qty <= 0 || toBeDrewCounter <= 0) {
				restProbabilites = 100;
				probabilities = 0;
			} else {
				const NotDrewOutProportion = (toBeDrewCounter / qty) * 100;
				const drewOutProportion = 100 - NotDrewOutProportion;
				console.log('>>>>> current proportion for not draw out result: ', NotDrewOutProportion);
				console.log('>>>>> current proportion for already draw out result: ', drewOutProportion);
				// if the prize is draw out too fast, then reset to initial setting
				if (resetpoint && drewOutProportion >= resetpoint) {
					probabilities = +setting.startPeriod;
				// if the prize is under minimum drewout proportion, 
				// then reset to initial setting, instead of the probabilities in the current timeslote
				} else if (checkpoint && NotDrewOutProportion < checkpoint) {
					probabilities = +setting.startPeriod;
				}
			}
			// console.log(probabilities, 100 - probabilities);
			probabilities = Math.round(probabilities);
			restProbabilites = 100 - probabilities;
			console.log('Big Prize: ' + probabilities + '%');
			console.log('Second Gift: ' + restProbabilites + '%');

			cb({
				first: probabilities,	// big prize
				second: restProbabilites, // ecoupon
				total: (probabilities + restProbabilites > 100) ? 100 : probabilities + restProbabilites,
				// [IMPORTANT] shuffle the pool for easy to handle concurrency insertion
				// pool: _.shuffle(prizes), // pool to be drawn from that pool
				pool: shufflePrizePool(prizes),
			});
				// checkpoint, resetpoint);
		});
		// cb({
		// 	first: 
		// });
	});

}
GameHandler.prototype.removeAllLog = function(req, res) {
	async.parallel([
		function(callback) {
			LotteryDrawnLogController.cleanAll(callback);
		},
		function(callback) {
			LotteryHistoryController.cleanAll(callback);
		},
		function(callback) {
			EligibilityLogController.cleanAll(callback);
		},
		function(callback) {
			RedeemLotteryHistoryController.cleanAll(callback);
		}
	], function(err, result) {
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, { result: 'Clean All' });
	});
}
GameHandler.prototype.sendEmail = function(req, res) {
	var error = null;
	const token = req.body.token;
	// const fromWhere = req.body.from;
	const to = req.body.to;
	const subject = req.body.subject;
	var content = encodeURIComponent(req.body.html);
	if (!token || !to || !subject || !content) {
		error = new Error(fplocalize(req.language).missingParameter);
		// return cb(error);
		return APIRequestHandler.sendDefaultJsonErrResponse(res, error);
	}
	// try {
	// 	content = JSON.parse(content);
	// } catch (err) {
	// 	error = new Error(fplocalize(req.language).missingParameter);
	// 	// return cb(error);
	// 	return APIRequestHandler.sendDefaultJsonErrResponse(res, error);
	// }
	emailLib.sendMail({
		from: conf.smtpSender,
		to: to,
		subject: subject,
		html: content,
	}, function(err, info) {
		if (err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, info);
	});
	
}
GameHandler.prototype.checkTicketUsed = function(req, res) {
	const ticketNo = Middleware.formatTicketNumber(req.query.ticketNo);;
	var error = null;
	const extraErrorFields = {
		errorType: 404,
		title: fplocalize(req.language).valicationError
	};
	if (!ticketNo) {
		error = new Error(fplocalize(req.language).invalidPassNo);
		// return cb(error);
		return APIRequestHandler.sendDefaultJsonErrResponse(res, error, extraErrorFields);
	}
	const secretKey = conf.appViewSecretKey;
	const token = req.query.token;
	if (!token || token !== secretKey) {
		error = new Error(fplocalize(req.language).missingParameter);
		// return cb(error);
		return APIRequestHandler.sendDefaultJsonErrResponse(res, error, extraErrorFields);
	}
	// invalidPassNo
	const tasks = [
/*
		function(cb) {
			if (!req.deviceId) {
				req.deviceId = conf.universalDeviceId;
			}
			Middleware.checkTicketValidFrom(req, ticketNo, req.language, function(err) {
				if (err){
					return cb(err);
				}
				return cb();
			});
		},
*/
		// get the lottery history
		function(cb) {
			EligibilityLogController.getLog(ticketNo, function(err, history) {
			// LotteryHistoryController.getHistoryByTicketNo(ticketNo, function(err, history) {
				if (history && history.status !== 'hold') {
					// if (req.deviceId && req.deviceId !== eligibilityLog.deviceId) {
					// 	console.log('>>>>>> [Danger!!!!!] many device use the same token to play this game.');
					// 	error_message = fplocalize(req.language).duplicatedLotteryToday;
					// 	return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
					// }
					// var error = null;
					// only invoice no will be checked
					// if the ticket can be passed by op ticket validation api then it is annual pass
					// if (history.completed && history.invoiceNo) {
						console.log('> The game is already completed: ', history);
						var error = new Error(fplocalize(req.language).duplicatedLotteryToday);
						// const error_message = fplocalize(req.language).invalidPassNo;
						return cb(error);
					// }
					// const drewLog = history.resultLog;
					// if (drewLog && drewLog.status === 'hold') {
					// 	error = new Error(fplocalize(req.language).duplicatedLotteryToday);
					// 	// const error_message = fplocalize(req.language).invalidPassNo;
					// 	return cb(error);
					// }
				}
				return cb();
			});
		},
	];
	async.waterfall(tasks, function(err) {
		// console.log(err);
		if (err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err, extraErrorFields);
		}
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, {});
	});
};

GameHandler.prototype.finishLottery = function(req, res) {
	//var error_message = '';
	const id =  mongoose.Types.ObjectId(req.body.id);
	const tasks = [
		// get the lottery history
		function(cb) {
			LotteryHistoryController.getHistoryById(id, function(err, history) {
				if (err) {
					return cb(err);
				} else if (!history) {
					console.log('> No such history');
					return cb(true);
				} else if (!history.resultLog) {
					console.log('> No Eligibility Log: ', history);
					return cb(true);
				} 
				history = history.toObject();
				history.completed = true;
				history.completedDate = moment().toDate();
				// else if (history.completed) {
				// 	console.log('> The game is already completed: ', history);
				// 	return cb(true);
				// }
				LotteryHistoryController.updateCompleted(history._id, function (err, result) {
					cb(err, history);
				});
				// history.completed = true;
				// history.completedDate = moment().toDate();
				// history.save();
				// cb(null, history);
			});
		},
		// update history.resultlog to drew result log
		function(history, cb) {
			EligibilityLogController.updateStatus({
				id: history.resultLog._id,
				status: 'completed',
				joinAt: moment().toDate(),
			}, function(err, updated) {
				cb(null, history);
			});
		},
		// populate history.resultlog to drew result log
		function(history, cb) {
			const eligibilityLog = history.resultLog;
			LotteryDrawnLogController.getLog(eligibilityLog.resultLog, function(err, drewLog) {
				if (drewLog) {
					// assign drew result log object to eligibility result log
					history.resultLog.resultLog = drewLog;
				}
				
				cb(null, history);
			});
		},
		// remove the gift from the pool
		function(history, cb) {
			const drewLog = history.resultLog.resultLog;
			if (drewLog.prize) {
				PrizePoolController.removeById(drewLog.prizePool, function(err, result) {
					cb(null, history);
				});
			} else if (drewLog.coupon){
				CouponPoolController.removeById(drewLog.coupon._id, function(err, result) {
					cb(null, history);
				});
			} else {
				cb(null, history);
			}
		},
		// add email to candidate list for the big luckdraw function
		function(history, cb) {
			console.log('> Add email to candidate list to DrawForm: ', history.email);
			DrawFormController.addCandidate(history, function(err, result) {
				cb(err, history);
			});
		},
		// update GA
		function(history, cb) {
			// var trackUrl = conf.gaHotelLuckyDrawUrl;
			// if (!!history.deviceId) {
			// 	trackUrl = conf.gaAppLuckyDrawUrl;
			// } else if (!history.deviceId && !!history.ticketNo) {
			// 	trackUrl = conf.gaAppLuckyDrawUrl;
			// }
			// trackUrl = conf.absolutePath + '' + trackUrl;
			// visitorForGA.pageview(trackUrl, function (err) {
			visitorForGA.event('ui_action', 'play_luckydraw', function(err) {
				console.log('> Log GA Track Event in Luckydraw Category for play action.');
				if (err) {
					console.log(err);
				}
				cb(null, history);
			});
			// .visitorForGA;
			
		},
		function(history, cb) {
			// only for out park luckydraw result by email
			// or hotel staff
			// emailLib.getDrawResultEmailFormat(history, function(mailConfig, historyData) {
			// 	LotteryHistoryController.updateEmailTpl(history._id || history.id, JSON.stringify(mailConfig));
			// });
			// or big prize
			if ((!history.deviceId || history.resultType === '0') && !history.emailSent) {
				// update language to current access language
				// history.language = req.language;

				// if ( process.env.NODE_ENV === 'production') {
				emailLib.sendSingleDrawResultEmail(history, function() {
					console.log('> after send email');
					// LotteryHistoryController.updateEmailSent(history._id || history.id, function(err, result) {
					history.emailSentAt = moment().toDate();
					history.emailSent = true;
					history.totalSent = 1;
					cb(null, history);
					// });
				});
				
				// // mix version
				// } else {
				// 	emailLib.sendDrawResultEmail(history, function() {
				// 		LotteryHistoryController.updateEmailSent(history._id || history.id);
				// 	});
				// }
			} else {
				cb(null, history);
			}
		},
		function(history, cb) {
			// set to send email action to be true except in-park ecoupon prize history
			history.emailAction = (!history.deviceId || history.resultType === '0');
			emailLib.getDrawResultEmailFormat(history, function(mailConfig, historyData) {
				// console.log('> updated mailConfig');
				const eformat = JSON.stringify(mailConfig);
				LotteryHistoryController.updateEmailTpl(history._id || history.id, eformat, function(err, result) {
					history.emailTpl = eformat;
					// console.log('> updated email format');
					cb(null, history);
				});
			});
		},
		function(history, cb) {
			// console.log('> create redeem log');
			RedeemLotteryHistoryController.createHistory(history, cb);
		},
	];

	async.waterfall(tasks, function(err, history) {
		// console.log(err);
		if (err) {
			const error_message = fplocalize(req.language).invalidUpdateGameHistory;
			return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
		}
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, history || {});
	});
}


// format to human response
function formatCoupon(language, drewLog) {
	// history.prizeName = result.name;
	// // console.log(result);
	// if (lang === 'sc') {
	// 	history.prizeName = result.scName || result.name;
	// } else if (lang === 'tc') {
	// 	history.prizeName = result.tcName || result.name;
	// }
	var data = {
		ecoupon_id: String(drewLog._id),
		ecoupon_code: drewLog.redeemCode,
		ecoupon_type_id: 'Free',
		discount_description: 'FREE',
		eCouponEndTime: '',
		eCouponStartTime: '',
		title: drewLog.name,
		terms_conditions: drewLog.tc,
		content: drewLog.description,
		validity_period_to: drewLog.expiryDate ? 
			moment(drewLog.expiryDate).format(conf.couponExpiryDateFormat) : '2019-02-19',
		usage_term: drewLog.usageTerm,
		location: [],
		redeemed: 0,
		surprise: 0,
		ecoupon_usage_id: 'Shopping',
		ecoupon_type_id: 'Free',
		keep: true,
		picked: false,
		pin_code: drewLog.redeemCode,
	};
	// console.log('> drewLog: ', drewLog);
	// if (drewLog.type) {
	if (drewLog.image) {
		data.image = conf.absolutePath+conf.couponTypeStaticPublicPath+'/'+drewLog.image;
	} else {
		data.image = '';
	}
	// else {
	// 	data.image = "http://masapp.oceanpark.com.hk/maws/rest/DownloadImage?refId=fec46abc-26a7-42dd-b175-cce3ff40c400";
	// }
	if (drewLog.icon) {
		data.banner_image = conf.absolutePath+conf.couponTypeStaticPublicPath+'/'+drewLog.icon;
	} else {
		data.banner_image = '';
	}
	// else {
	// 	data.banner_image = "http://masapp.oceanpark.com.hk/maws/rest/DownloadImage?refId=e47803cc-ba0f-41c6-94a2-a25b4734d022";	}

	if (language === 'tc') {
		data.title = drewLog.tcName || '';
		data.terms_conditions = drewLog.tcTC || '';
		data.content = drewLog.tcDescription || '';
		data.usage_term = drewLog.tcUsageTerm || '';
	} else if (language === 'sc') {
		data.title = drewLog.scName || '';
		data.terms_conditions = drewLog.scTC || '';
		data.content = drewLog.scDescription || '';
		data.usage_term = drewLog.scUsageTerm || '';
	}

	return data;
}
function formatPrize(response, lang, result, cb) {
	const tasks = [
		function (cb) {
			// console.log(typeof result.prize);
			// if the prize is not being populated
			if (mongoose.Types.ObjectId.isValid(result.prize)) {
				PrizeController.getPrizeById(result.prize, cb);
			} else {
				// use the populated object
				cb(null, result.prize);
			}
		}
	];
	async.waterfall(tasks, function(err, prize) {
		// console.log('>>>>>>>>> ', prize, lang);
		if (prize) {
			prize.prizeName = prize.gift;
			if (lang === 'sc') {
				prize.prizeName = prize.scGift;
			} else if (lang === 'tc') {
				prize.prizeName = prize.tcGift;
			}
			cb(null, prize);
		} else {
			cb(err, '');
		}
	});
	
}
function formatHistory(req, history, cb) {
	// console.log(history);
	if (history) {
		const printResult = req.gameCharacter[+history.resultType][0];
		var response = {
			id: history._id || history.id
		};
		response.prizeType = history.resultType;
		response.language = history.language;
		response.referenceNo = history.referenceNo;
		response.characteres = req.gameCharacterList;
		// mark as in park game
		response.inPark = !!history.deviceId;
		response.drawResult = printResult;
		// response.deviceId = history.deviceId;
		if (history.language === 'tc') {
			response.characterName = printResult.tcName || printResult.name;
		} else if (history.language === 'sc') {
			response.characterName = printResult.scName || printResult.name;
		} else {
			response.characterName = printResult.name;
		}

		response.drawResultIndex = _.findIndex(response.characteres, function(a) {
			return a === printResult.icon.fullPath;
		});
		const drewResult = history.resultLog.resultLog;
		// winning information
		// hotel staff
		if (history.invoiceNo && !history.deviceId) {
			response.winningInfo = drewResult.webHotelWinningInfo;
			if (history.language === 'tc') {
				response.winningInfo = drewResult.tcWebHotelWinningInfo;
			} else if (history.language === 'sc') {
				response.winningInfo = drewResult.scWebHotelWinningInfo;
			}
		// from app
		} else if (history.deviceId) {
			response.winningInfo = drewResult.appWinningInfo;
			if (history.language === 'tc') {
				response.winningInfo = drewResult.tcAppWinningInfo;
			} else if (history.language === 'sc') {
				response.winningInfo = drewResult.scAppWinningInfo;
			}
		// operator
		} else if (!history.deviceId){
			response.winningInfo = drewResult.webParkWinningInfo;
			if (history.language === 'tc') {
				response.winningInfo = drewResult.tcWebParkWinningInfo;
			} else if (history.language === 'sc') {
				response.winningInfo = drewResult.scWebParkWinningInfo;
			}
		}
		
		if (drewResult.coupon) {
			response.coupon = formatCoupon(req.language, drewResult);
		}
		if (drewResult.prize) {
			formatPrize(response, req.language, drewResult, function(err, prize) {
				response.prize = prize.prizeName;
				// response.prizeBox = prize;
				return cb(null, response);
			});
		} else {
			return cb(null, response);
		}
	} else {
		return cb(null, {});
	}
}



function formatHistoryCoupon(req, histories) {
	const language = req.language;
	return _.map(histories, function(history) {
		return formatCoupon(language, history.resultLog.resultLog);
	});
}

GameHandler.prototype.cleanPrizePool = function(req, res) {
	new PrizeRecycler().start();
}

GameHandler.prototype.cleanCouponPool = function(req, res) {
	new CouponRecycler().start();
}

GameHandler.prototype.cleanPool = function(req, res) {
	new PrizeRecycler().start();
	new CouponRecycler().start();
}

GameHandler.prototype.createPool = function(req, res) {
	new PrizeDistributor().start();
	new CouponDistributor().start();
}

GameHandler.prototype.createPrizePool = function(req, res) {
	new PrizeDistributor().start();
}

GameHandler.prototype.createCouponPool = function(req, res) {
	new CouponDistributor().start();
}

GameHandler.prototype.playLottery = function(req, res) {
	// 1.0 validate any pass no before start the game
	// var error_message = '';
	// if (!req.body.invoiceNo && !req.body.ticketNo) {
	// 	error_message = fplocalize(req.language).invalidPassNo;
	// 	return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
	// }
	// 2.0 Mandatary fields checking
	const requireds = ['country', 'card', 'name', 'phone', 'email'];
	const isValid = _.reduce(requireds, function(a, c) {
		if (!req.body[c]) {
			a = false;
		}
		return a;
	}, true);
	if (!isValid) {
		error_message = fplocalize(req.language).missingLotteryInfo;
		return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
	}

	// 3.0 setup history for and validation
	const historyData = initialHistory(req);
	// either invoice no or ticket no/ annual pass no
	var passNo = '';
	var isInvoice = false;
	if (req.invoiceNo ) {
		passNo = Middleware.formatTicketNumber(req.invoiceNo);
		isInvoice = true;
	} else {
		passNo = Middleware.formatTicketNumber(req.passNo);
	}

	// 4.0 check the 
	//isEligibilityLog(passNo, function(err, existingCount) {
	// 4.0 create eligibility log or using the previous eligibility log
	createEligibilityLog(req, passNo, isInvoice, req.deviceId, function(err, eligibilityLog) {
		// the user had to play the game before, reject the play request
		if (err) {
			return APIRequestHandler.sendDefaultJsonErrResponse(res, err);
		}
		// console.log(eligibilityLog);
		// 4.1 no matter whether it is a new eligibility log or the previous eligibility log
		if (eligibilityLog && !eligibilityLog.resultLog) {
			if (req.deviceId && req.deviceId !== eligibilityLog.deviceId && !req.bypassPaired) {
				console.log('>>>>>> [Danger!!!!!] many device use the same token to play this game.');
				error_message = fplocalize(req.language).duplicatedLotteryToday;
				return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
			}

			// 4.1.1 get the latest properly probabilities for big prize and second gift
			getProbabilities(req, function(profile) {
				// 4.1.1a get draw result either 0 big prize or 1 ecoupon
				const drawResult = Lottery(profile);
				// console.log('>>>>> ', drawResult);
				// Lottery(profile);
				var tasks = [];
				const bigPrizePool = profile.pool;
				// console.log('>>>>> ', bigPrizePool);
				/*
				** 4.1.1b declare aysnc function for non-stop drawout process
				** getCouponList - get all of valid ecoupon from the eCoupon Pool
				** saveBigPrize - draw out a big prize from the Prize Pool until find anyone
				** saveCouponGift - draw out a ecoupon gift from the eCoupon Pool until find anyone
				*/
				const getCouponList = function(cb) {
					CouponPoolController.getAvailableCouponList(cb);
				};
				const getCouponCount = function(cb) {
					CouponPoolController.getAvailableCouponCount(cb);
				};
				const getCoupon = function(skip, cb) {
					CouponPoolController.getAvailableCouponByIndex(skip, cb);
				};
				const saveBigPrize = function(cb) {
					var subTasks = [];
					
					// const test = [
					// 	{ _id: '5c2ce630d53ba47c71f101e1' },
					// 	{ _id: '5c2ce630d53ba47c71f101de' }
					// ];
					console.log('> [Game] start draw big prize.');
					_.forEach(bigPrizePool, function(prize) {
						subTasks.push(function(done) {
							// const field = [
							// 	'tcGift',
							// 	'scGift',
							// 	'tc',
							// 	'tcTC',
							// 	'scTC',
							// ];
							const data = {
								gift: String(prize._id),
								// link up to prize box instead of prize from the prize pool
								// since we will remove the prize from the prize pool later on
								prize: prize.prizeBox._id,
								prizePool: prize._id,
								name: prize.gift,
								tcName: prize.tcGift,
								scName: prize.scGift,
								isBigPrize: true,
								inParkFormat: prize.inParkFormat,
								tcInParkFormat: prize.tcInParkFormat,
								scInParkFormat: prize.scInParkFormat,
								outParkFormat: prize.outParkFormat,
								tcOutParkFormat: prize.tcOutParkFormat,
								scOutParkFormat: prize.scOutParkFormat,
								emailList: prize.emailList,
								appWinningInfo: prize.appWinningInfo,
								tcAppWinningInfo: prize.tcAppWinningInfo,
								scAppWinningInfo: prize.scAppWinningInfo,
								webHotelWinningInfo: prize.webHotelWinningInfo,
								tcWebHotelWinningInfo: prize.tcWebHotelWinningInfo,
								scWebHotelWinningInfo: prize.scWebHotelWinningInfo,
								webParkWinningInfo: prize.webParkWinningInfo,
								tcWebParkWinningInfo: prize.tcWebParkWinningInfo,
								scWebParkWinningInfo: prize.scWebParkWinningInfo,

								hotelFormat: prize.hotelFormat,
								tcHotelFormat: prize.tcHotelFormat,
								scHotelFormat: prize.scHotelFormat,
								
							};
							if (prize.prizeCoupon) {
								data.tc = prize.tc;
								data.prizeCoupon = true;
								data.tcTC = prize.tcTC;
								data.scTC = prize.scTC;
								data.expiryDate = prize.expiryDate;
								data.redeemCode = prize.redeemCode;
							}
							createDrawnLog(data, function(noError, log) {
								// console.log('>>>>>>> ', noError, log);
								// stop the rest chain of callback, take this prize with the log
								if (noError) {
									// set to big prize result
									// mark to true to stop coupon draw
									return done(true, log);
								}
								// continue coupon draw, if no prize can be drawn
								return done();
							});
						});
					});

					if (subTasks.length) {
						async.waterfall(subTasks, cb);
					} else {
						// continues to ecoupon drawing, if no big prize can be drawn
						cb();
					}
				};
				const saveCouponGift = function(maxCount, cb) {
				// const saveCouponGift = function(coupons, cb) {
					var subTasks = [];
					console.log('> [Game] start draw ecoupon.', maxCount);
					const makeCoupon = function(coupon) {
						const field = [
							'tcName',
							'scName',
							'tc',
							'tcTC',
							'scTC',
							'tcDescription',
							'scDescription',
							'type',
							'tcUsageTerm',
						    'scUsageTerm',
							'usageTerm',
						];
						var data = {
							// use redeemCode for indexing validation instead of objectid of coupon
							// prevent uncertain duplication for both of collections
							coupon: coupon._id,
							// couponPool: coupon._id,
							gift: coupon.redeemCode,
							expiryDate: coupon.expiryDate,
							type: coupon.type && coupon.type._id,
							isBigPrize: false,
							// inParkFormat: coupon.inParkFormat,
							// tcInParkFormat: coupon.tcInParkFormat,
							// scInParkFormat: coupon.scInParkFormat,
							outParkFormat: coupon.outParkFormat,
							tcOutParkFormat: coupon.tcOutParkFormat,
							scOutParkFormat: coupon.scOutParkFormat,
							// barcodeImage: coupon.barcodeImage,
							emailList: coupon.emailList,
							hotelFormat: coupon.hotelFormat,
							tcHotelFormat: coupon.tcHotelFormat,
							scHotelFormat: coupon.scHotelFormat,
							appWinningInfo: coupon.appWinningInfo,
							tcAppWinningInfo: coupon.tcAppWinningInfo,
							scAppWinningInfo: coupon.scAppWinningInfo,
							webHotelWinningInfo: coupon.webHotelWinningInfo,
							tcWebHotelWinningInfo: coupon.tcWebHotelWinningInfo,
							scWebHotelWinningInfo: coupon.scWebHotelWinningInfo,
							webParkWinningInfo: coupon.webParkWinningInfo,
							tcWebParkWinningInfo: coupon.tcWebParkWinningInfo,
							scWebParkWinningInfo: coupon.scWebParkWinningInfo,
							// tcName: coupon.tcName,
						 //    scName: coupon.scCName,

						    // tc: coupon.tc,
						    // tcTC: coupon.tcTC,
						    // scTC: coupon.scTC,

						    description: coupon.description,
						    // tcDescription: coupon.tcDescription,
						    // scDescription: coupon.scDescription,
							// String(coupon._id),
							name: coupon.name,
							redeemCode: coupon.redeemCode,
							icon: coupon.icon,
							image: coupon.image
						};

						_.forEach(field, function(f) {
							if (coupon[f]) {
								data[f] = coupon[f];
							}
						})
						// console.log(data);
						return data;
					};
					/*
					** ============================================================================
					** [IMPORTANT] shuffle the available coupon count as index list (shuffleCount)
					** _.times turn counter into Number Array
					** _.shuffle radom adjust the Number Array
					** Reduce somewhere to case racing condition and decrease the waiting time
					** to steal the same coupon with any players at the same time
					** Terry Chan
					** 11/01/2019
					** ============================================================================
					*/
					const shuffleCount = _.shuffle(_.times(maxCount));
					const getCouponTasks = function(skip, done) {
						getCoupon(skip, function(err, coupon) {
						
							// [!!!!!!!! Error Case !!!!!!!!]
							// no coupon can be queried
							if (err || coupon.length <= 0 || (!coupon[0])) {
								return done();
							}

							createDrawnLog(makeCoupon(coupon[0]), function(noError, log, coupon) {
								if (noError) {
									return done(true, log, coupon.type);
								}
								return done(null, null, null);
							});
						});
					};

					// console.log(shuffleCount);

					// console.log(shuffleCount.length);
					_.forEach(shuffleCount, function(skip, i) {
						// if (i === 0) {
						// 	subTasks.push(function(done) {
						// 		getCouponTasks(skip, done);
						// 	});
						// } else {
						// 	subTasks.push(function(done) {
						// 		getCouponTasks(skip, done);
						// 	});
						// }
						// console.log('skip: ', skip);
						subTasks.push(function(done) {
							getCouponTasks(skip, done);
						});
						// subTasks.push(function(log, couponType, callback) {
						// 	// console.log(skip);
						// 	getCoupon(skip, function(err, coupon) {
						// 		// console.log(coupon);
						// 		// [!!!!!!!! Error Case !!!!!!!!]
						// 		// no coupon can be queried
						// 		if (err || coupon.length <= 0 || (!coupon[0])) {
						// 			return callback(null);
						// 		}
						// 		console.log('>>> ', callback);
						// 		createDrawnLog(makeCoupon(coupon[0]), function(err, log) {
						// 			console.log('>>> ', err, callback);
						// 			return callback(err, log, coupon[0].type);
						// 		});
						// 	});
						// });
					});

					
					if (subTasks.length) {
						async.waterfall(subTasks, function(noError, log, couponType) {
							// console.log('>>>>>>> ', noError, couponType);
							// [!!!!!!!! Error Case !!!!!!!!]
							// the ecoupon pool must be served the lottery game
							if (!noError) {
								return cb();
							}
							
							if (log) {
								log = log.toObject();
								log.type = couponType;

								return cb(true, log);
							}

							return cb();
							// cb()
						});
					} else {
						// [!!!!!!!! Error Case !!!!!!!!]
						// the ecoupon pool must be served the lottery game
						cb();
					}
				};
				
				// 4.1.1c the get coupon list and draw out coupon list function is mandatary
				// no matter the drew out result if big prize or ecoupon gift
				tasks.push(getCouponCount, saveCouponGift);
				
				/*
					Honor Cheung @ 26/1/2019
				*/
/*
				const nothingPrizeEmpty = function(cb) {
					cb();
				}
*/
				
				// console.log('>>>>> ', drawResult);
				// 4.1.1d If the drew out result is big prize
				// push draw out big prize function to the front of tasks
				if (drawResult === 0) {
					console.log('> [Configuration] The luckydraw result is big prize.');
					// add big prize processor when the draw result is big prize
					tasks.splice(0, 0, saveBigPrize);
				// just in case no coupon can ben drawn and the prize pool has prize
				// the lottery result is ecoupon, then draw big prize to the player
				} else if (drawResult === 1) {
					tasks.push(saveBigPrize);
					
					/*
						Honor Cheung @ 26/1/2019
					*/					
// 					tasks.push(nothingPrizeEmpty);
				}

				// 4.1.2 Chain tasks for drawing out any prize or gift from the pools
				// the DrewLog will be returned
				async.waterfall(tasks, function(noError, drewLog) {
					// drewLog.populate('type', function(err, b) {
					// 	console.log(b);
					// });
					console.log('drewLog: ', drewLog, noError);
					
					// [!!!!!!!! Error Case !!!!!!!!], the ecoupon no coupon
					// the ecoupon pool cannot be served this lottery game
					// terminate the game
					// the worst case
					if (!noError) {
						console.log('>>>>>>>>>>>>> no eCoupon in the pool.');
						error_message = fplocalize(req.language).noLuckdrewPoolQty;
						var error = new Error(error_message);
						error.type = 406;
						// remove the useless eligibility Log
						eligibilityLog.remove();
						return APIRequestHandler.sendDefaultJsonErrResponse(res, error);
					}
					if (!drewLog) {
						eligibilityLog.remove();
						error_message = fplocalize(req.language).errorDraw;
						return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
					}
					// turn it into plain object, instead of Document Object
					eligibilityLog = eligibilityLog.toObject();
					// assign the result drew log to the current eligibility log
					eligibilityLog.resultLog = drewLog;
					// 4.1.2a update eligibility Log and check the racing condition at that moment with non-empty drawResultLog
					updateEligibilityLog(eligibilityLog, function(err, log) {
						// console.log('1', err);
						if (log) {
							console.log('>>>>>>> use the previous result log instead.');
							// use the current eligibility log instead, no need a new eligibility log created belows
							eligibilityLog.resultLog = log;
							// 4.1.2b async process, remove the current draw log, no need to wait
							removeEligibilityLog(eligibilityLog);
							removeNewDrawnLog(drewLog);
						}
						
						// console.log('>>>>>> ', drewLog);
						// assign the eligibility log to the inital history object created belows
						historyData.resultLog = eligibilityLog;
						// console.log('>1 ', historyData);
						
						// store store result type
						historyData.resultType = '0';
						// use redeem code as a reference no
						if (!drewLog.isBigPrize) {
							historyData.resultType = '1';
							historyData.referenceNo = drewLog.redeemCode;
						} else if (drewLog.prizeCoupon) {
							historyData.referenceNo = drewLog.redeemCode;
						}
						historyData.name = drewLog.name;
						historyData.tcName = drewLog.tcName;
						historyData.scName = drewLog.scName;

						// console.log(historyData);
					// console.log('>>>>>>> 1');
						//  return;
						// 4.1.3 update initial history with incompleted status
						// If the next player use the same pass no to join the game
						// the game result will be took from this history with eligibility log and drew result log
						saveInitialLotteryHistory(historyData, function(err, history) {
							// console.log(err);
							if (err) {
								// console.log(err, history);
								// remove Drawlog, for data inconsistence concern
								removeNewDrawnLog(drewLog);
								removeEligibilityLog(eligibilityLog);
								error_message = fplocalize(req.language).errorDraw;
								return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
							}
							
							// 4.1.3a update hold status from the target pool
							// the next player cannot use corresponding prize or gift from now
							holdTheTargetGift(eligibilityLog.resultLog, function(err, result) {
								// console.log('124124', err);
								// fake random pick first item since the list is already mixed before
								historyData.id = history._id;
								// historyData.emailTpl = emailLib.getDrawResultEmailFormat(historyData)
								// 3.1c finally, save the completed luckydraw history
								formatHistory(req, historyData, function(err, response) {
									return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, response);
								});
							});
						});
						// console.log(eligibilityLog);
					});
					// console.log(noError, drewLog);
				});
			});
		// 1.1 if the user is already drew out the prize result before, 
		//     return that result to app or web, no need to draw it again
		} else {
			console.log('>>>>>>> use the current result log instead, then no need to do the lottery process');
			// console.log(eligibilityLog);
			if (req.deviceId && req.deviceId !== eligibilityLog.deviceId && !req.bypassPaired) {
				console.log('>>>>>>> [Danger!!!!!] many device use the same token to play this game.');
				error_message = fplocalize(req.language).duplicatedLotteryToday;
				return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
			}
			const tasks = [
				function(cb) {
					LotteryHistoryController.getHistoryByGameLog(eligibilityLog._id, function(err, history) {
						
						if (err) {
							return cb(err);
						} else if (history && !history.resultLog) {
							console.log('> getHistoryByGameLog: ', history);
							return cb(true);
						}
						cb(null, history);
					});
				},
				function(history, cb) {
					LotteryDrawnLogController.getLog(eligibilityLog.resultLog, function(err, drewLog) {
						if (drewLog) {
							history.resultLog.resultLog = drewLog;
						}
						cb(err, history);
					});
				}
			];

			async.waterfall(tasks, function(err, history) {
				if (err) {
					error_message = fplocalize(req.language).errorDraw;
					return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
				}
				// formatHistory(req, history);
				formatHistory(req, history, function(err, response) {
					return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, response);
				});
			});
		}
	});

};

GameHandler.prototype.couponList = function(req, res) {
	const tasks = [
		function(cb) {
			LotteryHistoryController.getHistoriesByDeviceIdAndResult(req.body.deviceId, '1', function(err, histories) {
				if (err) {
					return cb(err);
				} 
				// console.log(histories.length);
				cb(null, histories);
			});
		},
		function(histories, cb) {
			const subTasks = _.map(histories, function(history, i) {
				// console.log(history);
				return function(cb) {
					LotteryDrawnLogController.getLog(history.resultLog.resultLog, function(err, drewLog) {
						
						if (drewLog) {
							histories[i].resultLog.resultLog = drewLog;
						}
						cb(err, histories);
					});
				}
			});
			async.parallel(subTasks, function(err, result) {
				cb(err, histories);
			});
		}
	];

	async.waterfall(tasks, function(err, histories) {
		if (err) {
			error_message = fplocalize(req.language).errorDraw;
			return APIRequestHandler.sendDefaultJsonErrResponse(res, new Error(error_message));
		}
		// console.log(histories);
		// formatHistory(req, history);
		return APIRequestHandler.sendDefaultJsonSuccessResponseWithResData(res, formatHistoryCoupon(req, histories));
	});
}



exports = module.exports = new GameHandler();
