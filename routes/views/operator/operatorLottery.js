const keystone = require('keystone');
const gua = require('universal-analytics');
var Config = require(global.__base + '/config/Config');
var conf = new Config();

const visitorForGA = gua(conf.gaTrackId);

exports = module.exports = function(req, res) {
	// async process
	const trackUrl = conf.gaOperatorLuckyDrawUrl;
	// conf.absolutePath + '' + conf.gaOperatorLuckyDrawUrl;
	visitorForGA.pageview(trackUrl, function(err) {
		console.log('> Log GA Track Operator Pageview: ', trackUrl);
		if (err) {
			console.log(err);
		}
		console.log(err);
	});

	var view = new keystone.View(req, res);
	var locals = res.locals;
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.title = 'Frenzy Instant Win';
	if (req.language === 'tc') {
		locals.title  = '「玩。住」賞贏大獎';
	} else if (req.language === 'sc') {
		locals.title  = '「玩。住」赏赢大奖';
	}
	locals.cookies = conf.cookiesMap;
	locals.appToken = req.staff.loginToken;
	locals.deviceId = req.deviceId;
	locals.countries = req.countryList || [];
	locals.setting = req.setting || {};
	locals.userType = 'op';
	locals.initialStep = !!(req.query.type && req.query.type === 'rule');
	locals.language = req.query.lang;
	locals.promotionNo = conf.promotionNo;
	locals.parkError = req.parkError ? req.parkError : null;
	locals.cookieOptions = keystone.get('cookie frontend language options');
	locals.passNo = req.passNo ? req.passNo : '';
	locals.landingUrl = conf.absolutePath + 'web/operator/luckyDraw';
	
	locals.action = conf.absolutePath + 'api/v2/operator/play';
	// locals.signinUrl = conf.absolutePath + 'web/hotel/signin';
	locals.signoutUrl = conf.absolutePath + 'api/v2/operator/singout';
	locals.user = req.staff;
	locals.submitLotteryUrl = conf.absolutePath + 'api/v2/operator/play';
	locals.finishLotteryUrl = conf.absolutePath + 'api/v2/operator/end';
	locals.query = req.query;
	
	// Render the view
	view.render('game/operator/index');
	
};
