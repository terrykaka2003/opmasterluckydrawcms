const keystone = require('keystone');
var Config = require(global.__base + '/config/Config');
const Utility = require(global.__base + "/helper/Utility");
var conf = new Config();

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.title = 'Frenzy Instant Win';
	if (req.language === 'tc') {
		locals.title  = '「玩 · 住」賞贏大獎';
	} else if (req.language === 'sc') {
		locals.title  = '"玩 · 住" 赏赢大奖';
	}
	locals.cookies = conf.cookiesMap;
	locals.countries = req.countryList || [];
	locals.setting = req.setting || {};
	locals.initialStep = !!(req.query.type && req.query.type === 'rule');
	locals.language = req.query.lang ? Utility.convertLangCode(req.query.lang) : null;
	locals.deviceId = req.deviceId;
	locals.parkError = req.parkError ? req.parkError : null;
	locals.promotionNo = conf.promotionNo;
	locals.cookieOptions = keystone.get('cookie frontend language options');
	locals.passNo = req.passNo;
	locals.tackId = conf.gaTrackId;
	locals.bypassview = req.byPassRegularWebview ? true : false;
	locals.landingUrl = conf.absolutePath + 'app/newyears/lottery';
	
	locals.action = conf.absolutePath + 'api/v2/app/play';
	// locals.signinUrl = conf.absolutePath + 'web/hotel/signin';
	// locals.signoutUrl = conf.absolutePath + 'api/v2/app/singout';
	locals.ticketCheckUrl = conf.absolutePath + 'api/v2/ticket/check';
	locals.submitLotteryUrl = conf.absolutePath + 'api/v2/app/play';
	locals.finishLotteryUrl = conf.absolutePath + 'api/v2/app/end';
	locals.query = req.query;
	
	// Render the view
	view.render('game/app/index');
	
};
