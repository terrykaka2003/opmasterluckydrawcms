const keystone = require('keystone');
const gua = require('universal-analytics');
const _ = require('lodash');
var Config = require(global.__base + '/config/Config');
var conf = new Config();

const visitorForGA = gua(conf.gaTrackId);

exports = module.exports = function(req, res) {
	// async process
	const trackUrl = conf.gaHotelLuckyDrawUrl;
	// conf.absolutePath + '' + conf.gaHotelLuckyDrawUrl;
	visitorForGA.pageview(trackUrl, function(err) {
		console.log('> Log GA Track Hotel Pageview: ', trackUrl);
		if (err) {
			console.log(err);
		}
		console.log(err);
	});

	var view = new keystone.View(req, res);
	var locals = res.locals;
	locals.title = 'Frenzy Instant Win';
	if (req.language === 'tc') {
		locals.title  = '「玩。住」賞贏大獎';
	} else if (req.language === 'sc') {
		locals.title  = '「玩。住」赏赢大奖';
	}
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.title = 'Hotel Luckydraw Game';
	locals.cookies = conf.cookiesMap;
	locals.appToken = req.staff.loginToken;
	locals.countries = req.countryList || [];
	locals.setting = req.setting || {};
	locals.userType = 'ht';
	locals.promotionNo = conf.promotionNo;
	locals.cookieOptions = keystone.get('cookie frontend language options');
	locals.landingUrl = conf.absolutePath + 'web/hotel/luckyDraw';
	locals.action = conf.absolutePath + 'api/v2/staff/play';
	// locals.signinUrl = conf.absolutePath + 'web/hotel/signin';
	locals.signoutUrl = conf.absolutePath + 'api/v2/staff/singout';
	locals.user = req.staff;
	locals.submitLotteryUrl = conf.absolutePath + 'api/v2/staff/play';
	locals.finishLotteryUrl = conf.absolutePath + 'api/v2/staff/end';

	// Render the view
	view.render('game/hotel/index');
	
};
