const keystone = require('keystone');
var Config = require(global.__base + '/config/Config');
var conf = new Config();

exports = module.exports = function(req, res) {
	// if the user logined the account
	if (req.staff) {
		return res.redirect('/web/hotel/luckyDraw');
	}
	var view = new keystone.View(req, res);
	var locals = res.locals;
	
	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.title = 'Hotel Staff Login';
	locals.cookies = conf.cookiesMap.hotel;
	locals.cookiesLang = conf.cookiesMap.hotelLang;
	locals.cookieOptions = keystone.get('cookie frontend language options');
	locals.type = 'ht';
	locals.action = conf.absolutePath + 'api/v2/signin/staff';
	locals.singoutUrl = conf.absolutePath + 'api/v2/staff/singout';
	locals.landingUrl = conf.absolutePath + 'web/hotel/luckyDraw';
	
	// Render the view
	view.render('game/signin');
	
};
