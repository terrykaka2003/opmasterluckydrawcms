/* 
* Staff / Operator Main Web Flows
* @Author: Terry Chan
* @Date:   2019-01-07
* @Last Modified by:   Terry Chan
* @Last Modified time: 2019-01-10
*/
$_DOC = $("html, body");
$_WINDOW = $(window);
$_DOCU = $(document);

(function ($) {
    // en - congrat / 2 - 10
    // tc & sc - congrat / 2 + 30

    // tc & sc
    // yellow bg: height / 2 - congrat - bg
    // character: height / 2 - character height + 30
    // text: height / 2 - 20
    "use strict";
    const emailFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const numberOnly = /^\d+$/;
    const animationPannelProportion = 1.531;
    const resizeOff = 1024;

    // dom selector
    const loginBtn = '#g-login-btn';
    const logoutBtn = '.g-logout-btn';
    const submitBtn = '#g-submit-btn';
    const rulBackButton = '.g-rule-btn';
    const languageSelector = '.lang select';
    const terms = '.terms-popup';
    const termsContent = '.terms-popup .content';
    const termsButton = '.terms a.tc';
    const privacyButton = '.terms a.privacy';
    const ruleButton = '.terms a.rules';
    const scanOtherButton = '.g-congrad-scan-other';
    const checkEcouponButton = '.g-congrad-check-ecoupon';
    const appResultBg = 'img.congrat';
    const personalButton = '.terms a.personal';
    const markingAgree = '.g-terms-marking-label';
    const popCancelButton = '.popup .cancel';
    const popupMask = '.popup-mask';
    const playedMessage = '#played-draw';
    const resultPage = '#page-draw, .page-draw-result';
    const animationResultContent = '.page-draw-result .content .g-content-wrapper';
    const animationResultCharacter = '.page-draw-result .getWinner';
    const gameTitle = '.game-title';
    const authMain = '.g-h-main-container';
    const nameInput = '.g-name-input';
    const telSelection = '.telephone select';
    const unavailableDrawMessage = '#unavailable-draw';
    const loading = '.loader';
    const form = '#g-h-form, .g-h-form';
    // const mainContentBlock = '.content-block';
    const characterImage = '.g-game-result-character';
    const prizeDescription = '.g-prize-desc';
    const resultReminder = '.g-reminder';
    const headerTitle = '.g-header-title-label';
    const resultRemark = '.g-remark';
    const resultConfirm = '.g-again-btn';
    const promotionCode = '.g-promotion-value';
    const againButton = '.g-again-btn';
    const loginForm = '.g-h-login';
    const ruleContentBlock = '#page-draw-rule';
    const hotelResultContainer = '#page-draw .content';
    const congratulationContent = '.g-congrad-body';
    const hotelResultBg = '.content .g-congrad-head';
    const ruleContentTitle = '.g-rule-title';
    const ruleCameraBtn = '.g-native-camera-btn';
    const ruleStatement = '.g-rule-statement';
    const animationPannelBg = '.white-bg';
    // const scanOtherButton = '.g-congrad-scan-other';
    // const checkEcouponButton = '.g-congrad-check-ecoupon';
    const animationRealPannel = '.slot-wrapper .jSlots-wrapper';
    const animationRealPannelItem = '.slot-wrapper .jSlots-wrapper li';
    const startButton = '#playBtn img';
    const stopButton = '#stopBtn img';
    // const animationTitleImage = '.g-an-title';
    const backBtn = '.g-native-banhome-btn';
    // const ul["data-group='"+ Companies + "'"];
    // parameters
    const authorizationPrefix = 'Bearer';
    const defaultLang = 'en';
    // UI localization mapping
    const uiLocalizationMapping = [
        {
            selector: rulBackButton,
            type: 'html',
            key: 'ruleTitleSmall',
        },
        {
            selector: loginBtn,
            type: 'html',
            key: 'loginForm.login',
        },
        {
            selector: logoutBtn,
            type: 'html',
            key: 'logout',
        },
        {
            selector: '.g-password-label',
            type: 'html',
            key: 'loginForm.password'
        },
        {
            selector: markingAgree,
            type: 'html',
            key: 'drawForm.marketing'
        },
        // {
        //     selector: checkEcouponButton,
        //     type: 'html',
        //     key: 'checkMyCoupon'
        // },
        // {
        //     selector: scanOtherButton,
        //     type: 'html',
        //     key: 'scanOther'
        // },
        {
            selector: '.g-username-label',
            type: 'html',
            key: 'loginForm.username'
        },
        {
            selector: '.g-required-label',
            type: 'html',
            key: 'error.required'
        },
        {
            selector: '.g-name-label',
            type: 'html',
            key: 'drawForm.name'
        },
        {
            selector: '.g-name-input',
            type: 'placeholder',
            key: 'drawForm.namePlaceholder'
        },
        {
            selector: '.g-id-label',
            type: 'html',
            key: 'drawForm.id'
        },
        {
            selector: '.g-email-label',
            type: 'html',
            key: 'drawForm.email'
        },
        {
            selector: '.g-email-input',
            type: 'placeholder',
            key: 'drawForm.emailPlaceholder'
        },
        {
            selector: '.g-confirmEmail-label',
            type: 'html',
            key: 'drawForm.confirmEmail'
        },
        {
            selector: '.g-invoiceNo-label',
            type: 'html',
            key: 'drawForm.invoiceNo'
        },
        {
            selector: '.g-ticketNo-label',
            type: 'html',
            key: 'drawForm.ticketNo'
        },
        {
            selector: '.g-telephone-label',
            type: 'html',
            key: 'drawForm.telephone'
        },
        {
            selector: '.g-terms-label',
            type: 'html',
            key: 'drawForm.terms'
        },
        {
            selector: submitBtn,
            type: 'html',
            key: 'drawForm.send'
        },
        {
            selector: resultConfirm,
            type: 'html',
            key: 'ok'
        },
        {
            selector: '.g-promotion-label',
            type: 'html',
            key: 'promotionCode'
        },
        {
            selector: ruleContentTitle,
            type: 'html',
            key: 'ruleTitle'
        },
        {
            selector: ruleCameraBtn,
            type: 'html',
            key: 'ruleBtn'
        },
        {
            selector: '.g-rule-terms',
            type: 'html',
            key: 'ruleTc'
        },
        {
            selector: '.g-rule-remark',
            type: 'html',
            key: 'ruleRemark'
        },
        {
            selector: backBtn,
            type: 'html',
            key: 'backHome'
        },
    ];
    // self properties
    var _currentLanguage;
    var _currentPack;
    var _loginToken;
    var _processing = false;
    var _step = 0;
    var _drawing = false;
    // const scanOtherButton = $('button', { class: '.g-congrad-scan-other' });
    // const checkEcouponButton = $('button', { class: '.g-congrad-check-ecoupon' });

    // var _gameResult;
    /* 
    * ------------------------------------------------------
    * Start of Helper Functions
    * ------------------------------------------------------
    */

    const step = {
        // rule page
        initial: function() {
            $(ruleContentBlock).show();
            $(authMain).hide();
            $(form).hide();
            $(resultPage).hide();
            $_DOC.animate({ scrollTop: 0 }, "slow");
            _step = 0;
        },
        first: function() {
            $(ruleContentBlock).hide();
            $(authMain).show();
            $(form).show();
            $(resultPage).hide();
            $('#playBtn').addClass('show');
            $('#stopBtn').removeClass('show');
            $_DOC.animate({ scrollTop: 0 }, "slow");
            _step = 1;
        },
        // animation page
        third: function() {
            $(ruleContentBlock).hide();
            $(authMain).hide();
            $(form).hide();
            $(resultPage).show();
            $_DOC.animate({ scrollTop: 0 }, "slow");
            _step = 2;
            // animationResize();
        },
        last: function() {
            $(ruleContentBlock).hide();
            $(authMain).hide();
            $(form).hide();
            $(resultPage).show();
            $_DOC.animate({ scrollTop: 0 }, "slow");
            _step = 3;
        }
    }

    const messageBox = {
        show: function(message) {
            if (keystoneConfig.native && keystoneConfig.inPark) {
                $_DOC.animate({ scrollTop: 0 }, "fast");
                $(playedMessage).html(message).show();
            } else {
                alert(message);
            }
        },
        hide: function() {
            $(playedMessage).html('').hide();
        }
    }

    const popup = {
        open: function() {
            $(popupMask).show();
            $(terms).show();
        },
        close: function() {
            $(popupMask).hide();
            $(terms).hide();
        }
    };

    const processor = {
        doing: function() {
            $(loading).show();
            $(form).hide();
            _processing = true;
        },
        // if fail
        stop: function() {
            $(loading).hide();
            $(form).show();
            _processing = false;
        },
        finished: function() {
            $(loading).hide();
            _processing = false;
        }
    };

    const updatePromotion = function() {
        // promotionCode
        $(promotionCode).html(keystoneConfig.promotionCode);
    }


    const switchStep = function() {
        const accountType = keystoneConfig.userType || (keystoneConfig.staff && keystoneConfig.staff.type);
        switch (accountType) {
            // operator flow
            // case 'op':
            //     if (keystoneConfig.initialStep) {
            //         step.initial();
            //     } else {
            //         step.first();
            //     }
            //     break;
            // // hotel staff flow
            case 'ht':
                updateCurrentLanguage(function() {
                    step.first();
                });
                break;
            // operator and app flow
            default:
                updateCurrentLanguage(function() {
                    if (keystoneConfig.initialStep && !keystoneConfig.parkError) {
                        step.initial();
                    } else {
                        step.first();
                    }
                });
                
                // TODO
        }
    }

    const updateResultBg = function() {
        
        $(hotelResultBg).prop('src', `/img/congrat_r_${_currentLanguage}.png`);
        $(appResultBg).prop('src', `/img/congrat_r_${_currentLanguage}.png`);
        if (_currentLanguage === 'en' && !$(hotelResultContainer).hasClass('en')) {
            $(hotelResultContainer).addClass('en');
        } else {
            $(hotelResultContainer).removeClass('en');
        }
        if (_currentLanguage === 'en') {
            if (!$('#popup').hasClass('en')) {
                $('#popup').addClass('en');
                // if ($_WIN.height() < 500) {
                //     $(appResultBg).prop('src', `/img/congrat_r_en_small.png`);
                // }
            }
        } else {
            // const half = Math.floor($_WIN.height() / 2);
            // $(animationResultCharacter).find('img').on('laod', function(){
            //     console.log($(animationResultCharacter).find('img')[0].height());
            // })
            // const characterHeight = $(animationResultCharacter).find('img').height();
            // console.log($(animationResultCharacter).find('img'));
            // $(animationResultCharacter).css({
            //     top: (half - characterHeight - 54) + 'px',
            // })
            // const congradBgHalf = Math.floor($(appResultBg).height());
            // console.log(half, $('img.congrat').height());
            $('#popup').removeClass('en');
        }
        
       
    }

    const updateGameTitle = function() {
        $(gameTitle).css({
            backgroundImage: `url('/img/lottery_title_${_currentLanguage}.png')`,
        });
        $(headerTitle).prop('src', `/img/short_title_${_currentLanguage}.png`);
    }

    const updateButtonLabel = function() {
        $(startButton).prop('src', `/img/start_${_currentLanguage}.png`);
        $(stopButton).prop('src', `/img/stop_${_currentLanguage}.png`);
    };

    const updateGameRule = function() {
        var rule = '';
        if (keystoneConfig.setting) {
            rule = keystoneConfig.setting.rule;
            if (_currentLanguage === 'tc') {
                rule = keystoneConfig.setting.tcRule || rule;
            } else if (_currentLanguage === 'sc') {
                rule = keystoneConfig.setting.scRule || rule;
            }
        }
        $(ruleStatement).html(rule);
        $(termsButton).click(function() {
            popup.open();
        });
    };

    const updateTermConditions = function() {
        $(termsButton).click(function() {
            var tc = '';
            if (keystoneConfig.setting) {
                tc = keystoneConfig.setting.tc;
                if (_currentLanguage === 'tc') {
                    tc = keystoneConfig.setting.tcTC || tc;
                } else if (_currentLanguage === 'sc') {
                    tc = keystoneConfig.setting.scTC || tc;
                }
            }
            $(termsContent).animate({ scrollTop: 0 }, "fast");
            $(termsContent).html(tc);
            popup.open();
        });
        $(privacyButton).click(function() {
            var privacy = '';
            if (keystoneConfig.setting) {
                privacy = keystoneConfig.setting.privacy;
                if (_currentLanguage === 'tc') {
                    privacy = keystoneConfig.setting.tcPrivacy || privacy;
                } else if (_currentLanguage === 'sc') {
                    privacy = keystoneConfig.setting.scPrivacy || privacy;
                }
            }
            $(termsContent).animate({ scrollTop: 0 }, "fast");
            $(termsContent).html(privacy);
            popup.open();
        });
        $(ruleButton).click(function() {
            var parkRule = '';
            if (keystoneConfig.setting) {
                parkRule = keystoneConfig.setting.parkRule;
                if (_currentLanguage === 'tc') {
                    parkRule = keystoneConfig.setting.tcParkRule|| parkRule;
                } else if (_currentLanguage === 'sc') {
                    parkRule = keystoneConfig.setting.scParkRule || parkRule;
                }
            }
            $(termsContent).animate({ scrollTop: 0 }, "fast");
            $(termsContent).html(parkRule);
            popup.open();
        });
        $(personalButton).click(function() {
            var personal = '';
            if (keystoneConfig.setting) {
                personal = keystoneConfig.setting.personal;
                if (_currentLanguage === 'tc') {
                    personal = keystoneConfig.setting.tcPersonal || personal;
                } else if (_currentLanguage === 'sc') {
                    personal = keystoneConfig.setting.scPersonal || personal;
                }
            }
            $(termsContent).animate({ scrollTop: 0 }, "fast");
            $(termsContent).html(personal);
            popup.open();
        });
    };

    const updateCurrentLanguage = function(cb) {
        _currentPack = LOCALIZATION[keystoneConfig.language || _currentLanguage];
        $.writeCookies(keystoneConfig.langCookies, _currentLanguage);
        $.localization(_currentPack, uiLocalizationMapping, $.evalPath, function() {
            // remove all of error, if shown before
            $(form).find('.error').removeClass('error');
            $(form).find('.error-box').remove();
            // set selected language in selection box
            $(languageSelector).val(_currentLanguage);
            // unbind the term link for poping up 
            updateTermConditions();
            // localize game title
            updateGameTitle();
            updateGameRule();
            updateButtonLabel();
            updateResultBg();
            if (cb) {
                cb();
            }
        });
    };

    // by fung
    const isValidLuckyDrawForm = function() {
        const formElem = $(form);
        const nameElem = formElem.find('[name="name"]');
        const idElem = formElem.find('[name="card"]');
        const emailElem = formElem.find('[name="email"]');
        const confirmEmailElem = formElem.find('[name="confirmEmail"]');
        const invoiceNoElem = formElem.find('[name="invoiceNo"]');
        const ticketNoElem = formElem.find('[name="ticketNo"]');
        const telephoneElem = formElem.find('[name="phone"]').parent('.telephone');
        const termsElem = formElem.find('.terms.required');
        
        const name = nameElem.val();
        const id = idElem.val();
        const email = emailElem.val();
        const confirmEmail = confirmEmailElem.val();
        const invoiceNo = invoiceNoElem.val();
        const ticketNo = ticketNoElem.val();
        const telephone = telephoneElem.find('input').val();
        const terms = termsElem.find('[name="terms"]').is(':checked');

        let invalidList = [];
        formElem.find('.error').removeClass('error');
        formElem.find('.error-box').remove();

        if (!name) {
            invalidList.push({ elem: nameElem, error: 'required' });
        }
        if (!id) {
            invalidList.push({ elem: idElem, error: 'required' });
        } else if (id.length !== 4) {
            invalidList.push({ elem: idElem, error: 'incorrectIdLength' });
        } else if (!numberOnly.test(id)) {
            invalidList.push({ elem: idElem, error: 'incorrectIdCharacter' });
        }

        if (!email) {
            invalidList.push({ elem: emailElem, error: 'required' });
        } else if (!emailFormat.test(email)) {
            invalidList.push({ elem: emailElem, error: 'invalidEmail' });
        }

        if (!confirmEmail) {
            invalidList.push({ elem: confirmEmailElem, error: 'required' });
        } else if (!(confirmEmail === email)) {
            invalidList.push({ elem: confirmEmailElem, error: 'incorrectConfirmEmail' });
        }

        // if (!invoiceNo) {
        //     invalidList.push({ elem: invoiceNoElem, error: 'required' });
        // }

        // in park for passno        
        if (keystoneConfig.inPark && !ticketNo) {
            invalidList.push({ elem: ticketNoElem, error: 'required' });
        } else if (keystoneConfig.inPark && ticketNo.length > 36) {
            invalidList.push({ elem: ticketNoElem, error: 'incorrectIdLength' });
        } else if (!keystoneConfig.inPark && !invoiceNo) {
            invalidList.push({ elem: invoiceNoElem, error: 'required' });
        }
        // console.log(keystoneConfig.inPark);

        if (!telephone) {
            invalidList.push({ elem: telephoneElem, error: 'required' });
        } else if (telephone.length < 8 || telephone.length > 15) {
            invalidList.push({ elem: telephoneElem, error: 'incorrectIdLength' });
        } else if (!numberOnly.test(telephone)) {
            invalidList.push({ elem: telephoneElem, error: 'incorrectIdCharacter' });
        }

        if (!terms) {
            invalidList.push({ elem: termsElem, error: 'requiredCheckbox' });
        }
        
        const isValid = !invalidList.length;
        var errorMessage = '';
        if (invalidList.length) {
            $.each(invalidList, function(i, item) {
                errorMessage = _currentPack.error[item.error];
                item.elem.addClass('error');
                item.elem.after(
                    `<div class="error-box" data-code="${item.error}">${errorMessage}</div>`
                )
            })
        }
        const countryCode = $('.telephone select').val(); 
        $('.error-box').eq(0).animate({ scrollTop: 0 }, "slow");
        return isValid;
    };

    const kickLogout = function() {
        // clear all of cookies belonging to the account
        $.removeCookies(keystoneConfig.cookies, keystoneConfig.cookiesConfig);
        $.removeCookies(keystoneConfig.langCookies, keystoneConfig.cookiesConfig);
        // reload the and force the middleware of request
        window.location.reload();
    };

    /*
    ** reset all of steps and reload the window to leave the language paramter only
    ** Terry Chan
    ** 09/01/2019
    */
    const reloadAllSteps = function() {
        var param = {};
        // only leave lang parameter
        if (keystoneConfig.query) {
            if (keystoneConfig.query.lang) {
                param.lang = keystoneConfig.query.lang
            }
        }
        const url = keystoneConfig.landingUrl + '?t='+Math.random()+'&type=rule';
        window.location.href = url + '&' + $.param(param);
    };

    // restriction account if langCookies set
    const accountRestriction = function() {
        _loginToken = keystoneConfig.appToken;
        // console.log(keystoneConfig.forceCheck,  _loginToken);
        // get the loginToken if there is, force to landing page if logined
        if (keystoneConfig.cookies && !_loginToken) {
            _loginToken = $.readCookies(keystoneConfig.cookies, keystoneConfig.cookiesConfig);
        }

        $(loginBtn).prop('disabled', true);
        if (keystoneConfig.forceCheck && _loginToken) {
            // console.log(_loginToken);
            // against history back
            // window.location.replace(keystoneConfig.landingUrl);
            // window.location.href = keystoneConfig.landingUrl;
            reloadAllSteps();
        } else {
            $(loginForm).animate({ opacity: 1 }, 2000)
            $(loginBtn).prop('disabled', false);

        }
    };

    // handle for common form no matter it is signin form or submit form for the game
    const commonServerRequest = function(header, callback) {
        if (!_processing) {
            var data = $(form).serializeObject();
            const api = $(form).prop('action');
            const method = $(form).prop('method') || 'GET';
            data = Object.assign({}, data, { lang: _currentLanguage });
            var configs = {
                url: api,
                method: method,
                data: data,
            };
            if (header) {
                configs = Object.assign({}, configs, {
                    headers: {
                        Authorization: 'Bearer ' + _loginToken
                    },
                });
            }
            
            processor.doing();

            $.serverRequest(configs, function(response) {
                const status = response.status;
                // authorization error, force logout and redirect to signin page
                if (!status || status === '0') {
                    // no matter login unauthorized or game period is closed or game setting not found
                    // force to logout the account or callback anything
                    if ( response.errorType === 403 || response.errorType === 404 ) {
                        alert(response.message);
                        return kickLogout();
                    } else {
                        processor.stop();
                    }
                } else {
                    processor.finished();
                }
                return callback(response);
            });
        }
    };
    const animationContentResize = function() {
        // var animalHeight = $('.getWinner img').outerHeight() + 10 + 60;
        // if ($_WINDOW.width() >= 768) {
        //     animalHeight += 80;
        // }
        // if ($_WINDOW.width() >= 900) {
        //     animalHeight += 95;
        // }
        // $(animationResultContent).css({
        //     top: animalHeight + 'px',
        // }).animate({ opacity: 1 }, 1000);
    }
    const animationResize = function() {
        var windowsize = $_WINDOW.width();
        
        if (windowsize <= resizeOff) {
            // console.log(windowsize * 0.9);
            windowsize *= 0.88;
            // animation view
            if (_step === 2) {
                const adjustedHeight = windowsize / animationPannelProportion;
                const style = {
                    height: (adjustedHeight - 10) + 'px',
                    width: windowsize + 'px',
                };
                $(animationPannelBg).css(style);
                $(animationRealPannel).css(Object.assign({}, style, {
                    height: (adjustedHeight - 45) + 'px',
                }));
                $(animationRealPannelItem).css({
                    height: (adjustedHeight - 45) + 'px',
                })
                // console.log(style);
            }
        } else {
            $(animationPannelBg).css({});
            $(animationRealPannel).css({});
        }
    }
    const renderFinalResult = function(data) {
        // const drawResult = data.drawResult;
        var infoTemplate = data.winningInfo;
        var prizeName = data.prize || '';
        var reminder = _currentPack.reminder;
        var remark = _currentPack.remark;
        var descText = _currentPack.giftDesc;
        const inPark = data.inPark;
        const referenceNo = data.referenceNo;
        if (data.coupon) {
            prizeName = data.coupon.title || '';
        }
        if (!infoTemplate) {
            if (data.coupon) {
                // prizeName = data.coupon.title || '';
                if (inPark) {
                    remark = _currentPack.inParkRemark;
                    descText = _currentPack.inParkGiftDesc;
                }
            } else {
                // prizeName = data.prize || '';
                remark = _currentPack.prizeRemark;
                descText = _currentPack.bigGiftDesc;
            }
            // .replace('{{character}}', data.characterName)
            descText = descText.replace('{{gift}}', prizeName)
                    .replace('{{number}}', referenceNo);
            $(prizeDescription).html(descText);
            $(resultRemark).html(remark);
            $(resultReminder).html(reminder);
        }　else {
            infoTemplate = infoTemplate.replace('[GiftName]', prizeName)
                    .replace('[ReferenceNo]', referenceNo);
            infoTemplate = $.parseHTML(infoTemplate);

            $(congratulationContent).empty().append(infoTemplate);
            // <h3 class="desc g-prize-desc">
            // </h3>
            // <div class="g-congrad-body-reminder remark g-remark">
            // </div>
            // <div class="g-congrad-button">
            //     <button class="g-congrad-scan-other">

            //     </button>
            //     <button class="g-congrad-check-ecoupon">

            //     </button>
            // </div>
            // <div class="g-congrad-body-remark reminder g-reminder">
            
            // </div>
            // remove the text and place the background image instead
            $(scanOtherButton).empty()
                .css({ backgroundImage: 'url(/img/button_orange_'+_currentLanguage+'.png)' });
            $(checkEcouponButton).empty()
                .css({ backgroundImage: `url(/img/button_blue_${_currentLanguage}.png)` });
            
            $(scanOtherButton).click(function(e) {
                e.preventDefault();
                openTicketScanner(getNativeCameraCallData());
            });
            $(checkEcouponButton).click(function(e) {
                e.preventDefault();
                gotoMyEcoupon();
            });
        }
        // const half = Math.floor($_WIN.height() / 2);
        // const height = $(this).height();
        // $('#popup:not(.en) .g-content-wrapper').css({
        //     top: (half - 10) + 'px',
        // });
    }
    const finalizeDrawResult = function(data, cb) {
        if (!_processing) {
            var options = {
                url: keystoneConfig.finishLotteryUrl,
                method: 'POST',
                data: {
                    id: data.id,
                },
            };
            if (_loginToken) {
                options = Object.assign(options, {
                    headers: {
                        Authorization: 'Bearer ' + _loginToken,
                    },
                });
            }
            $.serverRequest(options, function(response) {
                const status = response.status;
                // native call if the result is ecoupon and inpark native app
                if (!!status && data.coupon 
                    && keystoneConfig.native
                    && keystoneConfig.inPark) {
                    window.console.warn('> write native coupon', data.coupon);
                    // native call
                    addEcoupon(data.coupon);
                    $(checkEcouponButton).show();
                } else {
                    $(checkEcouponButton).hide();
                }
                updateCurrentLanguage();
                if (cb) {
                    cb(!status ? response.message : null);
                }
                // } else {
                //     alert(response.message);                
                // }
            });
        }
    }
    // by Nic Wong
    const initDrawAnimation = function(data) {
        const num = data.drawResultIndex;
        const characteres = data.characteres;
        if (characteres.length) {
            characteres.forEach(function(character) {
                $('.slot').append(`<li><img src=${character} /></li>`);
            });
            $('.getWinner img').prop('src', characteres[num]);
            // $('.getWinner img').on('load', function() {
            //     const half = Math.floor($_WIN.height() / 2);
            //     const height = $(this).height();
            //     $('#popup:not(.en) .getWinner').css({
            //         top: (half - height - 54) + 'px',
            //     });
            // })
            
        }
        processor.doing();
        $('.slot').imagesLoaded(() => {
            $('.slot').jSlots({
                spinner : '#playBtn',
                stopSpinner: '#stopBtn',
                number: 1,
                time : 7000,
                loop: -1,
                endNumber: num + 1,
                // desktop version is static width
                isStaticWidth: $_WINDOW.width > 1024,
                onStart: (f) => {
                    finalizeDrawResult(data, function(err) {
                        // if (!err) {
                        //     $('body')
                        // }
                    });
                    $('#playBtn').removeClass('show');
                    $('#stopBtn').addClass('show');
                },
                onStop: (f) => {
                    // fast workarround for desktop ux 
                    // if (!keystoneConfig.native) {
                    //     setTimeout(function() {
                    //         animation();
                    //     }, 2000);
                    // }
                },
                onEnd: (f) => {
                    // if (keystoneConfig.native) {
                    animation();
                    // }
                    // $('.slot').stop(true);
                    // $('.getWinner').html(items[f-1]);
                },
            });
            animationResize();
            processor.finished();
            $('.wrapper').addClass('loaded');
        });
    };

    // by Nic Wong, Terry Chan
    // const resetAnimation = function() {
    //     // tl.to()
    //     const popup = $('#popup');
    //     const animal = $('.getWinner');
    //     // + preserve height + margin from the top
    //     var tl = new TimelineMax({'paused':true});
    //     tl.to(popup, 0.7, {opacity:1, visibility: 'visible'});
    //     tl.fromTo(animal, 0.2,{opacity: 0}, {opacity: 1},0.4);
    //     tl.fromTo(animal, 1, {transform: 'scale(0.3)'}, {transform: 'scale(1.2)', ease: Bounce.easeOut},0.4);
    //     tl.play();
       
    // };
    const animation = function() {
        // tl.to()
        const popup = $('#popup');
        const animal = $('.getWinner');
        // + preserve height + margin from the top
        animationContentResize();
        var tl = new TimelineMax({'paused':true});
        tl.to(popup, 0.7, {opacity:1, visibility: 'visible'});
        tl.fromTo(animal, 0.2,{opacity: 0}, {opacity: 1},0.4);
        tl.fromTo(animal, 1, {transform: 'scale(0.3)'}, {transform: 'scale(1.2)', ease: Bounce.easeOut},0.4);
        tl.play();
       
    };

    const openGameRestriction = function() {
        if ((!(keystoneConfig.setting && keystoneConfig.setting.enabled && !keystoneConfig.bypassview) || !!keystoneConfig.parkError) && keystoneConfig.userType !== 'ht') {
            if (!!keystoneConfig.parkError) {
                $(unavailableDrawMessage).html(keystoneConfig.parkError).show();
            } else {
                $(unavailableDrawMessage).html(_currentPack.unavailableLuckyDraw).show();
            }
            $('#form-lucky-draw').remove();
            $('#lang').hide();
        } else {
            $(unavailableDrawMessage).hide();
        }
        $(loading).hide();
        $(terms).hide();
    }

    const initialCountryList = function() {
        const countryList = keystoneConfig.countries; 
        if (countryList && countryList.length) {
            countryList.forEach(function(country) {
                $(telSelection).append(`<option value=${country._id}>${country.code}</option>`);
            });
        }
    }
    const processHotelStaffLottery = function(data) {
        console.log('Do Staff Flow');
        const drawResult = data.drawResult;
        step.last();
        $(resultPage).find(characterImage).attr('src', drawResult.icon.fullPath);
        renderFinalResult(data);
        $(againButton).click(function() {
            finalizeDrawResult(data, function() {
                // use reload instead of go first step
                window.location.reload();
                // step.first()
            });
            // if (!_processing) {
            //     $.serverRequest({
            //         url: keystoneConfig.finishLotteryUrl,
            //         method: 'POST',
            //         data: {
            //             id: data.id,
            //         },
            //         headers: {
            //             Authorization: 'Bearer ' + _loginToken,
            //         },
            //     }, function(response) {
            //         const status = response.status;
            //         // if (!!status) {
            //         step.first();
            //         updateCurrentLanguage();
            //         // } else {
            //         //     alert(response.message);                
            //         // }
            //     });
            // }
        });

    };

    const processOperatorLottery = function(data) {
        console.log('Do Operator Flow');
        step.third();
        initDrawAnimation(data);
        renderFinalResult(data);
    };

    // processAppLottery = function(data) {
    //     console.log('Do App Flow');
    //     step.third();
    //     initDrawAnimation(data);
    //     renderFinalResult(data);
    // };

    const processPlayTheGame = function(response) {

        const accountType = keystoneConfig.userType || (keystoneConfig.staff && keystoneConfig.staff.type);
        // clear the form
        $(form)[0].reset();
        switch (accountType) {
            // operator flow
            case 'op':
                processOperatorLottery(response.data);
                break;
            // hotel staff flow
            case 'ht':

                processHotelStaffLottery(response.data);
                break;
            // same flow with operator
            default:
                processOperatorLottery(response.data);
                // TODO
        }
    };

    /* 
    * ------------------------------------------------------
    * End of Helper Functions
    * ------------------------------------------------------
    */

    /* 
    * ------------------------------------------------------
    * Event Listeners for the DOM
    * ------------------------------------------------------
    */
    const getNativeCameraCallData = function() {
        return {
            url: keystoneConfig.landingUrl,	// Callback url
            title: _currentPack.newYearDraw,
            usedCheckUrl: keystoneConfig.ticketCheckUrl,
            useWebViewTitle: true,
            alwaysInPark: true,
            noSameTicketCheck: true,
            count: 1,
            skipValidate: false,
        }
    };
    const eventBinder = function() {
        $('[name="card"]').on('keyup', function () {
            var tweet_length = $(this).val().length;
            if(tweet_length > 4) {
                $(this).val($(this).val().substring(0, 4));
            }
        });
        $('[name="name"]').on('keyup', function (e) {
            const value = $(this).val().replace(/[^a-zA-Z ]/g,'');
            $(this).val(value);
        });
        $(logoutBtn).on('click', function(e) {
            e.preventDefault();
            if (!_processing) {
                $.serverRequest({
                    url: keystoneConfig.singoutUrl,
                    headers: {
                        Authorization: 'Bearer ' + _loginToken,
                    },
                }, function(response) {
                    kickLogout();
                });
            }
        });
        $(popCancelButton).on('click', function(){
            popup.close()
        });
        $(popupMask).on('click', function(){
            popup.close();
        });
        $(languageSelector).on('change', function() {
            messageBox.hide(); 
            if (!_processing) {
                _currentLanguage = $(this).val();
                updateCurrentLanguage();
            }
        })
        $(rulBackButton).on('click', function() {
            window.location.href = keystoneConfig.landingUrl + '?type=rule';
        })
        $(loginBtn).on('click', function(e) {
            e.preventDefault();
            commonServerRequest(null, function(response) {
                const status = response.status;
                // console.log(response);
                if (!!status) {
                    // user = response.data;
                    // store the cookie
                    $.writeCookies(keystoneConfig.cookies, response.data.loginToken);
                    // window.location.href = keystoneConfig.landingUrl;
                    reloadAllSteps();
                } else {
                    alert(response.message);                
                }
            });
        });
        $(submitBtn).on('click', function(e) {
            e.preventDefault();
            const isValid = isValidLuckyDrawForm();
            messageBox.hide(); 
            if (isValid) {
                commonServerRequest(true, function(response) {
                    // console.log(response);
                    const status = response.status;
                    if (!!status) {
                        processPlayTheGame(response);
                    } else {
                        messageBox.show(response.message);       
                    }
                });
            }
        });
        // $(scanOtherButton).on('click', function(e) {
        //     e.preventDefault();
        //     const data = getNativeCameraCallData();
        //     openTicketScanner(data);
        // });
        // $(checkEcouponButton).on('click', function(e) {
        //     e.preventDefault();
        //     gotoMyEcoupon();
        // });
        $(ruleCameraBtn).on('click', function(e) {
            e.preventDefault();
            if (keystoneConfig.native) {
                // console.log(openTicketScanner);
                const data = getNativeCameraCallData();
                openTicketScanner(data);
                // step.first();
            } else {
                step.first();
            }
        });
        $(backBtn).on('click', function(e) {
            // native call
            if (keystoneConfig.native) {
                gotoHome();
            } else {
                reloadAllSteps();
            }
        })
    };

    /* 
    * ------------------------------------------------------
    * > Document Entry Point
    * Restriction under login status
    * ------------------------------------------------------
    */
    const initialDocument = function() {
        // 0.0 go to the first step view

        // step.first();
        

        // 1.0 must do, initial with pre-set cookies language
        _currentLanguage = keystoneConfig.language ||
            $.readCookies(keystoneConfig.langCookies, keystoneConfig.cookiesConfig) ||
            defaultLang;
        if (typeof _currentLanguage !== 'string') {
            _currentLanguage = defaultLang;
        }
        // console.log($.evalPath(_currentPack, 'loginForm.username'));
        // 2.0 account restriction under some of pages
        accountRestriction();

        switchStep();

        // 3.0 restricted by disable drawgame
        openGameRestriction();

        // 4.0 update terms and condition
        updateTermConditions();

        // 5.0 setup localization to ui, show the main content block after localized
        // initialLocaliation();
        updatePromotion();

        // 6.0 initial country code list
        initialCountryList();
        // $(mainContentBlock).fadeIn('fast');

        // special for animation
        // animationResize();
        $_WINDOW.on('resize', animationResize);
        $_WINDOW.on('resize', animationContentResize);
    };

    /* 
    * ------------------------------------------------------
    * > Initialize the app
    * ------------------------------------------------------
    */
    (function initialize() {
        $(function() {
            // 1.0 initialize base document 
            initialDocument();
            // 2.0 bind all of events needed
            eventBinder();
        });
    })();

})(jQuery);