const cookieParser   = require('cookie-parser')
const GameV2         = require(global.__base + '/routes/apiV2/game');

// Setup Route Bindings
exports = module.exports = function(view, app) {
    GameV2(view, app);
};