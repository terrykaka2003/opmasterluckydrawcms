const cluster       = require('cluster');
const GameHandler  	= require(global.__base + '/routes/handler/game');
const StaffHandler  = require(global.__base + '/routes/handler/game/staff');

const Middleware  	= require(global.__base + '/routes/middleware');

exports = module.exports = function(view, app) {
    // Cluster worker log
    app.use(function(req, res, next){
        if (cluster.worker) {
            console.log('Processing Worker Id: ', cluster.worker.id);
        }
        next();
    });

    app.all('/api/v2/*', Middleware.requireLanguage);

    // only for screen cap use, should be removed later
    if (process.env.NODE_ENV !== 'production') {
        // app.get('/api/v2/pool/clean', GameHandler.cleanPool);
        // app.get('/api/v2/pool/prize/clean', GameHandler.cleanPrizePool);
        // app.get('/api/v2/pool/coupon/clean', GameHandler.cleanCouponPool);
        // app.get('/api/v2/pool/create', GameHandler.createPool);
        // app.get('/api/v2/pool/prize/create', GameHandler.createPrizePool);
        // app.get('/api/v2/pool/coupon/create', GameHandler.createCouponPool);
        app.get('/api/v2/log/clean', GameHandler.removeAllLog);
    }

    app.post('/api/v2/game/send', GameHandler.sendEmail);
    //app.all('/api/v2/*', requiredMember);
    app.get('/api/v2/getLotteryConfig', GameHandler.getLotterySetting);
    app.get('/api/v2/getCountryList', GameHandler.getCountryList);
    app.post('/api/v2/signin/staff', StaffHandler.login);

    app.get('/api/v2/ticket/check',
        Middleware.requireLanguage,
        GameHandler.checkTicketUsed);

    app.post('/api/v2/gameCoupon',
        Middleware.requireLotterySetting,
        GameHandler.couponList);
     app.post('/api/v2/gameCoupon:id',
        Middleware.requireLotterySetting,
        GameHandler.couponList);

    // app view lottery part
    app.post('/api/v2/app/play',
        Middleware.requirePassNo,
        Middleware.requireGameCharacter,
        Middleware.requireLotterySetting,
        Middleware.requireParkLotteryPeriod,
        GameHandler.playLottery);
    app.post('/api/v2/app/end',
        Middleware.requireGameCharacter,
        Middleware.requireLotterySetting,
        Middleware.requireParkLotteryPeriod,
        GameHandler.finishLottery);
    
    // operator lottery part
    app.all('/api/v2/operator/*', Middleware.requireAuthorization, Middleware.requireOperatorStaff);
    app.get('/api/v2/operator/singout', StaffHandler.logout);
    app.get('/api/v2/operator/status', StaffHandler.status);

    app.post('/api/v2/operator/play',
        Middleware.requirePassNo,
        Middleware.requireGameCharacter,
        Middleware.requireLotterySetting,
        Middleware.requireParkLotteryPeriod,
        GameHandler.playLottery);
    
    app.post('/api/v2/operator/end',
        Middleware.requireGameCharacter,
        Middleware.requireLotterySetting,
        Middleware.requireParkLotteryPeriod,
        GameHandler.finishLottery);

    // hotel lottery part
    app.all('/api/v2/staff/*', Middleware.requireAuthorization, Middleware.requireHotelStaff);
    app.get('/api/v2/staff/singout', StaffHandler.logout);
    app.get('/api/v2/staff/status', StaffHandler.status);


    // when submit the form and draw out the lottery result
    app.post('/api/v2/staff/play',
        Middleware.requireInvoiceNo,
        Middleware.requireGameCharacter,
        Middleware.requireLotterySetting,
        Middleware.requireHotelLotteryPeriod,
        GameHandler.playLottery);
    // after play the game, update us
    app.post('/api/v2/staff/end',
        Middleware.requireGameCharacter,
        Middleware.requireLotterySetting,
        Middleware.requireHotelLotteryPeriod,
        GameHandler.finishLottery);

    // web frontend
    // hotel parts
    app.get('/web/hotel/',  
        Middleware.requireLanguage,
        Middleware.guardUnderLogin,
        Middleware.requiredCountryList,
        view.hotel.signin
    );
    app.get('/web/hotel/signin',
        Middleware.requireLanguage,
        Middleware.guardUnderLogin,
        view.hotel.signin
    );
    app.get('/web/hotel/luckyDraw',
    Middleware.requireLanguage,
        Middleware.staffRequiredAuthorization,
        Middleware.requiredCountryList,
        view.hotel.hotelLottery
    );

    app.get('/web/operator/signin',
    Middleware.requireLanguage,
        Middleware.guardOperatorUnderLogin,
        view.operator.signin
    );
    app.get('/web/operator/luckyDraw', 
        Middleware.requireLanguage,
        Middleware.operatorRequiredAuthorization,
        Middleware.requiredCountryList,
        view.operator.operatorLottery);
        
    app.get('/app/newyears/lottery',
        Middleware.requireLanguage,
        Middleware.appRequiredAuthorization,
        Middleware.requiredCountryList,
        view.app.appLottery);

    // app.get('/demo/newyears/lottery',
    //     Middleware.testingViewByPass,
    //     Middleware.requireLanguage,
    //     Middleware.appRequiredAuthorization,
    //     Middleware.requiredCountryList,
    //     view.app.appLottery);
    
};