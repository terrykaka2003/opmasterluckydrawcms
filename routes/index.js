var keystone = require('keystone');
const express = require('express');
const bwipjs = require('bwip-js');
const _ = require('lodash');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);

var Config = require(global.__base + '/config/Config');
var conf = new Config();
var AppEnv = require(global.__base + '/enum/AppEnv');

const RESTFullAPIV2         = require(global.__base + '/routes/apiV2');

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

keystone.pre('routes', middleware.auditTrailModel);
keystone.pre('routes', middleware.renewSession);
keystone.pre('routes', middleware.requirePermission);
keystone.pre('routes', middleware.signinChecking);
//disable
//keystone.pre('routes', middleware.checkPasswordExpire);
keystone.post('signin', middleware.auditTrailSignin);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	// admin: importRoutes('./admin'),
};

function nocache(req, res, next) {
  res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.header('Expires', '-1');
  res.header('Pragma', 'no-cache');
  next();
}

// Setup Route Bindings
exports = module.exports = function(app) {
	/*
	** ======================================================================
	** Start of Phase 2 Luckydraw Enhancements
	** Terry Chan
	** 07-01-2019
	** ======================================================================
	*/
	// app.use(express.static('public/images', {
	//  maxAge: '1d'
	// }));

	app.use('/app', nocache);
	app.use('/web', nocache);
	app.use('/demo', nocache);

	app.use('/static', express.static(conf.characterStaticPath, { maxAge: conf.staticCacheDuration }));
	app.use('/source', express.static(conf.globalUploadsStaticPath, { maxAge: conf.staticCacheDuration }));

	// public static file
	app.use('/css', express.static('public/styles', { maxAge: conf.staticCacheDuration }));
	app.use('/img', express.static('public/images', { maxAge: conf.staticCacheDuration }));
	app.use('/font', express.static('public/fonts', { maxAge: conf.staticCacheDuration }));
	app.use('/script', express.static('public/js', { maxAge: conf.staticCacheDuration }));


	// app.use(serveStatic(path.join(__dirname, 'styles'), {
	//   maxAge: '1d',
	// }));
	//api
	app.use(cors());
	app.options('*', cors());
	app.use(cookieParser());
	// barcode view routing
	app.get('/barcode', function(req, res) {
		bwipjs.toBuffer({
        	bcid:        'code128',       // Barcode type
	        text:        req.query.code || req.body.code,    // Text to encode
	        scale:       3,               // 3x scaling factor
	        height:      10,              // Bar height, in millimeters
	        includetext: false,            // Show human-readable text
	        textxalign:  'center',        // Always good to set this
	    }, function (err, png) {
	        if (err) {
	            // Decide how to handle the error
	            // `err` may be a string or Error object
	        } else {
	        	res.write(png, 'binary');

	            // `png` is a Buffer
	            // png.length           : PNG file length
	            // png.readUInt32BE(16) : PNG image width
	            // png.readUInt32BE(20) : PNG image height
	        }
	        res.end();
	    });
	});
	// app.use(bodyParser.json());
	// allow from all of origin requests
	// app.use(function (req, res, next) {
	// 	res.setHeader('Access-Control-Allow-Origin', '*');
	// 	// res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type');
	// 	next();
	// });
	// app.get('/service/v1', routes.restful.index);

	

	// bind RESTFUL API routings
	RESTFullAPIV2(routes.views, app);

	/*
	** ======================================================================
	** End of Phase 2 Luckydraw Enhancements
	** ======================================================================
	*/

	//custom admin page
	// app.all('/admin/user', middleware.requireUser, routes.admin.user.item);

	// Views
	app.get('/', routes.views.index);
	
	
};
