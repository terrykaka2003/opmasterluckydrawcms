/**
 * @author fourdirections
 * File name: PublicCallback.js
 */

//CallBack
findCallBack = function(callback){
  return function(err, resultData){
    if(err){
      callback(err, []);
    } else{
      callback("", resultData);
    }
  };
};


findSingleCallBack = function(callback){
  return function(err, resultData){
    if(err){
      callback(err, []);
    } else{
      callback("", resultData[0]);
    }
  };
};

addCallBack = function(callback){
  return function(err){
    if(err){
      callback(err);
    } else{
      callback("");
    }
  };
};

updateCallBack = function(callback){
  return function(err){
    if(err){
      callback(err);
    } else{
      callback("");
    }
  };
};

removeCallBack = function(callback){
  return function(err){
    if(err){
      callback(err);
    } else{
      callback("");
    }
  };
};

generalAddModelCallBackHandler = function(callback){
  return function(err, product, numberAffected){
    if(err){
    	console.log("General add model Error Print: "  + err);
    } else{
      	
    }
  };
};

generalFindErrorPrint = function(err, numberAffected, rawResponse){
  if(err){
      console.log("General find Error Print: "  + err);
  }
}

generalErrorPrint = function(err){
  if(err){
      console.log("General Error Print: " + err);
  }
}

exports.findCallBack  = findCallBack;
exports.findSingleCallBack = findSingleCallBack;
exports.addCallBack   = addCallBack;
exports.updateCallBack = updateCallBack;
exports.removeCallBack = removeCallBack;

exports.generalAddModelCallBackHandler = generalAddModelCallBackHandler;
exports.generalFindErrorPrint = generalFindErrorPrint;
exports.generalErrorPrint = generalErrorPrint;