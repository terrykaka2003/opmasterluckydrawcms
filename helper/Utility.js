/*
* Utility included some helper static function 
*/

var keystone = require('keystone');
var mongoose = keystone.mongoose;
// var ValidationError = mongoose.Error.ValidationError;
// var ValidatorError  = mongoose.Error.ValidatorError;
const barcode = require('barcode');
var Config = require(global.__base + '/config/Config');

function Utility(){
	
}

Utility.generateBarCodeImage = function(id, code, callback) {
	const code128 = barcode('code128', {
	    data: code,
	    width: 500,
	    height: 100,
	});
	const dir = 'code/'+ id + '.jpg';
	const outfile = keystone.get('global upload dir') + dir;
	// code128.getBase64(function (err, imgsrc) {
	//     if (err) {
	//     	callback(err);
	//     } else {
	//     	console.log('base64 Barcode '+  +' image has been generated: ' + imgsrc);
	//  		callback(null, imgsrc);
	// 	}
	// });
	code128.saveImage(outfile, function (err) {
	    if (err) {
	    	callback(err);
	    } else {
	    	console.log('Barcode '+  +' image has been written in: ' + outfile);
	 		callback(null, dir);
		}
	});
};

/**
* An enum object as argument and string as return value
* The enum need to have count defined
**/
Utility.enumToKeystonOptionStr = function(enumObj) {
	var returnOptionString = "";
	for(var i=0;i<enumObj.enums.length;i++){
		returnOptionString += enumObj.get(i).key;

		if(i < enumObj.enums.length - 1){
			returnOptionString += ",";
		}
	}
	return returnOptionString;
};

// Utility.createPreSaveError = function(msg) {
// 	var error = new ValidationError(this);
//   error.errors.email = new ValidatorError(null, msg, null, null);
//   return error;
// }

Utility.createError = function(msg) {
  return new Error(msg);
}


Utility.isString = function(str) {
  return typeof(str) === 'string' || s instanceof String;
}


Utility.capitalize = function(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

Utility.getClientIp = function(req) {
  var str = (req.headers["X-Forwarded-For"] ||
          req.headers["x-forwarded-for"] ||
          '').split(',')[0] ||
         req.client.remoteAddress;
	var array = str.split(':');
	return array.slice(-1)[0];
};

Utility.getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

Utility.convertPathToAbsolutePath = function(object){

	conf = new Config();

	var find = './media/';
	var replace = conf.absolutePath;

	var str = JSON.stringify(object);
	var re = new RegExp(find, 'g');

	str = str.replace(re,replace);

	return JSON.parse(str);
}

Utility.convertLangCode = function(lang) {
	var lang = lang;
    switch(lang){
    	case 'zh':
	        lang = 'tc';
	        break;
    	case 'cn':
	        lang = 'sc';
	        break;
   		default:
        	break;
    }
    return lang;
}

Utility.isValidTime = function(time) {
	var timeFormat = /^(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/;
    return timeFormat.test(time);
}

module.exports = Utility;