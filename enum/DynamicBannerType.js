var Enum = require('enum');

var DynamicBannerType = new Enum({
	'attraction-Waterfront': 0,
	'attraction-Summit': 1,
	'listview-notification': 2,
	'listview-news': 3,
});

module.exports = DynamicBannerType;
