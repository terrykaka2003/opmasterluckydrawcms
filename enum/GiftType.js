/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/

var Enum = require('enum');

var GiftType = new Enum({
	'Big Prize': 0,
  'ECoupon': 1,
});

module.exports = GiftType;
