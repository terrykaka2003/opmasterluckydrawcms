/* 
* @Author: patrickng
* @Date:   2016-01-22 15:14:19
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-22 15:14:39
*/

var Enum = require('enum');

var AppPlatform = new Enum({
  'ios':0,
  'android':1,
});

module.exports = AppPlatform;
