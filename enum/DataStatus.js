var Enum = require('enum');

var DataStatus = new Enum({
    'inactive':0,
    'draft':1,
    'active':2
});

module.exports = DataStatus;
