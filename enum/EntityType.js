var Enum=require('enum');

var EntityType=new Enum({
	'Attraction':0,
	'Ride':1,
	'Show':2,
	'Restaurant':3
});
exports=module.exports=EntityType;
