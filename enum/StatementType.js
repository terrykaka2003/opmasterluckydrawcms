var Enum=require('enum');
var StatementType=new Enum({
	'term of use':0,
	'personal information collection statement':1,
	'privacy policy':2,
	'rules and regulations':3
});
module.exports=StatementType;
