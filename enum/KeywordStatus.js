var Enum = require('enum');

var KeywordStatus = new Enum({
    'inactive':0,
    'active':1
});

module.exports = KeywordStatus;