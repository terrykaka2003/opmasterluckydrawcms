/* 
* @Author: patrickng
* @Date:   2016-01-05 15:01:10
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-05 15:01:31
*/
var Enum = require('enum');

var ErrorType = new Enum({
	'Unknown':0,
    'ServerInMaintenance':1,
    "MissingParameter":2,
    'WrongUser':3,
    'ExpiredUserToken':4,
    'MissingPushProject':5,
    'UserAlreadyBound':6,
    'ExpiredBindingCode':7,
    'WrongBindingCode':8,
    'BindingCodeTimeLock':9,
    'NoBindingCodeFound':10,
	'WrongEntity':11,
	'WrongEntitySchedule':12,
	'WrongAttraction':13,
	'WrongShow':14,
	'ShowWithoutEntity':15,
	'WrongConservationMatterIssue':16,
	'WrongConservationMatterTopic':17,
	'WrongConservationMatterPost':18,
	'WrongGuestService':19,
	'WrongGuestServicePost':20,
	'WrongPageBanner':21,
	'WrongGetCloserToAnimals':22,
	'WrongGetCloserToAnimalsPass':22,
	'Authorization': 23,
});

module.exports = ErrorType;
