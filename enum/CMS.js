/* 
* @Author: patrickng
* @Date:   2016-02-17 19:18:56
* @Last Modified by:   Patrick Ng
* @Last Modified time: 2016-03-08 11:15:00
*/

var Enum = require('enum');

var CMS = new Enum({
    'vgtcms':0,
    // 'mascms':1
});

module.exports = CMS;