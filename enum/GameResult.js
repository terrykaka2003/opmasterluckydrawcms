/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/

var Enum = require('enum');

var GameResult = new Enum({
	'idle': 0,
  'hold': 1,
  'completed': 2,
});

module.exports = GameResult;
