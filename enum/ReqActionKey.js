var Enum = require('enum');

var ReqActionKey = new Enum([
    "getAppSettings",
    "getNewsList",
    "getTutorial",
    "getSliderList",
    "getBannerList",
    "getTopBannerList",
    "getSliderList",
    "getMenuLocationList",
    "getLocationList",
    "getShopList",
    "getWeather",
    "getParkSettings",
	"getOpeningHours",
	"getFunctionEntityOperatingHour",
	"getParkMap",
    "createContactUs",
    //for multi lang
    "getMenuLocationLanguageTabById",
    "getNewsLanguageTabById",
    "getShopLanguageTabById",
    "getTopBannerLanguageTabById",
    "getBannerLanguageTabById",
    "getTutorialLanguageTabById",
	"getAttractionLanguageTabById",
	"getShowLanguageTabById",
	"getParkNoticeLanguageTabById",
    "getStatementLanguageTabById",
	"getPageBannerLanguageTabById",
	"getConservationMatterIssueLanguageTabById",
	"getConservationMatterTopicLanguageTabById",
	"getConservationMatterPostLanguageTabById",
	"getAnimalCollectionLanguageTabById",
	"getAnimalCollectionGameLanguageTabById",
	"getGuestServiceLanguageTabById",
	"getGuestServicePostLanguageTabById",
	'getTransportationLanguageTabById',
	'getGetCloserToTheAnimalsLanguageTabById',
	'getGetCloserToAnimalsPassLanguageTabById',
	"getTicketInformationLanguageTabById",
	"getTicketInformationPassLanguageTabById",
	"getTicketInformationGeneralAdmissionLanguageTabById",
	"getTicketInformationOceanFasTrackLanguageTabById",
    //for copy
    "copy",
    //for concurrentedit
    "getNewsUpdateTime",
    "getShopUpdateTime",
    "getTopBannerUpdateTime",
    "getBannerUpdateTime",
	"getAttractionUpdateTime",
    //for wifi location
    "getWifiLocationList",
    "registerFreeWifi",
	//for attractions
	"getAttractionList",
	"getAttractionDetail",
    "getFacilitiesUnderMaintenance",
	//for show
	"getShowList",
	"getShowDetail",
    "getParkSchedule",
    //for statement
    "getStatements",
    // for search
    "getSearchResult",
    "getKeywordListAndTrendingKeywordList",
	//for conservationMatters
	"getConservationMatterTopicByIssue",
	"getConservationMatterPostByTopic",
	"getConservationMatterIssue",
	//for guestService
	"getGuestService",
	"getGuestServicePost",
	//for animalCollection
	"getAnimalDetail",
	//for pageBanner
	'getPageBannerByName',
	//for transportation
	'getTransportationList',
	//for getCloserToAnimals
	'getGetCloserToAnimals',
	'getGetCloserToAnimalsDetail',
	//for ticketInformation
	'getTicketInformation',
	//for survey
	'getSurveyByDate',
	"addSurveyRecord",
	//promotion
	"getAdhocNotice",
	//for animalCollectionGame
	"getAnimalCollectionGame",
	"submitAnimalCollectionGameResult",
	"submitLuckyDraw",
	// DynamicIconandBanner
	"getDynamicIconAndBanner"
	]);

module.exports  = ReqActionKey;
