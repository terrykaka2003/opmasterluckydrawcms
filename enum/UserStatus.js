/* 
* @Author: patrickng
* @Date:   2016-01-21 11:59:37
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-21 11:59:48
*/

var Enum = require('enum');

var UserStatus = new Enum({
  'inactive':0,
  'locked':1,
  'active':2
});

module.exports = UserStatus;