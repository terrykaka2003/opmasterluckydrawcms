/* 
* @Author: patrickng
* @Date:   2016-01-05 15:01:10
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-19 22:15:45
*/
var Enum = require('enum');

var AppEnv = new Enum([
    'localhost',
    'development',
    'uat',
    'beta_close',
    'production'
]);

module.exports = AppEnv;
