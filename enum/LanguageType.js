 var Enum = require('enum');

var LanguageType = new Enum({
    'en':0,
    'sc':1,
    'tc':2
});

module.exports = LanguageType;