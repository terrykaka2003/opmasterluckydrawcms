var Enum=require('enum');
var AnimalCollectionGameType=new Enum({
	'multipleChoice': 0 ,
	'luckyDraw':1 ,
	'pop-up':2 ,
});
module.exports=AnimalCollectionGameType;
