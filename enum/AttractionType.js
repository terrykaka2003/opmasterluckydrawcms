var Enum=require('enum');
var AttractionType=new Enum({
	'Animals':0,
	'Thrill':1,
	'Family':2,
	'Wet':3,
	'In-Park Transportation':4,
	'Journey Experience':5,
	'Shows':6
});
module.exports=AttractionType;
