var Enum = require('enum');

var RedirectKey = new Enum({
	'animalcollection':0,
	'Gulu':1,
	'ParkAndShowSchedule': 2,
	'eScheduler': 3,
	'Special': 4,
	// 'ticketEvent': 4,
});

module.exports = RedirectKey;
