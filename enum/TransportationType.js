var Enum=require('enum');

var TransportationType=new Enum({
	'CarParkEntrances':0,
	'PublicTransportation':1,
	'CityBus':2,
	'ByCar':3,
	'MTRC':4,
});
module.exports=TransportationType;
