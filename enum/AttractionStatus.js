/* 
 * @Author: Chriszhang
 * @Date:    2016-05-06 11:30:32
 * @Last Modified by:   
 * @Last Modified time: 
 */
var Enum=require('enum');

var AttractionStatus=new Enum({
	'availability':0,
	'maintenance':1
});
module.exports=AttractionStatus;
