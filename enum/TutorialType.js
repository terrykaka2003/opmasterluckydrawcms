/* 
* @Author: patrickng
* @Date:   2016-01-07 16:00:50
* @Last Modified by:   Patrick Ng
* @Last Modified time: 2016-03-08 18:34:03
*/

var Enum = require('enum');

var TutorialType = new Enum({
    'escheduler':0,
    'virtual guided tour':1,
    'ecoupons':2,
    'about this app':3,
    'escheduler_scanticket':4,
    'escheduler_enterticketid':5,
    'escheduler_scannedticket':6,
    'escheduler_filterattraction':7,
    'escheduler_attractiondetail':8,
    'escheduler_pickedattraction':9,
    'escheduler_ticketparing':10,
    'escheduler_schedulingandredemption':11,
    'escheduler_introduction':12,
    'escheduler_howtofindyourticketid':13,
    'parkmap_onlinemap':14,
    'parkmap_offlinemap':15,
    'escheduler_transfedevice':16,
    'animal_collection':17,
    'general_scanticket':18,
    'general_enterticketid':19,

});

module.exports = TutorialType;