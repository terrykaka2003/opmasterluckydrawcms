const Enum = require('enum');

/*
** Pair up the type and health check target controller together
** Terry Chan@24/09/2018
** Updated At 27/09/2018
*/
const ScheduleJobTypes = new Enum({
	'Pflow Weather': 'RegularService',
    'Pflow Weather Warning': 'RegularService',
    'Pflow Wifi Location': 'RegularService',
	'Pflow Promotion List': 'RegularService',
    'Pflow Entity Waiting Time': 'RegularService',
    'Pflow Entity List': 'RegularService',
    'Pflow Show Schedule': 'RegularService',
    'Pflow Operation Hour': 'RegularService',
    'Pflow Entity Current Status': 'RegularService',
    'Pflow Entity Operating Hour': 'RegularService',
    'Pflow Entity Maintenance': 'RegularService',
  	'Pflow Zone Operation Hour': 'RegularService',
  	'Check LDAP User Status': 'RegularService',
    'Pflow Function Entity Operating Hour': 'RegularService',
    'Pflow Animal Activity List': 'RegularService',
});

module.exports = ScheduleJobTypes;