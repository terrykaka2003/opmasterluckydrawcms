/* 
* @Author: patrickng
* @Date:   2016-01-07 16:00:58
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-07 16:02:35
*/

var Enum = require('enum');

var SliderType = new Enum({
    'top':0,
    'bottom':1,
});

module.exports = SliderType;