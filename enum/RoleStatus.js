var Enum = require('enum');

var RoleStatus = new Enum({
	'denied':0,
	'readOnly':1,
	'editable':2
});

module.exports = RoleStatus;