var Enum = require('enum');

var GreetingIcon = new Enum({
	'In-park':0,
	'Out-park':1
});

module.exports = GreetingIcon;
