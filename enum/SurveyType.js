var Enum = require('enum');

var SurveyType = new Enum({
	'VGT':0,
	'Escheduler':1,
	'SurpriseECoupon':2,
	'Global':3,
});

module.exports = SurveyType;
