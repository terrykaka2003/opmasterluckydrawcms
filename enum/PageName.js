var Enum=require("enum");
var PageName=new Enum({
	'GetCloserToAnimal':0,
	'Attraction':1,
	'Schedule':2,
	'ConservationMatter':3,
	'AnimalCollection':4,
	'Samsung':5,
	'AnimalCollectionGame':6,
});
module.exports=PageName;
