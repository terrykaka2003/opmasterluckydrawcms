/* 
* @Author: patrickng
* @Date:   2016-01-11 16:18:54
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-11 16:19:41
*/

var Enum = require('enum');

var ShopType = new Enum({
    'souvenir':0,
    'photo':1,
    'game':2
});

module.exports = ShopType;