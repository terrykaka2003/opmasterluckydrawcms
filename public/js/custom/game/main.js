$(document).ready(() => {

    const animation = () => {
        // tl.to()
        const popup = $('#popup');
        const animal = $('.getWinner');
        var tl = new TimelineMax({'paused':true});
        tl.to(popup, 0.7, {opacity:1, visibility: 'visible'});
        tl.fromTo(animal, 0.2,{opacity: 0}, {opacity: 1},0.4);
        tl.fromTo(animal, 1, {transform: 'scale(0.3)'}, {transform: 'scale(1.2)', ease: Bounce.easeOut},0.4);
        tl.play();
    }

    const init = () => {
        const num = $('#item').val();
        $('.slot').imagesLoaded(() => {
            $('.slot').jSlots({
                spinner : '#playBtn',
                stopSpinner: '#stopBtn',
                number: 1,
                time : 7000,
                loop: -1,
                endNumber: num,
                onStart: (f) => {
                    $('#playBtn').removeClass('show');
                    $('#stopBtn').addClass('show');
                },
                onStop: (f) => {
                },
                onEnd: (f) => {
                    animation();
                    // $('.slot').stop(true);
                    // $('.getWinner').html(items[f-1]);
                },
            });
            $('.wrapper').addClass('loaded');
        });
    }
    $(window).bind('on',{
        'load': init()
    })
})