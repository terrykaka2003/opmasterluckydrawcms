var checkUpdatedTime = false;
var moment = require('moment');

jQuery(document).delegate(".btn-save", "click", function (e) {


	if (checkUpdatedTime === true) {
		checkUpdatedTime = false; // reset flag
		return; // let the event bubble away
	}
	var path = window.location.pathname;
	console.log("path:"+path);
	var pathnameArray = path.split('/');
	var currentPath = "/" + pathnameArray[1] + "/" + pathnameArray[2];
	var currentId = pathnameArray[3];
	var apiPath = document.location.origin + "/";
	var action = null;
	switch (pathnameArray[2]) {
		case 'news':
		{
			action = 'getNewsUpdateTime';
			break;
		}
		case 'shops':
		{
			action = 'getShopUpdateTime';
			break;
		}
		case 'top-banners':
		{
			action = 'getTopBannerUpdateTime';
			break;
		}
		case 'banners':
		{
			action = 'getBannerUpdateTime';
			break;
		}
		case 'attractions':
		{
			action = 'getAttractionUpdateTime';
			break;
		}
		default:
		{
			break;
		}
	}
	if (action != null) {

		e.preventDefault();
		var checkTime = function (result) {
			var result = JSON.parse(result);
			var loadDate = jQuery("span[data-reactid*='updatedAt']").text();

			//use create time if there is no update time
			if (loadDate == "") {
				loadDate = jQuery("span[data-reactid*='createdAt']").text();
			}
			if (result.data.updatedAt) {
				var resultDate = result.data.updatedAt;
			} else {
				var resultDate = result.data.createdAt;
			}
			var latestDate = moment(resultDate).format("Do MMM YY h:mm:ssa");
			//if api return success and date not match
			if (result.status == 1 && loadDate.indexOf(latestDate) == -1) {

				var confirmDialog = confirm('The current data is edited during the time you access. Do you want to overwrite the content?');
				if (confirmDialog) {
					checkUpdatedTime = true;
					jQuery('.btn-save').trigger('click');
				}
			}
			else {
				checkUpdatedTime = true;
				jQuery('.btn-save').trigger('click');
			}
		};

		jQuery.post(apiPath, {action: action, id: currentId}, checkTime);
	}
});
