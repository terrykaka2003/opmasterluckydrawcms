/* 
* @Author: patrickng
* @Date:   2016-01-26 11:20:03
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-26 11:24:56
*/

$('table.items-list tbody').on('mouseenter mouseleave', 'tr a.control-copy', function(e) {
	if (e.type == 'mouseenter') {// eslint-disable-line eqeqeq
		$(this).closest('tr').addClass('delete-hover');
	} else {
		$(this).closest('tr').removeClass('delete-hover');
	}
}).on('click', 'tr a.control-copy', function(e) {
	if (!confirm('Are you sure you want to copy this?')) {
		return false;
	}
})