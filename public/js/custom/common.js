/* 
* JQuery Extend Functions $.extend
* JQuery Customized Prototype $.fn
* @Author: Terry Chan
* @Date:   2019-01-07
* @Last Modified by:   Terry Chan
* @Last Modified time: 2019-01-07
*/
var $_WIN = $(window);
var $_DOM = $("html, body");

$.extend({
	readCookies: function(cookiesName, options) {
        // window.console.warn('Read Cookie with name: ' + cookiesName + ', options: ' + options);
		return Cookies.get(cookiesName, options);
	},
	writeCookies: function(cookiesName, value, options) {
		// window.console.warn('Write Cookie with name: ' + cookiesName + ', value: ' + value);
		Cookies.set(cookiesName, value, options);
	},
	removeCookies: function(cookiesName, options) {
		// window.console.warn('Remove Cookie with name: ' + cookiesName);
		Cookies.remove(cookiesName, options);
    },
	// requiredLanguage: function(language, callback) {
	// 	var lang = language || 'en';
	// 	const opts = {
    //         language:  lang,
    //         pathPrefix: "../../locales",
    //     };
	// 	$("[data-localize]").localize("application", opts);
	// 	callback(lang);
    // },
    /*
     * By Terry
     * | @param Object List to be found for the property
     * | @param String path from the Object List
     */
    evalPath: function(set, path) {
        var parts = path.split("."),
        length = parts.length, i, property = set || this;
          
        for ( i = 0; i < length; i++ ) {
            property = property[parts[i]];
        }
        return property;
    },
    /*
     * By Terry
     * | @param Language Pack to be read
     * | @param Selector Mapping with language key
     */
    localization: function(pack, mapping, evaluator, callback) {
        mapping.forEach(function(e) {
            const selector = $(e.selector);
            const value = evaluator(pack, e.key) || '';
            // console.log(selector, e.key, value);
            switch (e.type) {
                case 'html':
                    selector.html(value);
                    break;
                case 'value':
                    selector.val(value);
                    break;
                case 'placeholder':
                    selector.prop('placeholder', value);
                    break;
                default:
                    // TODO
            }
        });
        callback();
    },
	/*
     * By Terry
     * | option (url, method, data) params
     * | @func callback for succesful request call
     * | @func callback for failure request call
     * | @func callback for error request call (handle response code e.g. 400, 401)
     * | @func callback for complete request call no matter how the call to be finished
     */
    serverRequest: function(option, successCallback, failCallback, errorCallback, completeCallback, beforeSendCallback) {
        // var timestamp = Math.random();
        var config = {
			headers: Object.assign({}, option.headers),
            type: option.method || 'GET',
			url: option.url,
			// contentType: 'application/json',
			data: {},
			dataType: "json",
            async: true,
            beforeSend: function (request) {
                if (beforeSendCallback && $.isFunction(beforeSendCallback))
                    beforeSendCallback(request);
            },
            success: function (response) {
                if (successCallback && $.isFunction(successCallback))
                    successCallback(response);
            },
            error: function (header, responseText) {
                var response = header.responseJSON;
                if (errorCallback && $.isFunction(errorCallback)) {
                    errorCallback(header, response);
                }
            },
            fail: function (xhr, status, error) {
                if (failCallback && $.isFunction(failCallback))
                    failCallback(xhr, status, error);
            },
            complete: function(xhr, status) {
                if (completeCallback)
                    completeCallback(xhr, status);
            }
        };
        if (option.contentType) {
            config.contentType = option.contentType;
        }
        if (option.processData) {
            config.processData = option.processData;
        }
        if (option.data) {
            config.data = option.data;
        }

        // config.data = Object.assign(config.data, {
        //     t: timestamp
        // });
        $.ajax(config);
    }
});

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
