jQuery(document).ready(function () {

	var path = window.location.pathname;
	var pathnameArray = path.split('/');
	var currentPath = "/" + pathnameArray[1] + "/" + pathnameArray[2];
	var currentId = pathnameArray[3];
	var apiPath = document.location.origin;
	var languageTab = "";
	var action = null;
	var langArray = {
		en: 'ENG',
		tc: '繁',
		sc: '簡'
	};
	var previewContent = "<div class='preview container'>";
	var previewButton = "<div class='preview-btn container'><a>Preview All</a></div>";
	var fullLangArray = {
		en: 'English',
		tc: '繁體中文',
		sc: '简体中文'
	};
	var previewKeyArray = ["name", "description", "shortDescription", "contentSharing"];

	switch (pathnameArray[2]) {
		case 'news':
		{
			action = 'getNewsLanguageTabById';
			break;
		}
		case 'shops':
		{
			action = 'getShopLanguageTabById';
			break;
		}
		case 'top-banners':
		{
			action = 'getTopBannerLanguageTabById';
			break;
		}
		case 'banners':
		{
			action = 'getBannerLanguageTabById';
			break;
		}
		case 'tutorials':
		{
			action = 'getTutorialLanguageTabById';
			break;
		}
		case 'attractions':
		{
			action = 'getAttractionLanguageTabById';
			break;
		}
		case 'shows':
		{
			action = 'getShowLanguageTabById';
			break;
		}
		case 'park-notices':
		{
			action = 'getParkNoticeLanguageTabById';
			break;
		}
		case 'statements':
		{
			action = 'getStatementLanguageTabById';
			break;
		}
		case 'page-banners':
		{
			action = 'getPageBannerLanguageTabById';
			break;
		}
		case 'conservation-matter-issues':
		{
			action = 'getConservationMatterIssueLanguageTabById';
			break;
		}
		case 'conservation-matter-topics':
		{
			action = 'getConservationMatterTopicLanguageTabById';
			break;
		}
		case 'conservation-matter-posts':
		{
			action = 'getConservationMatterPostLanguageTabById';
			break;
		}
		case 'animal-collections':
		{
			action = 'getAnimalCollectionLanguageTabById';
			break;
		}
		case 'guest-services':
		{
			action = 'getGuestServiceLanguageTabById';
			break;
		}
		case 'guest-service-posts':
		{
			action = 'getGuestServicePostLanguageTabById';
			break;
		}
		case 'transportations':
		{
			action = 'getTransportationLanguageTabById';
			break;
		}
		case 'get-closer-to-the-animals':
		{
			action = 'getGetCloserToTheAnimalsLanguageTabById';
			break;
		}
		case 'get-closer-to-animals-passes':
		{
			action = 'getGetCloserToAnimalsPassLanguageTabById';
			break;
		}
		case 'ticket-information':
		{
			action = 'getTicketInformationLanguageTabById';
			break;
		}
		case 'ticket-information-passes':
		{
			action = 'getTicketInformationPassLanguageTabById';
			break;
		}
		case 'ticket-information-general-admissions':
		{
			action = 'getTicketInformationGeneralAdmissionLanguageTabById';
			break;
		}
		case 'ticket-information-ocean-fas-tracks':
		{
			action = 'getTicketInformationOceanFasTrackLanguageTabById';
			break;
		}
		case 'animal-collection-games':
		{
			action = 'getAnimalCollectionGameLanguageTabById';
			break;
		}
		default:
		{
			break;
		}
	}

	//add back btn language filter;
	jQuery(document).delegate(jQuery("a[data-reactid*='$back']"), "click", function (e) {
		if (action != null) {
			var backLink = jQuery("a[data-reactid*='$back']").attr('href');
			backLink += "?q=language:$select:en";
			jQuery("a[data-reactid*='$back']").attr('href', backLink);
		}
	});

	//preview btn function
	jQuery(document).on('click', '.preview-btn > a', function () {
		jQuery(".preview").toggle('fast');
	});
	jQuery(document).on('click', '.close-preview-btn', function () {
		jQuery(".preview").hide('fast');
	});


	if (action != null && currentId != null) {
		jQuery.post(apiPath, {action: action, id: currentId}, function (result) {

			var result = JSON.parse(result);
			//if api return success
			if (result.status == 1) {

				//sort language tab
				if (result.data) {
					function sortByKey(array, key) {
						return array.sort(function (a, b) {
							var x = a[key];
							var y = b[key];
							return ((x < y) ? -1 : ((x > y) ? 1 : 0));
						});
					}

					result.data = sortByKey(result.data, 'language');
				}
				//prepend language tab on item page
				languageTab += "<div class='container language-tab'>";
				for (i = 0; i < result.data.length; i++) {

					var className = result.data[i].language + '-tab';
					if (result.data[i]._id == currentId) {
						className += " current";
					}
					var link = currentPath + '/' + result.data[i]._id;
					languageTab += "<a class='" + className + "' href=" + link + ">" + langArray[result.data[i].language] + "</a>";


					//preview content
					previewContent += "<p class='lang-title'>" + fullLangArray[result.data[i].language] + "</p>";
					$.each(result.data[i], function (key, value) {

						if (previewKeyArray.indexOf(key) > -1) {
							previewContent += "<p>" + key + " : " + value + "</p>";
						}
					});

				}
				languageTab += "</div>";
				previewContent += "<a class='close-preview-btn'>Close</a></div>";
				jQuery("#body").prepend(languageTab);
				jQuery("#body").prepend(previewButton + previewContent);


			}
		});
	}
	//  if(action != null && currentId != null){
	//    jQuery.ajax({
	//     type: "POST",
	//     url: apiPath,
	//     data: JSON.stringify({data: { action: action , id: currentId }}),
	//     contentType: "application/json; charset=utf-8",
	//     dataType: "json",
	//     success: function(result){	
	// 			//if api return success
	// 			console.log(result);
	//       if(result.status == 1){
	//       	//prepend language tab on item page
	//       	languageTab += "<div class='container language-tab'>";
	//         for (i = 0; i < result.data.length; i++) { 

	// 			    var className = result.data[i].language + '-tab';
	// 			    if(result.data[i]._id == currentId){
	// 			    	className += " current";
	// 			    }
	// 			    var link = currentPath + '/' + result.data[i]._id;
	// 			    languageTab += "<a class='"+className+"' href="+link+">"+langArray[result.data[i].language]+"</a>";
	// 				}
	// 				languageTab += "</div>";
	// 				jQuery("#body").prepend(languageTab);
	// 			}
	//     },
	//     failure: function(err) {
	//        console.log(err);
	//     }
	// 	});

	// }
	else {
		console.log('invalid');
	}
});
