var gIndex = 0;
var actions = {};
function getAction(key) {
	var action = actions[key];
	if ( action ) {
		delete actions[key];
		return JSON.stringify(action);
	}
	return null;
}

function execute(action)
{
  var index = gIndex++;
  url = "action://" + index;
  actions[url] = action;
  var iframe = document.createElement("IFRAME");
  iframe.setAttribute("src", url);
  document.documentElement.appendChild(iframe);
  iframe.parentNode.removeChild(iframe);
  iframe = null;
}

function setData(data){
	window.data = data;
	if ( typeof(window.onDataReady) == 'function' ) 
		window.onDataReady(data);
}

function openTicketScanner(data)
{
	if ( window['app'] ) {
		app.openTicketScanner(JSON.stringify(data));
	} else {
		execute({'action': 'scanTicket', 'data': data});
	}
}

function gotoHome()
{
	if ( window['app'] ) {
		app.gotoHomePage();
	} else {
		execute({'action': 'gotoHomePage'});
	}
}

function gotoMyEcoupon() {
	if ( window['app'] ) {
		app.gotoMyEcouponPage();
	} else {
		execute({'action': 'gotoMyEcouponPage'});
	}
}

function addEcoupon(ecoupon) {
	if ( window['app'] ) {
		app.addEcoupon(JSON.stringify(ecoupon));
	} else {
		execute({'action': 'addEcoupon', 'data': ecoupon});
	}
}

if ( window['app'] ) {
	window.addEventListener('load', function() {
		window.data = {
			ticketNo: app.getData('ticketNo'),
			deviceId: app.getDeviceId(),
			lang: app.getLangId(),
			bssid: app.getBssId()
		}
		if ( typeof(window.onDataReady) == 'function' ) 
			window.onDataReady(window.data);
	}, false);
}

