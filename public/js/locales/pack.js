const LOCALIZATION = {
  tc: {
    loginForm: {
        username: '用戶名稱：',
        password: '密碼：',
        login: '登入',
    },
    logout: '登出',
    language: '語言',
    newYearDraw: '「玩 · 住」賞贏大獎',
    unavailableLuckyDraw: '抱歉，新年大抽獎並未開放。',
    drawForm: {
        name: '英文全名 *',
        namePlaceholder: '需與身份證明文件一樣以作核實用途',
        id: '香港身份證/護照的頭4個數字 *',
        email: '電郵地址 *',
        emailPlaceholder: '部份獎品換領信將獲個別電郵發出得獎通知',
        confirmEmail: '確認電郵 *',
        invoiceNo: '收據號碼 *',
        ticketNo: '門票編號 *',
        telephone: '手提電話號碼 *',
        terms: '本人已清楚閱讀並同意「玩 . 住」賞贏大獎活動的<a class="tc">條款及細則</a>。<br /><br /><p>有關海洋公園的個人資料收集聲明及在直接促銷中使用你的資料，請<a class="personal">按此</a>。如果你已滿18歲及欲海洋公園在直接促銷中使用你的資料 (主要為你的姓名及聯絡詳情)，煩請在下列方格內加上剔號 “√”。</p>',
        marketing: '請向我提供直接促銷資訊。',
        send: '送出',
    },
    scanOther: '掃另一門票',
    checkMyCoupon: '檢查優惠券',
    backHome: '返回首頁',
    ruleTitle: '遊戲規則',
    ruleTitleSmall: '遊戲規則',
    ruleTc: '*受<a class="tc">條款及細則</a>約束。參加者須保留入場票正本以作領獎之用。推廣期由2019年1月26日至2月19日；海洋公園員工、香港海洋公園萬豪酒店員工及其家屬均不得參加以示公允。',
    ruleBtn: 'OK',
    ruleRemark: '',
    // ruleRemark: '參加者須保留入場 票正本以作領獎之用。推廣期由2019年1 26日至2月19日；參加者必須為年滿18歲。',
    congratulations: '恭喜您!',
    promotionCode: '推廣生意的競賽牌照號碼',
    giftDesc: '「{{gift}}」<br/>參考編號：{{number}}',
    inParkGiftDesc: '園內「{{gift}}」電子優惠券1張<br />參考編號：{{number}}',
    bigGiftDesc: '「{{gift}}」<br />參考編號：{{number}}',
    inParkRemark: '現金優惠券將自動加入此應用程式中「我的優惠券」頁面。如有任何問題,請記下上面顯示的參考編號並致電(852) 3923 2323查詢。祝您有愉快的一天!',
    remark: '閣下將收到得奬確認電郵。請於今天公園關門前親臨本公園獎品換領處(位於海濱樂園內的水都餅店旁)出示確認電郵並提供身份證明文件(以作核實用途)領奬。逾期恕不接受。',
    prizeRemark: '閣下將收到得奬確認電郵。請於今天公園關門前親臨本公園獎品換領處(位於海濱樂園內的水都餅店旁)出示確認電郵並提供身份證明文件(以作核實用途)領奬。逾期恕不接受。',
    reminder: '提提您:終極大獎將於2019年2月21日由電腦隨機方式抽出得獎者,得獎名單將於海洋公園官方網站公佈。得獎者將獲專人以電話於14個工作天內通知領獎。',
    ok: '確認',
    error: {
        requiredLoginUsername: '請輸入帳號名稱',
        requiredLoginPassword: '請輸入密碼',
        required: '*必須填寫',
        requiredCheckbox: '*請剔取',
        invalidEmail: '無效電郵，請重新輸入',
        incorrectConfirmEmail: '不正確，請重新輸入',
        incorrectIdLength: '不正確',
        incorrectIdCharacter: '不正確，必須全數字',
    },
},
sc: {
    loginForm: {
        username: '用户名称：',
        password: '密码：',
        login: '登入',
    },
    backHome: '返回首页',
    logout: '登出',
    language: '语言',
    newYearDraw: '"玩 · 住" 赏赢大奖',
    unavailableLuckyDraw: '抱歉，新年大抽奖并未开放。',
    drawForm: {
        name: '英文全名 *',
        namePlaceholder: '需与身份证明文件一样以作核实用途',
        id: '香港身份证/护照的头4个数字 *',
        email: '电邮地址 *',
        emailPlaceholder: '部份奖品换领信将获个别电邮发出得奖通知',
        confirmEmail: '确认电邮 *',
        invoiceNo: '收据号码 *',
        ticketNo: '门票编号 *',
        telephone: '手提电话号码 *',
        terms: '本人已清楚阅读并同意「玩 . 住」赏赢大奖活动的<a class="tc">条款及细则</a>。<br /><br /><p>有关海洋公园的个人资料收集声明及在直接促销中使用你的资料，请<a class="personal">按此</a>。如果你已满18岁及欲海洋公园在直接促销中使用你的资料 (主要为你的姓名及联络详情)，烦请在下列方格内加上剔号 “√”。</p>',
        marketing: '请向我提供直接促销资讯。',
        send: '送出',
    },
    scanOther: '扫另一门票',
    checkMyCoupon: '检查优惠券',
    ruleTitle: '游戏规则',
    ruleTitleSmall: '游戏规则',
    ruleTc: '*受<a class="tc">条款及细则</a>约束。参加者须保留入场票正本以作领奖之用。推广期由2019年1月26日至2月19日；海洋公园员工、香港海洋公园万豪酒店员工及其家属均不得参加以示公允。',
    ruleBtn: 'OK',
    ruleRemark: '',
    // ruleRemark: '参加者须保留入场门票正本以作领奖之用。推广期由2019年1月26日至2月19日;参加者必须为年满18岁。',
    promotionCode: '推广生意的竞赛牌照号码',
    congratulations: '恭喜您!',
    giftDesc: '「{{gift}}」<br/ >参考编号：{{number}}',
    inParkGiftDesc: '園內「{{gift}}」电子优惠券1张<br />参考编号：{{number}}',
    bigGiftDesc: '「{{gift}}」<br />参考编号：{{number}}',
    ok: '确认',
    inParkRemark: '现金优惠券将自动加入此应用程序中「我的优惠券」页面。如有任何问题,请记下上方显示的参考编号并致电(852) 3923 2323查询。祝您有愉快的一天!',
    remark: '阁下将收到得奖确认电邮。请于今天公园关门前亲临本公园奖品换领处（位于海滨乐园，水都饼店旁）出示确认电邮并提供身份证明文件（以作核实用途）领奖。逾期恕不接受。',
    prizeRemark: '阁下将收到得奖确认电邮。请于今天公园关门前亲临本公园奖品换领处（位于海滨乐园，水都饼店旁）出示确认电邮并提供身份证明文件（以作核实用途）领奖。逾期恕不接受。',
    reminder: '提提您:终极大奖将于2019年2月21日由电脑随机方式抽出得奖者,得奖名单将于海洋公园官方网站公布。得奖者将获专人以电话于14个工作天内通知领奖。',
    error: {
        requiredLoginUsername: '请输入帐号名称',
        requiredLoginPassword: '请输入密码',
        required: '*必须填写',
        requiredCheckbox: '*请剔取',
        invalidEmail: '无效电邮，请重新输入',
        incorrectConfirmEmail: '不正确，请重新输入',
        incorrectIdLength: '不正确',
        incorrectIdCharacter: '不正确，必须全数字',
    },
},
en: {
    loginForm: {
        username: 'Username:',
        password: 'Password:',
        login: 'Login',
    },
    backHome: 'Back To Home',
    logout: 'Logout',
    language: 'Lang',
    newYearDraw: 'Frenzy Instant Win',
    unavailableLuckyDraw: 'Sorry, the New Year Lucky Draw is not available now.',
    drawForm: {
        name: 'Full Name (English Only) *: ',
        namePlaceholder: 'As shown on HKID / passport and will be recorded for verification purposes',
        id: 'HKID/Passport First 4 Digits*: ',
        email: 'Email *: ',
        emailPlaceholder: 'some prize redemption letter will be notified individually through registered email',
        confirmEmail: 'Confirm Email *: ',
        invoiceNo: 'Invoice No *: ',
        ticketNo: 'Ticket No *: ',
        telephone: 'Mobile Phone *: ',
        terms: 'I have read and agree to the <a class="tc">Terms and Conditions</a> of this "Frenzy Instant Win".<br /><br /><p>Please <a class="personal">click here</a> for Ocean Park’s Personal Information Collection Statement and use of Your information in Direct Marketing. If you are aged above 18 and wish us to use Your information in direct marketing (primarily your name and contact details), please tick “√” at the box below.</p>',
        marketing: 'Please send direct marketing information to me.',
        send: 'Send',
    },
    ruleTitle: 'GAME RULES',
    ruleTitleSmall: 'Game Rules',
    ruleTc: '*<a class="tc">Terms and conditions </a> Apply. Participants must keep the original admission ticket/SmartFun Pass to redeem the prize. The promotion period is from Jan 26, 2019 to Feb 19, 2019. Ocean Park employees, Hong Kong Ocean Park Marriott Hotel employees and their relatives are not allowed to participate.',
    ruleBtn: 'OK',
    scanOther: 'Scan Next Ticket',
    checkMyCoupon: 'Check eCoupon',
    ruleRemark: '',
    // ruleRemark: 'Participants must be aged 18 or above. Participants must keep the original admission ticket/SmartFun Pass to redeem the prize. The promotion period is from Jan 26, 2019 to Feb 19, 2019.', 
    congratulations: 'Congratulations!',
    promotionCode: 'Trade Promotion Competition Licence Number: ',
    giftDesc: '「{{gift}}」<br/>Reference Code：{{number}}',
    inParkGiftDesc: '「{{gift}}」<br />ReferenceNo: {{number}}',
    bigGiftDesc: '「{{gift}}」<br />Reference Code：{{number}}',
    ok: 'OK',
    inParkRemark: 'The above e-coupon will automatically installed in this mobile app “My eCoupons” page. For any enquiries, please drop down the reference number above and contact us at (852) 3923 2323. Have a nice day!',
    remark: 'Please present this email notification and personal identification (for verification) at our Prize Redemption Center (near Aqua City Bakery in  Waterfront) on or before Park Close today to claim the prize. Claims after this date will not be accepted.',
    prizeRemark: 'Please present this email notification and personal identification (for verification) at our Prize Redemption Center (near Aqua City Bakery in  Waterfront) on or before Park Close today to claim the prize. Claims after this date will not be accepted.',
    reminder: 'Friendly reminder: The ultimate Grand Prizes draw will be conducted on 21 Feb, 2019 and winners will be drawn randomly by the computer system. The winner list will be announced on Hong Kong Ocean Park official website and winner of the Grand Prizes will be notified by phone within 14 working days.',
    error: {
        requiredLoginUsername: 'Enter username',
        requiredLoginPassword: 'Enter password',
        required: '*Required field',
        requiredCheckbox: '*Required field',
        invalidEmail: 'Invalid email, please re-enter.',
        incorrectConfirmEmail: 'Incorrect email, please re-enter.',
        incorrectIdLength: 'Incorrect length.',
        incorrectIdCharacter: 'Incorrect, only number',
    },
}
}