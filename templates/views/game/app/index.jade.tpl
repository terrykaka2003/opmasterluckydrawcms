extends ../../../layouts/common

block css
	link(href="/css/main.css", rel="stylesheet")

block head
	script(type='text/javascript').
		var keystoneConfig = {
			cookiesConfig: !{JSON.stringify(cookieOptions)},
			countries: !{JSON.stringify(countries)},
			setting: !{JSON.stringify(setting)},
			query: !{JSON.stringify(query)},
			inPark: true,
			native: true,
			/* nativeUse: true, /* disable all of login and cookie features */
			language: '#{language}',
			landingUrl: "#{landingUrl}",
			initialStep: #{initialStep},
			parkError: "#{parkError}",
			ticketCheckUrl: "#{ticketCheckUrl}",
			submitLotteryUrl: "#{submitLotteryUrl}",
			finishLotteryUrl: "#{finishLotteryUrl}",
			promotionCode: "#{promotionNo}",
		};
	// customized controller for web main flow

block js
	script(src='https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js')
	script(src='/script/jquery.jSlots.js')
	script(src='/script/jquery.easing.1.3.js')
	script(src='https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js')
	script(src='/script/native/app.js')
	script(src='/script/custom/common.js')
	script(src='/script/custom/game/function.js')
	
block content
	.g-native-module
		#page-draw-rule.page.container-fluid
			.bg
			.content
				h3.g-rule-title
				.g-rule-statement
				.g-rule-terms.terms
				.g-rule-remark
				button.ok.submit.g-native-camera-btn
			.g-promotion-code.last.bottom
				.g-promotion-label
				.g-promotion-value
		.g-h-main-container.app-view-page.page.container-fluid
			#unavailable-draw
			form#form-lucky-draw.form.g-h-form(action='#{action}', method='POST')
				label.g-name-label
				input(type='text', name='name', class='g-name-input', required='required', value='austerau')
				label.g-id-label
				input(type='text', maxlength='4', name='card', required='required', value='1234')
				label.g-email-label
				input(type='text', name='email', class='g-email-input', required='required', value='austerau@4d.com.hk')
				label.g-confirmEmail-label
				input(type='text', name='confirmEmail', required='required', value='austerau@4d.com.hk')
				label.g-telephone-label
				.telephone
					.styled-select
						select(name='country')
					input(type='text', name='phone', minlength='8', maxlength='15', required='required', value='38374355')
				label.g-ticketNo-label
				input(type='text', name='ticketNo', class='readonly', readonly='readonly', required='required', value='#{passNo}')
				#played-draw
				.required-label.g-required-label
				.terms.prev
					label.checkbox-terms
						input(type='checkbox', name='terms', required='required', checked='checked')
						i
						label.label-terms.g-terms-label
				.terms.prev
					label.checkbox-terms
						input(type='checkbox', name='marketing', checked='checked')
						i
						label.label-terms.g-terms-marking-label
				button#g-submit-btn.submit.send
				input(type='hidden', name='deviceId', value='#{deviceId}')
			.loader
			.g-promotion-code.bottom
				.g-promotion-label
				.g-promotion-value
		.popup-mask
			.terms-popup.popup
				i.cancel
				.content
		.page-draw-result.app-view-page
			.wrapper
				#coin
				#bg
				#popup.g-congrad-container.content
					.g-relative-container.gift.popup-wrapper
						.img-congrat
							img.g-congrad-head
						.g-congrad-center-pos.g-congrad-character.getWinner
							img.g-game-result-character
					.g-congrad-body.content
					.g-relative-container.g-congrad-footer
						img(src='/img/congrat_r_ft.png')
						button.g-congrad-center-pos.g-native-banhome-btn.submit.send
				.frame
				.white-bg
					.sub-bg
				.game-title
				.slot-wrapper
					ul.slot
					.button-group
						button#playBtn.show(type='button')
							img
						button#stopBtn(type='button')
							img
				.g-promotion-code.last.bottom
					.g-promotion-label
					.g-promotion-value
		