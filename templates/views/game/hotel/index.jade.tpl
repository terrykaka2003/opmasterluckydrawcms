extends ../../../layouts/common

block head
	script(type='text/javascript').
		var keystoneConfig = {
			cookies: "#{cookies.hotel}",
			appToken: "#{appToken}",
			langCookies: "#{cookies.hotelLang}",
			cookiesConfig: !{JSON.stringify(cookieOptions)},
			user: !{JSON.stringify(user)},
			countries: !{JSON.stringify(countries)},
			setting: !{JSON.stringify(setting)},
			singoutUrl: "#{signoutUrl}",
			userType: "#{userType}",
			landingUrl: "#{landingUrl}",
			submitLotteryUrl: "#{submitLotteryUrl}",
			finishLotteryUrl: "#{finishLotteryUrl}",
			promotionCode: "#{promotionNo}",
		};
	// customized controller for web main flow
	script(src='/script/custom/common.js')
	script(src='/script/custom/game/function.js')

block content
	.g-h-main-container.hotel-page.page.container-fluid
		header
			img.g-header-title-label
		#unavailable-draw
		form#form-lucky-draw.form.g-h-form(action='#{action}', method='POST')
			.button-box
				#g-logout-btn.logout.g-logout-btn
				#lang.styled-select.lang
					select
						option#1(value='en') Eng
						option#2(value='tc') 繁
						option#3(value='sc') 简
			label.g-name-label
			input(type='text', name='name', class='g-name-input', required='required', value='austerau')
			label.g-id-label
			input(type='text', name='card', maxlength='4', required='required', value='1234')
			label.g-email-label
			input(type='text', name='email', class='g-email-input', required='required', value='austerau@4d.com.hk')
			label.g-confirmEmail-label
			input(type='text', name='confirmEmail', required='required', value='austerau@4d.com.hk')
			label.g-invoiceNo-label
			input(type='text', name='invoiceNo', required='required', value='#{Math.random()}')
			// label.g-ticketNo-label
			// input(type='text', name='ticketNo', required='required')
			label.g-telephone-label
			.telephone
				.styled-select
					select(name='country')
				input(type='text', name='phone', minlength='8', maxlength='15', required='required', value='3436436645')
			.terms
				label.checkbox-terms
					input(type='checkbox', name='terms', required='required', checked='checked')
					i
					label.label-terms.g-terms-label
			.terms
				label.checkbox-terms
					input(type='checkbox', name='marketing', checked='checked')
					i
					label.label-terms.g-terms-marking-label
			button#g-submit-btn.submit.send
			.g-promotion-code.bottom
				.g-promotion-label
				.g-promotion-value
			input(type='hidden', name='type')
		.popup-mask
		.terms-popup.popup
			i.cancel
			.content
		.loader
	#page-draw.page.hotel-page.container-fluid
		.bg
		.game-title
		.g-coin-bg
		.g-congrad-container.content
			.g-relative-container.gift
				img.g-congrad-head
				.g-congrad-center-pos.g-congrad-character
					img.g-game-result-character
			.g-congrad-body
			.g-relative-container.g-congrad-footer
				img(src='/img/congrat_r_ft.png')
				button.g-congrad-center-pos.ok.submit.g-again-btn
		.g-promotion-code.last.bottom
			.g-promotion-label
			.g-promotion-value
		