//store the base path
global.__base = __dirname;
// "keystone": "git://github.com/thon0112/keystone.git#v0.3.x",
var Config = require(global.__base + '/config/Config');

var conf = new Config();

var AppEnv = require(global.__base + "/enum/AppEnv");

// var Helper = require(global.__base + "/helper/Utility");
// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
// require('dotenv').load();

// Require keystone
var keystone = require('keystone');

var dbHost = conf.dbHost;
if (!!(+process.env.CRON_ENV)) {
	dbHost = 'localhost'
}

var dbUrl = "mongodb://"+dbHost+"/"+conf.dbName;
if(conf.dbAuthEnable){
	dbUrl = "mongodb://"+conf.dbUser+":"+conf.dbPassword+"@"+dbHost+"/"+conf.dbName;
}


keystone.init({
	'name': 'OP Master Luckydraw CMS',
	'brand': 'OP Master Luckydraw CMS',
	'mongo': dbUrl,
	'port': conf.port,
	'sass': 'public',
	'static': ['public', 'media'],
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'global upload dir': global.__base+'/uploads/',
	'view engine': 'jade',
	'auto update': true,	
	'session': true,
	'session store': 'mongo',
    'session store options': {
        ttl: 60 * 60,
    },
	'auth': true,
	'user model': 'User',
	'cookie frontend language options': {
		// httpOnly: true,
		path: '/',
	},
	'cookie secret': '?a++Z~EbmTggbs(Fn4)<R^@tf-ryPTo7Ix@oIZ3,]xSHCf;MaB"m=CP>*}#@6{L&',
});

keystone.import('models');

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	userManagemnt: [
        'Role',
       	'User',
    ],
	'GameResources': ['Staff', 'CountryList', 'GameCharacter', 'BigPrize', 'Prize', 'Coupon', 'RedeemCodePool'],
	'Game': [
		{
			label: 'Game Setting',
			key: 'LotterySetting',
			path:'/keystone/lottery-settings/5c30f1ab126524546c85ab31'
		},
		'LotteryHistory', 'PrizePool', 'CouponPool',
		{
			label: 'Ultimate Luckydraw',
			key: 'DrawForm',
			path:'/keystone/draw-forms/5c386ccab813ec4403abe534'
		}
	],
	'RedeemPrizes': ['RedeemLotteryHistory'],
	'SystemLog': ['EligibilityLog', 'LotteryDrawnLog', 'AuditTrail'],
});

var	Role = require(global.__base + "/models/user/Role");

var registerRole = function(){
	Role.registerRole();
}

keystone.start({
	onHttpsServerCreated:function(){
		// if(!cacheCalled){
		// 	cacheCalled = true;
		// 	cacheData();
		// }
		// startCron();
		// registerRole();
	},
	onHttpServerCreated:function(){
		registerRole();
		if (!!(+process.env.CRON_ENV)) {
			startCron();
		}
		// startCron();
		console.log('> Started.');
		// new MonitorJob().start();
	}
});



const PrizeDistributor = require(global.__base + "/services/Distributor/prize");
const CouponDistributor = require(global.__base + "/services/Distributor/coupon");
const PrizeRecycler = require(global.__base + "/services/Recycler/prize");
const CouponRecycler = require(global.__base + "/services/Recycler/coupon");
const EligibilityLogRecycler = require(global.__base + "/services/Recycler/log/eligibility");

// var Utility = require(global.__base + "/helper/Utility");
// Utility.getBarcode('35223523', function(err, src) {
// 	console.log(err, src);
// });

/*
** Add dummy ecoupon for testing only
*/
// var DrawDormController		= require(global.__base + '/models/game/bigGame/drawForm');
// DrawDormController 			= new DrawDormController();
// DrawDormController.addCandidate('',function(err, counted) {
// 	console.log(err, counted);
// })
var CouponPoolController		= require(global.__base + '/models/game/couponPool');
var CouponController		= require(global.__base + '/models/game/coupon');
var PrizeController 		= require(global.__base + '/models/game/prize');
var PrizePoolController 	= require(global.__base + '/models/game/prizePool');
var CountryController 		= require(global.__base + '/models/game/countryList');
CouponController				= new CouponController();
CouponPoolController 			= new CouponPoolController();
PrizeController					= new PrizeController();
PrizePoolController				= new PrizePoolController();
CountryController   			= new CountryController();
var RedeemCodePoolController = require(global.__base + '/models/game/redeemCodePool');
RedeemCodePoolController = new RedeemCodePoolController();
const async = require('async');
const _ = require('lodash');
const mongoose = require('mongoose');
const moment = require('moment');
var tasks = [];
var referenceStart = conf.referenceStart;
const referenceNoPrefix = conf.referenceNoPrefix;
const referencePrefix = conf.referencePrefix;
const referenceNoPostfix = conf.referenceNoPostfix;



// CouponPoolController.updateForDate(function(err, result) {
// 	console.log('update coupon pool for today');
// });
// PrizePoolController.updateForDate(function(err, result) {
// 	console.log('update prize pool for today');
// });

// const code1 = [
//   {
//     "CouponNo": "19MA22903FA010000013"
//   },
//   {
//     "CouponNo": "19MA22903FA010000022"
//   },
//   {
//     "CouponNo": "19MA22903FA010000030"
//   },
//   {
//     "CouponNo": "19MA22903FA010000040"
//   },
//   {
//     "CouponNo": "19MA22903FA010000052"
//   },
//   {
//     "CouponNo": "19MA22903FA010000066"
//   },
//   {
//     "CouponNo": "19MA22903FA010000072"
//   },
//   {
//     "CouponNo": "19MA22903FA010000080"
//   },
//   {
//     "CouponNo": "19MA22903FA010000090"
//   },
//   {
//     "CouponNo": "19MA22903FA010000102"
//   },
//   {
//     "CouponNo": "19MA22903FA010000111"
//   },
//   {
//     "CouponNo": "19MA22903FA010000129"
//   },
//   {
//     "CouponNo": "19MA22903FA010000139"
//   },
//   {
//     "CouponNo": "19MA22903FA010000141"
//   },
//   {
//     "CouponNo": "19MA22903FA010000155"
//   },
//   {
//     "CouponNo": "19MA22903FA010000161"
//   },
//   {
//     "CouponNo": "19MA22903FA010000179"
//   },
//   {
//     "CouponNo": "19MA22903FA010000189"
//   },
//   {
//     "CouponNo": "19MA22903FA010000191"
//   },
//   {
//     "CouponNo": "19MA22903FA010000200"
//   },
//   {
//     "CouponNo": "19MA22903FA010000218"
//   },
//   {
//     "CouponNo": "19MA22903FA010000228"
//   },
//   {
//     "CouponNo": "19MA22903FA010000230"
//   },
//   {
//     "CouponNo": "19MA22903FA010000244"
//   },
//   {
//     "CouponNo": "19MA22903FA010000250"
//   },
//   {
//     "CouponNo": "19MA22903FA010000268"
//   },
//   {
//     "CouponNo": "19MA22903FA010000278"
//   },
//   {
//     "CouponNo": "19MA22903FA010000280"
//   },
//   {
//     "CouponNo": "19MA22903FA010000294"
//   },
//   {
//     "CouponNo": "19MA22903FA010000307"
//   },
//   {
//     "CouponNo": "19MA22903FA010000317"
//   },
//   {
//     "CouponNo": "19MA22903FA010000329"
//   },
//   {
//     "CouponNo": "19MA22903FA010000333"
//   },
//   {
//     "CouponNo": "19MA22903FA010000349"
//   },
//   {
//     "CouponNo": "19MA22903FA010000357"
//   },
//   {
//     "CouponNo": "19MA22903FA010000367"
//   },
//   {
//     "CouponNo": "19MA22903FA010000379"
//   },
//   {
//     "CouponNo": "19MA22903FA010000383"
//   },
//   {
//     "CouponNo": "19MA22903FA010000399"
//   },
//   {
//     "CouponNo": "19MA22903FA010000406"
//   },
//   {
//     "CouponNo": "19MA22903FA010000418"
//   },
//   {
//     "CouponNo": "19MA22903FA010000422"
//   },
//   {
//     "CouponNo": "19MA22903FA010000438"
//   },
//   {
//     "CouponNo": "19MA22903FA010000446"
//   },
//   {
//     "CouponNo": "19MA22903FA010000456"
//   },
//   {
//     "CouponNo": "19MA22903FA010000468"
//   },
//   {
//     "CouponNo": "19MA22903FA010000472"
//   },
//   {
//     "CouponNo": "19MA22903FA010000488"
//   },
//   {
//     "CouponNo": "19MA22903FA010000496"
//   },
//   {
//     "CouponNo": "19MA22903FA010000507"
//   },
//   {
//     "CouponNo": "19MA22903FA010000511"
//   },
//   {
//     "CouponNo": "19MA22903FA010000527"
//   },
//   {
//     "CouponNo": "19MA22903FA010000535"
//   },
//   {
//     "CouponNo": "19MA22903FA010000545"
//   },
//   {
//     "CouponNo": "19MA22903FA010000557"
//   },
//   {
//     "CouponNo": "19MA22903FA010000561"
//   },
//   {
//     "CouponNo": "19MA22903FA010000577"
//   },
//   {
//     "CouponNo": "19MA22903FA010000585"
//   },
//   {
//     "CouponNo": "19MA22903FA010000595"
//   },
//   {
//     "CouponNo": "19MA22903FA010000600"
//   },
//   {
//     "CouponNo": "19MA22903FA010000616"
//   },
//   {
//     "CouponNo": "19MA22903FA010000624"
//   },
//   {
//     "CouponNo": "19MA22903FA010000634"
//   },
//   {
//     "CouponNo": "19MA22903FA010000646"
//   },
//   {
//     "CouponNo": "19MA22903FA010000650"
//   },
//   {
//     "CouponNo": "19MA22903FA010000666"
//   },
//   {
//     "CouponNo": "19MA22903FA010000674"
//   },
//   {
//     "CouponNo": "19MA22903FA010000684"
//   },
//   {
//     "CouponNo": "19MA22903FA010000696"
//   },
//   {
//     "CouponNo": "19MA22903FA010000705"
//   },
//   {
//     "CouponNo": "19MA22903FA010000713"
//   },
//   {
//     "CouponNo": "19MA22903FA010000723"
//   },
//   {
//     "CouponNo": "19MA22903FA010000735"
//   },
//   {
//     "CouponNo": "19MA22903FA010000749"
//   },
//   {
//     "CouponNo": "19MA22903FA010000755"
//   },
//   {
//     "CouponNo": "19MA22903FA010000763"
//   },
//   {
//     "CouponNo": "19MA22903FA010000773"
//   },
//   {
//     "CouponNo": "19MA22903FA010000785"
//   },
//   {
//     "CouponNo": "19MA22903FA010000799"
//   },
//   {
//     "CouponNo": "19MA22903FA010000802"
//   },
//   {
//     "CouponNo": "19MA22903FA010000812"
//   },
//   {
//     "CouponNo": "19MA22903FA010000824"
//   },
//   {
//     "CouponNo": "19MA22903FA010000838"
//   },
//   {
//     "CouponNo": "19MA22903FA010000844"
//   },
//   {
//     "CouponNo": "19MA22903FA010000852"
//   },
//   {
//     "CouponNo": "19MA22903FA010000862"
//   },
//   {
//     "CouponNo": "19MA22903FA010000874"
//   },
//   {
//     "CouponNo": "19MA22903FA010000888"
//   },
//   {
//     "CouponNo": "19MA22903FA010000894"
//   },
//   {
//     "CouponNo": "19MA22903FA010000901"
//   },
//   {
//     "CouponNo": "19MA22903FA010000913"
//   },
//   {
//     "CouponNo": "19MA22903FA010000927"
//   },
//   {
//     "CouponNo": "19MA22903FA010000933"
//   },
//   {
//     "CouponNo": "19MA22903FA010000941"
//   },
//   {
//     "CouponNo": "19MA22903FA010000951"
//   },
//   {
//     "CouponNo": "19MA22903FA010000963"
//   },
//   {
//     "CouponNo": "19MA22903FA010000977"
//   },
//   {
//     "CouponNo": "19MA22903FA010000983"
//   },
//   {
//     "CouponNo": "19MA22903FA010000991"
//   },
//   {
//     "CouponNo": "19MA22903FA010001002"
//   }
// ];

// _.map(code1, function(code, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c1'),
// 			null,
// 			code.CouponNo,
// 			cb
// 		);
// 	});
// });

// const code2 = [
//   {
//     "CouponNo": "19MA22903FA020000011"
//   },
//   {
//     "CouponNo": "19MA22903FA020000020"
//   },
//   {
//     "CouponNo": "19MA22903FA020000031"
//   },
//   {
//     "CouponNo": "19MA22903FA020000044"
//   },
//   {
//     "CouponNo": "19MA22903FA020000059"
//   },
//   {
//     "CouponNo": "19MA22903FA020000066"
//   },
//   {
//     "CouponNo": "19MA22903FA020000075"
//   },
//   {
//     "CouponNo": "19MA22903FA020000086"
//   },
//   {
//     "CouponNo": "19MA22903FA020000099"
//   },
//   {
//     "CouponNo": "19MA22903FA020000100"
//   },
//   {
//     "CouponNo": "19MA22903FA020000119"
//   },
//   {
//     "CouponNo": "19MA22903FA020000120"
//   },
//   {
//     "CouponNo": "19MA22903FA020000133"
//   },
//   {
//     "CouponNo": "19MA22903FA020000148"
//   },
//   {
//     "CouponNo": "19MA22903FA020000155"
//   },
//   {
//     "CouponNo": "19MA22903FA020000164"
//   },
//   {
//     "CouponNo": "19MA22903FA020000175"
//   },
//   {
//     "CouponNo": "19MA22903FA020000188"
//   },
//   {
//     "CouponNo": "19MA22903FA020000193"
//   },
//   {
//     "CouponNo": "19MA22903FA020000208"
//   },
//   {
//     "CouponNo": "19MA22903FA020000219"
//   },
//   {
//     "CouponNo": "19MA22903FA020000222"
//   },
//   {
//     "CouponNo": "19MA22903FA020000237"
//   },
//   {
//     "CouponNo": "19MA22903FA020000244"
//   },
//   {
//     "CouponNo": "19MA22903FA020000253"
//   },
//   {
//     "CouponNo": "19MA22903FA020000264"
//   },
//   {
//     "CouponNo": "19MA22903FA020000277"
//   },
//   {
//     "CouponNo": "19MA22903FA020000282"
//   },
//   {
//     "CouponNo": "19MA22903FA020000299"
//   },
//   {
//     "CouponNo": "19MA22903FA020000308"
//   },
//   {
//     "CouponNo": "19MA22903FA020000311"
//   },
//   {
//     "CouponNo": "19MA22903FA020000326"
//   },
//   {
//     "CouponNo": "19MA22903FA020000333"
//   },
//   {
//     "CouponNo": "19MA22903FA020000342"
//   },
//   {
//     "CouponNo": "19MA22903FA020000353"
//   },
//   {
//     "CouponNo": "19MA22903FA020000366"
//   },
//   {
//     "CouponNo": "19MA22903FA020000371"
//   },
//   {
//     "CouponNo": "19MA22903FA020000388"
//   },
//   {
//     "CouponNo": "19MA22903FA020000397"
//   },
//   {
//     "CouponNo": "19MA22903FA020000400"
//   },
//   {
//     "CouponNo": "19MA22903FA020000415"
//   },
//   {
//     "CouponNo": "19MA22903FA020000422"
//   },
//   {
//     "CouponNo": "19MA22903FA020000431"
//   },
//   {
//     "CouponNo": "19MA22903FA020000442"
//   },
//   {
//     "CouponNo": "19MA22903FA020000455"
//   },
//   {
//     "CouponNo": "19MA22903FA020000460"
//   },
//   {
//     "CouponNo": "19MA22903FA020000477"
//   },
//   {
//     "CouponNo": "19MA22903FA020000486"
//   },
//   {
//     "CouponNo": "19MA22903FA020000497"
//   },
//   {
//     "CouponNo": "19MA22903FA020000504"
//   },
//   {
//     "CouponNo": "19MA22903FA020000511"
//   },
//   {
//     "CouponNo": "19MA22903FA020000520"
//   },
//   {
//     "CouponNo": "19MA22903FA020000531"
//   },
//   {
//     "CouponNo": "19MA22903FA020000544"
//   },
//   {
//     "CouponNo": "19MA22903FA020000559"
//   },
//   {
//     "CouponNo": "19MA22903FA020000566"
//   },
//   {
//     "CouponNo": "19MA22903FA020000575"
//   },
//   {
//     "CouponNo": "19MA22903FA020000586"
//   },
//   {
//     "CouponNo": "19MA22903FA020000599"
//   },
//   {
//     "CouponNo": "19MA22903FA020000600"
//   },
//   {
//     "CouponNo": "19MA22903FA020000619"
//   },
//   {
//     "CouponNo": "19MA22903FA020000620"
//   },
//   {
//     "CouponNo": "19MA22903FA020000633"
//   },
//   {
//     "CouponNo": "19MA22903FA020000648"
//   },
//   {
//     "CouponNo": "19MA22903FA020000655"
//   },
//   {
//     "CouponNo": "19MA22903FA020000664"
//   },
//   {
//     "CouponNo": "19MA22903FA020000675"
//   },
//   {
//     "CouponNo": "19MA22903FA020000688"
//   },
//   {
//     "CouponNo": "19MA22903FA020000693"
//   },
//   {
//     "CouponNo": "19MA22903FA020000708"
//   },
//   {
//     "CouponNo": "19MA22903FA020000719"
//   },
//   {
//     "CouponNo": "19MA22903FA020000722"
//   },
//   {
//     "CouponNo": "19MA22903FA020000737"
//   },
//   {
//     "CouponNo": "19MA22903FA020000744"
//   },
//   {
//     "CouponNo": "19MA22903FA020000753"
//   },
//   {
//     "CouponNo": "19MA22903FA020000764"
//   },
//   {
//     "CouponNo": "19MA22903FA020000777"
//   },
//   {
//     "CouponNo": "19MA22903FA020000782"
//   },
//   {
//     "CouponNo": "19MA22903FA020000799"
//   },
//   {
//     "CouponNo": "19MA22903FA020000808"
//   },
//   {
//     "CouponNo": "19MA22903FA020000811"
//   },
//   {
//     "CouponNo": "19MA22903FA020000826"
//   },
//   {
//     "CouponNo": "19MA22903FA020000833"
//   },
//   {
//     "CouponNo": "19MA22903FA020000842"
//   },
//   {
//     "CouponNo": "19MA22903FA020000853"
//   },
//   {
//     "CouponNo": "19MA22903FA020000866"
//   },
//   {
//     "CouponNo": "19MA22903FA020000871"
//   },
//   {
//     "CouponNo": "19MA22903FA020000888"
//   },
//   {
//     "CouponNo": "19MA22903FA020000897"
//   },
//   {
//     "CouponNo": "19MA22903FA020000900"
//   },
//   {
//     "CouponNo": "19MA22903FA020000915"
//   },
//   {
//     "CouponNo": "19MA22903FA020000922"
//   },
//   {
//     "CouponNo": "19MA22903FA020000931"
//   },
//   {
//     "CouponNo": "19MA22903FA020000942"
//   },
//   {
//     "CouponNo": "19MA22903FA020000955"
//   },
//   {
//     "CouponNo": "19MA22903FA020000960"
//   },
//   {
//     "CouponNo": "19MA22903FA020000977"
//   },
//   {
//     "CouponNo": "19MA22903FA020000986"
//   },
//   {
//     "CouponNo": "19MA22903FA020000997"
//   },
//   {
//     "CouponNo": "19MA22903FA020001000"
//   }
// ];

// _.map(code2, function(code, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c2'),
// 			null,
// 			code.CouponNo,
// 			cb
// 		);
// 	});
// });

// const code3 = [
//   {
//     "CouponNo": "19MA22903FA030000010"
//   },
//   {
//     "CouponNo": "19MA22903FA030000022"
//   },
//   {
//     "CouponNo": "19MA22903FA030000036"
//   },
//   {
//     "CouponNo": "19MA22903FA030000042"
//   },
//   {
//     "CouponNo": "19MA22903FA030000050"
//   },
//   {
//     "CouponNo": "19MA22903FA030000060"
//   },
//   {
//     "CouponNo": "19MA22903FA030000072"
//   },
//   {
//     "CouponNo": "19MA22903FA030000086"
//   },
//   {
//     "CouponNo": "19MA22903FA030000092"
//   },
//   {
//     "CouponNo": "19MA22903FA030000109"
//   },
//   {
//     "CouponNo": "19MA22903FA030000111"
//   },
//   {
//     "CouponNo": "19MA22903FA030000125"
//   },
//   {
//     "CouponNo": "19MA22903FA030000131"
//   },
//   {
//     "CouponNo": "19MA22903FA030000149"
//   },
//   {
//     "CouponNo": "19MA22903FA030000159"
//   },
//   {
//     "CouponNo": "19MA22903FA030000161"
//   },
//   {
//     "CouponNo": "19MA22903FA030000175"
//   },
//   {
//     "CouponNo": "19MA22903FA030000181"
//   },
//   {
//     "CouponNo": "19MA22903FA030000199"
//   },
//   {
//     "CouponNo": "19MA22903FA030000200"
//   },
//   {
//     "CouponNo": "19MA22903FA030000214"
//   },
//   {
//     "CouponNo": "19MA22903FA030000220"
//   },
//   {
//     "CouponNo": "19MA22903FA030000238"
//   },
//   {
//     "CouponNo": "19MA22903FA030000248"
//   },
//   {
//     "CouponNo": "19MA22903FA030000250"
//   },
//   {
//     "CouponNo": "19MA22903FA030000264"
//   },
//   {
//     "CouponNo": "19MA22903FA030000270"
//   },
//   {
//     "CouponNo": "19MA22903FA030000288"
//   },
//   {
//     "CouponNo": "19MA22903FA030000298"
//   },
//   {
//     "CouponNo": "19MA22903FA030000303"
//   },
//   {
//     "CouponNo": "19MA22903FA030000319"
//   },
//   {
//     "CouponNo": "19MA22903FA030000327"
//   },
//   {
//     "CouponNo": "19MA22903FA030000337"
//   },
//   {
//     "CouponNo": "19MA22903FA030000349"
//   },
//   {
//     "CouponNo": "19MA22903FA030000353"
//   },
//   {
//     "CouponNo": "19MA22903FA030000369"
//   },
//   {
//     "CouponNo": "19MA22903FA030000377"
//   },
//   {
//     "CouponNo": "19MA22903FA030000387"
//   },
//   {
//     "CouponNo": "19MA22903FA030000399"
//   },
//   {
//     "CouponNo": "19MA22903FA030000408"
//   },
//   {
//     "CouponNo": "19MA22903FA030000416"
//   },
//   {
//     "CouponNo": "19MA22903FA030000426"
//   },
//   {
//     "CouponNo": "19MA22903FA030000438"
//   },
//   {
//     "CouponNo": "19MA22903FA030000442"
//   },
//   {
//     "CouponNo": "19MA22903FA030000458"
//   },
//   {
//     "CouponNo": "19MA22903FA030000466"
//   },
//   {
//     "CouponNo": "19MA22903FA030000476"
//   },
//   {
//     "CouponNo": "19MA22903FA030000488"
//   },
//   {
//     "CouponNo": "19MA22903FA030000492"
//   },
//   {
//     "CouponNo": "19MA22903FA030000505"
//   },
//   {
//     "CouponNo": "19MA22903FA030000515"
//   },
//   {
//     "CouponNo": "19MA22903FA030000527"
//   },
//   {
//     "CouponNo": "19MA22903FA030000531"
//   },
//   {
//     "CouponNo": "19MA22903FA030000547"
//   },
//   {
//     "CouponNo": "19MA22903FA030000555"
//   },
//   {
//     "CouponNo": "19MA22903FA030000565"
//   },
//   {
//     "CouponNo": "19MA22903FA030000577"
//   },
//   {
//     "CouponNo": "19MA22903FA030000581"
//   },
//   {
//     "CouponNo": "19MA22903FA030000597"
//   },
//   {
//     "CouponNo": "19MA22903FA030000604"
//   },
//   {
//     "CouponNo": "19MA22903FA030000616"
//   },
//   {
//     "CouponNo": "19MA22903FA030000620"
//   },
//   {
//     "CouponNo": "19MA22903FA030000636"
//   },
//   {
//     "CouponNo": "19MA22903FA030000644"
//   },
//   {
//     "CouponNo": "19MA22903FA030000654"
//   },
//   {
//     "CouponNo": "19MA22903FA030000666"
//   },
//   {
//     "CouponNo": "19MA22903FA030000670"
//   },
//   {
//     "CouponNo": "19MA22903FA030000686"
//   },
//   {
//     "CouponNo": "19MA22903FA030000694"
//   },
//   {
//     "CouponNo": "19MA22903FA030000705"
//   },
//   {
//     "CouponNo": "19MA22903FA030000719"
//   },
//   {
//     "CouponNo": "19MA22903FA030000725"
//   },
//   {
//     "CouponNo": "19MA22903FA030000733"
//   },
//   {
//     "CouponNo": "19MA22903FA030000743"
//   },
//   {
//     "CouponNo": "19MA22903FA030000755"
//   },
//   {
//     "CouponNo": "19MA22903FA030000769"
//   },
//   {
//     "CouponNo": "19MA22903FA030000775"
//   },
//   {
//     "CouponNo": "19MA22903FA030000783"
//   },
//   {
//     "CouponNo": "19MA22903FA030000793"
//   },
//   {
//     "CouponNo": "19MA22903FA030000808"
//   },
//   {
//     "CouponNo": "19MA22903FA030000814"
//   },
//   {
//     "CouponNo": "19MA22903FA030000822"
//   },
//   {
//     "CouponNo": "19MA22903FA030000832"
//   },
//   {
//     "CouponNo": "19MA22903FA030000844"
//   },
//   {
//     "CouponNo": "19MA22903FA030000858"
//   },
//   {
//     "CouponNo": "19MA22903FA030000864"
//   },
//   {
//     "CouponNo": "19MA22903FA030000872"
//   },
//   {
//     "CouponNo": "19MA22903FA030000882"
//   },
//   {
//     "CouponNo": "19MA22903FA030000894"
//   },
//   {
//     "CouponNo": "19MA22903FA030000903"
//   },
//   {
//     "CouponNo": "19MA22903FA030000911"
//   },
//   {
//     "CouponNo": "19MA22903FA030000921"
//   },
//   {
//     "CouponNo": "19MA22903FA030000933"
//   },
//   {
//     "CouponNo": "19MA22903FA030000947"
//   },
//   {
//     "CouponNo": "19MA22903FA030000953"
//   },
//   {
//     "CouponNo": "19MA22903FA030000961"
//   },
//   {
//     "CouponNo": "19MA22903FA030000971"
//   },
//   {
//     "CouponNo": "19MA22903FA030000983"
//   },
//   {
//     "CouponNo": "19MA22903FA030000997"
//   },
//   {
//     "CouponNo": "19MA22903FA030001009"
//   }
// ];

// _.map(code3, function(code, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c3'),
// 			null,
// 			code.CouponNo,
// 			cb
// 		);
// 	});
// });

// const code4 = [
//   {
//     "CouponNo": "19MA22903FB010000011"
//   },
//   {
//     "CouponNo": "19MA22903FB010000020"
//   },
//   {
//     "CouponNo": "19MA22903FB010000031"
//   },
//   {
//     "CouponNo": "19MA22903FB010000044"
//   },
//   {
//     "CouponNo": "19MA22903FB010000059"
//   },
//   {
//     "CouponNo": "19MA22903FB010000066"
//   },
//   {
//     "CouponNo": "19MA22903FB010000075"
//   },
//   {
//     "CouponNo": "19MA22903FB010000086"
//   },
//   {
//     "CouponNo": "19MA22903FB010000099"
//   },
//   {
//     "CouponNo": "19MA22903FB010000100"
//   },
//   {
//     "CouponNo": "19MA22903FB010000119"
//   },
//   {
//     "CouponNo": "19MA22903FB010000120"
//   },
//   {
//     "CouponNo": "19MA22903FB010000133"
//   },
//   {
//     "CouponNo": "19MA22903FB010000148"
//   },
//   {
//     "CouponNo": "19MA22903FB010000155"
//   },
//   {
//     "CouponNo": "19MA22903FB010000164"
//   },
//   {
//     "CouponNo": "19MA22903FB010000175"
//   },
//   {
//     "CouponNo": "19MA22903FB010000188"
//   },
//   {
//     "CouponNo": "19MA22903FB010000193"
//   },
//   {
//     "CouponNo": "19MA22903FB010000208"
//   },
//   {
//     "CouponNo": "19MA22903FB010000219"
//   },
//   {
//     "CouponNo": "19MA22903FB010000222"
//   },
//   {
//     "CouponNo": "19MA22903FB010000237"
//   },
//   {
//     "CouponNo": "19MA22903FB010000244"
//   },
//   {
//     "CouponNo": "19MA22903FB010000253"
//   },
//   {
//     "CouponNo": "19MA22903FB010000264"
//   },
//   {
//     "CouponNo": "19MA22903FB010000277"
//   },
//   {
//     "CouponNo": "19MA22903FB010000282"
//   },
//   {
//     "CouponNo": "19MA22903FB010000299"
//   },
//   {
//     "CouponNo": "19MA22903FB010000308"
//   },
//   {
//     "CouponNo": "19MA22903FB010000311"
//   },
//   {
//     "CouponNo": "19MA22903FB010000326"
//   },
//   {
//     "CouponNo": "19MA22903FB010000333"
//   },
//   {
//     "CouponNo": "19MA22903FB010000342"
//   },
//   {
//     "CouponNo": "19MA22903FB010000353"
//   },
//   {
//     "CouponNo": "19MA22903FB010000366"
//   },
//   {
//     "CouponNo": "19MA22903FB010000371"
//   },
//   {
//     "CouponNo": "19MA22903FB010000388"
//   },
//   {
//     "CouponNo": "19MA22903FB010000397"
//   },
//   {
//     "CouponNo": "19MA22903FB010000400"
//   },
//   {
//     "CouponNo": "19MA22903FB010000415"
//   },
//   {
//     "CouponNo": "19MA22903FB010000422"
//   },
//   {
//     "CouponNo": "19MA22903FB010000431"
//   },
//   {
//     "CouponNo": "19MA22903FB010000442"
//   },
//   {
//     "CouponNo": "19MA22903FB010000455"
//   },
//   {
//     "CouponNo": "19MA22903FB010000460"
//   },
//   {
//     "CouponNo": "19MA22903FB010000477"
//   },
//   {
//     "CouponNo": "19MA22903FB010000486"
//   },
//   {
//     "CouponNo": "19MA22903FB010000497"
//   },
//   {
//     "CouponNo": "19MA22903FB010000504"
//   },
//   {
//     "CouponNo": "19MA22903FB010000511"
//   },
//   {
//     "CouponNo": "19MA22903FB010000520"
//   },
//   {
//     "CouponNo": "19MA22903FB010000531"
//   },
//   {
//     "CouponNo": "19MA22903FB010000544"
//   },
//   {
//     "CouponNo": "19MA22903FB010000559"
//   },
//   {
//     "CouponNo": "19MA22903FB010000566"
//   },
//   {
//     "CouponNo": "19MA22903FB010000575"
//   },
//   {
//     "CouponNo": "19MA22903FB010000586"
//   },
//   {
//     "CouponNo": "19MA22903FB010000599"
//   },
//   {
//     "CouponNo": "19MA22903FB010000600"
//   },
//   {
//     "CouponNo": "19MA22903FB010000619"
//   },
//   {
//     "CouponNo": "19MA22903FB010000620"
//   },
//   {
//     "CouponNo": "19MA22903FB010000633"
//   },
//   {
//     "CouponNo": "19MA22903FB010000648"
//   },
//   {
//     "CouponNo": "19MA22903FB010000655"
//   },
//   {
//     "CouponNo": "19MA22903FB010000664"
//   },
//   {
//     "CouponNo": "19MA22903FB010000675"
//   },
//   {
//     "CouponNo": "19MA22903FB010000688"
//   },
//   {
//     "CouponNo": "19MA22903FB010000693"
//   },
//   {
//     "CouponNo": "19MA22903FB010000708"
//   },
//   {
//     "CouponNo": "19MA22903FB010000719"
//   },
//   {
//     "CouponNo": "19MA22903FB010000722"
//   },
//   {
//     "CouponNo": "19MA22903FB010000737"
//   },
//   {
//     "CouponNo": "19MA22903FB010000744"
//   },
//   {
//     "CouponNo": "19MA22903FB010000753"
//   },
//   {
//     "CouponNo": "19MA22903FB010000764"
//   },
//   {
//     "CouponNo": "19MA22903FB010000777"
//   },
//   {
//     "CouponNo": "19MA22903FB010000782"
//   },
//   {
//     "CouponNo": "19MA22903FB010000799"
//   },
//   {
//     "CouponNo": "19MA22903FB010000808"
//   },
//   {
//     "CouponNo": "19MA22903FB010000811"
//   },
//   {
//     "CouponNo": "19MA22903FB010000826"
//   },
//   {
//     "CouponNo": "19MA22903FB010000833"
//   },
//   {
//     "CouponNo": "19MA22903FB010000842"
//   },
//   {
//     "CouponNo": "19MA22903FB010000853"
//   },
//   {
//     "CouponNo": "19MA22903FB010000866"
//   },
//   {
//     "CouponNo": "19MA22903FB010000871"
//   },
//   {
//     "CouponNo": "19MA22903FB010000888"
//   },
//   {
//     "CouponNo": "19MA22903FB010000897"
//   },
//   {
//     "CouponNo": "19MA22903FB010000900"
//   },
//   {
//     "CouponNo": "19MA22903FB010000915"
//   },
//   {
//     "CouponNo": "19MA22903FB010000922"
//   },
//   {
//     "CouponNo": "19MA22903FB010000931"
//   },
//   {
//     "CouponNo": "19MA22903FB010000942"
//   },
//   {
//     "CouponNo": "19MA22903FB010000955"
//   },
//   {
//     "CouponNo": "19MA22903FB010000960"
//   },
//   {
//     "CouponNo": "19MA22903FB010000977"
//   },
//   {
//     "CouponNo": "19MA22903FB010000986"
//   },
//   {
//     "CouponNo": "19MA22903FB010000997"
//   },
//   {
//     "CouponNo": "19MA22903FB010001000"
//   }
// ];

// _.map(code4, function(code, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c4'),
// 			null,
// 			code.CouponNo,
// 			cb
// 		);
// 	});
// });

// const code5 = [
//   {
//     "CouponNo": "19MA22903RC010000010"
//   },
//   {
//     "CouponNo": "19MA22903RC010000028"
//   },
//   {
//     "CouponNo": "19MA22903RC010000038"
//   },
//   {
//     "CouponNo": "19MA22903RC010000040"
//   },
//   {
//     "CouponNo": "19MA22903RC010000054"
//   },
//   {
//     "CouponNo": "19MA22903RC010000060"
//   },
//   {
//     "CouponNo": "19MA22903RC010000077"
//   },
//   {
//     "CouponNo": "19MA22903RC010000089"
//   },
//   {
//     "CouponNo": "19MA22903RC010000093"
//   },
//   {
//     "CouponNo": "19MA22903RC010000109"
//   },
//   {
//     "CouponNo": "19MA22903RC010000117"
//   },
//   {
//     "CouponNo": "19MA22903RC010000127"
//   },
//   {
//     "CouponNo": "19MA22903RC010000139"
//   },
//   {
//     "CouponNo": "19MA22903RC010000143"
//   },
//   {
//     "CouponNo": "19MA22903RC010000159"
//   },
//   {
//     "CouponNo": "19MA22903RC010000166"
//   },
//   {
//     "CouponNo": "19MA22903RC010000178"
//   },
//   {
//     "CouponNo": "19MA22903RC010000182"
//   },
//   {
//     "CouponNo": "19MA22903RC010000198"
//   },
//   {
//     "CouponNo": "19MA22903RC010000206"
//   },
//   {
//     "CouponNo": "19MA22903RC010000216"
//   },
//   {
//     "CouponNo": "19MA22903RC010000228"
//   },
//   {
//     "CouponNo": "19MA22903RC010000232"
//   },
//   {
//     "CouponNo": "19MA22903RC010000248"
//   },
//   {
//     "CouponNo": "19MA22903RC010000255"
//   },
//   {
//     "CouponNo": "19MA22903RC010000267"
//   },
//   {
//     "CouponNo": "19MA22903RC010000271"
//   },
//   {
//     "CouponNo": "19MA22903RC010000287"
//   },
//   {
//     "CouponNo": "19MA22903RC010000295"
//   },
//   {
//     "CouponNo": "19MA22903RC010000305"
//   },
//   {
//     "CouponNo": "19MA22903RC010000317"
//   },
//   {
//     "CouponNo": "19MA22903RC010000321"
//   },
//   {
//     "CouponNo": "19MA22903RC010000337"
//   },
//   {
//     "CouponNo": "19MA22903RC010000344"
//   },
//   {
//     "CouponNo": "19MA22903RC010000356"
//   },
//   {
//     "CouponNo": "19MA22903RC010000360"
//   },
//   {
//     "CouponNo": "19MA22903RC010000376"
//   },
//   {
//     "CouponNo": "19MA22903RC010000384"
//   },
//   {
//     "CouponNo": "19MA22903RC010000394"
//   },
//   {
//     "CouponNo": "19MA22903RC010000406"
//   },
//   {
//     "CouponNo": "19MA22903RC010000410"
//   },
//   {
//     "CouponNo": "19MA22903RC010000426"
//   },
//   {
//     "CouponNo": "19MA22903RC010000433"
//   },
//   {
//     "CouponNo": "19MA22903RC010000445"
//   },
//   {
//     "CouponNo": "19MA22903RC010000459"
//   },
//   {
//     "CouponNo": "19MA22903RC010000465"
//   },
//   {
//     "CouponNo": "19MA22903RC010000473"
//   },
//   {
//     "CouponNo": "19MA22903RC010000483"
//   },
//   {
//     "CouponNo": "19MA22903RC010000495"
//   },
//   {
//     "CouponNo": "19MA22903RC010000509"
//   },
//   {
//     "CouponNo": "19MA22903RC010000515"
//   },
//   {
//     "CouponNo": "19MA22903RC010000522"
//   },
//   {
//     "CouponNo": "19MA22903RC010000534"
//   },
//   {
//     "CouponNo": "19MA22903RC010000548"
//   },
//   {
//     "CouponNo": "19MA22903RC010000554"
//   },
//   {
//     "CouponNo": "19MA22903RC010000562"
//   },
//   {
//     "CouponNo": "19MA22903RC010000572"
//   },
//   {
//     "CouponNo": "19MA22903RC010000584"
//   },
//   {
//     "CouponNo": "19MA22903RC010000598"
//   },
//   {
//     "CouponNo": "19MA22903RC010000604"
//   },
//   {
//     "CouponNo": "19MA22903RC010000611"
//   },
//   {
//     "CouponNo": "19MA22903RC010000623"
//   },
//   {
//     "CouponNo": "19MA22903RC010000637"
//   },
//   {
//     "CouponNo": "19MA22903RC010000643"
//   },
//   {
//     "CouponNo": "19MA22903RC010000651"
//   },
//   {
//     "CouponNo": "19MA22903RC010000661"
//   },
//   {
//     "CouponNo": "19MA22903RC010000673"
//   },
//   {
//     "CouponNo": "19MA22903RC010000687"
//   },
//   {
//     "CouponNo": "19MA22903RC010000693"
//   },
//   {
//     "CouponNo": "19MA22903RC010000700"
//   },
//   {
//     "CouponNo": "19MA22903RC010000712"
//   },
//   {
//     "CouponNo": "19MA22903RC010000726"
//   },
//   {
//     "CouponNo": "19MA22903RC010000732"
//   },
//   {
//     "CouponNo": "19MA22903RC010000740"
//   },
//   {
//     "CouponNo": "19MA22903RC010000750"
//   },
//   {
//     "CouponNo": "19MA22903RC010000762"
//   },
//   {
//     "CouponNo": "19MA22903RC010000776"
//   },
//   {
//     "CouponNo": "19MA22903RC010000782"
//   },
//   {
//     "CouponNo": "19MA22903RC010000790"
//   },
//   {
//     "CouponNo": "19MA22903RC010000801"
//   },
//   {
//     "CouponNo": "19MA22903RC010000815"
//   },
//   {
//     "CouponNo": "19MA22903RC010000821"
//   },
//   {
//     "CouponNo": "19MA22903RC010000839"
//   },
//   {
//     "CouponNo": "19MA22903RC010000849"
//   },
//   {
//     "CouponNo": "19MA22903RC010000851"
//   },
//   {
//     "CouponNo": "19MA22903RC010000865"
//   },
//   {
//     "CouponNo": "19MA22903RC010000871"
//   },
//   {
//     "CouponNo": "19MA22903RC010000889"
//   },
//   {
//     "CouponNo": "19MA22903RC010000899"
//   },
//   {
//     "CouponNo": "19MA22903RC010000904"
//   },
//   {
//     "CouponNo": "19MA22903RC010000910"
//   },
//   {
//     "CouponNo": "19MA22903RC010000928"
//   },
//   {
//     "CouponNo": "19MA22903RC010000938"
//   },
//   {
//     "CouponNo": "19MA22903RC010000940"
//   },
//   {
//     "CouponNo": "19MA22903RC010000954"
//   },
//   {
//     "CouponNo": "19MA22903RC010000960"
//   },
//   {
//     "CouponNo": "19MA22903RC010000978"
//   },
//   {
//     "CouponNo": "19MA22903RC010000988"
//   },
//   {
//     "CouponNo": "19MA22903RC010000990"
//   },
//   {
//     "CouponNo": "19MA22903RC010001009"
//   }
// ];

// _.map(code5, function(code, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c6'),
// 			null,
// 			code.CouponNo,
// 			cb
// 		);
// 	});
// });

// const code6 = [
//   {
//     "CouponNo": "19MA22903RB010000013"
//   },
//   {
//     "CouponNo": "19MA22903RB010000028"
//   },
//   {
//     "CouponNo": "19MA22903RB010000035"
//   },
//   {
//     "CouponNo": "19MA22903RB010000044"
//   },
//   {
//     "CouponNo": "19MA22903RB010000055"
//   },
//   {
//     "CouponNo": "19MA22903RB010000068"
//   },
//   {
//     "CouponNo": "19MA22903RB010000073"
//   },
//   {
//     "CouponNo": "19MA22903RB010000088"
//   },
//   {
//     "CouponNo": "19MA22903RB010000099"
//   },
//   {
//     "CouponNo": "19MA22903RB010000102"
//   },
//   {
//     "CouponNo": "19MA22903RB010000117"
//   },
//   {
//     "CouponNo": "19MA22903RB010000124"
//   },
//   {
//     "CouponNo": "19MA22903RB010000133"
//   },
//   {
//     "CouponNo": "19MA22903RB010000144"
//   },
//   {
//     "CouponNo": "19MA22903RB010000157"
//   },
//   {
//     "CouponNo": "19MA22903RB010000162"
//   },
//   {
//     "CouponNo": "19MA22903RB010000177"
//   },
//   {
//     "CouponNo": "19MA22903RB010000188"
//   },
//   {
//     "CouponNo": "19MA22903RB010000191"
//   },
//   {
//     "CouponNo": "19MA22903RB010000206"
//   },
//   {
//     "CouponNo": "19MA22903RB010000213"
//   },
//   {
//     "CouponNo": "19MA22903RB010000222"
//   },
//   {
//     "CouponNo": "19MA22903RB010000233"
//   },
//   {
//     "CouponNo": "19MA22903RB010000246"
//   },
//   {
//     "CouponNo": "19MA22903RB010000251"
//   },
//   {
//     "CouponNo": "19MA22903RB010000266"
//   },
//   {
//     "CouponNo": "19MA22903RB010000277"
//   },
//   {
//     "CouponNo": "19MA22903RB010000280"
//   },
//   {
//     "CouponNo": "19MA22903RB010000295"
//   },
//   {
//     "CouponNo": "19MA22903RB010000302"
//   },
//   {
//     "CouponNo": "19MA22903RB010000311"
//   },
//   {
//     "CouponNo": "19MA22903RB010000322"
//   },
//   {
//     "CouponNo": "19MA22903RB010000335"
//   },
//   {
//     "CouponNo": "19MA22903RB010000340"
//   },
//   {
//     "CouponNo": "19MA22903RB010000355"
//   },
//   {
//     "CouponNo": "19MA22903RB010000366"
//   },
//   {
//     "CouponNo": "19MA22903RB010000379"
//   },
//   {
//     "CouponNo": "19MA22903RB010000384"
//   },
//   {
//     "CouponNo": "19MA22903RB010000391"
//   },
//   {
//     "CouponNo": "19MA22903RB010000400"
//   },
//   {
//     "CouponNo": "19MA22903RB010000411"
//   },
//   {
//     "CouponNo": "19MA22903RB010000424"
//   },
//   {
//     "CouponNo": "19MA22903RB010000439"
//   },
//   {
//     "CouponNo": "19MA22903RB010000444"
//   },
//   {
//     "CouponNo": "19MA22903RB010000455"
//   },
//   {
//     "CouponNo": "19MA22903RB010000468"
//   },
//   {
//     "CouponNo": "19MA22903RB010000473"
//   },
//   {
//     "CouponNo": "19MA22903RB010000480"
//   },
//   {
//     "CouponNo": "19MA22903RB010000499"
//   },
//   {
//     "CouponNo": "19MA22903RB010000500"
//   },
//   {
//     "CouponNo": "19MA22903RB010000513"
//   },
//   {
//     "CouponNo": "19MA22903RB010000528"
//   },
//   {
//     "CouponNo": "19MA22903RB010000533"
//   },
//   {
//     "CouponNo": "19MA22903RB010000544"
//   },
//   {
//     "CouponNo": "19MA22903RB010000557"
//   },
//   {
//     "CouponNo": "19MA22903RB010000562"
//   },
//   {
//     "CouponNo": "19MA22903RB010000579"
//   },
//   {
//     "CouponNo": "19MA22903RB010000588"
//   },
//   {
//     "CouponNo": "19MA22903RB010000599"
//   },
//   {
//     "CouponNo": "19MA22903RB010000602"
//   },
//   {
//     "CouponNo": "19MA22903RB010000617"
//   },
//   {
//     "CouponNo": "19MA22903RB010000622"
//   },
//   {
//     "CouponNo": "19MA22903RB010000633"
//   },
//   {
//     "CouponNo": "19MA22903RB010000646"
//   },
//   {
//     "CouponNo": "19MA22903RB010000651"
//   },
//   {
//     "CouponNo": "19MA22903RB010000668"
//   },
//   {
//     "CouponNo": "19MA22903RB010000677"
//   },
//   {
//     "CouponNo": "19MA22903RB010000688"
//   },
//   {
//     "CouponNo": "19MA22903RB010000691"
//   },
//   {
//     "CouponNo": "19MA22903RB010000706"
//   },
//   {
//     "CouponNo": "19MA22903RB010000711"
//   },
//   {
//     "CouponNo": "19MA22903RB010000722"
//   },
//   {
//     "CouponNo": "19MA22903RB010000735"
//   },
//   {
//     "CouponNo": "19MA22903RB010000740"
//   },
//   {
//     "CouponNo": "19MA22903RB010000757"
//   },
//   {
//     "CouponNo": "19MA22903RB010000766"
//   },
//   {
//     "CouponNo": "19MA22903RB010000777"
//   },
//   {
//     "CouponNo": "19MA22903RB010000780"
//   },
//   {
//     "CouponNo": "19MA22903RB010000795"
//   },
//   {
//     "CouponNo": "19MA22903RB010000800"
//   },
//   {
//     "CouponNo": "19MA22903RB010000811"
//   },
//   {
//     "CouponNo": "19MA22903RB010000824"
//   },
//   {
//     "CouponNo": "19MA22903RB010000839"
//   },
//   {
//     "CouponNo": "19MA22903RB010000846"
//   },
//   {
//     "CouponNo": "19MA22903RB010000855"
//   },
//   {
//     "CouponNo": "19MA22903RB010000866"
//   },
//   {
//     "CouponNo": "19MA22903RB010000879"
//   },
//   {
//     "CouponNo": "19MA22903RB010000884"
//   },
//   {
//     "CouponNo": "19MA22903RB010000891"
//   },
//   {
//     "CouponNo": "19MA22903RB010000900"
//   },
//   {
//     "CouponNo": "19MA22903RB010000913"
//   },
//   {
//     "CouponNo": "19MA22903RB010000928"
//   },
//   {
//     "CouponNo": "19MA22903RB010000935"
//   },
//   {
//     "CouponNo": "19MA22903RB010000944"
//   },
//   {
//     "CouponNo": "19MA22903RB010000955"
//   },
//   {
//     "CouponNo": "19MA22903RB010000968"
//   },
//   {
//     "CouponNo": "19MA22903RB010000973"
//   },
//   {
//     "CouponNo": "19MA22903RB010000980"
//   },
//   {
//     "CouponNo": "19MA22903RB010000999"
//   },
//   {
//     "CouponNo": "19MA22903RB010001002"
//   }
// ];

// _.map(code6, function(code, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c5'),
// 			null,
// 			code.CouponNo,
// 			cb
// 		);
// 	});
// });

// const code7 = [
//   {
//     "CouponNo": "19MA22903RB010000013"
//   },
//   {
//     "CouponNo": "19MA22903RB010000028"
//   },
//   {
//     "CouponNo": "19MA22903RB010000035"
//   },
//   {
//     "CouponNo": "19MA22903RB010000044"
//   },
//   {
//     "CouponNo": "19MA22903RB010000055"
//   },
//   {
//     "CouponNo": "19MA22903RB010000068"
//   },
//   {
//     "CouponNo": "19MA22903RB010000073"
//   },
//   {
//     "CouponNo": "19MA22903RB010000088"
//   },
//   {
//     "CouponNo": "19MA22903RB010000099"
//   },
//   {
//     "CouponNo": "19MA22903RB010000102"
//   },
//   {
//     "CouponNo": "19MA22903RB010000117"
//   },
//   {
//     "CouponNo": "19MA22903RB010000124"
//   },
//   {
//     "CouponNo": "19MA22903RB010000133"
//   },
//   {
//     "CouponNo": "19MA22903RB010000144"
//   },
//   {
//     "CouponNo": "19MA22903RB010000157"
//   },
//   {
//     "CouponNo": "19MA22903RB010000162"
//   },
//   {
//     "CouponNo": "19MA22903RB010000177"
//   },
//   {
//     "CouponNo": "19MA22903RB010000188"
//   },
//   {
//     "CouponNo": "19MA22903RB010000191"
//   },
//   {
//     "CouponNo": "19MA22903RB010000206"
//   },
//   {
//     "CouponNo": "19MA22903RB010000213"
//   },
//   {
//     "CouponNo": "19MA22903RB010000222"
//   },
//   {
//     "CouponNo": "19MA22903RB010000233"
//   },
//   {
//     "CouponNo": "19MA22903RB010000246"
//   },
//   {
//     "CouponNo": "19MA22903RB010000251"
//   },
//   {
//     "CouponNo": "19MA22903RB010000266"
//   },
//   {
//     "CouponNo": "19MA22903RB010000277"
//   },
//   {
//     "CouponNo": "19MA22903RB010000280"
//   },
//   {
//     "CouponNo": "19MA22903RB010000295"
//   },
//   {
//     "CouponNo": "19MA22903RB010000302"
//   },
//   {
//     "CouponNo": "19MA22903RB010000311"
//   },
//   {
//     "CouponNo": "19MA22903RB010000322"
//   },
//   {
//     "CouponNo": "19MA22903RB010000335"
//   },
//   {
//     "CouponNo": "19MA22903RB010000340"
//   },
//   {
//     "CouponNo": "19MA22903RB010000355"
//   },
//   {
//     "CouponNo": "19MA22903RB010000366"
//   },
//   {
//     "CouponNo": "19MA22903RB010000379"
//   },
//   {
//     "CouponNo": "19MA22903RB010000384"
//   },
//   {
//     "CouponNo": "19MA22903RB010000391"
//   },
//   {
//     "CouponNo": "19MA22903RB010000400"
//   },
//   {
//     "CouponNo": "19MA22903RB010000411"
//   },
//   {
//     "CouponNo": "19MA22903RB010000424"
//   },
//   {
//     "CouponNo": "19MA22903RB010000439"
//   },
//   {
//     "CouponNo": "19MA22903RB010000444"
//   },
//   {
//     "CouponNo": "19MA22903RB010000455"
//   },
//   {
//     "CouponNo": "19MA22903RB010000468"
//   },
//   {
//     "CouponNo": "19MA22903RB010000473"
//   },
//   {
//     "CouponNo": "19MA22903RB010000480"
//   },
//   {
//     "CouponNo": "19MA22903RB010000499"
//   },
//   {
//     "CouponNo": "19MA22903RB010000500"
//   },
//   {
//     "CouponNo": "19MA22903RB010000513"
//   },
//   {
//     "CouponNo": "19MA22903RB010000528"
//   },
//   {
//     "CouponNo": "19MA22903RB010000533"
//   },
//   {
//     "CouponNo": "19MA22903RB010000544"
//   },
//   {
//     "CouponNo": "19MA22903RB010000557"
//   },
//   {
//     "CouponNo": "19MA22903RB010000562"
//   },
//   {
//     "CouponNo": "19MA22903RB010000579"
//   },
//   {
//     "CouponNo": "19MA22903RB010000588"
//   },
//   {
//     "CouponNo": "19MA22903RB010000599"
//   },
//   {
//     "CouponNo": "19MA22903RB010000602"
//   },
//   {
//     "CouponNo": "19MA22903RB010000617"
//   },
//   {
//     "CouponNo": "19MA22903RB010000622"
//   },
//   {
//     "CouponNo": "19MA22903RB010000633"
//   },
//   {
//     "CouponNo": "19MA22903RB010000646"
//   },
//   {
//     "CouponNo": "19MA22903RB010000651"
//   },
//   {
//     "CouponNo": "19MA22903RB010000668"
//   },
//   {
//     "CouponNo": "19MA22903RB010000677"
//   },
//   {
//     "CouponNo": "19MA22903RB010000688"
//   },
//   {
//     "CouponNo": "19MA22903RB010000691"
//   },
//   {
//     "CouponNo": "19MA22903RB010000706"
//   },
//   {
//     "CouponNo": "19MA22903RB010000711"
//   },
//   {
//     "CouponNo": "19MA22903RB010000722"
//   },
//   {
//     "CouponNo": "19MA22903RB010000735"
//   },
//   {
//     "CouponNo": "19MA22903RB010000740"
//   },
//   {
//     "CouponNo": "19MA22903RB010000757"
//   },
//   {
//     "CouponNo": "19MA22903RB010000766"
//   },
//   {
//     "CouponNo": "19MA22903RB010000777"
//   },
//   {
//     "CouponNo": "19MA22903RB010000780"
//   },
//   {
//     "CouponNo": "19MA22903RB010000795"
//   },
//   {
//     "CouponNo": "19MA22903RB010000800"
//   },
//   {
//     "CouponNo": "19MA22903RB010000811"
//   },
//   {
//     "CouponNo": "19MA22903RB010000824"
//   },
//   {
//     "CouponNo": "19MA22903RB010000839"
//   },
//   {
//     "CouponNo": "19MA22903RB010000846"
//   },
//   {
//     "CouponNo": "19MA22903RB010000855"
//   },
//   {
//     "CouponNo": "19MA22903RB010000866"
//   },
//   {
//     "CouponNo": "19MA22903RB010000879"
//   },
//   {
//     "CouponNo": "19MA22903RB010000884"
//   },
//   {
//     "CouponNo": "19MA22903RB010000891"
//   },
//   {
//     "CouponNo": "19MA22903RB010000900"
//   },
//   {
//     "CouponNo": "19MA22903RB010000913"
//   },
//   {
//     "CouponNo": "19MA22903RB010000928"
//   },
//   {
//     "CouponNo": "19MA22903RB010000935"
//   },
//   {
//     "CouponNo": "19MA22903RB010000944"
//   },
//   {
//     "CouponNo": "19MA22903RB010000955"
//   },
//   {
//     "CouponNo": "19MA22903RB010000968"
//   },
//   {
//     "CouponNo": "19MA22903RB010000973"
//   },
//   {
//     "CouponNo": "19MA22903RB010000980"
//   },
//   {
//     "CouponNo": "19MA22903RB010000999"
//   },
//   {
//     "CouponNo": "19MA22903RB010001002"
//   }
// ];

// _.map(code7, function(code, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c7'),
// 			null,
// 			code.CouponNo,
// 			cb
// 		);
// 	});
// });



// const mcls = [
//   {
//     "code": "HKOPMH_MCLmo001ZaZ"
//   },
//   {
//     "code": "HKOPMH_MCLmo001YbY"
//   },
//   {
//     "code": "HKOPMH_MCLmo001XcX"
//   },
//   {
//     "code": "HKOPMH_MCLmo001WdW"
//   },
//   {
//     "code": "HKOPMH_MCLmo001VeV"
//   },
//   {
//     "code": "HKOPMH_MCLmo001UfU"
//   },
//   {
//     "code": "HKOPMH_MCLmo001TgT"
//   },
//   {
//     "code": "HKOPMH_MCLmo001ShS"
//   },
//   {
//     "code": "HKOPMH_MCLmo001RiR"
//   },
//   {
//     "code": "HKOPMH_MCLmo001QjQ"
//   },
//   {
//     "code": "HKOPMH_MCLmo001PkP"
//   },
//   {
//     "code": "HKOPMH_MCLmo001OlO"
//   },
//   {
//     "code": "HKOPMH_MCLmo001NmN"
//   },
//   {
//     "code": "HKOPMH_MCLmo001MnM"
//   },
//   {
//     "code": "HKOPMH_MCLmo001LoL"
//   },
//   {
//     "code": "HKOPMH_MCLmo001KpK"
//   },
//   {
//     "code": "HKOPMH_MCLmo001JqJ"
//   },
//   {
//     "code": "HKOPMH_MCLmo001IrI"
//   },
//   {
//     "code": "HKOPMH_MCLmo001HsH"
//   },
//   {
//     "code": "HKOPMH_MCLmo001GtG"
//   },
//   {
//     "code": "HKOPMH_MCLmo001FuF"
//   },
//   {
//     "code": "HKOPMH_MCLmo001EvE"
//   },
//   {
//     "code": "HKOPMH_MCLmo001DwD"
//   },
//   {
//     "code": "HKOPMH_MCLmo001CxC"
//   },
//   {
//     "code": "HKOPMH_MCLmo001ByB"
//   },
//   {
//     "code": "HKOPMH_MCLmo001AzA"
//   },
//   {
//     "code": "HKOPMH_MCLmo001ZaZ"
//   },
//   {
//     "code": "HKOPMH_MCLmo003YbY"
//   },
//   {
//     "code": "HKOPMH_MCLmo003XcX"
//   },
//   {
//     "code": "HKOPMH_MCLmo003WdW"
//   },
//   {
//     "code": "HKOPMH_MCLmo003VeV"
//   },
//   {
//     "code": "HKOPMH_MCLmo003UfU"
//   },
//   {
//     "code": "HKOPMH_MCLmo003TgT"
//   },
//   {
//     "code": "HKOPMH_MCLmo003ShS"
//   },
//   {
//     "code": "HKOPMH_MCLmo003RiR"
//   },
//   {
//     "code": "HKOPMH_MCLmo003QjQ"
//   },
//   {
//     "code": "HKOPMH_MCLmo003PkP"
//   },
//   {
//     "code": "HKOPMH_MCLmo003OlO"
//   },
//   {
//     "code": "HKOPMH_MCLmo003NmN"
//   },
//   {
//     "code": "HKOPMH_MCLmo003MnM"
//   },
//   {
//     "code": "HKOPMH_MCLmo003LoL"
//   },
//   {
//     "code": "HKOPMH_MCLmo003KpK"
//   },
//   {
//     "code": "HKOPMH_MCLmo003JqJ"
//   },
//   {
//     "code": "HKOPMH_MCLmo003IrI"
//   },
//   {
//     "code": "HKOPMH_MCLmo003HsH"
//   },
//   {
//     "code": "HKOPMH_MCLmo003GtG"
//   },
//   {
//     "code": "HKOPMH_MCLmo003FuF"
//   },
//   {
//     "code": "HKOPMH_MCLmo003EvE"
//   },
//   {
//     "code": "HKOPMH_MCLmo003DwD"
//   },
//   {
//     "code": "HKOPMH_MCLmo003CxC"
//   },
//   {
//     "code": "HKOPMH_MCLmo003ByB"
//   },
//   {
//     "code": "HKOPMH_MCLmo003AzA"
//   },
//   {
//     "code": "HKOPMH_MCLmo005ZaZ"
//   },
//   {
//     "code": "HKOPMH_MCLmo005YbY"
//   },
//   {
//     "code": "HKOPMH_MCLmo005XcX"
//   },
//   {
//     "code": "HKOPMH_MCLmo005WdW"
//   },
//   {
//     "code": "HKOPMH_MCLmo005VeV"
//   },
//   {
//     "code": "HKOPMH_MCLmo005UfU"
//   },
//   {
//     "code": "HKOPMH_MCLmo005TgT"
//   },
//   {
//     "code": "HKOPMH_MCLmo005ShS"
//   },
//   {
//     "code": "HKOPMH_MCLmo005RiR"
//   },
//   {
//     "code": "HKOPMH_MCLmo005QjQ"
//   },
//   {
//     "code": "HKOPMH_MCLmo005PkP"
//   },
//   {
//     "code": "HKOPMH_MCLmo005OlO"
//   },
//   {
//     "code": "HKOPMH_MCLmo005NmN"
//   },
//   {
//     "code": "HKOPMH_MCLmo005MnM"
//   },
//   {
//     "code": "HKOPMH_MCLmo005LoL"
//   },
//   {
//     "code": "HKOPMH_MCLmo005KpK"
//   },
//   {
//     "code": "HKOPMH_MCLmo005JqJ"
//   },
//   {
//     "code": "HKOPMH_MCLmo005IrI"
//   },
//   {
//     "code": "HKOPMH_MCLmo005HsH"
//   },
//   {
//     "code": "HKOPMH_MCLmo005GtG"
//   },
//   {
//     "code": "HKOPMH_MCLmo005FuF"
//   },
//   {
//     "code": "HKOPMH_MCLmo005EvE"
//   },
//   {
//     "code": "HKOPMH_MCLmo005DwD"
//   },
//   {
//     "code": "HKOPMH_MCLmo005CxC"
//   },
//   {
//     "code": "HKOPMH_MCLmo005ByB"
//   },
//   {
//     "code": "HKOPMH_MCLmo005AzA"
//   },
//   {
//     "code": "HKOPMH_MCLmo007ZaZ"
//   },
//   {
//     "code": "HKOPMH_MCLmo007YbY"
//   },
//   {
//     "code": "HKOPMH_MCLmo007XcX"
//   },
//   {
//     "code": "HKOPMH_MCLmo007WdW"
//   },
//   {
//     "code": "HKOPMH_MCLmo007VeV"
//   },
//   {
//     "code": "HKOPMH_MCLmo007UfU"
//   },
//   {
//     "code": "HKOPMH_MCLmo007TgT"
//   },
//   {
//     "code": "HKOPMH_MCLmo007ShS"
//   },
//   {
//     "code": "HKOPMH_MCLmo007RiR"
//   },
//   {
//     "code": "HKOPMH_MCLmo007QjQ"
//   },
//   {
//     "code": "HKOPMH_MCLmo007PkP"
//   },
//   {
//     "code": "HKOPMH_MCLmo007OlO"
//   },
//   {
//     "code": "HKOPMH_MCLmo007NmN"
//   },
//   {
//     "code": "HKOPMH_MCLmo007MnM"
//   },
//   {
//     "code": "HKOPMH_MCLmo007LoL"
//   },
//   {
//     "code": "HKOPMH_MCLmo007KpK"
//   },
//   {
//     "code": "HKOPMH_MCLmo007JqJ"
//   },
//   {
//     "code": "HKOPMH_MCLmo007IrI"
//   },
//   {
//     "code": "HKOPMH_MCLmo007HsH"
//   },
//   {
//     "code": "HKOPMH_MCLmo007GtG"
//   },
//   {
//     "code": "HKOPMH_MCLmo007FuF"
//   },
//   {
//     "code": "HKOPMH_MCLmo007EvE"
//   }
// ];
// _.map(mcls, function(m, i) {
// 	tasks.push(function(cb) {
// 		RedeemCodePoolController.createRedeemCodes(
// 			null,
// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769b5'),
// 			m.code,
// 			cb
// 		);
// 	});
// });


// async.parallel(tasks, function(err, result) {
// 	console.log('> Generate Real redeemCode');
// });

// var total = 3000;
// _.times(total, function(i) {
// 	tasks.push(function(cb) {
// 		referenceNoPart = (Math.floor(Math.random() * 300000) + 1) + 1;
// 		const num = i + 1 * total;
// 		CouponPoolController.addCoupon({
// 			name: 'Test Coupon ' + i,
// 			tcName: '測試券' + i,
// 			scName: '测试券' + i,
// 			tc: 'T & C',
// 			tcTC: '條款和條件',
// 			scTC: '条款和条件',
// 			description: 'Description',
// 			tcDescription: '描述',
// 			scDescription: '描述',
// 			type: mongoose.Types.ObjectId('5c384bc334fbdc916dc3a80e'),
// 			expiryDate: moment('2019-02-26 18:00:00').toDate(),
// 			forDate: moment('2019-01-15 00:00:00').toDate(),
// 			redeemCode: moment().format(referenceNoPrefix) + referenceNoPart + moment().format(referenceNoPostfix),
// 		}, function(err, result) {
// 			cb(err);
// 		});
// 	});
// });

// CouponController.replaceNewLine(function() {
// 	console.log('> done coupon tc replacement');
// });
// CouponController.addWinningInfo(function(err, updated) {
//     console.log('> update winning information', updated);
//     if (err) {
//       console.log('> error');
//       console.log(err);
//     }
// });
// PrizeController.addWinningInfo(function(err, updated) {
//     console.log('> update prize winning information', updated);
//     if (err) {
//       console.log('> error');
//       console.log(err);
//     }
// });
const addDummyRedeem = false;
if (addDummyRedeem) {
	total = 10000;
	var subTotal = 0;
	// _.forEach(souvenirCodes, function(s) {
	// 	tasks.push(function(cb) {
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c7'),
	// 			null, s.couponNo, cb);
	// 	});
	// });
	// _.forEach(aquaCodes, function(s) {
	// 	tasks.push(function(cb) {
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c1'),
	// 			null, s.couponNo, cb);
	// 	});
	// });
	// _.times(total, function(i) {
	// 	tasks.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * total;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c1'),
	// 			null,
	// 			referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	var subTasks1 = [];
	_.times(700, function(i) {
		subTasks1.push(function(cb) {
			referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
			const num = i + 1 * 700;
			RedeemCodePoolController.createRedeemCodes(
				mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c1'),
				null,
				'bakery' + referenceNoPart + moment().format(referenceNoPostfix), cb);
		});
	});
	//tasks.push(function(callback){
		async.waterfall(subTasks1, function(err, result) {
			console.log('> Generate Dummy redeemCode 1.');
			//callback(null);
		});
	//});
	// var subTasks2 = [];
	// _.times(30000, function(i) {
	// 	subTasks2.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 30000;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c2'),
	// 			null,
	// 			'cafe' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks2, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 2.');
	// 		callback(null);
	// 	});
	// });

	// var subTasks3 = [];
	// _.times(30000, function(i) {
	// 	subTasks3.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 30000;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c3'),
	// 			null,
	// 			'panda' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks3, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 3.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks4 = [];
	// _.times(30000, function(i) {
	// 	subTasks4.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 30000;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c4'),
	// 			null,
	// 			'foodkiosk' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks4, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 4.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks5 = [];
	// _.times(30000, function(i) {
	// 	subTasks5.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 30000;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c6'),
	// 			null,
	// 			'photo' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks5, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 5.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks6 = [];
	// _.times(30000, function(i) {
	// 	subTasks6.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 30000;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c5'),
	// 			null,
	// 			'skillGame' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks6, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 6.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks7 = [];
	// _.times(5000, function(i) {
	// 	subTasks7.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 5000;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769c7'),
	// 			null,
	// 			'souvenir' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks7, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 7.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks8 = [];
	// _.times(100, function(i) {
		
	// 	subTasks8.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 30000;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			null,
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769b5'),
	// 			'mcl' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks8, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 8.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks9 = [];
	// _.times(1250, function(i) {
	// 	subTasks9.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 1250;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			null,
	// 			mongoose.Types.ObjectId('5c3bf72f39d3c3b02fe769b1'),
	// 			'buyOneGetOne' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks9, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 9.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks10 = [];
	// _.times(25, function(i) {
		
	// 	subTasks10.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 25;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			null,
	// 			mongoose.Types.ObjectId('5c3dccc868241ba46bb6135c'),
	// 			'cantonBistro' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks10, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 10.');
	// 		callback(null);
	// 	});
	// });
	// var subTasks11 = [];
	// _.times(25, function(i) {
		
	// 	subTasks11.push(function(cb) {
	// 		referenceNoPart = (Math.floor(Math.random() * 30000) + 1) + 1;
	// 		const num = i + 1 * 25;
	// 		RedeemCodePoolController.createRedeemCodes(
	// 			null,
	// 			mongoose.Types.ObjectId('5c3dccea68241ba46bb61367'),
	// 			'cocktailBar' + referenceNoPart + moment().format(referenceNoPostfix), cb);
	// 	});
	// });
	// tasks.push(function(callback){
	// 	async.parallel(subTasks11, function(err, result) {
	// 		console.log('> Generate Dummy redeemCode 11.');
	// 		callback(null);
	// 	});
	// });
	console.time('Time');
	// CouponPoolController.removeAllCoupons(function() {
	// 	console.log('> Remove all dummy coupons');
		async.waterfall(tasks, function(err, result) {
			console.log('> Generate All redeemCode.');
			console.timeEnd('Time');
		});
	// 	console.timeEnd('Time');
	// // });
}
// CouponPoolController.getAvailableCouponByIndex(0, function(er, coupon) {
// 	console.log('eCoupon: ', coupon);
// 	console.timeEnd('Time');
// });

var startCron = function(){
	var CronJob = require('cron').CronJob;

	console.log('> Register all of luckydraw cronjobs.');

	// new PrizeRecycler().start();
	// new CouponRecycler().start();

	// new PrizeDistributor().start();
	// new CouponDistributor().start();

	/*
	** ========================================================================================
	** Start of Luckydraw CronJobs - Daily Cronjobs for Luckydraw game
	** ========================================================================================
	**
	** 1. clear logs for performance concern
	** 2. assign prize or coupon to the pool
	** 3. remain coupon from the coupon pool, which is not being to draw
	**
	** Terry Chan
	** 11/01/2018
	** ========================================================================================
	*/
	// remove all of non-invoice log, speed up the luck draw process, remove all of data 00:00:00
	// there is no gap to draw the result for the people who use the ticket or annual pass
	// the park draw period is conducting after 09:00:00
	new CronJob('00 00 00 * * *', function() {
		console.log('***** Excuting non invoice recycler.');
		new EligibilityLogRecycler().start();
	}, null, true, 'Asia/Hong_Kong');

	// distribute tomrrow prize into the pool
	new CronJob('00 00 22 * * *', function() {
		console.log('***** Excuting prize distributor to the Prize Pool.');
		new PrizeDistributor().start();
	}, null, true, 'Asia/Hong_Kong');

	// distribute tomrrow coupon into the pool
	new CronJob('00 00 22 * * *', function() {
		console.log('***** Excuting coupon distributor to the Coupon Pool.');
		new CouponDistributor().start();
	}, null, true, 'Asia/Hong_Kong');
	
	// recycle yesterday prize from the pool
	new CronJob('00 00 00 * * *', function() {
		console.log('***** Excuting prize recycler from the Prize Pool.');
		new PrizeRecycler().start();
	}, null, true, 'Asia/Hong_Kong');

	// recycle yesterday coupon from the pool
	// new CronJob('00 00 00 * * *', function() {
	// 	console.log('***** Excuting coupon recycler from the Coupon Pool.');
	// 	new CouponRecycler().start();
	// }, null, true, 'Asia/Hong_Kong');
	/*
	** ========================================================================================
	** End of Luckydraw CronJobs
	** ========================================================================================
	*/
}
