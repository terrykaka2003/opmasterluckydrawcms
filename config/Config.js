var _ = require("underscore");
var AppEnv= require(global.__base + "/enum/AppEnv");

// use OPCorpCMS;
// db.lotterydrawnlogs.ensureIndex({ gift: 1 }, { unique: true }) 

// db.eligibilitylogs.remove({});
// db.lotterydrawnlogs.remove({});
// db.lotteryhistories.remove({});
// db.redeemcodepools.remove({});

var commonSetting = {
	name: 'Ocean Park Master App CMS',
	sessionExpireLimit: 30, //minutes
	loginAttemptLimit :5, 
	passwordExpireLimit: 60, //days
	secret: "de56f419fc2a876a0b25eb65b8b38ee1",
	smtpHost: "smtp.gmail.com",
	smtpPort: 465,
	smtpUser: "hksar.oceanpark@gmail.com",
	smtpSender: "noreply@oceanpark.com.hk",
	smtpPass: "oceanPark6789",
	internalSmtpHost: '10.10.34.215',
	defaultDataLanguage: 'en',
	cookieSecret: 'OdFT|KznqROQfMXNP8[KN>.>7Wm3tvAUp+c+8yG:d8J!Ix[iT^[Zvm*8&wc=,',
	cronJobTimeFormat: 'YYYY-MM-DD HH:mm:ss',
	monitorExecutionDuration: 900000, // 15mins
	pflowWeatherDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/current', 
	pflowEntityWaitTimeDest:'https://apimaster-pflow.oceanpark.com.hk/api/wait_time/current/?format=json',
	pflowEntityCurrentStatusDest:'https://apimaster-pflow.oceanpark.com.hk/api/status/current/?format=json',
	pflowEntityOperatingHourDest:'https://apimaster-pflow.oceanpark.com.hk/api/entity/operating_hour/?format=json',
	pflowEntityMaintenanceDest:'https://apimaster-pflow.oceanpark.com.hk/api/maintenance/schedule/?format=json',
	pflowZoneOperationHourDest: 'https://apimaster-pflow.oceanpark.com.hk/api/zone/operating_hour/?format=json',
	pflowOperatingHourDest:'https://apimaster-pflow.oceanpark.com.hk/api/park/operating_hour/?format=json',
	pflowFunctionEntityOperatingHourDest: 'https://apimaster-pflow.oceanpark.com.hk/api/function/list/?format=json',
	pflowShowScheduleDest:'https://apimaster-pflow.oceanpark.com.hk/api/show/schedule/?format=json',
	pflowEntityDest: 'https://apimaster-pflow.oceanpark.com.hk/api/entity/list/?format=json',
	pflowAnimalActivityListDest: 'https://apimaster-pflow.oceanpark.com.hk/api/animal_activity/list/?format=json',
	pflowAnimalActivityScheduleDest: 'https://apimaster-pflow.oceanpark.com.hk/api/animal_activity/schedule/?format=json',
	ldapGroup :'mapps_corporate',
	ldapAutoCreateAccount : true,
	ldapDefaultRole : 'Admin',

	/*
	** For lottery game setting
	** Terry Chan
	** 31/12/2018
	*/
	referencePrefix: 'OPLT',
	// no meaning
	referenceStart: '11',
	referenceNoPostfix: 'ss',	// moment seconds
	referenceNoPrefix: 'YYYYMMDD',	// moment full date
	timezone: 'Asian/HongKong',
	promotionNo: '51688',
	timeformat: 'HH:mm:ss',
	characterStaticPath: 'media/upload',
	globalUploadsStaticPath: 'uploads/',
	characterStaticPublicPath: 'static/game-character',
	globalUploadsStaticPublicPath: 'source/',
	gameStaticPublicPath: 'static/game',

	couponTypeStaticPublicPath: 'static/coupon',

	couponExpiryDateFormat: 'YYYY-MM-DD',

	cookiesMap: {
		hotel: 'hotel-token',
		operator: 'operator-token',
		hotelLang: 'hotel-locale',
		operatorLang: 'operator-locale'
	},

	ticketValidationURL: 'http://dev-esch.oceanpark.com.hk/eschedws/rest/BatchTicketValidation',
	wifiMac: '34:12:98:0b:25:e9',

	lotteryProperties: {
		popoverBanner: {
			url: 'popupBanner_{{language}}.png',
			width: 1125,
			height: 2001,
		},
		gameButton: {
			url: 'lottery_button.png',
			width: 1125,
			height: 2001,
		},
	},
	//GA Part
	gaTrackId: 'UA-75306701-1',
	gaHotelLuckyDrawUrl: 'web/hotel/luckyDraw',
	gaOperatorLuckyDrawUrl: 'web/operator/luckyDraw',
	gaAppLuckyDrawUrl: 'app/newyears/lottery',

	universalDeviceId: 'ceba384a-c793-3179-beff-58b4ff3f3d41',
	appViewSecretKey: 'Apb7p4Vo2HWr3Ri8KMwc3mTrledJy0TVnSRa0PvZtGz1nqo2q',
	// !!!! by pass ticket check from op server, disable in PROD
	byPassPassCheck: false,
	btPassRegularWebview: false,
	byPassMobileDetection: false,

	emailSenderAPI: 'http://dev-corpapp.oceanpark.com.hk/api/v2/game/send',

	staticCacheDuration: '1d', // 1day
}

var localhostSetting = {
	dbHost:'localhost',
	dbName:'opmasterappcms',
	dbLogName:'opmasterappcmslog',
	dbAuthEnable: false,
	dbUser:'',
	dbPassword:'',
	dbPort: 27017,
	port: 3001,
	absolutePath:"http://localhost:3001/",
	corpcms:"",
	vgtcms:"",
	pushcms:"",
	oftcms:"",
	mascms:"",
	promotionListDest: 'http://dev-masapp.oceanpark.com.hk/maws/rest/GetPromotionList', 
	wifiLocationDest: 'http://10.51.72.31/staging/api/LBS/LocationList?appid=Ma_itc&token=83ce4c65c6827e53fd03e5adfdb68781251f00fc720199b99687c867ce99c9e0', 
	registerFreeWifiDest: 'http://10.51.72.31/staging/api/FreeWiFi/Register', 
	pflowWeatherDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/current', 
	pflowWeatherWarningDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/warning', 
	ldapDest: 'ldaps://dc11.oceanpark.com.hk:636', 
	ldapBaseDN: 'DC=OCEANPARK,DC=COM,DC=HK',
	contactUsEmail: "patrickng@4d.com.hk",
	pflowOperatingHourDest:'https://apimaster-pflow.oceanpark.com.hk/api/park/operating_hour/?format=json',
	pflowShowScheduleDest:'https://apimaster-pflow.oceanpark.com.hk/api/show/schedule/?format=json',
	pflowEntityDest: 'https://apimaster-pflow.oceanpark.com.hk/api/entity/list/?format=json',
	pflowZoneOperationHourDest: 'https://apimaster-pflow.oceanpark.com.hk/api/zone/operating_hour/?format=json',
	pflowFunctionEntityOperatingHourDest: 'https://apimaster-pflow.oceanpark.com.hk/api/function/list/?format=json',
	registerFreeWiFiTrackerCategory : 'hkbn_wifi_api_local',
	pushCmsApi:"http://localhost:3003/cms",

}

var developmentSetting = {
	dbHost:'localhost',
	dbName:'opmasterappcms',
	dbLogName:'opmasterappcmslog',
	dbAuthEnable: false,
	dbUser:'',
	dbPassword:'',
	dbPort: 27017,
	port: 3002,
	absolutePath: "http://0.0.0.0:3002/",
	// absolutePath:"http://server.4d.com.hk:3002/",
	corpcms:"http://server.4d.com.hk:3002",
	vgtcms:"http://server.4d.com.hk:3000",
	pushcms:"",
	oftcms:"",
	mascms:"",

	promotionListDest: 'http://dev-masapp.oceanpark.com.hk/maws/rest/GetPromotionList', 
	wifiLocationDest: 'http://10.51.72.31/staging/api/LBS/LocationList?appid=Ma_itc&token=83ce4c65c6827e53fd03e5adfdb68781251f00fc720199b99687c867ce99c9e0', 
	registerFreeWifiDest: 'http://10.51.72.31/staging/api/FreeWiFi/Register', 
	pflowWeatherDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/current', 
	pflowWeatherWarningDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/warning', 
	ldapDest: 'ldaps://10.10.34.212:636', 
	ldapBaseDN: 'DC=OCEANPARK,DC=COM,DC=HK',
	contactUsEmail: "patrickng@4d.com.hk",
	pflowOperatingHourDest:'https://apimaster-pflow.oceanpark.com.hk/api/park/operating_hour/?format=json',
	pflowShowScheduleDest:'https://apimaster-pflow.oceanpark.com.hk/api/show/schedule/?format=json',
	pflowEntityDest: 'https://apimaster-pflow.oceanpark.com.hk/api/entity/list/?format=json',
	pflowZoneOperationHourDest: 'https://apimaster-pflow.oceanpark.com.hk/api/zone/operating_hour/?format=json',
	registerFreeWiFiTrackerCategory : 'hkbn_wifi_api_dev',
	pushCmsApi:"http://dev-corppush1.oceanpark.com.hk/cms",
}

var uatSetting = {
	dbHost:'localhost',
	dbName:'OPCorpCMS',
	dbLogName:'opmasterappcmslog',
	dbAuthEnable: false,
	dbUser:'opdbadmin',
	dbPassword:'w9DhbWwB5JxqA97k',
	dbPort: 27017,
	port: 3000,
	absolutePath:"https://luckydraw2019-uat.oceanpark.com.hk/",
	corpcms:"http://dev-corpapp.oceanpark.com.hk",
	vgtcms:"http://dev-vgtapp.oceanpark.com.hk",
	pushcms:"",
	oftcms:"",
	mascms:"",
	promotionListDest: 'http://dev-masapp.oceanpark.com.hk/maws/rest/GetPromotionList', 
	wifiLocationDest: 'http://10.51.72.31/staging/api/LBS/LocationList?appid=Ma_itc&token=83ce4c65c6827e53fd03e5adfdb68781251f00fc720199b99687c867ce99c9e0', 
	registerFreeWifiDest: 'http://10.51.72.31/staging/api/FreeWiFi/Register', 
	pflowWeatherDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/current', 
	pflowWeatherWarningDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/warning',
	pflowEntityWaitTimeDest:'https://apimaster-pflow-qa.oceanpark.com.hk/api/wait_time/current/?format=json',
	ldapDest: 'ldaps://10.10.34.212:636', 
	ldapBaseDN: 'DC=OCEANPARK,DC=COM,DC=HK',
	contactUsEmail: "dennisyue@4d.com.hk",
	// pflowOperatingHourDest:'https://apimaster-pflow-qa.oceanpark.com.hk/api/park/operating_hour/?format=json',
	// pflowShowScheduleDest:'https://apimaster-pflow-qa.oceanpark.com.hk/api/show/schedule/?format=json',
	// pflowEntityDest: 'https://apimaster-pflow-qa.oceanpark.com.hk/api/entity/list/?format=json',
	// pflowZoneOperationHourDest: 'https://apimaster-pflow-qa.oceanpark.com.hk/api/zone/operating_hour/?format=json',
	registerFreeWiFiTrackerCategory : 'hkbn_wifi_api_UAT',
	pushCmsApi:"http://dev-corppush1.oceanpark.com.hk/cms",
}

var productionSetting = {
	dbHost:'mongodb',
	dbName:'OPCorpCMS',
	dbLogName:'opmasterappcmslog',
	dbAuthEnable: false,
	dbUser:'opdbadmin',
	dbPassword:'w9DhbWwB5JxqA97k',
	dbPort: 27017,
	port: 3000,
	absolutePath:"https://luckydraw2019.oceanpark.com.hk/",
	corpcms:"http://corpapp.oceanpark.com.hk",
	vgtcms:"http://vgtapp.oceanpark.com.hk",
	pushcms:"",
	oftcms:"",
	ticketValidationURL: 'https://esch.oceanpark.com.hk/eschedws/rest/BatchTicketValidation',
	mascms:"",
	promotionListDest: 'https://masapp.oceanpark.com.hk/maws/rest/GetPromotionList', 
	wifiLocationDest: 'https://wfapp01.oceanpark.com.hk/live/api/LBS/LocationList?appid=Ma_itc&token=83ce4c65c6827e53fd03e5adfdb68781251f00fc720199b99687c867ce99c9e0', 
	registerFreeWifiDest: 'https://wfapp01.oceanpark.com.hk/live/api/FreeWiFi/Register', 
	pflowWeatherDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/current', 
	pflowWeatherWarningDest: 'http://api-pflow.oceanpark.com.hk/v1/weather/warning', 
	ldapDest: 'ldaps://10.10.34.212:636', 
	ldapBaseDN: 'DC=OCEANPARK,DC=COM,DC=HK',
	contactUsEmail: "gr@oceanpark.com.hk",
	pflowOperatingHourDest:'https://apimaster-pflow.oceanpark.com.hk/api/park/operating_hour/?format=json',
	pflowShowScheduleDest:'https://apimaster-pflow.oceanpark.com.hk/api/show/schedule/?format=json',
	pflowEntityDest: 'https://apimaster-pflow.oceanpark.com.hk/api/entity/list/?format=json',
	pflowZoneOperationHourDest: 'https://apimaster-pflow.oceanpark.com.hk/api/zone/operating_hour/?format=json',
	registerFreeWiFiTrackerCategory : 'hkbn_wifi_api',
	pushCmsApi:"http://corppush1.oceanpark.com.hk/cms",

	byPassPassCheck: false,
	byPassMobileDetection: true,
	emailSenderAPI: 'http://corpapp.oceanpark.com.hk/api/v2/game/send',
	ticketValidationURL: 'https://esch.oceanpark.com.hk/eschedws/rest/BatchTicketValidation',
}



function Config(){
	if ( arguments.callee._singletonInstance )
		return arguments.callee._singletonInstance;
	arguments.callee._singletonInstance = this;


	var environmentStr = process.env.NODE_ENV;
	var environmentArray = "";

	if(environmentStr != undefined){
		environmentArray = environmentStr.split(",");
	}

	this.appEnv = environmentArray[0];
	_.extend(this,commonSetting);
	switch(this.appEnv){
		case AppEnv.localhost.key:
		_.extend(this,localhostSetting);
		break;
		case AppEnv.development.key:
		_.extend(this,developmentSetting);
		break;
		case AppEnv.uat.key:
		_.extend(this,uatSetting);
		break;
		case AppEnv.production.key:
		_.extend(this,productionSetting);
		break;
		default:
		_.extend(this,localhostSetting);
	}

	console.log("environmentArray: " + environmentStr);
}


module.exports = Config;