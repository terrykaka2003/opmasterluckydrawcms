/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone      = require('keystone');
const async         = require('async');
const moment        = require('moment');
const mongoose      = require('mongoose');
const Types         = keystone.Field.Types;
const Utility       = require(global.__base + '/helper/Utility');
const emailLib      = require(global.__base + '/lib/email');
// var LotteryHistoryController  = require(global.__base + '/models/game/lotteryHistory');
var LotteryDrawnLogController = require(global.__base + '/models/game/log/lotteryDrawnLog');
var LotteryHistoryController  = require(global.__base + '/models/game/lotteryHistory');
LotteryDrawnLogController = new LotteryDrawnLogController();
LotteryHistoryController = new LotteryHistoryController();
// const GameResult  = require(global.__base + '/enum/GameResult');
const Config = require(global.__base + '/config/Config');
const conf = new Config();
var LanguageType = require(global.__base + '/enum/LanguageType');
LanguageType = Utility.enumToKeystonOptionStr(LanguageType);
// const GameResultOptionStr = Utility.enumToKeystonOptionStr(GameResult);
/**
 * Lottery History Model
 * ==========
 */
//
const referencePrefix = conf.referencePrefix;

var RedeemLotteryHistoryModel = new keystone.List('RedeemLotteryHistory',{
    track: true,
    // history: true,
    label: 'Redeem Prize',
    nocreate: true,
    nodelete: true,
    map: {
        name: 'referenceNo',
    },
    searchFields: 'player, email, phone, card, invoiceNo, ticketNo, referenceNo',
    defaultColumns: 'referenceNo, redeemed, player, drawPlatform, resultType, invoiceNo, ticketNo',
    defaultSort: '-createdAt',
});

RedeemLotteryHistoryModel.add(
  {
    referenceNo: {
      type: Types.Text,
      required: true,
      initial: true,
      noedit: true,
    },
  },
  "Personal Information",
  {
    player: {
      type: Types.Text,
      required: true,
      initial: true,
      noedit: true,
    },
    email: {
      type: Types.Email,
      required: true,
      initial: true,
      noedit: true,
      index: true,
    },
    country: {
      type: Types.Relationship,
      ref: 'CountryList',
      required: true,
      label: 'Country Code',
      noedit: true,
      initial: true,
    },
    phone: {
      type: Types.Text,
      required: true,
      noedit: true,
      initial: true,
    },
    card: {
      type: Types.Text,
      required: true,
      initial: true,
      noedit: true,
      label: 'HKID Card / Passpord No',
    },
    deviceId: {
      type: Types.Text,
      noedit: true,
      hidden: true,
    },
    language: {
      type: Types.Select,
      options: LanguageType,
      noedit: true,
      hidden: true,
    },
  },
  "Ticket / Staff Information",
  {
    staff: {
      type: Types.Relationship,
      ref: 'Staff',
      label: 'Played from Staff',
      dependsOn: {
        drawPlatform: ['1', '2']
      },
      noedit: true,
    },
    invoiceNo: {
      type: Types.Text,
      note: 'Either use Invoice No or Ticket No / SmartFun Pass No',
      dependsOn: {
        drawPlatform: ['2']
      },
      noedit: true,
    },
    ticketNo: {
      type: Types.Text,
      label: 'Ticket No / SmartFun Pass No',
      noedit: true,
    },
  },
  "Game Result Information",
  {
    history: {
      type: Types.Relationship,
      ref: 'LotteryHistory',
      noedit: true,
      // hidden: true,
    },
    drawPlatform: {
      type: Types.Select,
      label: 'Platform for the Game Play',
      options: [
        {
          label: 'Mobile App',
          value: '0',
        },
        {
          label: 'Operator Web',
          value: '1',
        },
        {
          label: 'Hotel Web',
          value: '2',
        }
      ],
      noedit: true,
      default: '0',
    },
    resultType: {
      type: Types.Select,
      label: 'Prize Type',
      options: [
        {
          label: 'Grand Prize*',
          value: '0',
        },
        {
          label: 'eCoupon',
          value: '1',
        }
      ],
      noedit: true,
      default: '1',
    },
    name: {
      type: Types.Text,
      noedit: true,
      label: 'Prize Name'
    },
    tcName: {
      type: Types.Text,
      noedit: true,
      label: 'Prize Chinese Traditional Name'
    },
    scName: {
      type: Types.Text,
      noedit: true,
      label: 'Prize Chinese Simplified Name'
    },
    completedDate: {
      type: Types.Date,
      noedit: true,
    },
    completed: {
      type: Types.Boolean,
      default: false,
      noedit: true,
    },
    agreed: {
      type: Types.Datetime,
      required: true,
      initial: true,
      noedit: true,
      label: 'Terms & Conditions Agreed At',
      note: 'Agreed date & time of Terms & Conditions agreement',
    },
    shouldMarketing: {
      type: Types.Boolean,
      noedit: true,
      default: false,
      label: 'Is agreed Marketing Information',
    },
    marketing: {
      noedit: true,
      type: Types.Datetime,
      label: 'Marketing Information Agreed At',
      note: 'Agreed date & time of Marketing Information',
    },
    resultLog: {
      type: Types.Relationship,
      ref: 'EligibilityLog',
      noedit: true,
      hidden: true,
    },
    emailTpl: {
      hidden: true,
      noedit: true,
      type: Types.Text,
    },
  },
  "Email Action Information",
  {
    emailAction: {
      hidden: true,
      noedit: true,
      default: false,
      type: Types.Boolean,
      label: 'Enable to Re-send Email (should not be abled to re-send email to the player for all of in-park game histories with the result of winning eCoupon)',
    },
    resendEmail: {
      type: Types.Boolean,
      label: 'Re-send the notification email to the player if this box is checked',
      dependsOn: {
        emailAction: true
      },
    },
    // emailSent: {
    //   type: Types.Boolean,
    //   default: false,
    //   noedit: true,
    //   label: 'Last Email Sent At',
    //   dependsOn: {
    //     emailAction: true
    //   }
    // },
    totalSent: {
      type: Types.Number,
      default: 0,
      noedit: true,
      dependsOn: {
        emailAction: true
      },
      note: 'Total number of sent out the email to the player'
    },
    emailSentAt: {
      type: Types.Datetime,
      noedit: true,
      dependsOn: {
        emailAction: true
      },
      note: 'Last Email Sent At'
    },
  },
  "Redemption Information",
  {
    redeemed: {
      type: Types.Boolean,
      label: 'Picked By Player',
      set: function(redeemed) {
        this._redeemed = redeemed;
        return redeemed;
      },
      default: false,
    },
    redeemedAt: {
      type: Types.Datetime,
      label: 'Date of picked By Player',
    },
  }
);

/**
 * Registration
 */

// RedeemLotteryHistoryModel.schema.index({
//     ticketNo: 1,
//     invoiceNo: 1,
// }, {
//     unique: true,
// });

function getHistoryById(id, callback) {
  RedeemLotteryHistoryModel.model
    .findOne({
      // status: 'completed',
      _id: id,
    }).populate('resultLog')
    .exec(callback);
}



function sendCompletedEmail(doc, callback) {
  // console.log('> resend email: ', JSON.parse(doc.emailTpl));
  if (!!doc.emailTpl) {
    const eFormat = JSON.parse(doc.emailTpl);
    emailLib.sendSingleDrawResultEmailAdpater(eFormat, callback);
  } else {
    callback();
  }
}

RedeemLotteryHistoryModel.schema.pre('save', function(next) {
  // once redeemed, no update anymore
  const self = this;
  // isRedeemed(this._id, function(err, redeemedCount) {
  //   if (redeemedCount) {
  //     next(new Error('You are not allowed to edit all of information as the prize is already redeemed by player.'));
  //   } else {
     //  console.log(redeemedCount, self.redeemed);
      if (self.redeemed && !self.redeemedAt) {
        self.redeemedAt = moment().toDate();
      } else if (!self.redeemed) {
        self.redeemedAt = null;
      }
      if (self.completed) {
        // console.log('Send Email Again.');
        
        if (!self.redeemed && self.resendEmail) {
          sendCompletedEmail(self, function(err) {
            // console.log(err);
            if (err) {
              self.emailSent = false;
              next(err);
            }

            self.resendEmail = false;
            self.emailSentAt = moment().toDate();
            self.totalSent += 1;
            LotteryHistoryController.updateEmailStatus(self, function(err, result) {
              next();
            });
            // next();
          });
        } else if (self.redeemed) {
          LotteryHistoryController.redeemHistory(self, function(err, result) {
            next();
          });
        } else {
          next();
        }
        
      } else {
        next();
      } 
  //   }
  // });
});
// RedeemLotteryHistoryModel.schema.pre('save', function(next) {
  // const isFunctionDate = moment.duration(
  //     moment(this.completedDate)
  //       .startOf('day').diff(moment().startOf('day'))
  // ).asMinutes()

  // // not allowed to edit prize not at 19-02-2019
  // if (!doc.isNew && isFunctionDate >= 0 && !this.invoiceNo) {
  //   return next('The game is not today history. Please do not edit any information.');
  // }
//   next();
// });
RedeemLotteryHistoryModel.register();
function isRedeemed(id, callback){
  RedeemLotteryHistoryModel.model
    .count({
      // status: 'completed',
      _id: mongoose.Types.ObjectId(id),
      redeemed: true,
    })
    .exec(callback);
}
/**
 * Public Class and functions
 */
function LotteryHistory(){}
LotteryHistory.prototype.cleanAll = function(callback) {
  RedeemLotteryHistoryModel.model.remove({}).exec(callback);
}
LotteryHistory.prototype.getParticipantsByDate = function(date, callback) {
  RedeemLotteryHistoryModel.model
	  .find({
      // status: 'completed',
      completedDate: {
        lte: moment(date).toDate(),
      },
	  }, {
      email: 1,
      phone: 1,
      player: 1,
      card: 1,
    })
	  .exec(callback);
}

LotteryHistory.prototype.getHistoryById = getHistoryById;

LotteryHistory.prototype.getPassHistoryList = function(callback) {
  RedeemLotteryHistoryModel.model
	  .find({
      completedDate: {
        lte: moment(date).toDate(),
      },
	  }, {
      email: 1,
      phone: 1,
      player: 1,
      card: 1,
    })
	  .exec(callback);
}

LotteryHistory.prototype.updateCompleted = function(id, callback) {
  RedeemLotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: id,
    }, {
      completed: true,
      completedDate: moment().toDate(),
    })
    .exec(callback);
}

LotteryHistory.prototype.updateEmailSent = function(id, callback) {
  RedeemLotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: mongoose.Types.ObjectId(id),
    }, {
      emailSentAt: moment().toDate(),
      emailSent: true,
    })
    .exec(callback);
}

LotteryHistory.prototype.updateEmailTpl = function(id, config, callback) {
  RedeemLotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: mongoose.Types.ObjectId(id),
    }, {
      emailTpl: config
    })
    .exec(callback);
}

LotteryHistory.prototype.getHistoryByTicketNo = function(ticketNo, callback) {
  RedeemLotteryHistoryModel.model
    .findOne({
      // status: 'completed',
      ticketNo: ticketNo,
    }).populate('resultLog').lean()
    .exec(callback);
}

LotteryHistory.prototype.getHistoryByGameLog = function(id, callback) {
  RedeemLotteryHistoryModel.model
    .findOne({
      // status: 'completed',
      resultLog: id,
    }).populate('resultLog').lean()
    .exec(callback);
}

LotteryHistory.prototype.getHistoriesByDeviceIdAndResult = function(id, resultType, callback) {
  RedeemLotteryHistoryModel.model
    .find({
      // status: 'completed',
      deviceId: id,
      resultType: resultType,
      completed: true,
    }).populate('resultLog').sort({ completedDate: -1 }).lean()
    .exec(callback);
}

LotteryHistory.prototype.createHistory = function(data, callback) {
  // var toBeUpdated = {
  //   email: data.email,
  //   phone: data.phone,
  //   player: data.player,
  //   card: data.card,
  //   country: data.country,
  //   resultLog: data.resultLog,
  //   resultType: data.resultType,
  //   referenceNo: data.referenceNo,
  //   marketing: data.marketing,
  //   shouldMarketing: data.shouldMarketing,
  //   completed: data.completed,
  //   language: data.language,
  //   drawPlatform: data.drawPlatform,
  //   name: data.name,
  //   tcName: data.tcName,
  //   scName: data.scName,
  // };
  // if (data.agreed) {
  //   toBeUpdated.agreed = data.agreed;
  // }
  // if (data.deviceId) {
  //   toBeUpdated.deviceId = data.deviceId;
  // }
  // // if (data.ecoupon) {
  // //   toBeUpdated.ecoupon = data.ecoupon;
  // //   toBeUpdated.redeemCode = data.redeemCode;
  // // } else {
  // //   toBeUpdated.gift = data.gift;
  // // }

  // if (data.ticketNo) {
  //   toBeUpdated.ticketNo = data.ticketNo;
  // } else {
  //   toBeUpdated.invoiceNo = data.invoiceNo;
  // }

  // if (data.staff) {
  //   toBeUpdated.staff = data.staff;
  // }
  const currentHistoryId = data._id;
  data.isNew = true;
  data.history = currentHistoryId;
  data._id = mongoose.Types.ObjectId();
  if (data.emailAction) {
    data.totalSent = 1;
  }
  const history = new RedeemLotteryHistoryModel.model(data);
  history.save(function(err, result) {
    callback(err, history);
  });
}

// LotteryHistory.prototype.getDrawnHistoryCount = function(prize, callback) {
//   RedeemLotteryHistoryModel.model
//     .count({ gift: prize })
//     .exec(callback);
// }

exports = module.exports = LotteryHistory;
