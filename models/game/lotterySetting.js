/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
// const moment    = require('moment-timezone');
const Utility   = require(global.__base + '/helper/Utility');
const Types     = keystone.Field.Types;

/**
 * LotterySetting Model
 * ==========
 */

var LotterySettingModel = new keystone.List('LotterySetting',{
    track: true,
    // history: true,
    label: 'Game Setting',
});

LotterySettingModel.add(
  "App Config",
  {
    enabled: {
      type: Types.Boolean,
      default: true,
      label: 'Open for Luckydraw Game',
      note: 'The lottery game will be closed in app if it is disabled'
    },
    // buttonIcon:{
    //   type: Types.LocalFile,
    //   dest: './media/upload/game',
    //   allowedTypes: [
    //     'image/jpeg', 'image/png', 'image/gif'
    //   ],
    //   filename: function(item, file){
    //     return item.id + '-' + Utility.getRandomInt(0, 999999) + '.' + file.extension
    //   },
    //   format: function(item, file){
    //     return '<img src="/upload/game/' +file.filename+'" style="max-width: 100px">'
    //   },
    //   maxSize: 1048576,
    //   note: 'Notice:the image should under 1MB'
    // },
    // banner:{
    //   type: Types.LocalFile,
    //   dest: './media/upload/game',
    //   allowedTypes: [
    //     'image/jpeg', 'image/png', 'image/gif'
    //   ],
    //   filename: function(item, file){
    //     return item.id + '-' + Utility.getRandomInt(0, 999999) + '.' + file.extension
    //   },
    //   format: function(item, file){
    //     return '<img src="/upload/game/' +file.filename+'" style="max-width: 100px">'
    //   },
    //   maxSize: 5242880,
    //   note: 'Notice:the image should under 5MB'
    // },
    webviewUrl: {
      type: Types.Url,
      require: true,
      initial: true,
      noedit: true,
      note: 'The mobile application entry point',
    },
  },
  "Terms And Conditions Information",
  {
    tc: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Version'
    },
    tcTC: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Version'
    },
    scTC: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simpified Version'
    },
  },
  "Personal Statement Information",
  {
    personal: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Version'
    },
    tcPersonal: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Version'
    },
    scPersonal: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simpified Version'
    },
  },
  "Policy & Privacy Information",
  {
    privacy: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Version'
    },
    tcPrivacy: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Version'
    },
    scPrivacy: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Version'
    },
  },
  "Park Rules Information",
  {
    parkRule: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Version'
    },
    tcParkRule: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Version'
    },
    scParkRule: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Version'
    },
  },
  "Game Rules Information",
  {
    rule: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Version'
    },
    tcRule: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Version'
    },
    scRule: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Version'
    },
  },
  "Game Setting", 
  {
    parkStartGame: {
      type: Types.Text,
      require: true,
      initial: true,
      default: '09:00:00',
      note: 'The start time of the lottery game for the park player'
    },
    parkEndGame: {
      type: Types.Text,
      require: true,
      initial: true,
      default: '21:00:00',
      note: 'The end time of the lottery game for the park player'
    },
    
    // hotelStartGame: {
    //   type: Types.Number,
    //   require: true,
    //   initial: true,
    //   note: 'The start time of the lottery game for the hotel staff or the hotel guest'
    // },
    // hotelEndGame: {
    //   type: Types.Number,
    //   require: true,
    //   initial: true,
    //   note: 'The end time of the lottery game for the hotel staff or the hotel guest'
    // }
  },
  "1st Prize Probabilities Setting",
  {
    startPeriod: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Initial Probability from 00:00 to 09:00',
      default: '2',
      note: 'The Probability % will be set to that value for the followings Maximum Drew out of checkpoint is fulfilled.',
    }
  },
  "CheckPoint 11:00",
  {
    checkmax1: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Drew out Percentage (reset to Initial Probability)',
      default: '60',
      note: 'Reset to Initial Probability if more than this percentage of Grand Prize is drew out at 11:00',
    },
    checkProbability1: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Probability at 11:00 for Grand Prize',
      default: '8',
      note: 'Example: 0.5% for 1st prize = 0.5, 10% = 10, 90% = 90)',
    },
    check1: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Not Drew out Percentage',
      default: '80',
      note: 'Set to "Probability at 11:00" if more than this percentage of Grand Prize is still not drew out at 11:00',
    },
  },
  "CheckPoint 13:00",
  {
    checkmax2: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Drew out Percentage (reset to Initial Probability)',
      default: '90',
      note: 'Reset to Initial Probabilities if more than this percentage of Grand Prize is drew out at 13:00',
    },
    checkProbability2: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Probability at 13:00 for Grand Prize',
      default: '50',
      note: 'Example: 0.5% for 1st prize = 0.5, 10% = 10, 90% = 90)',
    },
    check2: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Not Drew out Percentage',
      default: '49',
      note: 'Set to "Probability at 13:00" if more than this percentage of Grand Prize is still not drew out at 13:00',
    },
  },
  "CheckPoint 15:00",
  {
    checkProbability3: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Probability at 15:00 for Grand Prize',
      default: '30',
      note: 'Example: 0.5% for 1st prize = 0.5, 10% = 10, 90% = 90)',
    },
    check3: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Not Drew out Percentage',
      default: '20',
      note: 'Set to "Probability at 15:00" if more than this percentage of Grand Prize is still not drew out at 15:00',
    },
  },
  "CheckPoint 16:30",
  {
    checkProbability4: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Probability at 16:30 for Grand Prize',
      default: '80',
      note: 'Example: 0.5% for 1st prize = 0.5, 10% = 10, 90% = 90)',
    },
    check4: {
      type: Types.Number,
      require: true,
      initial: true,
      label: 'Not Drew out Percentage',
      default: '1',
      note: 'Set to "Probability at 16:30" if more than this percentage of Grand Prize is still not drew out at 16:30',
    },
  }
  // "CheckPoint 19:00",
  // {
  //   check5: {
  //     type: Types.Number,
  //     require: true,
  //     initial: true,
  //     label: 'Minimum Percentage',
  //     default: '20',
  //     note: 'If at 19:00, more than this percentage of total Big Prize still not drew out',
  //   },
  //   checkProbability5: {
  //     type: Types.Number,
  //     require: true,
  //     initial: true,
  //     label: 'Initial Probabilities at 19:00',
  //     default: '40',
  //     note: 'Example: 0.5% for 1st prize = 0.5, 10% = 10, 90% = 90)',
  //   }
  // }
);

// NewsModel.schema.pre('save', function(next){
//   if(!this.isNew) return next();

//   var parentLang = new ParentLang;
//   parentLang.create(this, (function(err, result){
//     this.parentLang = result._id;
//     next();
//   }).bind(this));
// });


/**
 * Registration
 */

LotterySettingModel.register();


/**
 * Public Class and functions
 */
function LotterySetting(){}

LotterySetting.prototype.getGameConfig = function(callback) {
  LotterySettingModel.model
	  .findOne({}, {
        enabled: 1,
        // tc: 1,
        // rule: 1,
        banner: 1,
        webviewUrl: 1,
        buttonIcon: 1,

    })
    .lean()
	  .exec(callback);
}

LotterySetting.prototype.getGameSetting = function(callback) {
  LotterySettingModel.model
    .findOne({})
    .lean()
    .exec(callback);
}


// LotterySetting.prototype.isValidParkPeriod = function(data, callback) {
//   const start = data.parkStartGame;
//   const end = data.parkEndGame;
  
//   LotterySettingModel.model
//     .findOne({}, {
//         parkStartGame: 1,
//         parkEndGame: 1,
//         enabled: 1,
//     })
//     .exec(callback);
// }

// LotterySetting.prototype.is = function(data, callback) {
//   LotterySettingModel.model
//     .findOne({}, {
//         enabled: 1,
//         tc: 1,
//         rule: 1,
//     })
//     .exec(callback);
// }

exports = module.exports = LotterySetting;
