/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * News Model
 * ==========
 */

var CountryListModel = new keystone.List('CountryList',{
    track: true,
    // history: true,
    map: {
        name: 'code',
    },
    searchFields: 'code',
    defaultSort: 'ordering',
    defaultColumns: 'code, ordering',
});

CountryListModel.add({
  // country: {
  //   type: Types.Text,
  //   required: true,
  //   initial: true,
  //   label: 'Country Name',
  // },
  code: {
    type: Types.Text,
    required: true,
    initial: true,
    label: 'Country Code',
  },
  ordering: {
    type: Types.Number,
    required: true,
    initial: true,
    label: 'Display Ordering',
  },
  enabled: {
    type: Types.Boolean,
    required: true,
    initial: true,
    default: true,
  },
});

/**
 * Registration
 */

CountryListModel.register();

/**
 * Public Class and functions
 */
function CountryList(){}

CountryList.prototype.createCountryCode = function(code, ordering, callback) {
  
  const newCode = new CountryListModel.model({
    code: code,
    ordering: ordering,
    enabled: true,
  });

  newCode.save(callback);
}


CountryList.prototype.getCountryList = function(callback) {
  
  CountryListModel.model
	  .find({
      enabled: true
	  }, { code: 1, ordering: 1 })
    .sort({ ordering: 1, createdAt: 1 })
    .lean()
	  .exec(callback);
}

CountryList.prototype.getCountryById= function(id, callback) {
  CountryListModel.model
    .findById(id)
    .lean()
    .exec(callback);
}

exports = module.exports = CountryList;
