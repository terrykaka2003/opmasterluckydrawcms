/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const _         = require('lodash');
const Config    = require(global.__base + '/config/Config');
const conf      = new Config();
const Types     = keystone.Field.Types;
const Utility   = require(global.__base + '/helper/Utility');

// var GiftType = require(global.__base + '/enum/GiftType');
// GiftType = Utility.enumToKeystonOptionStr(GiftType);
/**
 * Coupon Model
 * Template for each kind of ecoupon
 * ==========
 */

var CouponModel = new keystone.List('Coupon',{
    track: true,
    // history: true,
    label: 'eCoupon Box',
    searchFields: 'name, redeemCode, tcName, scName',
    defaultColumns: 'icon, name, tcName, scName, quantity, forStartDate, forEndDate',
});

CouponModel.add(
  "Setting",
  {
    quantity: {
      type: Types.Number,
      required: true,
      initial: true,
      default: 0,
      label: 'Daily Quota',
      note: 'Total Number for Coupon Distribution on everyday',
    },
    expiryDate: {
      type: Types.Date,
      required: true,
      initial: true,
      label: 'Coupon Expiry Date'
    },
    enabled: {
      type: Types.Boolean,
      default: true,
      note: 'The coupon will not be drawn to the player if it is disabled',
    },
    forStartDate: {
      type: Types.Date,
      label: 'Start Date for Lucky Draw',
      required: true,
      initial: true,
      note: 'The start date for the coupon distribution into the coupon pool in the midnight on everyday'
    },
    forEndDate: {
      type: Types.Date,
      label: 'End Date for Lucky Draw',
      required: true,
      initial: true,
      note: 'The end date for the coupon distribution into the coupon pool in the midnight on everyday'
    },
    emailList: {
      type: Types.TextArray,
      note: 'Forward prize email to the recipient from this list'
    },
  },
  "Image",
  {
    icon:{
      type: Types.LocalFile,
      dest: './media/upload/coupon',
      allowedTypes: [
        'image/jpeg', 'image/png', 'image/gif'
      ],
      filename: function(item, file){
        return item.id + '-' + Utility.getRandomInt(0,999999) + '.' + file.extension
      },
      format: function(item, file){
        return '<img src="/upload/coupon/' +file.filename+'" style="max-width: 100px">'
      },
      maxSize: 900000,
      note: 'Notice:the image should under 900KB',
    },
    image:{
      type: Types.LocalFile,
      dest: './media/upload/coupon',
      allowedTypes: [
        'image/jpeg', 'image/png', 'image/gif'
      ],
      filename: function(item, file){
        return item.id + '-' + Utility.getRandomInt(0,999999) + '.' + file.extension
      },
      format: function(item, file){
        return '<img src="/upload/coupon/' +file.filename+'" style="max-width: 100px">'
      },
      maxSize: 1000000,
      note: 'Notice:the image should under 1MB',
    },
  },
  "Information",
  {
    name: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'English Title',
    },
    tcName: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Traditional Title',
    },
    scName: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Simplified Title',
    },
    usageTerm: {
      type: Types.Textarea,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Usage Terms',
    },
    tcUsageTerm: {
      type: Types.Textarea,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Usage Terms',
    },
    scUsageTerm: {
      type: Types.Textarea,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Usage Terms',
    },
    tc: {
      type: Types.Textarea,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English T&C',
    },
    tcTC: {
      type: Types.Textarea,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional T&C',
    },
    scTC: {
      type: Types.Textarea,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified T&C',
    },
    description: {
      type: Types.Textarea,
      wysiwyg: true,
      label: 'English Description',
    },
    tcDescription: {
      type: Types.Text,
      wysiwyg: true,
      label: 'Chinese Traditional Description',
    },
    scDescription: {
      type: Types.Text,
      wysiwyg: true,
      label: 'Chinese Simplified Description',
    },
  },
  "Email Template",
  {
    outParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web Email Format'
    },
    tcOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web Email Format'
    },
    scOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web Email Format'
    },
    hotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Hotel Web Email Format'
    },
    tcHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Hotel Web Email Format'
    },
    scHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Hotel Web Email Format'
    },
  },
  "Game Winning Information",
  {
    appWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English App Winning Information'
    },
    tcAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional App Winning Information'
    },
    scAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified App Winning Information'
    },
    webParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Park) Winning Information'
    },
    tcWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Park) Winning Information'
    },
    scWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Park) Winning Information'
    },
    webHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Hotel) Winning Information'
    },
    tcWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Hotel) Winning Information'
    },
    scWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Hotel) Winning Information'
    },
  }
);

const checkValidUpdate = function(doc) {
  // const from = moment.duration(
  //     moment('23:00:00', conf.timeformat).diff(moment())
  // ).asMinutes();
  const from = moment('22:59:59', conf.timeformat);
  const to = moment('00:00:00', conf.timeformat);
  // const to = moment.duration(
  //   moment('23:59:59', 'HH:mm:ss').add(1, 'days').diff(moment())).asMinutes();
  // the prize will be blocked during 23:00 to 23:59 everyday
  if (moment().isBetween(from, to)) {
    return new Error('You are not allowed to edit the coupon template from 23:00:00 to 23:59:59. The coupon is being distributed into the Daily Coupon Pool.');
  }

  return null;
}


CouponModel.schema.pre('save', function(next) {
  const error = checkValidUpdate(this);
  if (error) {
    next(error);
  }
  this.forEndDate = moment(this.forEndDate).endOf('day').toDate();
  next();
});

//   next();
// });
// CouponModel.schema.index({
//     // enabled: 1,
//     // hold: 1,
// });
/**
 * Registration
 */
CouponModel.register();

/**
 * Public Class and functions
 */
function Coupon(){}


Coupon.prototype.getAvailableCouponList = function(callback) {
  CouponModel.model.find({
    // type: 'prize',
    quantity: {
      $gte: 1,
    },
    forStartDate: {
      $lte: moment().startOf('day').toDate(),
    },
    forEndDate: {
      $gte: moment().endOf('day').toDate(),
    },
    // type: type,
    enabled: true,
  }).lean().exec(callback);
}
Coupon.prototype.addCoupon = function(data, callback) {
  // console.log('add coupon....');
  const coupon = new CouponModel.model({
    name: data.name,
    tcName: data.tcName,
    scName: data.scName,
    description: data.description,
    tcDescription: data.tcDescription,
    scDescription: data.scDescription,
    forStartDate: data.forStartDate,
    forEndDate: data.forEndDate,
    tc: data.tc,
    tcTC: data.tcTC,

    appWinningInfo: data.appWinningInfo,
    tcAppWinningInfo: data.tcAppWinningInfo,
    scAppWinningInfo: data.scAppWinningInfo,
    webHotelWinningInfo: data.webHotelWinningInfo,
    tcWebHotelWinningInfo: data.tcWebHotelWinningInfo,
    scWebHotelWinningInfo: data.scWebHotelWinningInfo,
    webParkWinningInfo: data.webParkWinningInfo,
    tcWebParkWinningInfo: data.tcWebParkWinningInfo,
    scWebParkWinningInfo: data.scWebParkWinningInfo,

    inParkFormat: data.inParkFormat,
    tcInParkFormat: data.tcInParkFormat,
    scInParkFormat: data.scInParkFormat,
    outParkFormat: data.outParkFormat,
    tcOutParkFormat: data.tcOutParkFormat,
    emailList: data.emailList,
    scOutParkFormat: data.scOutParkFormat,
    hotelFormat: data.hotelFormat,
    tcHotelFormat: data.tcHotelFormat,
    scHotelFormat: data.scHotelFormat,
    scTC: data.scTC,
    type: data.type,
    tcUsageTerm: data.tcUsageTerm,
    scUsageTerm: data.scUsageTerm,
    usageTerm: data.usageTerm,
    expiryDate: data.expiryDate,
    hold: false,
    enabled: true,
    quantity: data.quantity,
  });
  coupon.save(callback);
}

Coupon.prototype.replaceNewLine = function(callback) {
  var tasks = [];
  CouponModel.model.find({}).exec(function(err, coupons){
    _.map(coupons, function(coupon) {
      if (coupon.tc) {
        coupon.tc = coupon.tc.replace(/<br \/>/gi,'\r\n');
      }
      if (coupon.tcTC) {
        coupon.tcTC = coupon.tcTC.replace(/<br \/>/gi,'\r\n');
      }
      if (coupon.scTC) {
        coupon.scTC = coupon.scTC.replace(/<br \/>/gi,'\r\n');
      }
      coupon.save(function(err, result) {
        console.log(err, result);
      });
    })
  });
  callback();
}

Coupon.prototype.addWinningInfo = function(callback) {
  CouponModel.model.update({}, {
    $set: {
      tcUsageTerm: `此券只限使用一次`,
      scUsageTerm: `此券只限使用一次`,
      usageTerm: `One Time Use Only`,
      tc: `
        <b>Terms & Conditions:</b><br /><br />
        Present this coupon QR code before payment to enjoy the offer. <br />
        Holder of this coupon is entitled to enjoy HK$20 discount on any consumption of food or drink items at Ocean Park “Aqua City Bakery”.<br />
        This coupon is exclusive of Ocean Park admission ticket.<br />
        The validity of this coupon shall be 26 January to 19 February 2019, both days inclusive.<br />
        Spending and discount amount is based on one single receipt.<br />
        This coupon is neither refundable nor exchangeable for cash. <br />
        This coupon cannot be used in conjunction with other promotional offers (except “SmartFun Annual Pass” membership discount). <br />
        This coupon cannot be sold to any party.  Any person caught selling this coupon will be subject to prosecution. <br />
        In case of any dispute, the decision of Ocean Park Corporation shall be final and binding. <br />
        Ocean Park Corporation reserves the right to change/terminate the contents of this promotion at any time without prior notice.<br />
      `,
      tcTC: `
        <b>使用條款及細則:</b><br /><br />
        請於付款前出示此優惠券二維碼。 <br />
        憑券於海洋公園「水都餅店」惠顧任何食品或飲品，可作HK$20使用。 <br />
        此券不包括海洋公園入場門票。 <br />
        此券只適用於2019年1月26日至2月19日。 <br />
        消費及折扣金額以單一收據計算。 <br />
        此券不能兌換現金或作現金找續。 <br />
        此券不可與其他優惠共用 (｢智紛全年入場證」會員折扣除外)。 <br />
        此券不可作售賣用途，違者可被檢控。  <br />
        海洋公園公司對本優惠之所有事宜均有最終而具約束力的決定權。 <br />
        海洋公園公司有權隨時更改/終止此項優惠的內容而毋須預先另行通知
      `,
      scTC: `
        <b>使用条款及细则：</b><br /><br />
        请于付款前出示此优惠券二维码。<br />
        凭券于海洋公园「水都饼店」惠顾任何食品或饮品，可作HK $20使用。<br />
        此券不包括海洋公园入场门票。<br />
        此券只适用于2019年1月26日至2月19日。<br />
        消费及折扣金额以单一收据计算。<br />
        此券不能兑换现金或作现金找续。<br />
        此券不可与其他优惠共用（「智纷全年入场证」会员折扣除外）。<br />
        此券不可作售卖用途，违者可被检控。<br />
        海洋公园公司对本优惠之所有事宜均有最终而具约束力的决定权。<br />
        海洋公园公司有权随时更改/终止此项优惠的内容而毋须预先另行通知
      `,
      appWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        Reference no: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          The above e-coupon will automatically installed in this mobile app “My eCoupons” page. For any enquiries, please drop down the reference number above and contact us at (852) 3923 2323. Have a nice day!
        </div>
        <div class="g-congrad-button">
          <button class="g-congrad-scan-other">Scan Next Ticket</button>
          <button class="g-congrad-check-ecoupon">Check eCoupon</button>
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          Friendly reminder: The ultimate Grand Prizes draw will be conducted on 21 Feb, 2019 and winners will be drawn randomly by the computer system. The winner list will be announced on Hong Kong Ocean Park official website and winner of the Grand Prizes will be notified by phone within 14 working days.
        </div>
      `,
      tcAppWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        參考編號: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          現金優惠券將自動加入此應用程式中「我的優惠券」頁面。 如有任何問題,請記下上面顯示的參考編號並致電(852) 3923 2323查詢。祝您有愉快的一天! 
        </div>
        <div class="g-congrad-button">
          <button class="g-congrad-scan-other">掃另一門票</button>
          <button class="g-congrad-check-ecoupon">檢查優惠券</button>
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您:終極大獎將於2019年2月21日由電腦隨機方式抽出得獎者,得獎名單將於海洋公園官方網站公佈。得獎者將獲專人以電話於14個工作天內通知領獎。
        </div>
      `,
      scAppWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        参考编号: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          现金优惠券将自动加入此应用程式中「我的优惠券」页面。如有任何问题，请记下上面显示的参考编号并致电（852）3923 2323查询。祝您有愉快的一天！
        </div>
        <div class="g-congrad-button">
          <button class="g-congrad-scan-other">扫另一门票</button>
          <button class="g-congrad-check-ecoupon">检查优惠券</button>
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您：终极大奖将于2019年2月21日由电脑随机方式抽出得奖者，得奖名单将于海洋公园官方网站公布得奖者将获专人以电话于14个工作天内通知领奖。
        </div>
      `,
      webHotelWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        Reference no: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          Prize redemption letter will be notified through registered email. For any enquiries, please drop down the reference number above and contact us at (852) 3923 2323. Have a nice day!
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          Friendly reminder: The ultimate Grand Prizes draw will be conducted on 21 Feb, 2019 and winners will be drawn randomly by the computer system. The winner list will be announced on Hong Kong Ocean Park official website and winner of the Grand Prizes will be notified by phone within 14 working days.
        </div>
      `,
      tcWebHotelWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        參考編號: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          閣下將收到得奬確認電郵。 如有任何問題,請記下上面顯示的參考編號並致電(852) 3923 2323查詢。祝您有愉快的一天! 
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您:終極大獎將於2019年2月21日由電腦隨機方式抽出得獎者,得獎名單將於海洋公園官方網站公佈。得獎者將獲專人以電話於14個工作天內通知領獎。
        </div>
      `,
      scWebHotelWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        参考编号: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          阁下将收到得奖确认电邮。如有任何问题，请记下上面显示的参考编号并致电（852）3923 2323查询。祝您有愉快的一天！
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您：终极大奖将于2019年2月21日由电脑随机方式抽出得奖者，得奖名单将于海洋公园官方网站公布得奖者将获专人以电话于14个工作天内通知领奖。
        </div>
      `,
      webParkWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        Reference no: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          Prize redemption letter will be notified through registered email. For any enquiries, please drop down the reference number above and contact us at (852) 3923 2323. Have a nice day!
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          Friendly reminder: The ultimate Grand Prizes draw will be conducted on 21 Feb, 2019 and winners will be drawn randomly by the computer system. The winner list will be announced on Hong Kong Ocean Park official website and winner of the Grand Prizes will be notified by phone within 14 working days.
        </div>
      `,
      tcWebParkWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        參考編號: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          閣下將收到得奬確認電郵。 如有任何問題,請記下上面顯示的參考編號並致電(852) 3923 2323查詢。祝您有愉快的一天! 
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您:終極大獎將於2019年2月21日由電腦隨機方式抽出得獎者,得獎名單將於海洋公園官方網站公佈。得獎者將獲專人以電話於14個工作天內通知領獎。
        </div>
      `,
      scWebParkWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        参考编号: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          阁下将收到得奖确认电邮。如有任何问题，请记下上面显示的参考编号并致电（852）3923 2323查询。祝您有愉快的一天！
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您：终极大奖将于2019年2月21日由电脑随机方式抽出得奖者，得奖名单将于海洋公园官方网站公布得奖者将获专人以电话于14个工作天内通知领奖。
        </div>
      `
    }
  }, { multi: true }).exec(callback);
}

exports = module.exports = Coupon;
