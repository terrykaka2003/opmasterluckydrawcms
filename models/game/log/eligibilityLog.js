/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const Utility   = require(global.__base + '/helper/Utility');
const Types     = keystone.Field.Types;

var GameResult = require(global.__base + '/enum/GameResult');
GameResult = Utility.enumToKeystonOptionStr(GameResult);
/**
 * EligibilityLog Model
 * for racing condition handling by using unqiue index for ticket validation
 * ==========
 */

var EligibilityLogModel = new keystone.List('EligibilityLog',{
    track: true,
    // history: true,
    noedit: true,
    nocreate: true,
    nodelete: true,
    label: 'Player\'s Eligibility Log',
    map: {
        name: 'token',
    },
    defaultSort: '-createdBy',
    searchFields: 'token, playDate',
    defaultColumns: 'token, playDate',
});

EligibilityLogModel.add({
  token: {
    type: Types.Text,
    required: true,
    initial: true,
    note: 'It is representive to Ticket No, Smartfun Pass No, or Invoice No'
  },
  // text date only for index validation, no need to use for any calculation
  playDate: {
    type: Types.Text,
    required: true,
    initial: true,
    node: 'Date of request the Luckydraw game'
  },
  resultLog: {
    type: Types.Relationship,
    ref: 'LotteryDrawnLog',
    note: 'Link up to the result of lottery'
  },
  status: {
    type: Types.Select,
    options: GameResult,
    default: 'hold'
  },
  deviceId: {
    type: Types.Text,
  },
  isInvoice: {
    type: Types.Boolean,
    isInvoice: false,
  },
  joinAt: {
    type: Types.Datetime,
    note: 'Date of actually get the Luckydraw result'
  },
});


/**
 * Registration
 */

// [IMPORTANT] for racing condition
// ensure index from Mongodb Shell if not work
// db.eligibilitylogs.ensureIndex({ token: 1 }, { unique: true }) 
EligibilityLogModel.schema.index({
    token: 1,
    // playDate: 1,
}, {
    unique: true,
});

EligibilityLogModel.register();


/**
 * Public Class and functions
 */
function EligibilityLog(){}
EligibilityLog.prototype.cleanAll = function(callback) {
  EligibilityLogModel.model.remove({}).exec(callback);
}
EligibilityLog.prototype.createLog = function(date, ticket, isInvoice, deviceId, callback) {
  const log = new EligibilityLogModel.model({
    token: ticket,
    playDate: date,
    isInvoice: isInvoice,
    deviceId: deviceId,
    status: 'hold',
  });
  log.save(function(err, result) {
    callback(err, log);
  });
}

/*
** 1. All of non-invoice should be removed for the case of annual pass or ticket no
** 2. ticketNo will be rejected by OP Ticket Validation API
** 3. Annual Pass will be also accepted by OP Ticket Validation API before the valid date
** 4. Invoice No cannot be checked for the valid, and all of invoice no must be left in this log table
**    for trigger Index Duplication Error for the racing condition
** Terry Chan
** 10/01/2019
*/
EligibilityLog.prototype.getAllOldNonInvoiceLogs = function(callback) {
  EligibilityLogModel.model.find({
    $or: [
      {
        joinAt: {
          $lte: moment().toDate(),
        },
        isInvoice: false,
        status: {
          $ne: 'hold',
        },
      },
      // {
      //   joinAt: {
      //     $lt: null,
      //   },
      // },
      {
        joinAt: { $exists: false },
        isInvoice: { $exists: false },
        status: {
          $ne: 'hold',
        },
      }
    ]
    // joinAt: {
    //   lte: moment().toDate(),
    // },
    // isInvoice: false,
    
    // $and: [
    //   {
    //     $or: [
    //       {
    //         updatedAt: {
    //           lte: moment().toDate(),
    //         },
    //       },
    //       {
    //         createdAt: {
    //           lte: moment().toDate(),
    //         },
    //       }
    //     ]
    //   },
    //   {
    //     isInvoice: false,
    //   }
    // ],
    // playDate: date,
  }).exec(callback);
}

EligibilityLog.prototype.getLog = function(ticket, callback) {
  EligibilityLogModel.model.findOne({
    token: ticket,
    // playDate: date,
  }).populate('resultLog').exec(callback);
}

EligibilityLog.prototype.getLogById = function(data, callback) {
  EligibilityLogModel.model.findOne({
    _id: data._id
  }).populate('resultLog').exec(callback);
}

EligibilityLog.prototype.removeLog = function(id, callback) {
  EligibilityLogModel.model.findOne({
    _id: id,
  }).remove().exec(callback);
}

EligibilityLog.prototype.updateStatus = function(data, callback) {
  EligibilityLogModel.model.update({
    _id: data.id
  }, {
    status: data.status,
    joinAt: data.joinAt
  }, {}, callback);
}

EligibilityLog.prototype.getLogByPassNo = function(passNo, callback) {
  EligibilityLogModel.model.count({
    token: passNo,
  }, callback);
}

EligibilityLog.prototype.updateLog = function(data, callback) {
  EligibilityLogModel.model.update({
    $and: [
      {
        _id: data._id,
      },
      {
        $or: [
          {
            resultLog: {
              $eq: null
            }
          },
          {
            resultLog: {
              $lt: null
            }
          }
        ]
      }
    ]
  }, {
    resultLog: data.resultLog._id,
  }, {}, callback);
}

exports = module.exports = EligibilityLog;
