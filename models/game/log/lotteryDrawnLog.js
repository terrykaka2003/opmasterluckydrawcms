/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const Types     = keystone.Field.Types;
const Utility       = require(global.__base + '/helper/Utility');
const GameResult  = require(global.__base + '/enum/GameResult');
const GameResultOptionStr = Utility.enumToKeystonOptionStr(GameResult);
/**
 * PrizeDrawnLog Model
 * for racing condition handling by using unqiue index for prize or ecoupon pick validation
 * the first step to guard for the lucky drawing result
 * ==========
 */

var LotteryDrawnLogModel = new keystone.List('LotteryDrawnLog',{
    track: true,
    // history: true,
    map: {
        name: 'gift',
    },
    noedit: true,
    nocreate: true,
    nodelete: true,
    label: 'Player\'s Game Drawing Log',
    defaultSort: '-createdBy',
    searchFields: 'gift',
    defaultColumns: 'gift',
});

LotteryDrawnLogModel.add({
  gift: {
    type: Types.Text,
    required: true,
    initial: true,
    note: 'It is representative the objectID to either 1st Prize or eCoupon',
  },
  prize: {
    type: Types.Relationship,
    ref: 'Prize',
  },
  prizePool: {
    type: Types.Relationship,
    ref: 'PrizePool',
  },
  prizeCoupon: {
    type: Types.Boolean,
    default: false,
    label: 'Special PrizeCoupon',
    note: 'Is Special PrizeCoupon Type'
  },
  coupon: {
    type: Types.Relationship,
    ref: 'CouponPool',
  },
  emailList: {
    type: Types.TextArray,
    note: 'Forward prize email to the recipient from this list'
  },
  // couponPool: {
  //   type: Types.Relationship,
  //   ref: 'CouponPool',
  // },
  name: {
    type: Types.Text,
    label: 'English Coupon',
  },
  tcName: {
    type: Types.Text,
    label: 'Chinese Traditional Coupon',
  },
  scName: {
    type: Types.Text,
    label: 'Chinese Simplified Coupon',
  },
  usageTerm: {
    type: Types.Textarea,
    wysiwyg: true,
    label: 'English Usage Terms',
  },
  tcUsageTerm: {
    type: Types.Textarea,
    wysiwyg: true,
    label: 'Chinese Traditional Usage Terms',
  },
  scUsageTerm: {
    type: Types.Textarea,
    wysiwyg: true,
    label: 'Chinese Simplified Usage Terms',
  },
  tc: {
    type: Types.Textarea,
    wysiwyg: true,
    label: 'English Coupon T&C',
  },
  tcTC: {
    type: Types.Textarea,
    wysiwyg: true,
    label: 'Chinese Traditional Coupon T&C',
  },
  scTC: {
    type: Types.Textarea,
    wysiwyg: true,
    label: 'Chinese Simplified Coupon T&C',
  },
  description: {
    type: Types.Text,
    wysiwyg: true,
    label: 'English Coupon Description',
  },
  tcDescription: {
    type: Types.Text,
    wysiwyg: true,
    label: 'Chinese Traditional Coupon Description',
  },
  scDescription: {
    type: Types.Text,
    wysiwyg: true,
    label: 'Chinese Simplified Coupon Description',
  },
  type: {
    type: Types.Relationship,
    ref: 'Coupon',
  },
  redeemCode: {
    type: Types.Text,
  },
  expiryDate: {
    type: Types.Date,
    // required: true,
    // initial: true,
  },
  isBigPrize:  {
    type: Types.Boolean,
    default: false,
  },
  icon:{
    type: Types.Text,
  },
  image: {
    type: Types.Text,
  },
  // barcodeImage: {
  //   type: Types.Text,
  // },
  inParkFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'English App Email Format'
  },
  tcInParkFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Traditional App Email Format'
  },
  scInParkFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Simplified App Email Format'
  },
  outParkFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'English Web Email Format'
  },
  tcOutParkFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Traditional Web Email Format'
  },
  scOutParkFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Simplified Web Email Format'
  },

  hotelFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'English Hotel Web Email Format'
  },
  tcHotelFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Traditional Hotel Web Email Format'
  },
  scHotelFormat: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Simplified Hotel Web Email Format'
  },
  
  appWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'English App Winning Information'
  },
  tcAppWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Traditional App Winning Information'
  },
  scAppWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Simplified App Winning Information'
  },
  webParkWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'English Web (Park) Winning Information'
  },
  tcWebParkWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Traditional Web (Park) Winning Information'
  },
  scWebParkWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Simplified Web (Park) Winning Information'
  },
  webHotelWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'English Web (Hotel) Winning Information'
  },
  tcWebHotelWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Traditional Web (Hotel) Winning Information'
  },
  scWebHotelWinningInfo: {
    type: Types.Html,
    wysiwyg: true,
    require: true,
    initial: true,
    label: 'Chinese Simplified Web (Hotel) Winning Information'
  },
});


/**
 * Registration
 */

// [IMPORTANT] for racing condition
// db.lotterydrawnlogs.ensureIndex({ gift: 1 }, { unique: true }) 
LotteryDrawnLogModel.schema.index({
    gift: 1,
}, {
    unique: true,
});

LotteryDrawnLogModel.register();


/**
 * Public Class and functions
 */
function LotteryDrawnLog(){}

LotteryDrawnLog.prototype.cleanAll = function(callback) {
  LotteryDrawnLogModel.model.remove({}).exec(callback);
}

LotteryDrawnLog.prototype.createLog = function(data, callback) {
  const log = new LotteryDrawnLogModel.model({
    gift: data.gift,
    prize: data.prize,
    prizePool: data.prizePool,

    redeemCode: data.redeemCode,
    name: data.name,
    tcName: data.tcName,
    scName: data.scName,

    prizeCoupon: data.prizeCoupon,

    expiryDate: data.expiryDate,
    isBigPrize: data.isBigPrize,

    tcUsageTerm: data.tcUsageTerm,
    scUsageTerm: data.scUsageTerm,
    usageTerm: data.usageTerm,

    tc: data.tc,
    tcTC: data.tcTC,
    scTC: data.scTC,

    type: data.type && data.type._id ? data.type._id : null,

    icon: data.icon && data.icon.filename ? data.icon.filename : '',
    image: data.image && data.image.filename ? data.image.filename : '',

    description: data.description,
    tcDescription: data.tcDescription,
    scDescription: data.scDescription,

    inParkFormat: data.inParkFormat,
    tcInParkFormat: data.tcInParkFormat,
    scInParkFormat: data.scInParkFormat,
    outParkFormat: data.outParkFormat,
    tcOutParkFormat: data.tcOutParkFormat,
    scOutParkFormat: data.scOutParkFormat,

    appWinningInfo: data.appWinningInfo,
    tcAppWinningInfo: data.tcAppWinningInfo,
    scAppWinningInfo: data.scAppWinningInfo,
    webHotelWinningInfo: data.webHotelWinningInfo,
    tcWebHotelWinningInfo: data.tcWebHotelWinningInfo,
    scWebHotelWinningInfo: data.scWebHotelWinningInfo,
    webParkWinningInfo: data.webParkWinningInfo,
    tcWebParkWinningInfo: data.tcWebParkWinningInfo,
    scWebParkWinningInfo: data.scWebParkWinningInfo,

    emailList: data.emailList,
    // barcodeImage: data.barcodeImage,

    // appFormat: data.appFormat,
    // tcAppFormat: data.tcAppFormat,
    // scAppFormat: data.scAppFormat,
    // webFormat: data.webFormat,
    // tcWebFormat: data.tcWebFormat,
    // scWebFormat: data.scWebFormat,
    hotelFormat: data.hotelFormat,
    tcHotelFormat: data.tcHotelFormat,
    scHotelFormat: data.scHotelFormat,

    coupon: data.coupon
    // couponPool: data.couponPool
  });
  log.save(callback);
}

// LotteryDrawnLog.prototype.getCouponLog = function(id, callback) {
//   LotteryDrawnLogModel.model.find({
//     _id: id,
//     coupon: {
//       $ne: null,
//     }, 
//   }).populate('coupon').populate('type').lean().exec(callback);
// }

LotteryDrawnLog.prototype.getLog = function(id, callback) {
  LotteryDrawnLogModel.model.findOne({
    _id: id,
  }).populate('prize').populate('coupon').populate('type').lean().exec(callback);
}

LotteryDrawnLog.prototype.removeLog = function(id, callback) {
  LotteryDrawnLogModel.model.findOne({
    _id: id,
  }).remove().exec(callback);
}

LotteryDrawnLog.prototype.removeLogs = function(ids, callback) {
  LotteryDrawnLogModel.model.find({
    _id: {
      $in: ids
    },
  }).remove().exec(callback);
}

exports = module.exports = LotteryDrawnLog;
