/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const mongoose  = keystone.mongoose;
const Types     = keystone.Field.Types;

const Utility = require(global.__base + "/helper/Utility");
// GiftType = Utility.enumToKeystonOptionStr(GiftType);
/**
 * RedeemCodePool Model
 * Redeem code poolf
 * ==========
 */

var RedeemCodePoolModel = new keystone.List('RedeemCodePool',{
    track: true,
    // history: true,
    searchFields: 'redeemCode',
    defaultColumns: 'redeemCode',
});

RedeemCodePoolModel.add({
  redeemCode: {
    type: Types.Text,
    required: true,
    initial: true,
  },
  forCoupon: {
    type: Types.Relationship,
    ref: 'Coupon',
    note: 'Either for ECoupon or PrizeCoupon',
  },
  forPrize: {
    type: Types.Relationship,
    ref: 'Prize',
    note: 'Either for ECoupon or PrizeCoupon',
  },
  // image: {
  //   type: Types.Text,
  //   note: 'for ECoupon barcode image path',
  // }
});

// PrizeModel.schema.pre('save', function(next) {
//   const durationFromToday = moment.duration(
//       moment().startOf('day').diff(moment(this.forDate).startOf('day'))
//   ).asMinutes()

//   // not allowed to edit today prize
//   if (durationFromToday >= 0) {
//     next(new Error('You are not allowed to edit today prize since the Lottery Game is being started.'));
//   }

//   const isTmr = moment.duration(
//       moment().add(1, 'days').startOf('day').diff(moment(this.forDate).startOf('day'))
//   ).asMinutes();
//   const durationToTmr = moment.duration(
//       moment('23:00:00', conf.timeformat).diff(moment())
//   ).asMinutes();
//   if (isTmr === 0 && durationToTmr <= 0) {
//     next(new Error('You are not allowed to edit tomorrow prize after 23:00:00.'));
//   }

//   next();
// });
// RedeemCodePoolModel.schema.index({
//     redeemCode: 1,
//     hold: 1,
// });
/**
 * Registration
 */
RedeemCodePoolModel.register();

/**
 * Public Class and functions
 */
function RedeemCode(){}


RedeemCode.prototype.getAvailableCodeList = function(id, qty, callback) {
  RedeemCodePoolModel.model.find({
    forCoupon: id
  })
  .sort({
    _id: 1,
    redeemCode: -1,
  })
  .limit(qty)
  .exec(callback);
}

RedeemCode.prototype.getAvailablePrizeCouponCodeList = function(id, qty, callback) {
  RedeemCodePoolModel.model.find({
    forPrize: id
  })
  .sort({
    _id: 1,
    redeemCode: -1,
  })
  .limit(qty)
  .exec(callback);
}

RedeemCode.prototype.getAllCouponCodes = function(callback) {
  RedeemCodePoolModel.model.find({
    forCoupon: {
      $exists: true,
    }
  })
  .exec(callback);
}

RedeemCode.prototype.createRedeemCodes = function(couponType, prizeCouponType, redeemCode, callback) {
  var data = {
    redeemCode: redeemCode,
    _id: mongoose.Types.ObjectId(),
  };
  if (couponType) {
    data.forCoupon = couponType;
  }else if (prizeCouponType) {
    data.forPrize = prizeCouponType;
  }
  // if (!!redeemCode && !!couponType) {
  //   // console.log('>>>>>> ', redeemCode, data._id);
  //   Utility.generateBarCodeImage(String(data._id), redeemCode, function(err, path) {
  //     if (path) {
  //       data.image = path;
  //     } else {
  //       console.log('> generate barcode error: ', err);
  //     }
  //     const code = new RedeemCodePoolModel.model(data);
  //     code.save(callback);
  //   });
  // } else {
  // RedeemCodePoolModel.model.findOneAndUpdate({
  //   redeemCode: redeemCode,
  // }, data, {
  //   upsert: true,
  //   new: true,
  // }).exec(callback);
  const code = new RedeemCodePoolModel.model(data);
  code.save(callback);
  // }
}

RedeemCode.prototype.removeCodes = function(ids, callback) {
  RedeemCodePoolModel.model.find({
    _id: {
      $in: ids,
    },
  })
  .remove()
  .exec(callback);
}

exports = module.exports = RedeemCode;
