/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone      = require('keystone');
const async         = require('async');
const moment        = require('moment');
const mongoose      = require('mongoose');
const Types         = keystone.Field.Types;
const Utility       = require(global.__base + '/helper/Utility');
const emailLib      = require(global.__base + '/lib/email');
// var LotteryHistoryController  = require(global.__base + '/models/game/lotteryHistory');
var LotteryDrawnLogController = require(global.__base + '/models/game/log/lotteryDrawnLog');
LotteryDrawnLogController = new LotteryDrawnLogController();
// const GameResult  = require(global.__base + '/enum/GameResult');
const Config = require(global.__base + '/config/Config');
const conf = new Config();
var LanguageType = require(global.__base + '/enum/LanguageType');
LanguageType = Utility.enumToKeystonOptionStr(LanguageType);
// const GameResultOptionStr = Utility.enumToKeystonOptionStr(GameResult);
/**
 * Lottery History Model
 * ==========
 */
//
const referencePrefix = conf.referencePrefix;

var LotteryHistoryModel = new keystone.List('LotteryHistory',{
    track: true,
    // history: true,
    nocreate: true,
    nodelete: true,
    noedit: false,
    label: 'All Game Histories',
    map: {
        name: 'referenceNo',
    },
    searchFields: 'player, email, phone, card, invoiceNo, ticketNo, referenceNo',
    defaultColumns: 'referenceNo, player, drawPlatform, resultType, invoiceNo, ticketNo, redeemed, completed, createdAt',
    defaultSort: '-createdAt',
});

LotteryHistoryModel.add(
  {
    referenceNo: {
      type: Types.Text,
      required: true,
      initial: true,
      noedit: true,
    },
  },
  "Player Information",
  {
    player: {
      type: Types.Text,
      required: true,
      initial: true,
      noedit: true,
    },
    email: {
      type: Types.Email,
      required: true,
      initial: true,
      index: true,
      noedit: true,
    },
    country: {
      type: Types.Relationship,
      ref: 'CountryList',
      label: 'Country Code',
      required: true,
      noedit: true,
      initial: true,
    },
    phone: {
      type: Types.Text,
      required: true,
      noedit: true,
      initial: true,
    },
    card: {
      type: Types.Text,
      required: true,
      noedit: true,
      initial: true,
      label: 'HKID Card / Passpord No',
    },
    deviceId: {
      type: Types.Text,
      noedit: true,
      hidden: true,
    },
    language: {
      type: Types.Select,
      noedit: true,
      options: LanguageType,
      hidden: true,
    },
  },
  "Ticket / Staff Information",
  {
    staff: {
      type: Types.Relationship,
      ref: 'Staff',
      label: 'Played from Staff',
      dependsOn: {
        drawPlatform: ['1', '2']
      },
      noedit: true,
    },
    invoiceNo: {
      type: Types.Text,
      note: 'Either use Invoice No or Ticket No / SmartFun Pass No',
      dependsOn: {
        drawPlatform: ['2']
      },
      noedit: true,
    },
    ticketNo: {
      type: Types.Text,
      label: 'Ticket No / SmartFun Pass No',
      note: 'Either use Invoice No or Ticket No / SmartFun Pass No',
      noedit: true,
    },
  },
  "Game Result Information",
  {
    drawPlatform: {
      type: Types.Select,
      label: 'Platform for the Game Play',
      options: [
        {
          label: 'Mobile App',
          value: '0',
        },
        {
          label: 'Operator Web',
          value: '1',
        },
        {
          label: 'Hotel Web',
          value: '2',
        }
      ],
      noedit: true,
      default: '0',
    },
    resultType: {
      type: Types.Select,
      label: 'Prize Type',
      options: [
        {
          label: 'Grand Prize*',
          value: '0',
        },
        {
          label: 'eCoupon',
          value: '1',
        }
      ],
      noedit: true,
      default: '1',
    },
    name: {
      type: Types.Text,
      noedit: true,
      label: 'Prize Name'
    },
    tcName: {
      type: Types.Text,
      noedit: true,
      label: 'Prize Chinese Traditional Name'
    },
    scName: {
      type: Types.Text,
      noedit: true,
      label: 'Prize Chinese Simplified Name'
    },
    completedDate: {
      type: Types.Date,
      noedit: true,
    },
    completed: {
      type: Types.Boolean,
      noedit: true,
    },
    emailSent: {
      type: Types.Boolean,
      default: false,
      noedit: true,
      // hidden: true
    },
    emailSentAt: {
      type: Types.Datetime,
      noedit: true,
      label: 'Last Email Sent At',
      // hidden: true
    },
    agreed: {
      type: Types.Datetime,
      required: true,
      initial: true,
      label: 'Terms & Conditions Agreed At',
      note: 'Agreed date & time of Terms & Conditions agreement',
      noedit: true,
    },
    shouldMarketing: {
      type: Types.Boolean,
      label: 'Is agreed Marketing Information',
      noedit: true,
    },
    marketing: {
      type: Types.Datetime,
      label: 'Marketing Information Agreed At',
      note: 'Agreed date & time of Marketing Information',
      noedit: true,
    },
    resultLog: {
      type: Types.Relationship,
      ref: 'EligibilityLog',
      noedit: true,
      hidden: true,
    },
    emailTpl: {
      hidden: true,
      type: Types.Text,
      noedit: true,
    }
  },
  "Redemption Result",
  {
    redeemed: {
      type: Types.Boolean,
      label: 'Picked By Player (Please redeem the prize in Redeem Prize page)',
      noedit: true,
      default: false,
    },
    redeemedAt: {
      type: Types.Datetime,
      label: 'Date of picked By Player',
      noedit: true,
    },
  }
);

/**
 * Registration
 */

// LotteryHistoryModel.schema.index({
//     ticketNo: 1,
//     invoiceNo: 1,
// }, {
//     unique: true,
// });

function getHistoryById(id, callback) {
  LotteryHistoryModel.model
    .findOne({
      // status: 'completed',
      _id: id,
    }).populate('resultLog')
    .exec(callback);
}



//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU
//>>>>>>>>>>> FUCK YOU HERE
function sendCompletedEmail(doc, callback) {
  var tasks = [
    // get lottery history with populated eligibility log
    function(cb) {
      getHistoryById(doc._id, cb);
    },
    // populate lottery draw log
    function(history, cb) {
      const eligibilityLog = history.resultLog;
      if (eligibilityLog) {
        history = history.toObject();
        LotteryDrawnLogController.getLog(eligibilityLog.resultLog, function(err, drewLog) {
          if (drewLog) {
            // assign drew result log object to eligibility result log
            history.resultLog.resultLog = drewLog;
          }
          
          cb(null, history);
        });
      } else {
        cb(null);
      }
    },
  ];

  async.waterfall(tasks, function(err, history) {
    // send out email again
    if (history) {
      emailLib.sendSingleDrawResultEmail(history, function(err) {
        if (!err) {
          updateEmailSent(history._id, function(err) {
            console.log('> Update Email Sent: ', history._id, ', Error: ', err);
            callback();
          });
        } else {
          console.log('> error: ', err);
          callback();
        }
      });
    }
  });  
}

LotteryHistoryModel.schema.pre('save', function(next) {
  // trigger resend if hotel play or any completed history
  if (this.completed || !!this.invoiceNo) {
    sendCompletedEmail(this, next);
  }
});
// LotteryHistoryModel.schema.pre('save', function(next) {
  // const isFunctionDate = moment.duration(
  //     moment(this.completedDate)
  //       .startOf('day').diff(moment().startOf('day'))
  // ).asMinutes()

  // // not allowed to edit prize not at 19-02-2019
  // if (!doc.isNew && isFunctionDate >= 0 && !this.invoiceNo) {
  //   return next('The game is not today history. Please do not edit any information.');
  // }
//   next();
// });
LotteryHistoryModel.register();

function updateEmailSent(id, callback) {
  LotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: mongoose.Types.ObjectId(id),
    }, {
      emailSent: true,
      emailSentAt: moment().toDate(),
    })
    .exec(callback);
}

/**
 * Public Class and functions
 */
function LotteryHistory(){}
LotteryHistory.prototype.cleanAll = function(callback) {
  LotteryHistoryModel.model.remove({}).exec(callback);
}

LotteryHistory.prototype.redeemHistory = function(data, callback) {
  LotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: mongoose.Types.ObjectId(data.history),
    }, {
      redeemed: true,
      redeemedAt: data.redeemedAt,
    })
    .exec(callback);
}

LotteryHistory.prototype.updateEmailStatus = function(data, callback) {
  LotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: mongoose.Types.ObjectId(data.history),
    }, {
      emailSent: true,
      emailSentAt: data.emailSentAt,
    })
    .exec(callback);
}

LotteryHistory.prototype.getParticipantsByDate = function(date, callback) {
  LotteryHistoryModel.model
	  .find({
      // status: 'completed',
      completedDate: {
        lte: moment(date).toDate(),
      },
	  }, {
      email: 1,
      phone: 1,
      player: 1,
      card: 1,
    })
	  .exec(callback);
}

LotteryHistory.prototype.getHistoryById = getHistoryById;

LotteryHistory.prototype.getPassHistoryList = function(callback) {
  LotteryHistoryModel.model
	  .find({
      completedDate: {
        lte: moment(date).toDate(),
      },
	  }, {
      email: 1,
      phone: 1,
      player: 1,
      card: 1,
    })
	  .exec(callback);
}

LotteryHistory.prototype.updateCompleted = function(id, callback) {
  LotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: id,
    }, {
      completed: true,
      completedDate: moment().toDate(),
    })
    .exec(callback);
}

// LotteryHistory.prototype.updateEmailSent = function(id, callback) {
//   LotteryHistoryModel.model
//     .findOneAndUpdate({
//       // status: 'completed',
//       _id: mongoose.Types.ObjectId(id),
//     }, {
//       emailSentAt: moment().toDate(),
//       emailSent: true,
//     })
//     .exec(callback);
// }

LotteryHistory.prototype.updateEmailTpl = function(id, config, callback) {
  LotteryHistoryModel.model
    .findOneAndUpdate({
      // status: 'completed',
      _id: mongoose.Types.ObjectId(id),
    }, {
      emailTpl: config,
      emailSent: true,
      emailSentAt: moment().toDate(),
    })
    .exec(callback);
}

LotteryHistory.prototype.getHistoryByTicketNo = function(ticketNo, callback) {
  LotteryHistoryModel.model
    .findOne({
      // status: 'completed',
      ticketNo: ticketNo,
    }).populate('resultLog').lean()
    .exec(callback);
}

LotteryHistory.prototype.getHistoryByGameLog = function(id, callback) {
  LotteryHistoryModel.model
    .findOne({
      // status: 'completed',
      resultLog: id,
    }).populate('resultLog').lean()
    .exec(callback);
}

LotteryHistory.prototype.getHistoriesByDeviceIdAndResult = function(id, resultType, callback) {
  LotteryHistoryModel.model
    .find({
      // status: 'completed',
      deviceId: id,
      resultType: resultType,
      completed: true,
    }).populate('resultLog').sort({ completedDate: -1 }).lean()
    .exec(callback);
}

LotteryHistory.prototype.createHistory = function(data, callback) {
  var toBeUpdated = {
    email: data.email,
    phone: data.phone,
    player: data.player,
    card: data.card,
    country: data.country,
    resultLog: data.resultLog,
    resultType: data.resultType,
    referenceNo: data.referenceNo,
    marketing: data.marketing,
    shouldMarketing: data.shouldMarketing,
    completed: data.completed,
    language: data.language,
    drawPlatform: data.drawPlatform,
    name: data.name,
    tcName: data.tcName,
    scName: data.scName,
  };
  if (data.agreed) {
    toBeUpdated.agreed = data.agreed;
  }
  if (data.deviceId) {
    toBeUpdated.deviceId = data.deviceId;
  }
  // if (data.ecoupon) {
  //   toBeUpdated.ecoupon = data.ecoupon;
  //   toBeUpdated.redeemCode = data.redeemCode;
  // } else {
  //   toBeUpdated.gift = data.gift;
  // }

  if (data.ticketNo) {
    toBeUpdated.ticketNo = data.ticketNo;
  } else {
    toBeUpdated.invoiceNo = data.invoiceNo;
  }

  if (data.staff) {
    toBeUpdated.staff = data.staff;
  }
  const history = new LotteryHistoryModel.model(toBeUpdated);
    
  history.save(function(err, result) {
    console.log(err, data.drawPlatform);
    callback(err, history);
  });
}

// LotteryHistory.prototype.getDrawnHistoryCount = function(prize, callback) {
//   LotteryHistoryModel.model
//     .count({ gift: prize })
//     .exec(callback);
// }

exports = module.exports = LotteryHistory;
