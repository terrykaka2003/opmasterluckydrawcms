/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
var keystone = require('keystone');
var Types = keystone.Field.Types;
const Utility   = require(global.__base + '/helper/Utility');
/**
 * Game Character Model
 * ==========
 */

var GameCharacterModel = new keystone.List('GameCharacter',{
    track: true,
    // history: true,
    label: 'Game Character Icon',
    defaultColumns: 'type, icon',
});

GameCharacterModel.add({
  icon:{
    type: Types.LocalFile,
    dest: './media/upload/game-character',
    allowedTypes: [
      'image/jpeg', 'image/png', 'image/gif'
    ],
    filename: function(item, file){
      return item.id + '-' + Utility.getRandomInt(0,999999) + '.' + file.extension
    },
    format: function(item, file){
      return '<img src="/upload/game-character/' +file.filename+'" style="max-width: 100px">'
    },
    maxSize: 900000,
    note: 'Notice:the image should under 900KB',
  },
  // name: {
  //   type: Types.Text, 
  //   initial: true,
  //   index: true,
  //   required: true,
  //   note: 'English Name'
  // }, 
  // tcName: {
  //   type: Types.Text, 
  //   require: true,
  //   initial: true,
  //   note: 'Chinese Traditional Name'
  // }, 
  // scName: {
  //   type: Types.Text,
  //   require: true,
  //   initial: true,
  //   note: 'Chinese Simpified Name'
  // }, 
  type: {
    type: Types.Select,
    options: [
      {
        label: 'Big Prize',
        value: 'bp'
      },
      {
        label: 'Ecoupon',
        value: 'ec'
      }
    ],
    default: 'ec',
  },
});

// NewsModel.schema.pre('save', function(next){
//   if(!this.isNew) return next();

//   var parentLang = new ParentLang;
//   parentLang.create(this, (function(err, result){
//     this.parentLang = result._id;
//     next();
//   }).bind(this));
// });


/**
 * Registration
 */

GameCharacterModel.register();


/**
 * Public Class and functions
 */
function GameCharacter(){}

GameCharacter.prototype.getByType = function(type, callback) {
  GameCharacterModel.model
	  .find({
      type: type
	  },{
      name: 1,
      icon: 1,
      type: 1
    }).sort({ type: 1 }).lean()
	  .exec(callback);
}
GameCharacter.prototype.getAll = function(callback) {
  GameCharacterModel.model
    .find({}, {
      name: 1,
      icon: 1,
      type: 1
    }).sort({ type: 1 }).lean()
    .exec(callback);
}

exports = module.exports = GameCharacter;
