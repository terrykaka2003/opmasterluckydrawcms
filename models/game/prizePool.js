/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const Utility   = require(global.__base + '/helper/Utility');
const Config    = require(global.__base + '/config/Config');
const conf      = new Config();
const Types     = keystone.Field.Types;

var GameResult = require(global.__base + '/enum/GameResult');
GameResult = Utility.enumToKeystonOptionStr(GameResult);

var GiftType = require(global.__base + '/enum/GiftType');
GiftType = Utility.enumToKeystonOptionStr(GiftType);
/**
 * PrizePool Model
 * For draw out the prize to player for transaction concern
 * Separate each of prize with qty into one single prize
 * ==========
 */

var PrizePoolModel = new keystone.List('PrizePool',{
    track: true,
    // history: true,
    label: 'Daily Prize Pool',
    map: {
        name: 'gift',
    },
    searchFields: 'gift, tcGift, scGift',
    defaultColumns: 'gift, tcGift, scGift, quality, priority, forDate',
});

PrizePoolModel.add(
  "Setting",
  {
    priority: {
      type: Types.Number,
      default: 0,
      note: '0 is the lowest priority, and the large qty of the prize should be set the higher priority',
    },
    hold: {
      type: Types.Boolean,
      // options: GameResult,
      default: true,
    },
    forDate: {
      type: Types.Date,
      label: 'Date for Lucky Draw',
      required: true,
      initial: true,
      index: true,
    },
    prizeBox: {
      type: Types.Relationship,
      ref: 'Prize',
    },
    prizeCoupon: {
      type: Types.Boolean,
      default: false,
      label: 'Special PrizeCoupon',
      note: 'Is Special PrizeCoupon Type'
    },
    redeemCode: {
      type: Types.Text,
      dependsOn: {
        prizeCoupon: true,
      }
    },
    emailList: {
      type: Types.TextArray,
      note: 'Forward prize email to the recipient from this list'
    },
  },
  "Information",
  {
    gift: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'English Prize Name',
    },
    tcGift: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Traditional Prize Name',
    },
    scGift: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Simplified Prize Name',
    },
    tc: {
      type: Types.Html,
      wysiwyg: true,
      label: 'English T&C',
      dependsOn: {
        prizeCoupon: true,
      },
      note: 'For PrizeCoupon Type only'
    },
    tcTC: {
      type: Types.Html,
      wysiwyg: true,
      label: 'Chinese Traditional T&C',
      dependsOn: {
        prizeCoupon: true,
      },
      note: 'For PrizeCoupon Type only'
    },
    scTC: {
      type: Types.Html,
      wysiwyg: true,
      label: 'Chinese Simplified T&C',
      dependsOn: {
        prizeCoupon: true,
      },
      note: 'For PrizeCoupon Type only'
    }
  },
  "Email Template",
  {
    inParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English App Email Format'
    },
    tcInParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional App Email Format'
    },
    scInParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified App Email Format'
    },
    outParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web Email Format'
    },
    tcOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web Email Format'
    },
    scOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web Email Format'
    },
    hotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Hotel Web Email Format'
    },
    tcHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Hotel Web Email Format'
    },
    scHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Hotel Web Email Format'
    },
  },
  "Game Winning Information",
  {
    appWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English App Winning Information'
    },
    tcAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional App Winning Information'
    },
    scAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified App Winning Information'
    },
    webParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Park) Winning Information'
    },
    tcWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Park) Winning Information'
    },
    scWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Park) Winning Information'
    },
    webHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Hotel) Winning Information'
    },
    tcWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Hotel) Winning Information'
    },
    scWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Hotel) Winning Information'
    },
  }
);

/**
 * Registration
 */

PrizePoolModel.register();

/**
 * Public Class and functions
 */
function PrizePool(){}

PrizePool.prototype.getAvailablePrizeList = function(callback) {
  PrizePoolModel.model.find({
    forDate: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().endOf('day').toDate(),
    },
    hold: false,
    // status: {
    //   $eq: 'idle',
    // },
    // type: type,
  }).populate('prizeBox').sort({ priority: -1 }).exec(callback);
}
PrizePool.prototype.updateForDate = function(callback) {
  PrizePoolModel.model.update({}, {
    forDate: moment().startOf('day').toDate(),
  }, {
    multi: true,
  }, callback);
}
PrizePool.prototype.addPrize = function(data, callback) {
  const newPrize = new PrizePoolModel.model({
    // forDate: process.env.NODE_ENV === 'production' ? moment().add(1, 'days').startOf('day').toDate() : moment().startOf('day').toDate(),
    forDate: moment().add(1, 'days').startOf('day').toDate(),
    gift: data.gift,
    tcGift: data.tcGift,
    scGift: data.scGift,
    tcDescription: data.tcDescription,
    scDescription: data.scDescription,
    prizeBox: data._id,
    description: data.description,
    priority: data.priority,
    hold: false,
    prizeCoupon: data.prizeCoupon,
    inParkFormat: data.inParkFormat,
    tcInParkFormat: data.tcInParkFormat,
    scInParkFormat: data.scInParkFormat,
    outParkFormat: data.outParkFormat,
    tcOutParkFormat: data.tcOutParkFormat,
    scOutParkFormat: data.scOutParkFormat,
    hotelFormat: data.hotelFormat,
    tcHotelFormat: data.tcHotelFormat,
    scHotelFormat: data.scHotelFormat,
    emailList: data.emailList,
    
    appWinningInfo: data.appWinningInfo,
    tcAppWinningInfo: data.tcAppWinningInfo,
    scAppWinningInfo: data.scAppWinningInfo,
    webHotelWinningInfo: data.webHotelWinningInfo,
    tcWebHotelWinningInfo: data.tcWebHotelWinningInfo,
    scWebHotelWinningInfo: data.scWebHotelWinningInfo,
    webParkWinningInfo: data.webParkWinningInfo,
    tcWebParkWinningInfo: data.tcWebParkWinningInfo,
    scWebParkWinningInfo: data.scWebParkWinningInfo,

    tc: data.tc,
    tcTC: data.tcTC,
    scTC: data.scTC,
    redeemCode: data.redeemCode,
    // type: data.type,
  });
  newPrize.save(callback);
}

PrizePool.prototype.updateStatus = function(data, callback) {
  PrizePoolModel.model.update({
    _id: data.id
  }, {
    hold: data.hold
  }, {}, callback);
}

PrizePool.prototype.isExistingPrize = function(data, callback) {
  PrizePoolModel.model.find({
    prizeBox: data._id,
  }).exec(callback);
}

PrizePool.prototype.removePrize = function(data, callback) {
  PrizePoolModel.model.find({
    prizeBox: data._id,
  }).remove().exec(callback);
}

PrizePool.prototype.removeById = function(id, callback) {
  PrizePoolModel.model.find({
    _id: id,
  }).remove().exec(callback);
}

// remove all of the pass prize from the pool
PrizePool.prototype.removeAllExpiredPrize = function(callback) {
  PrizePoolModel.model.remove({
    forDate: {
      $lt: moment().startOf('day').toDate(),
      // $lte: moment().endOf('day').toDate()
    }
    // $and: [
    //   {
    //     $or: [
    //       {
    //         forDate: {
    //           $lt: moment().startOf('day').toDate(),
    //         },
    //       },
    //       {
    //         forDate: {
    //           $ne: moment().startOf('day').toDate(),
    //         },
    //       }
    //     ]
    //   },
    // ]
  }, callback);
}

exports = module.exports = PrizePool;
