/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const Config    = require(global.__base + '/config/Config');
const conf      = new Config();
const Types     = keystone.Field.Types;
const Utility   = require(global.__base + '/helper/Utility');

var GiftType = require(global.__base + '/enum/GiftType');
GiftType = Utility.enumToKeystonOptionStr(GiftType);
/**
 * News Model
 * ==========
 */

var PrizeModel = new keystone.List('Prize',{
    track: true,
    // history: true,
    label: 'Prize Box',
    map: {
        name: 'gift',
    },
    searchFields: 'gift, tcGift, scGift',
    defaultColumns: 'gift, tcGift, scGift, quantity, priority, prizeCoupon, forStartDate, forEndDate',
    defaultSort: '-priority, forStartDate',
});

PrizeModel.add(
  "Config",
  {
    quantity: {
      type: Types.Number,
      required: true,
      initial: true,
      default: 0,
      label: 'Daily Quota',
      note: 'Total Number for Prize Distribution on everyday or in the white list',
    },
    forStartDate: {
      type: Types.Date,
      label: 'Start Date for Lucky Draw',
      required: true,
      initial: true,
      note: 'The start date for the prize distribution into the prize pool in the midnight on everyday'
    },
    forEndDate: {
      type: Types.Date,
      label: 'End Date for Lucky Draw',
      required: true,
      initial: true,
      note: 'The end date for the prize distribution into the prize pool in the midnight on everyday'
    },
    whiteList: {
      type: Types.DateArray,
      label: 'Including Date List',
      note: 'While list to distribution the prize to The Prize Pool, everyday distribution will be ignored'
    },
    priority: {
      type: Types.Number,
      default: 0,
      note: '0 is the lowest priority, and the huge qty prize should be set the higher priority',
    },
    emailList: {
      type: Types.TextArray,
      note: 'Forward prize email to the recipient from this list'
    },
    enabled: {
      type: Types.Boolean,
      default: true,
      note: 'The gift will not be drawn to the player if it is disabled',
    },
    prizeCoupon: {
      type: Types.Boolean,
      default: false,
      label: 'Special PrizeCoupon',
      note: 'Is Special PrizeCoupon Type'
    }
  },
  "Information",
  {
    gift: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Prize Name',
    },
    tcGift: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Traditional Prize Name',
    },
    scGift: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Simplified Prize Name',
    },
    tc: {
      type: Types.Html,
      wysiwyg: true,
      dependsOn: {
        prizeCoupon: true,
      },
      label: 'English T&C',
      note: 'For PrizeCoupon Type only'
    },
    tcTC: {
      type: Types.Html,
      wysiwyg: true,
      dependsOn: {
        prizeCoupon: true,
      },
      label: 'Chinese Traditional T&C',
      note: 'For PrizeCoupon Type only'
    },
    scTC: {
      type: Types.Html,
      wysiwyg: true,
      dependsOn: {
        prizeCoupon: true,
      },
      label: 'Chinese Simplified T&C',
      note: 'For PrizeCoupon Type only'
    },
  },
  "Email Template",
  {
    inParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English App Email Format'
    },
    tcInParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional App Email Format'
    },
    scInParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified App Email Format'
    },
    outParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web Email Format'
    },
    tcOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web Email Format'
    },
    scOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web Email Format'
    },
    hotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Hotel Web Email Format'
    },
    tcHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Hotel Web Email Format'
    },
    scHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Hotel Web Email Format'
    },
  },
  "Game Winning Information",
  {
    appWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English App Winning Information'
    },
    tcAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional App Winning Information'
    },
    scAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified App Winning Information'
    },
    webParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Park) Winning Information'
    },
    tcWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Park) Winning Information'
    },
    scWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Park) Winning Information'
    },
    webHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Hotel) Winning Information'
    },
    tcWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Hotel) Winning Information'
    },
    scWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Hotel) Winning Information'
    },
  }
);

const checkValidUpdate = function(doc) {
  // const durationFromToday = moment.duration(
  //     moment().startOf('day').diff(moment(doc.forDate).startOf('day'))
  // ).asMinutes()

  // // not allowed to edit today prize
  // if (durationFromToday >= 0) {
  //   return new Error('You are not allowed to edit today prize since the Lottery Game is being started.');
  // }

  // const isTmr = moment.duration(
  //     moment().add(1, 'days').startOf('day').diff(moment(doc.forDate).startOf('day'))
  // ).asMinutes();
  const from = moment('22:59:59', conf.timeformat);
  const to = moment('00:00:00', conf.timeformat);
  // const from = moment.duration(
  //     moment('23:00:00', conf.timeformat).diff(moment())
  // ).asMinutes();
  // const to = moment.duration(
  //   moment('23:59:59', 'HH:mm:ss').add(1, 'days').diff(moment())).asMinutes();
  // the prize will be blocked during 23:00 to 23:59 everyday
  // if (from < 0 || to > 0) {
    if (moment().isBetween(from, to)) {
    return new Error('You are not allowed to edit the grand prize from 23:00:00 to 23:59:59. The prize is being distributed into the Daily Prize Pool.');
  }

  return null;
}

PrizeModel.schema.pre('remove', function(next) {
  const error = checkValidUpdate(this);
  if (error) {
    next(error);
  }
  next();
});

PrizeModel.schema.pre('save', function(next) {
  const error = checkValidUpdate(this);
  if (error) {
    next(error);
  }
  this.forEndDate = moment(this.forEndDate).endOf('day').toDate();
  next();
});

/**
 * Registration
 */
PrizeModel.register();

/**
 * Public Class and functions
 */
function Prize(){}

// Prize.prototype.getThePrizeQty = function(callback) {
//   PrizeModel.model.find({
//     forDate: {
//       $gte: moment().startOf('day').toDate(),
//       $lte: moment().endOf('day').toDate(),
//     },
//   }, { quantity: 1 }).exec(function(err, prize) {
//     callback(err, (prize ? !prize.quantity : 0));
//   });
// }
Prize.prototype.getPrizeById = function(id, callback) {
  PrizeModel.model.findById(id).lean().exec(callback);
}

Prize.prototype.addPrize = function(data, callback) {
  const newPrize = new PrizeModel.model({
    gift: data.gift,
    tcGift: data.tcGift,
    scGift: data.scGift,
    priority: data.priority,
    tc: data.tc,
    tcTC: data.tcTC,
    scTC: data.scTC,
    forStartDate: data.forStartDate,
    forEndDate: data.forEndDate,
    whiteList: data.whiteList,
    prizeCoupon: data.prizeCoupon,
    inParkFormat: data.inParkFormat,
    tcInParkFormat: data.tcInParkFormat,
    scInParkFormat: data.scInParkFormat,
    outParkFormat: data.outParkFormat,
    tcOutParkFormat: data.tcOutParkFormat,
    scOutParkFormat: data.scOutParkFormat,

    appWinningInfo: data.appWinningInfo,
    tcAppWinningInfo: data.tcAppWinningInfo,
    scAppWinningInfo: data.scAppWinningInfo,
    webHotelWinningInfo: data.webHotelWinningInfo,
    tcWebHotelWinningInfo: data.tcWebHotelWinningInfo,
    scWebHotelWinningInfo: data.scWebHotelWinningInfo,
    webParkWinningInfo: data.webParkWinningInfo,
    tcWebParkWinningInfo: data.tcWebParkWinningInfo,
    scWebParkWinningInfo: data.scWebParkWinningInfo,

    emailList: data.emailList,

    // appFormat: data.appFormat,
    // tcAppFormat: data.tcAppFormat,
    // scAppFormat: data.scAppFormat,
    // webFormat: data.webFormat,
    // tcWebFormat: data.tcWebFormat,
    // scWebFormat: data.scWebFormat,
    hotelFormat: data.hotelFormat,
    tcHotelFormat: data.tcHotelFormat,
    scHotelFormat: data.scHotelFormat,


    quantity: data.quantity,
    enabled: data.enabled,
    // type: data.type,
  });
  newPrize.save(callback);
}

Prize.prototype.getAvailablePrizeList = function(callback) {
  PrizeModel.model.find({
    // type: 'prize',
    $and: [
      {
        quantity: {
          $gte: 1,
        },
        forStartDate: {
          $lte: moment().startOf('day').toDate(),
        },
        forEndDate: {
          $gte: moment().endOf('day').toDate(),
        },
        enabled: true,
      },
      {
        $or: [
          {
            whiteList: {
              $elemMatch: {
                $lte: moment().endOf('day').toDate(),
                $gte: moment().startOf('day').toDate(),
              }
            }
          },
          {
            whiteList: {
              $exists: false,
            },
          },
          {
            'whiteList.0': {
              $exists: false,
            }
          },
          // {
          //   'whiteList': {
          //     $size: {
          //       $lte: 0,
          //     },
          //   }
          // }
        ],
      }
    ]
  }).sort({ priority: -1 }).lean().exec(callback);
  // const pipeline = [
  //   // 1.0 find out in of stock prize for first prize, which are served for today
  //   {
  //     $match: {
  //       type: 'prize',
  //       quantity: {
  //         $gte: 1,
  //       },
  //       forDate: {
  //         $gte: moment().startOf('day'),
  //         $lt: moment().endOf('day'),
  //       },
  //     },
  //   },
  //   // 2.0 lookup all of histories for that gift which is drawn to player before
  //   {
  //     $lookup: {
  //       from: 'lotteryhistories',
  //       localField: '_id',
  //       foreignField: 'gift',
  //       as: 'records',
  //     },
  //   },
  //   {
  //     $project: {
  //       gift: 1,
  //       quantity: {

  //       },
  //       description: 1,
  //     },
  //   },
  // ];
  // PrizeModel.model
	 //  .findOne({

	 //  })
	 //  .exec(callback);
}

Prize.prototype.addWinningInfo = function(callback) {
  PrizeModel.model.update({}, {
    $set: {

      appWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        Reference no: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          Prize redemption letter will be notified through registered email. Please present the email notification and personal identification (for verification) at our Prize Redemption Center (near Aqua City Bakery in  Waterfront) on or before Park Close today to claim the prize. Claims after this date will not be accepted.
        </div>
        <div class="g-congrad-button">
          <button class="g-congrad-scan-other">Scan Next Ticket</button>
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          Friendly reminder: The ultimate Grand Prizes draw will be conducted on 21 Feb, 2019 and winners will be drawn randomly by the computer system. The winner list will be announced on Hong Kong Ocean Park official website and winner of the Grand Prizes will be notified by phone within 14 working days.
        </div>
      `,
      tcAppWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        參考編號: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          閣下將收到得奬確認電郵。請於今天公園關門前親臨本公園獎品換領處(位於海濱樂園內的水都餅店旁)出示確認電郵並提供身份證明文件(以作核實用途)領奬。逾期恕不接受。 
        </div>
        <div class="g-congrad-button">
          <button class="g-congrad-scan-other">掃另一門票</button>
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您:終極大獎將於2019年2月21日由電腦隨機方式抽出得獎者,得獎名單將於海洋公園官方網站公佈。得獎者將獲專人以電話於14個工作天內通知領獎
        </div>
      `,
      scAppWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        参考编号: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          阁下将收到得奖确认电邮。请于今天公园关门前亲临本公园奖品换领处（位于海滨乐园内的水都饼店旁）出示确认电邮并提供身份证明文件（以作核实用途）领奖。逾期恕不接受。
        </div>
        <div class="g-congrad-button">
          <button class="g-congrad-scan-other">扫另一门票</button>
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您：终极大奖将于2019年2月21日由电脑随机方式抽出得奖者，得奖名单将于海洋公园官方网站公布得奖者将获专人以电话于14个工作天内通知领奖
        </div>
      `,
      webHotelWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        Reference no: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          Prize redemption letter will be notified through registered email. Please present the email notification to our Prize Redemption Center (near Aqua City Bakery in  Waterfront) on or before Park close to claim the prize. Claims after this date will not be accepted
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          Friendly reminder: The ultimate Grand Prizes draw will be conducted on 21 Feb, 2019 and winners will be drawn randomly by the computer system. The winner list will be announced on Hong Kong Ocean Park official website and winner of the Grand Prizes will be notified by phone within 14 working days.
        </div>
      `,
      tcWebHotelWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        參考編號: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          閣下將收到得奬確認電郵。請於今天公園關門前親臨本公園獎品換領處(位於海濱樂園內的水都餅店旁)出示確認電郵領奬並出。逾期恕不接受。
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您:終極大獎將於2019年2月21日由電腦隨機方式抽出得獎者,得獎名單將於海洋公園官方網站公佈。得獎者將獲專人以電話於14個工作天內通知領獎。
        </div>
      `,
      scWebHotelWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        参考编号: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          阁下将收到得奖确认电邮。请于今天公园关门前亲临本公园奖品换领处（位于海滨乐园内的水都饼店旁）出示确认电邮领奖并出。逾期恕不接受。
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您：终极大奖将于2019年2月21日由电脑随机方式抽出得奖者，得奖名单将于海洋公园官方网站公布得奖者将获专人以电话于14个工作天内通知领奖。
        </div>
      `,
      webParkWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        Reference no: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          Prize redemption letter will be notified through registered email. For any enquiries, please drop down the reference number above and contact us at (852) 3923 2323. Have a nice day!
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          Friendly reminder: The ultimate Grand Prizes draw will be conducted on 21 Feb, 2019 and winners will be drawn randomly by the computer system. The winner list will be announced on Hong Kong Ocean Park official website and winner of the Grand Prizes will be notified by phone within 14 working days.
        </div>
      `,
      tcWebParkWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        參考編號: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          閣下將收到得奬確認電郵。請於今天公園關門前親臨本公園獎品換領處(位於海濱樂園內的水都餅店旁)出示確認電郵領奬並出。逾期恕不接受。
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您:終極大獎將於2019年2月21日由電腦隨機方式抽出得獎者,得獎名單將於海洋公園官方網站公佈。得獎者將獲專人以電話於14個工作天內通知領獎。
        </div>
      `,
      scWebParkWinningInfo: `
        <h3 class="desc g-prize-desc">
        [GiftName]<br />
        参考编号: [ReferenceNo]
        </h3>
        <div class="g-congrad-body-reminder remark g-remark">
          阁下将收到得奖确认电邮。请于今天公园关门前亲临本公园奖品换领处（位于海滨乐园内的水都饼店旁）出示确认电邮领奖并出。逾期恕不接受。
        </div>
        <div class="g-congrad-body-remark reminder g-reminder">
          提提您：终极大奖将于2019年2月21日由电脑随机方式抽出得奖者，得奖名单将于海洋公园官方网站公布得奖者将获专人以电话于14个工作天内通知领奖。
        </div>
      `
    }
  }, { multi: true }).exec(callback);
}

exports = module.exports = Prize;
