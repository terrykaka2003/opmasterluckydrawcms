/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const _         = require('lodash');
const Config    = require(global.__base + '/config/Config');
const conf      = new Config();
const Types     = keystone.Field.Types;
const Utility   = require(global.__base + '/helper/Utility');

var GiftType = require(global.__base + '/enum/GiftType');
GiftType = Utility.enumToKeystonOptionStr(GiftType);
/**
 * News Model
 * ==========
 */
 const levelPrize = [
  {
    label: '1st Prize',
    value: 1
  },
  {
    label: '2nd Prize',
    value: 2
  },
  {
    label: '3rd Prize',
    value: 3
  },
];

var BigPrizeModel = new keystone.List('BigPrize',{
    track: true,
    label: 'Ultimate Prize',
    map: {
        name: 'gift',
    },
    searchFields: 'gift',
    defaultColumns: 'gift, level, enabled',
    defaultSort: '-level',
});

BigPrizeModel.add(
  "Setting",
  {
    level: {
      type: Types.Select,
      default: 3,
      options: levelPrize,
      note: 'level of Ultimate Prize (e.g. 1st to 3rd)'
    },
    enabled: {
      type: Types.Boolean,
      default: true,
    },
  },
  "Information",
  {
    gift: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Prize Name',
    },
    tcGift: {
      type: Types.Text,
      require: true,
      initial: true,
      label: 'Chinese Traditional Prize Name',
    },
    scGift: {
      type: Types.Text,
      require: true,
      initial: true,
      label: 'Chinese Simplified Prize Name',
    },
    tc: {
      type: Types.Html,
      wysiwyg: true,
      note: 'English T&C',
    },
    tcTC: {
      type: Types.Html,
      wysiwyg: true,
      label: 'Chinese Traditional T&C',
    },
    scTC: {
      type: Types.Html,
      wysiwyg: true,
      label: 'Chinese Simplified T&C',
    },
  }
);

const checkValidUpdate = function(doc) {
  const isFunctionStartDate = moment.duration(
      moment('2019-02-19', 'YYYY-MM-DD')
        .startOf('day').diff(moment().startOf('day'))
  ).asMinutes()

  // not allowed to edit prize not at 19-02-2019
  if (isFunctionStartDate === 0) {
    return new Error('The date of special luckydraw is started. You are not allowed to edit the prize.');
  } else if (isFunctionStartDate < 0) {
    return new Error('The date of special luckydraw is over. You are not allowed to edit the prize.');
  }

  return null;
}

BigPrizeModel.schema.pre('remove', function(next) {
  const error = checkValidUpdate(this);
  if (error) {
    next(error);
  }
  next();
});

BigPrizeModel.schema.pre('save', function(next) {
  const error = checkValidUpdate(this);
  if (error) {
    next(error);
  }
  next();
});

/**
 * Registration
 */
BigPrizeModel.register();

/**
 * Public Class and functions
 */
function BigPrize(){}

// Prize.prototype.getThePrizeQty = function(callback) {
//   PrizeModel.model.find({
//     forDate: {
//       $gte: moment().startOf('day').toDate(),
//       $lte: moment().endOf('day').toDate(),
//     },
//   }, { quantity: 1 }).exec(function(err, prize) {
//     callback(err, (prize ? !prize.quantity : 0));
//   });
// }
BigPrize.prototype.getLevel = function(callback) {
  return _.map(levelPrize, function(l) {
    return l.value;
  });
}

// BigPrize.prototype.getAvailableByIndex = function(level, , callback) {
//   BigPrizeModel.model.find({
//     enabled: true,
//     level: level,
//   }).skip(skip).limit(1).sort({ createdAt: -1 }).exec(callback);
// }

BigPrize.prototype.getList = function(callback) {
  BigPrizeModel.model.find({
    // type: 'prize',
    enabled: true,
    // type: type,
    // enabled: true,
  }).sort({ level: -1 }).exec(callback);
  // const pipeline = [
  //   // 1.0 find out in of stock prize for first prize, which are served for today
  //   {
  //     $match: {
  //       type: 'prize',
  //       quantity: {
  //         $gte: 1,
  //       },
  //       forDate: {
  //         $gte: moment().startOf('day'),
  //         $lt: moment().endOf('day'),
  //       },
  //     },
  //   },
  //   // 2.0 lookup all of histories for that gift which is drawn to player before
  //   {
  //     $lookup: {
  //       from: 'lotteryhistories',
  //       localField: '_id',
  //       foreignField: 'gift',
  //       as: 'records',
  //     },
  //   },
  //   {
  //     $project: {
  //       gift: 1,
  //       quantity: {

  //       },
  //       description: 1,
  //     },
  //   },
  // ];
  // PrizeModel.model
	 //  .findOne({

	 //  })
	 //  .exec(callback);
}

BigPrize.prototype.addPrize = function(data, callback) {
  const newPrize = new BigPrizeModel.model({
    gift: data.gift,
    tcGift: data.tcGift,
    scGift: data.scGift,
    level: data.level,
    tc: data.tc,
    tcTC: data.tcTC,
    scTC: data.scTC,
    enabled: data.enabled,
  });
  newPrize.save(callback);
}

exports = module.exports = BigPrize;
