/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const _         = require('lodash');
const Config    = require(global.__base + '/config/Config');
const conf      = new Config();
const Types     = keystone.Field.Types;
var BigPrizeController      = require(global.__base + '/models/game/bigGame/bigPrize');
const Utility   = require(global.__base + '/helper/Utility');
const emailLib = require(global.__base + '/lib/email');

BigPrizeController = new BigPrizeController();
var GiftType = require(global.__base + '/enum/GiftType');
GiftType = Utility.enumToKeystonOptionStr(GiftType);
/**
 * Draw Form Model
 * this form only for the special one day 
 * this only show one record like a form view
 * ==========
 */

var DrawFormModel = new keystone.List('DrawForm',{
    track: true,
    nodelete: true,
    label: 'Ultimate LuckyDraw',
    // noedit: true,
    // nocreate: true,
});

// winnerEmail, winnerNameList, bigPrize the sequence must be same

DrawFormModel.add({
  // display only
  winner: {
    type: Types.Textarea,
    label: 'Winners List',
    height: 100,
    note: 'No effect if you change the content and Please press Save button on 19/02/2019 and the winners list will be also drawn automatically.',
  },
  // use for sending email
  winnerEmail: {
    type: Types.TextArray,
    noedit: true,
    hidden: true,
  },
  // use for showing in the email
  winnerNameList: {
    type: Types.TextArray,
    noedit: true,
    hidden: true,
  },
  winnerLanguageList: {
    type: Types.TextArray,
    noedit: true,
    hidden: true,
  },
  winnerReferenceNoList: {
    type: Types.TextArray,
    noedit: true,
    hidden: true,
  },
  winnerPhoneNoList: {
    type: Types.TextArray,
    noedit: true,
    hidden: true,
  },
  winnerCardList: {
    type: Types.TextArray,
    noedit: true,
    hidden: true,
  },
  winnerCountryList: {
    // type: Types.TextArray,
    type: Types.Relationship,
    noedit: true,
    ref: 'CountryList',
    many: true,
    hidden: true,
  },
  candidate: {
    type: Types.TextArray,
    label: 'Candidates List',
    noedit: true,
    note: 'All of candidates who joined the luckydraw game before 19/02/2019',
  },
  bigPrize: {
    type: Types.Relationship,
    ref: 'BigPrize',
    noedit: true,
    many: true,
    // hidden: true,
  },
  candidateEmail: {
    type: Types.TextArray,
    hidden: true,
  },
  // use for showing in the email
  candidateNameList: {
    type: Types.TextArray,
    hidden: true,
  },
  candidateCardList: {
    type: Types.TextArray,
    hidden: true,
  },
  candidatePhoneNoList: {
    type: Types.TextArray,
    hidden: true,
  },
  candidateCountryList: {
    // type: Types.TextArray,
    type: Types.Relationship,
    ref: 'CountryList',
    many: true,
    hidden: true,
  },
  candidateLanguageList: {
    type: Types.TextArray,
    hidden: true,
  },
  forDate: {
    type: Types.Datetime,
    initial: true,
    noedit: true,
    required: true,
  },
  finished: {
    type: Types.Boolean,
    note: 'Please press Save button to resent the notification email for the winners'
  },
  emailSent: {
    noedit: true,
    type: Types.Datetime,
  }
});

function checkValidUpdate(doc) {
  const isFunctionStartDate = moment.duration(
      moment('2019-02-19', 'YYYY-MM-DD')
        .startOf('day').diff(moment().startOf('day'))
  ).asMinutes()

  // not allowed to edit prize not at 19-02-2019
  if (!doc.isNew && isFunctionStartDate >= 0) {
    return new Error('The Ultimate Luckydraw will start on 2019-02-19 or later. The game is not allowed to start at that moment.');
  }

  return null;
}

function getWinnerFullInfo (callback) {
   DrawFormModel.model.findOne({})
    .populate('winnerCountryList')
    .populate('bigPrize')
    .exec(callback);
}

function generateRederenceNo() {
  const referenceNoPart = conf.referenceStart + '' + (Math.floor(Math.random() * 1000) + 1) + 1;
  return conf.referencePrefix + referenceNoPart + moment().format(conf.referenceNoPostfix);
}

function drawBigPrize(doc, cb) {
  const prizePostfix = ['st', 'nd', 'rd'];
  BigPrizeController.getList(function(err, prizes) {
    if (err) {
      return cb(err);
    }

    const level = BigPrizeController.getLevel();
    const clasifyPrizes = _.groupBy(prizes, 'level');
    const clasiftLevel = _.sortBy(_.keys(clasifyPrizes));
    if (clasiftLevel.length !== level.length) {
      return cb(new Error('No any Ultimate Prize can be used for the Ultimate Luckydraw.'));
    }
    const candidateEmail = doc.candidateEmail || [];
    // winner info
    const winnerEmail = doc.winnerEmail || [];
    const winnerNameList = doc.winnerNameList || [];
    const bigPrize = doc.bigPrize || [];
    const winnerCountryList = doc.winnerCountryList || [];
    const winnerLanguageList = doc.winnerLanguageList || [];
    const winnerCardList = doc.winnerCardList || [];
    const winnerPhoneNoList = doc.winnerPhoneNoList || [];
    const winnerReferenceNoList = doc.winnerReferenceNoList || [];
    var winnerList = doc.winner || '';
    if (candidateEmail.length > 0) {
      // draw out the prize descending order from 1st to 3rd
      var winner = null;
      var luckyPos = 0;
      clasiftLevel.forEach(function(l) {
        // until pick one valid winner from the email list
        while (!winner) {
          luckyPos = Math.floor(Math.random() * candidateEmail.length);
          winner = candidateEmail[luckyPos] && candidateEmail[luckyPos] !== '' 
            ? candidateEmail[luckyPos] : null;
        }

        // pick any one of prize from the level set for the current prize list
        const luckyPrize = clasifyPrizes[l][Math.floor(Math.random() * clasifyPrizes[l].length)];
        bigPrize.push(luckyPrize);
        winnerEmail.push(winner);
        winnerPhoneNoList.push(doc.candidatePhoneNoList[luckyPos]);
        winnerNameList.push(doc.candidateNameList[luckyPos]);
        winnerCountryList.push(doc.candidateCountryList[luckyPos]);
        winnerLanguageList.push(doc.candidateLanguageList[luckyPos]);
        winnerCardList.push(doc.candidateCardList[luckyPos]);
        winnerReferenceNoList.push(generateRederenceNo());
        winnerList = winnerList + (prizePostfix[l]+' Prize Winner: ' + doc.candidateNameList[luckyPos] + ' (Email: '+ winner +'), \r\n');
      });

      doc.winner = winnerList;
      doc.winnerEmail = winnerEmail;
      doc.bigPrize = bigPrize;
      doc.winnerPhoneNoList = winnerPhoneNoList;
      doc.winnerNameList = winnerNameList;
      doc.winnerCountryList = winnerCountryList;
      doc.winnerLanguageList = winnerLanguageList;
      doc.winnerCardList = winnerCardList;
      doc.winnerReferenceNoList = winnerReferenceNoList;
      doc.finished = true;
      cb(null, doc);
    } else {
      cb(new Error('No anyone played the luckydraw before.'));
    }
  });
}

// const message = self.getNotiEmailFormat();
//                         self.logger("To be sent out error report: " + message + " to " + emailList);
//                         const subject = "[System Failure] " + mobileName + " cronjobs not work";
//                         const mailConfig = {
//                             from: senderEmail,
//                             to: emailList.join(', '),
//                             subject: subject,
//                             html: message
//                         };
//                         emailLib.sendMail(mailConfig, function(err, info) {
//                             cb(err, info);
//                         });

// DrawFormModel.schema.pre('remove', function(next) {
//   const error = checkValidUpdate(this);
//   if (error) {
//     next(error);
//   }
//   next();
// });
DrawFormModel.schema.post('save', function(doc) {
  if (doc.finished) {
    getWinnerFullInfo(function(err, winner) {
      if (winner) {
        emailLib.sendBigPrizeEmail(winner);
      }
    });
  }
});

DrawFormModel.schema.pre('save', function(next) {
  const error = checkValidUpdate(this);
  if (error) {
    next(error);
  }

  // draw out the winner from 3 type of prize
  if (!this.finished) {
    drawBigPrize(this, function(err, doc) {
      if (err) {
        console.log(err);
        next(err);
      }
      console.log(err, doc);
      this.winnerEmail = doc.winnerEmail;
      this.winnerNameList = doc.winnerNameList;
      this.winnerPhoneNoList = doc.winnerPhoneNoList;
      this.winnerCountryList = doc.winnerCountryList;
      this.bigPrize = doc.bigPrize;
      this.winnerLanguageList = doc.winnerLanguageList;
      this.winnerCardList = doc.winnerCardList;
      this.winnerReferenceNoList = doc.winnerReferenceNoList;
      this.winner = doc.winnerList;
      this.finished = true;
      this.emailSent = moment().toDate();
      next();
    });
    // next();
    
  // send the notification email again
  } else {
    next();
  }
});

/**
 * Registration
 */
DrawFormModel.register();

/**
 * Public Class and functions
 */
function DrawForm(){}

DrawForm.prototype.addCandidate = function(data, callback) {
  console.log(data._id, data.language);
  DrawFormModel.model.update({
    $or: [
      { finished: null },
      { finished: false }
    ],
  }, 
  { 
    $push: {
      candidate: data.email + ', ',
      candidateEmail: data.email,
      candidateLanguageList: data.language,
      candidateNameList: data.player,
      candidatePhoneNoList: data.phone,
      candidateCardList: data.card,
      candidateCountryList: data.country,
      // candidateReferenceNoList: data.referenceNo,
    },
  }, callback);
}

DrawForm.prototype.getCandidates = function(email, callback) {
  DrawFormModel.model.find({}, { candidate: 1 }).exec(function(err, c) {
    if (!err) {
      callback(null, c.candidate);
    }
    callback(err);
  });
}

DrawForm.prototype.getResult = function(callback) {
  DrawFormModel.model.findOne({}).lean().exec(callback);
}

exports = module.exports = DrawForm;
