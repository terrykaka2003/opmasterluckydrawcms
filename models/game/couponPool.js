/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
const keystone  = require('keystone');
const moment    = require('moment');
const Config    = require(global.__base + '/config/Config');
const conf      = new Config();
const Types     = keystone.Field.Types;
const Utility   = require(global.__base + '/helper/Utility');

// var GiftType = require(global.__base + '/enum/GiftType');
// GiftType = Utility.enumToKeystonOptionStr(GiftType);
/**
 * CouponPool Model
 * ==========
 */

var CouponPoolModel = new keystone.List('CouponPool',{
    track: true,
    // history: true,
    label: 'Daily eCoupon Pool',
    searchFields: 'name, tcName, scName, redeemCode',
    defaultColumns: 'name, tcName, scName, redeemCode, forDate',
});

CouponPoolModel.add(
  "Setting",
  {
    redeemCode: {
      type: Types.Text,
      required: true,
      initial: true,
    },
    expiryDate: {
      type: Types.Date,
      required: true,
      initial: true,
    },
    type: {
      type: Types.Relationship,
      ref: 'Coupon',
    },
    enabled: {
      type: Types.Boolean,
      default: true,
      note: 'The coupon will not be drawn to the player if it is disabled',
    },
    forDate: {
      type: Types.Date,
      label: 'Date for Lucky Draw',
      required: true,
      initial: true,
      index: true,
    },
    hold: {
      type: Types.Boolean,
      // options: GameResult,
      default: false,
    },
    emailList: {
      type: Types.TextArray,
      note: 'Forward prize email to the recipient from this list'
    },
  },
  "Image",
  {
    icon:{
      type: Types.LocalFile,
      dest: './media/upload/coupon',
      allowedTypes: [
        'image/jpeg', 'image/png', 'image/gif'
      ],
      filename: function(item, file){
        return item.id + '-' + Utility.getRandomInt(0,999999) + '.' + file.extension
      },
      format: function(item, file){
        return '<img src="/upload/coupon/' +file.filename+'" style="max-width: 100px">'
      },
      maxSize: 900000,
      note: 'Notice:the image should under 900KB',
    },
    image:{
      type: Types.LocalFile,
      dest: './media/upload/coupon',
      allowedTypes: [
        'image/jpeg', 'image/png', 'image/gif'
      ],
      filename: function(item, file){
        return item.id + '-' + Utility.getRandomInt(0,999999) + '.' + file.extension
      },
      format: function(item, file){
        return '<img src="/upload/coupon/' +file.filename+'" style="max-width: 100px">'
      },
      maxSize: 1000000,
      note: 'Notice:the image should under 1MB',
    },
    barcodeImage: {
      type: Types.Text,
    },
  },
  "Information",
  {
    name: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'English Title',
    },
    tcName: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Traditional Title',
    },
    scName: {
      type: Types.Text,
      required: true,
      initial: true,
      label: 'Chinese Simplified Title',
    },
    usageTerm: {
      type: Types.Textarea,
      wysiwyg: true,
      label: 'English Usage Terms',
    },
    tcUsageTerm: {
      type: Types.Textarea,
      wysiwyg: true,
      label: 'Chinese Traditional Usage Terms',
    },
    scUsageTerm: {
      type: Types.Textarea,
      wysiwyg: true,
      label: 'Chinese Simplified Usage Terms',
    },
    tc: {
      type: Types.Textarea,
      wysiwyg: true,
      label: 'English T&C',
    },
    tcTC: {
      type: Types.Textarea,
      wysiwyg: true,
      label: 'Chinese Traditional T&C',
    },
    scTC: {
      type: Types.Textarea,
      wysiwyg: true,
      label: 'Chinese Simplified T&C',
    },
    description: {
      type: Types.Text,
      wysiwyg: true,
      label: 'English Description',
    },
    tcDescription: {
      type: Types.Text,
      wysiwyg: true,
      label: 'Chinese Traditional Description',
    },
    scDescription: {
      type: Types.Text,
      wysiwyg: true,
      label: 'Chinese Simplified Description',
    },
  },
  "Email Template",
  {
    // inParkFormat: {
    //   type: Types.Html,
    //   wysiwyg: true,
    //   require: true,
    //   initial: true,
    //   label: 'English App Email Format'
    // },
    // tcInParkFormat: {
    //   type: Types.Html,
    //   wysiwyg: true,
    //   require: true,
    //   initial: true,
    //   label: 'Chinese Traditional App Email Format'
    // },
    // scInParkFormat: {
    //   type: Types.Html,
    //   wysiwyg: true,
    //   require: true,
    //   initial: true,
    //   label: 'Chinese Simplified App Email Format'
    // },
    outParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web Email Format'
    },
    tcOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web Email Format'
    },
    scOutParkFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web Email Format'
    },

    hotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Hotel Web Email Format'
    },
    tcHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Hotel Web Email Format'
    },
    scHotelFormat: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Hotel Web Email Format'
    },
  },
  "Game Winning Information",
  {
    appWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English App Winning Information'
    },
    tcAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional App Winning Information'
    },
    scAppWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified App Winning Information'
    },
    webParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Park) Winning Information'
    },
    tcWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Park) Winning Information'
    },
    scWebParkWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Park) Winning Information'
    },
    webHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'English Web (Hotel) Winning Information'
    },
    tcWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Traditional Web (Hotel) Winning Information'
    },
    scWebHotelWinningInfo: {
      type: Types.Html,
      wysiwyg: true,
      require: true,
      initial: true,
      label: 'Chinese Simplified Web (Hotel) Winning Information'
    },
  }
);

// PrizeModel.schema.pre('save', function(next) {
//   const durationFromToday = moment.duration(
//       moment().startOf('day').diff(moment(this.forDate).startOf('day'))
//   ).asMinutes()

//   // not allowed to edit today prize
//   if (durationFromToday >= 0) {
//     next(new Error('You are not allowed to edit today prize since the Lottery Game is being started.'));
//   }

//   const isTmr = moment.duration(
//       moment().add(1, 'days').startOf('day').diff(moment(this.forDate).startOf('day'))
//   ).asMinutes();
//   const durationToTmr = moment.duration(
//       moment('23:00:00', conf.timeformat).diff(moment())
//   ).asMinutes();
//   if (isTmr === 0 && durationToTmr <= 0) {
//     next(new Error('You are not allowed to edit tomorrow prize after 23:00:00.'));
//   }

//   next();
// });
CouponPoolModel.schema.index({
    enabled: 1,
    hold: 1,
});
/**
 * Registration
 */
CouponPoolModel.register();

/**
 * Public Class and functions
 */
function CouponPool(){}


CouponPool.prototype.getAvailableCouponList = function(callback) {
  CouponPoolModel.model.find({
    enabled: true,
    hold: false,
    forDate: {
      $gte: moment().startOf('day').toDate(),
      $lte: moment().endOf('day').toDate(),
    },
  }).lean().exec(callback);
}

CouponPool.prototype.getAvailableCouponCount = function(callback) {
  CouponPoolModel.model.count({
    enabled: true,
    hold: false,
    forDate: {
      // $gte: moment().startOf('day').toDate(),
      $lte: moment().endOf('day').toDate(),
    },
  }, callback);
}


CouponPool.prototype.getAvailableCouponByIndex = function(skip, callback) {
  CouponPoolModel.model.find({
    enabled: true,
    hold: false,
    forDate: {
      // $gte: moment().startOf('day').toDate(),
      $lte: moment().endOf('day').toDate(),
    },
  }).skip(skip).limit(1).populate('type').exec(callback);
}

CouponPool.prototype.removeById = function(id, callback) {
  CouponPoolModel.model.find({
    _id: id,
  }).remove().exec(callback);
}

CouponPool.prototype.updateStatus = function(data, callback) {
  CouponPoolModel.model.update({
    _id: data.id
  }, {
    hold: data.hold
  }, {}, callback);
}
CouponPool.prototype.updateForDate = function(callback) {
  CouponPoolModel.model.update({

  }, {
    forDate: moment().startOf('day').toDate(),
  }, {
    multi: true,
  }, callback);
}
CouponPool.prototype.removeAllCoupons = function(callback) {
  CouponPoolModel.model.remove({}, callback);
}
CouponPool.prototype.addCoupon = function(data, callback) {
  // console.log('add coupon....');
  const coupon = new CouponPoolModel.model({
    name: data.name,
    tcName: data.tcName,
    scName: data.scName,
    description: data.description,
    tcDescription: data.tcDescription,
    scDescription: data.scDescription,
    // forDate: process.env.NODE_ENV === 'production' ? moment().add(1, 'days').startOf('day').toDate() : moment().startOf('day').toDate(),
    forDate: moment().add(1, 'days').startOf('day').toDate(),
    icon: data.icon,
    image: data.image,
    tc: data.tc,
    tcTC: data.tcTC,
    scTC: data.scTC,
    type: data.type,
    tcUsageTerm: data.tcUsageTerm,
    scUsageTerm: data.scUsageTerm,
    // barcodeImage: data.barcodeImage,
    usageTerm: data.usageTerm,
    expiryDate: data.expiryDate,
    redeemCode: data.redeemCode,
    inParkFormat: data.inParkFormat,
    tcInParkFormat: data.tcInParkFormat,
    scInParkFormat: data.scInParkFormat,
    outParkFormat: data.outParkFormat,
    tcOutParkFormat: data.tcOutParkFormat,
    scOutParkFormat: data.scOutParkFormat,

    appWinningInfo: data.appWinningInfo,
    tcAppWinningInfo: data.tcAppWinningInfo,
    scAppWinningInfo: data.scAppWinningInfo,
    webHotelWinningInfo: data.webHotelWinningInfo,
    tcWebHotelWinningInfo: data.tcWebHotelWinningInfo,
    scWebHotelWinningInfo: data.scWebHotelWinningInfo,
    webParkWinningInfo: data.webParkWinningInfo,
    tcWebParkWinningInfo: data.tcWebParkWinningInfo,
    scWebParkWinningInfo: data.scWebParkWinningInfo,

    webFormat: data.webFormat,
    tcWebFormat: data.tcWebFormat,
    scWebFormat: data.scWebFormat,
    hotelFormat: data.hotelFormat,
    tcHotelFormat: data.tcHotelFormat,
    scHotelFormat: data.scHotelFormat,

    hold: false,
    enabled: true,
  });
  coupon.save(callback);
}

// remove all of the pass coupon from the pool
CouponPool.prototype.removeAllExpiredCoupon = function(callback) {
  CouponPoolModel.model.remove({
    forDate: {
      $lt: moment().startOf('day').toDate(),
    }
    // $and: [
    //   {
    //     $or: [
    //       {
    //         forDate: {
    //           $lt: moment().startOf('day').toDate(),
    //         },
    //       },
    //       {
    //         forDate: {
    //           $ne: moment().startOf('day').toDate(),
    //         },
    //       }
    //     ]
    //   }
    // ]
  }, callback);
}




exports = module.exports = CouponPool;
