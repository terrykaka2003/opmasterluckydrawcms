/* 
* @Author: terrychan
* @Date:   2018-12-31
* @Last Modified by:   terrychan
* @Last Modified time: 2018-12-31
*/
var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * News Model
 * ==========
 */

var StaffModel = new keystone.List('Staff',{
    track: true,
    // history: true,
    map: {
        name: 'staffName',
    },
    searchFields: 'staffName, staffID',
    defaultColumns: 'staffName, staffID',
});

StaffModel.add({
  staffName: {
    type: Types.Text,
    required: true,
    initial: true,
  },
  staffId: {
    type: Types.Text,
    required: true,
    initial: true,
    index: true,
  },
  password: {
    type: Types.Password,
    required: true,
    initial: true,
    note: 'At least 8 characters with uppercase'
  },
  loginToken: {
    type: Types.Text,
    noedit: true,
  },
  activate: {
    type: Types.Boolean,
    default: true,
  },
  type: {
    type: Types.Select,
    options: [
      {
        label: 'Hotel Staff',
        value: 'ht'
      },
      {
        label: 'Operator',
        value: 'op'
      }
    ],
    default: 'ht',
    required: true,
    initial: true,
  },
});

// NewsModel.schema.pre('save', function(next){
//   if(!this.isNew) return next();

//   var parentLang = new ParentLang;
//   parentLang.create(this, (function(err, result){
//     this.parentLang = result._id;
//     next();
//   }).bind(this));
// });


/**
 * Registration
 */

StaffModel.register();


/**
 * Public Class and functions
 */
function Staff(){}

Staff.prototype.getstaffInfoByToken = function(type, loginToken, callback) {
  StaffModel.model
	  .findOne({
	    // staffId: data.staffId,
      loginToken: loginToken,
      activate: true,
      type: type
	  },{
      staffName: 1,
      staffId: 1,
      loginToken: 1,
      type: 1,
    })
	  .exec(callback);
}

Staff.prototype.updateInfo = function(data, callback) {
  StaffModel.model
    .findOneAndUpdate({
      loginToken: data.token,
    })
    .exec(callback);
}

Staff.prototype.login = function(data, callback) {
  StaffModel.model
    .findOne({
      staffId: data.staffId,
      type: data.type,
      activate: true,
    })
    .exec(function(err, member) {
      if (err) {
        return callback(err);
      }
      if (member) {
        member._.password.compare(data.password, function(err, result) {
            if (err) {
                return callback(err);
            }

            if (!result) {
                return callback(new Error('Invalid login'));
            }
            
            return callback(null, member);
        });
      } else {
        return callback(new Error('No such staff'));
      }
    });
}
exports = module.exports = Staff;
