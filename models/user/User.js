var keystone = require('keystone');
var Types = keystone.Field.Types;
var Utility = require(global.__base + '/helper/Utility');
var UserStatus = require(global.__base + "/enum/UserStatus");
var bcrypt = require('bcrypt-nodejs');
var async = require('async');
var Role = require(global.__base+"/models/user/Role");
var Config = require(global.__base + '/config/Config');
var conf = new Config();
var moment = require('moment');
/**
 * User Model
 * ==========
 */

var userStatusOptionStr = Utility.enumToKeystonOptionStr(UserStatus);
var UserModel = new keystone.List('User', {
	track: true,
});

UserModel.add({
	name: { type: Types.Name, required: true, index: true },
	email: { type: Types.Email, initial: true, required: true, index: true },
	password: { type: Types.Password, initial: true, required: true },
	role: { type: Types.Relationship, ref: 'Role', initial: true},
	status: {type: Types.Select, options:userStatusOptionStr, initial: true, index: true, required: true },
	department: { type: Types.Text,initial: true },
	remarks: { type: Types.Textarea,initial: true },
	loginFailed : { type: Types.Number },
	isSuperAdmin: { type: Boolean, index: true, hidden: true },
	pastPasswords: { type: Types.TextArray, hidden: true },
	passwordUpdatedAt: { type: Types.Datetime, hidden: true, default: Date.now },
	dn: { type: Types.Text },
	isLdapUser: { type: Boolean, index: true, default:false },
	isAdmin: { type: Boolean, label: 'Can access Admin', index: true, hidden:true, default:true }
});

// Provide access to Keystone
UserModel.schema.virtual('canAccessKeystone').get(function() {
	return this.isAdmin;
});

UserModel.schema.pre('validate', function(next) {
	if(this.isNew) return next();

	UserModel.model.findById(this._id, (function(err,result){
  	if(err) return next(err);

		// check if is superadmin
		this.isSuperAdmin = result.isSuperAdmin;

		// check if password same as previous five passwords
		if(this.password != result.password){
			// save last password change
			this.passwordUpdatedAt = new Date();

			var matchedPastFive = false;
  		//password changed
			var pastFivePasswords = result.pastPasswords.slice(Math.max(result.pastPasswords.length - 5, 0));
			var ref = this;
			var matchResult = false;

			async.forEach(pastFivePasswords, function(pastFivePassword, callback){
				bcrypt.compare(ref.password, pastFivePassword, function(err, isMatch){
					if (isMatch) {
						matchResult = true;
					};
					callback();
				});
			}, function (err) {
		    if (err) console.error(err.message);

		    if(matchResult){
		    	next(Error("The password must be different from the 5 previously used passwords."));
		    }else{
		    	next();
		    }
			});
  	}else{
  		next();
  	}

	}).bind(this));

});

UserModel.schema.pre('save', function(next) {
	//always focus isadmin is true
	this.isAdmin = true;
	next();
});

UserModel.schema.post('save', function(data) {
	UserModel.model.findById(data._id, function(err,result){
		if(result.pastPasswords.indexOf(data.password)<0){
			//if change password
			UserModel.model.findByIdAndUpdate(
		    data._id,
		    {$push: {"pastPasswords": data.password}},
		    {upsert: false})
		  .exec(function(err,result){
		  	//
		  });
		}
	})
});

/**
 * Registration
 */

UserModel.defaultColumns = 'name, email, isAdmin';
UserModel.register();



/**
 * Public Class and functions
 */

function User(){

}

User.prototype.doRegister = function(ldapUser,lookup, callback) {
	var role = new Role;
	role.findRoleByName(conf.ldapDefaultRole,function(err,result){

		var now = moment().format();
		var newData = {
			name : { last :ldapUser.displayName} ,
			email: lookup.email,
			password: lookup.password,
			status: 'active',
			passwordUpdatedAt: now,
			isAdmin: true,
			isLdapUser: true
		};
		if(result && result._id){
			newData.role = result._id;
		}
		var newUser = new UserModel.model(newData);
			newUser.save(function(err) {
	  		callback(err,newUser);
		});
	});
};

User.prototype.updateUser = function(condition,data,callback) {

	UserModel.model.findOneAndUpdate(
	condition,
	data,
	{upsert: false})
	.exec(callback);
};

User.prototype.getLdapUsers = function(callback) {
	var condition = {
		isLdapUser: true	
	};
	UserModel.model.find(condition).exec(callback);
};
exports = module.exports = User;