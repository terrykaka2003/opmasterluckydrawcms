/* 
* @Author: patrickng
* @Date:   2016-01-14 15:26:53
* @Last Modified by:   patrickng
* @Last Modified time: 2016-01-14 15:27:01
*/

var keystone = require('keystone'),
	Types = keystone.Field.Types;
var Utility = require(global.__base + '/helper/Utility');
var RoleStatus = require(global.__base + "/enum/RoleStatus");


/**
 * Role Model
 * ==========
 */

var RoleModel = new keystone.List('Role');
RoleModel.register();

var roleStatusOptionStr = Utility.enumToKeystonOptionStr(RoleStatus);
var optionArray = roleStatusOptionStr.split(",");

function Role(){

}

/**
Static Functions
**/

Role.registerRole = function(){

	var registeredModel = keystone.lists;
	var modelObjs = {};
	Object.keys(registeredModel).forEach(function (key) {
		//modelObjs[registeredModel[key].path] = {type: Types.Boolean, initial: true};
		modelObjs[registeredModel[key].path] = {type: Types.Select, options:roleStatusOptionStr, default:optionArray[0] ,initial: true};
	})

	RoleModel.add({
		name: { type: Types.Text, initial: true },
		adminPermissions: modelObjs
	});

	/**
	 * Registration
	 */

	RoleModel.defaultColumns = 'name, adminPermissions';
	RoleModel.register();
}

/**
Class Functions
**/

Role.prototype.findById = function(roleId, callback){

	RoleModel.model.findById(roleId).exec(callback);
}
Role.prototype.findRoleByName = function(name, callback){
	var condition ={
		name : name		
	}
	RoleModel.model.findOne(condition).exec(callback);
}




exports = module.exports = Role;