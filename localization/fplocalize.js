var LanguageType = require(global.__base + "/enum/LanguageType");

exports = module.exports = function(language){
	switch(language) {
		case LanguageType.en.key:{
			return require("./en");
			break;
		}
		case LanguageType.sc.key:{
			return require("./cn");
			break;
		}
		case LanguageType.tc.key:{
			return require("./tw");
			break;
		}
		default:{
			return require("./en");
		}
	}
}
